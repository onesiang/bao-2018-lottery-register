<?php
// get domain
$get_domain = explode(':', get_domain());
$fqdn = $get_domain[0];
$port = $get_domain[1];
$explode_FQDN = explode('.', $fqdn);
$subdomain = $explode_FQDN[0];
$last_one = array_slice($explode_FQDN, -1);
$last_second = array_slice($explode_FQDN, -2);
$current_domain = array_shift($last_second) . '.' . array_shift($last_one);

// check domain
if (!$_SESSION['verified_domain'] && !in_array($fqdn, array('127.0.0.1', '::1'))) {
    $config_handle_pre = new Config();
    $backup_domain = $config_handle_pre->check_cache_exists_by_hash();
    $backup_domain = $config_handle_pre->get_cache_by_name(array('ALLOW_DOMAIN'));
    unset($config_handle_pre);

    if (stripos($backup_domain['ALLOW_DOMAIN'], $current_domain) !== false) {
        $_SESSION['verified_domain'] = true;
    } else {
        header("HTTP/1.1 503 Service Temporarily Unavailable");
        header("Status: 503 Service Temporarily Unavailable");
        header("Retry-After: 3600");
        die('Service Temporarily Unavailable');
    }
}

// redirect to web or mobile
$verified_mobile_subdomain = 'm';

$detect_handle = new Mobile_Detect;
$is_mobile = $detect_handle->isMobile();
unset($detect_handle);

if ($is_mobile && $subdomain != $verified_mobile_subdomain) {
    $parameter = array(
        'replace_domain' => array('/^www./'),
        'add_subdomain' => $verified_mobile_subdomain
    );
    redirect_domain($parameter, $fqdn, $port);
} elseif (!$is_mobile && $subdomain == $verified_mobile_subdomain) {
    $parameter = array(
        'replace_domain' => array('/^' . $verified_mobile_subdomain . './'),
    );
    redirect_domain($parameter, $fqdn, $port);
}
