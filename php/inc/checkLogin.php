<?php
use \Firebase\JWT\JWT;

// [iss] => localhost:3000
// [sub] => kangkang12345
// [aud] => http://example.com
// [iat] => 1532788483
// [jti] => 15327884835b5c7f031e281
try {
    if (!isset($_SERVER['HTTP_AUTHORIZATION'])) {
        throw new \Exception('NO HTTP AUTHORIZATION DATA');
    }
    $key = "example_key";
    $decoded = JWT::decode($_SERVER['HTTP_AUTHORIZATION'], $key, array('HS256'));

    if (!isset($_SESSION[SESSION_NAME . '_user_uid'])) {
        throw new \Exception('No uid');
    }
    if ($_SESSION[SESSION_NAME . '_user_uid'] !== $decoded->jti) {
        throw new \Exception('Not MATCH');
    }

    $now = date("Y-m-d H:i:s");
    $lastActionTime = $_SESSION[SESSION_NAME . '_user_login_time'];
    if (strtotime($now) > strtotime($lastActionTime) + 1800) {
        $uid = $_SESSION[SESSION_NAME . '_user_uid'];
        $member_handle = new Member();
        $result = $member_handle->logout($uid);
        unset($member_handle);
        session_destroy();
        session_regenerate_id();
        throw new \Exception("Login TimeOut NOW:{$now} , SESSION TIME : {$lastActionTime}");
    } else {
        $_SESSION[SESSION_NAME . '_user_login_time'] = date('Y-m-d H:i:s'); //未逾時修改登入時間
    }
} catch (\Exception $ex) {
    Log::record($ex->getMessage(), 'error', 'checkLogin/');
    Log::record($ex->getTraceAsString()."\n", 'error', 'checkLogin/');
    echo -401;
    exit;
}
