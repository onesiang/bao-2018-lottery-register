<!-- 共用的script -->
<script type="text/javascript" src="../vendor/js/jquery-3.2.1.min.js?v=<?=$config['VERSION'];?>"></script>
<script type="text/javascript" src="../vendor/js/bootstrap.min.js?v=<?=$config['VERSION'];?>"></script>
<script type="text/javascript" src="../../js/global_common.js?v=<?=$config['VERSION'];?>"></script>
<script src="../../js/msg.js?v=<?php kg_echo($config['VERSION']);?>"></script>
<script src="../vendor/js/layer.js?v=<?php kg_echo($config['VERSION']);?>"></script>
<script src="../../js/base.js?v=<?php kg_echo($config['VERSION']);?>"></script>
<script src="../../js/common.js?v=<?php kg_echo($config['VERSION']);?>"></script>
<script src="../../js/function.js?v=<?php kg_echo($config['VERSION']);?>"></script>
<script src="../../js/auth_code.js?v=<?php kg_echo($config["></script>
<script src="../../js/stay_standalone.js" type="text/javascript"></script>

<script type="text/javascript">
    if(hasLogin) {
        $(document).ready(function(){

            var a = '<?php echo kg_get('show', 'hide'); ?>';
            if(a == 'true'){
                var $login_success_modal = $('#login_success_modal');
                $login_success_modal.modal('show');

                setTimeout(function(){
                    $login_success_modal.modal('hide');
                },1000)

            }

            $login_success_modal.on('click', function(){
                $login_success_modal.modal('hide');
            });
        });
    }

</script>

<script type="text/javascript">
    $(document).ready(function(){

        $.getJSON( "notices.php", function( data ) {
            $('#notices_bar').html(data);
        });

        setInterval(function(){
            showOrHideAnnoucementModal();
            getUnreadNotice();
        },60000)

        function getUnreadNotice(){
            $.ajax({
                url:'/unread_notice.php',
                type:'post',
                success:function(data){
                    $('.notification').text(data);
                }
            });
        }
    })

    function inboxCheckLogin(){
        if(!hasLogin) {
            msg_alert(language.msg_do_login);
            $('#login_here').modal('show');
            return false;
        }
        else{
            location.href='./inbox.php'
        }
    }

    function refreshData(){

        if( hasLogin ) {
            $('#user_center').modal('show');
        } else {
            $('#login_here').modal('show');
        }

        if( hasLogin ){
            $.post("./money.php", function(v){
                $("#rechargeAmount").text(v);
                $(".member_owns_balance").text("余额："+v+"元");
                $("#amt").text(v);

            }, "json");

            $.ajax({
                url:'/unread_notice.php',
                type:'post',
                success:function(data){
                    $('#sysNotice').text(data);
                }
            });

            $.ajax({
                url:'/notice_remain.php',
                type:'post',
                success:function(e){
                    $('#mailCount').text(e);
                }
            });
        }else{
            // 防止iphone 登入 modal 游標位移
            if($('body').hasClass('modal-open')){
                $('.modal-open').css({
                    position:'fixed',
                    width: '100%'
                });
            }

            // 要清除css樣式否則無法滾動
            $('#login_here').on('hidden.bs.modal',function(){
                $('body').removeAttr('style');
            })
        }
    }

    //fix Iphone add screen opens links in new window
    let a_link = document.getElementsByTagName("a");
    for (let i = 0; i < a_link.length; i++) {
        if(a_link[i].href){
            a_link[i].onclick = function () {
                window.location = this.getAttribute("href");
                return false
            }
        }
    }

</script>
