<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * CURL封装类
 *
 * @package common
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Curl {
    
    private $_ch     = NULL;
    private $_info   = array();
    private $_setopt = array(
        'port'                => 80,
        'user_agent'          => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
        'connect_timeout'     => 25,
        'timeout'             => 90,
        'use_cookie'          => FALSE,
        'ssl'                 => FALSE,
        'gzip'                => TRUE,
        'proxy'               => FALSE,
        'proxy_type'          => 'HTTP',
        'proxy_host'          => '',
        'proxy_port'          => '',
        'proxy_auth'          => FALSE,
        'proxy_auth_type'     => 'BASIC',
        'proxy_auth_user'     => 'user',
        'proxy_auth_password' => 'password'
    );

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    /**
     * __construct
     * 构造函数
     *
     * @access public
     * @param array $setopt 参数
     * @return self
     */
    public function __construct($setopt = array()) {
        $this->_setopt = array_merge($this->_setopt, $setopt);
        $this->_ch     = curl_init();

        curl_setopt($this->_ch, CURLOPT_PORT, $this->_setopt['port']);

        if ($this->_setopt['proxy']) {
            $proxy_type = ($this->_setopt['proxy_type'] == 'HTTP') ? CURLPROXY_HTTP : CURLPROXY_SOCKS5;

            curl_setopt($this->_ch, CURLOPT_PROXYTYPE, $proxy_type);
            curl_setopt($this->_ch, CURLOPT_PROXY, $this->_setopt['proxy_host']);
            curl_setopt($this->_ch, CURLOPT_PROXYPORT, $this->_setopt['proxy_port']);

            if ($this->_setopt['proxy_auth']) {
                $proxy_auth_type = ($this->_setopt['proxy_auth_type'] == 'BASIC') ? CURLAUTH_BASIC : CURLAUTH_NTLM;
                $user_pwd        = $this->_setopt['proxy_auth_user'] . ':' . $this->_setopt['proxy_auth_password'];

                curl_setopt($this->_ch, CURLOPT_PROXYAUTH, $proxy_auth_type);
                curl_setopt($this->_ch, CURLOPT_PROXYUSERPWD, $user_pwd);
            }
        }

        curl_setopt($this->_ch, CURLOPT_FOLLOWLOCATION, TRUE);

        if ($this->_setopt['ssl']) {
            curl_setopt($this->_ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($this->_ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }

        $user_agent = ($this->_setopt['user_agent'] == '') ? $_SERVER['HTTP_USER_AGENT'] : $this->_setopt['user_agent'];

        curl_setopt($this->_ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($this->_ch, CURLOPT_CONNECTTIMEOUT, $this->_setopt['connect_timeout']);
        curl_setopt($this->_ch, CURLOPT_TIMEOUT, $this->_setopt['timeout']);
        curl_setopt($this->_ch, CURLOPT_NOSIGNAL, 1); // 毫秒级超时bug解决
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, array('Expect:')); // 请求lighttpd 417 Expectation错误

        if ($this->_setopt['gzip']) {
            curl_setopt($this->_ch, CURLOPT_ENCODING, 'gzip');
        }

        if ($this->_setopt['use_cookie']) {
            $cookie_file = tempnam(sys_get_temp_dir(), 'cookie');
            curl_setopt($this->_ch, CURLOPT_COOKIEJAR, $cookie_file);
            curl_setopt($this->_ch, CURLOPT_COOKIEFILE, $cookie_file);
        }

        curl_setopt($this->_ch, CURLOPT_HEADER, TRUE);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->_ch, CURLOPT_BINARYTRANSFER, TRUE);
    }

    /**
     * parseUrl
     * 解析网址
     *
     * @access private
     * @param string $url 网址
     * @param array $argument 参数
     * @return string
     */
    private function parseUrl($url, $argument) {
        $field_string = http_build_query($argument);
        if ($field_string) {
            $url .= (strstr($url, '?') === FALSE) ? '?' : '&';
            $url .= $field_string;
        }
        return $url;
    }

    /**
     * request
     * 请求
     *
     * @access private
     * @param string $method 方式
     * @param string $url 网址
     * @param array $arugment 参数
     * @param string $referrer 来源
     * @return string|boolean
     */
    private function request($method, $url, $argument = array(), $referrer = '') {
        if ($method == 'GET') {
            $url = $this->parseUrl($url, $argument);
        }

        curl_setopt($this->_ch, CURLOPT_URL, $url);

        if ($method == 'POST') {
            if (is_array($argument)) {
                $post_data = http_build_query($argument);
            }
            else {
                $post_data = $argument;
            }
            $post_data = $argument;

            curl_setopt($this->_ch, CURLOPT_POST, TRUE);
            curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $post_data);
        }

        if ($referrer) {
            curl_setopt($this->_ch, CURLOPT_REFERER, $referrer);
        }
        else {
            curl_setopt($this->_ch, CURLOPT_AUTOREFERER, TRUE);
        }

        $this->_info['before'] = curl_getinfo($this->_ch);

        $result = curl_exec($this->_ch);

        $header_size = curl_getinfo($this->_ch, CURLINFO_HEADER_SIZE);
        $this->_info['header'] = substr($result, 0, $header_size);

        $result = substr($result, $header_size);
        $this->_info['after'] = curl_getinfo($this->_ch);

        if ($this->errno() == 0) {
            return $result;
        }
        else {
            $this->_info['after']['errno'] = curl_errno($this->_ch);
            $this->_info['after']['error'] = curl_error($this->_ch);
            return FALSE;
        }
    }

    /**
     * get
     * GET方法
     *
     * @access public
     * @param string $url 网址
     * @param array $argument 参数
     * @param string $referrer 来源
     * @return string
     */
    public function get($url, $argument = array(), $referrer = '') {
        return $this->request('GET', $url, $argument, $referrer);
    }

    /**
     * post
     * POST方法
     *
     * @param string $url 网址
     * @param array $argument 参数
     * @param string $referrer 来源
     * @return string
     */
    public function post($url, $argument = array(), $referrer = '') {
        return $this->request('POST', $url, $argument, $referrer);
    }

    /**
     * error
     * 错误消息
     *
     * @access public
     * @return string
     */
    public function error() {
        return curl_error($this->_ch);
    }

    /**
     * errno
     * 错误代码
     *
     * @access public
     * @return int
     */
    public function errno() {
        return curl_errno($this->_ch);
    }

    /**
     * getInfo
     * 获取信息
     *
     * @access public
     * @return array
     */
    public function getInfo() {
        return $this->_info;
    }

    /**
     * __destruct
     * 析构函数
     *
     * @access public
     * @return void
     */
    public function __destruct() {
        curl_close($this->_ch);
    }
}