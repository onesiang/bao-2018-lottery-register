<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * Redis封装类
 *
 * @package common
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class KgRedis
{
    private $_handle;
    private $_connected = false;

    private function connect()
    {
        try {
            if (defined('CACHE_DB_CLUSTER') && CACHE_DB_CLUSTER === 1) {
                $this->_handle = new RedisCluster(null, array(CACHE_HOST . ':' . CACHE_PORT));
            } else {
                $this->_handle = new Redis();
                $this->_handle->connect(CACHE_HOST, CACHE_PORT);
                $this->_handle->select(CACHE_DB);
            }

            $this->_connected = true;
        } catch (RedisException $e) {
            Log::record('Connection failed: ' . $e->getMessage(), 'error', 'redis/');
        }
    }

    private function checkConnect()
    {
        if (!$this->_connected) {
            $this->connect();
        }
    }

    public function set($key, $value, $time = 0)
    {
        $this->checkConnect();
        if ($time > 0) {
            return $this->_handle->setex($key, $time, $value);
        } else {
            return $this->_handle->set($key, $value);
        }
    }

    public function get($key)
    {
        $this->checkConnect();
        return $this->_handle->get($key);
    }

    public function del($key)
    {
        $this->checkConnect();
        return $this->_handle->del($key);
    }

    public function expire($key, $time)
    {
        $this->checkConnect();
        return $this->_handle->expire($key, $time);
    }

    public function exists($key)
    {
        $this->checkConnect();
        return $this->_handle->exists($key);
    }

    public function hGet($hash, $key)
    {
        $this->checkConnect();
        return $this->_handle->hGet($hash, $key);
    }

    public function hMGet($hash, $keys)
    {
        $this->checkConnect();
        return $this->_handle->hmGet($hash, $keys);
    }

    public function mGet($keys)
    {
        $this->checkConnect();
        return $this->_handle->mGet($keys);
    }

    public function hSet($hash, $key, $value)
    {
        $this->checkConnect();
        return $this->_handle->hSet($hash, $key, $value);
    }

    public function hDel($hash, $key)
    {
        $this->checkConnect();
        return $this->_handle->hDel($hash, $key);
    }

    public function hMset($hash, $array)
    {
        $this->checkConnect();
        return $this->_handle->hMset($hash, $array);
    }

    public function hGetAll($hash)
    {
        $this->checkConnect();
        return $this->_handle->hGetAll($hash);
    }

    public function hLen($hash)
    {
        $this->checkConnect();
        return $this->_handle->hLen($hash);
    }

    public function hExists($hash, $key)
    {
        $this->checkConnect();
        return $this->_handle->hExists($hash, $key);
    }
}
