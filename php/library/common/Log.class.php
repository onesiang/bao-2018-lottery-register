<?php

use Psr\Log\LogLevel;
use Gelf\Transport\TcpTransport;
use Gelf\Transport\UdpTransport;
use Gelf\Publisher;
use Gelf\Message;

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 日志类
 *
 * @package common
 * @author iwin <iwin.org@gmail.com>
 * @final
 */

final class Log {

    private static $_instance = null;

    private static $logLevels = array(
        0 => LogLevel::EMERGENCY,
        1 => LogLevel::ALERT,
        2 => LogLevel::CRITICAL,
        3 => LogLevel::ERROR,
        4 => LogLevel::WARNING,
        5 => LogLevel::NOTICE,
        6 => LogLevel::INFO,
        7 => LogLevel::DEBUG,
    );

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
	private function __clone(){}

    public static function record($message, $type = 'error', $file_path = '') {

        if (!is_array($message) && (strpos($message, "Undefined") === false)) {

            if ($message) {
                $message = trim($message, "\n") . "\n";
            } else {
                $message = 'Empty variable';
            }

            $log_level = array_search(strtolower($type),self::$logLevels);
            if($log_level === false){
                $log_level = 7;
            }

            $type = self::$logLevels[$log_level];

            //通通寫入GaryLog   有寫成功才判斷log是否符合本機寫入等級
            if (self::garyLog($message,$log_level,$file_path)) {
                if($log_level > WRITE_LOG_LEVEL){
                    return;
                }
            }

            $log_path = ROOT . DS . 'log' . DS;
            if($file_path) {
                $file_array = explode('/',$file_path);
                $count = count($file_array);
                if($count>0){
                    unset($file_array[$count-1]);
                }

                $check_path = $log_path . implode('/',$file_array);
                if(!self::mkdirs($check_path, 0777)){
                    Log::record('mkdirs is error','error');
                }

                $log_path = $log_path . $file_path;

            }
            $filename  = $log_path . $type . '_' . date('Ymd') . '.log';

            $time = explode(' ', microtime());
            if (1 == floatval($time[0])) {
                $head =  '[' . date('H:i:s') . '.000] ';
            } else {
                $head =  '[' . date('H:i:s') . '.' . sprintf('%-03s', round($time[0] * 1000)) . '] ';
            }

            $callers = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            $line = 0;
            $error_file = null;
            $log_line = null;

            foreach ( $callers as $trac ) {
                if(isset($trac['class']) && strrpos($trac['class'], "Log") !== false) {

                    $line = $trac['line'];
                    $file = explode("/",$trac['file']);
                    $error_file = $file[count($file)-2]."/".$file[count($file)-1];
                    continue;
                }
                $log_line = $error_file.'->' . $trac['function'].":[$line] : ";
                break;
            }

            if(empty($log_line)) {
                $trac = $callers[0];
                $log_line = $error_file.'->' . $trac['function'].":[".$trac['line']."] : ";
            }

            $handle = fopen($filename, 'ab');
            fwrite($handle, $head . $log_line . $message);
            fclose($handle);
            unset($handle);
        }
    }

    private static function garyLog($message_data,$log_type,$file_path)
    {

        try {
            if (empty(GRAYLOG_HOST) || empty(GRAYLOG_PORT)) {
                throw new \Exception('GRAYLOG_SET EMPTY');
            }

            $transport = self::get_instance();
            $publisher = new Publisher();
            $publisher->addTransport($transport);

            $message = new Message();
            $message->setLevel($log_type);
            $message->setFullMessage($message_data);
            $message->setShortMessage($message_data);
            $message->setFacility(CUSTOMER_NAME."_".$file_path);

            $publisher->publish($message);
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    private static function get_instance()
    {
        if (self::$_instance === null) {
            if(GRAYLOG_TYPE !== 'TCP'){
                self::$_instance = new UdpTransport(GRAYLOG_HOST, GRAYLOG_PORT, UdpTransport::CHUNK_SIZE_LAN);
            }else{
                self::$_instance = new TcpTransport(GRAYLOG_HOST, GRAYLOG_PORT);
            }
        }
        return self::$_instance;
    }

    private static function mkdirs($dir,$mode = 0777){
        if(is_dir($dir)){
            return true;
        }

        if(!self::mkdirs(dirname($dir),$mode)){
            return false;
        }

        return @mkdir($dir,$mode);
    }

}

