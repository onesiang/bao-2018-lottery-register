<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 数据库操作类
 *
 * @package common
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class KgMysql
{

    /**
     * 数据库句柄
     *
     * @access private
     * @var PDO
     */
    private $_handle = null;

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * connect
     * 数据库连接
     *
     * @access private
     * @return void
     */
    private function connect()
    {
        global $g_db_handle;
        if (!$this->_handle) {
            if (!$g_db_handle) {
                try {
                    $dns = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8';
                    $this->_handle = new PDO($dns, DB_USER, DB_PASS);
                    $this->_handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                    $this->_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $this->_handle->setAttribute(PDO::ATTR_PERSISTENT, true);
                    $this->_handle->exec('SET NAMES UTF8');

                    $g_db_handle = $this->_handle;
                } catch (PDOException $e) {
                    Log::record('Connection failed: ' . $e->getMessage(), 'error', 'sql/');
                }
            } else {
                $this->_handle = $g_db_handle;
            }
        }
    }
    public function nativeQuery($sql)
    {
        if ($this->_handle === null) {
            $this->connect();
        }
        if ($this->_handle === null) {
            return false;
        }
        if (!$sql) {
            return false;
        }
        //$sql = trim($sql);
        for ($i = 0; $i < count($sql); $i++) {
            try {
                $return = $this->_handle->exec($sql[$i]);
            } catch (Exception $ex) {
                Log::record('[nativeQuery]: ' . $ex->getMessage(), 'error', 'sql/');
                Log::record('[nativeQuery]: ' . json_encode($sql), 'error', 'sql/');
            }
        }
        return $return;
    }

    public function pdoMultiInsert($tableName, $data)
    {
        if ($this->_handle === null) {
            $this->connect();
        }
        if ($this->_handle === null) {
            return false;
        }

        $rowsSQL = array(); //Will contain SQL snippets.

        $toBind = array(); //Will contain the values that we need to bind.

        $columnNames = array_keys($data[0]); //Get a list of column names to use in the SQL statement.

        foreach ($data as $arrayIndex => $row) {
            $params = array();
            foreach ($row as $columnName => $columnValue) {
                $param = ":" . $columnName . $arrayIndex;
                $params[] = $param;
                $toBind[$param] = $columnValue;
            }
            $rowsSQL[] = "(" . implode(", ", $params) . ")";
        }

        $sql = "INSERT INTO `$tableName` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

        try {
            $insert_id = $this->_handle->lastInsertId();

            $sth = $this->_handle->prepare($sql); //Prepare our PDO statement.

            foreach ($toBind as $param => $val) {
                $sth->bindValue($param, $val); //Bind our values
            }
            $sth->execute(); //Execute our statement (i.e. insert the data).

            return $this->_handle->lastInsertId();
        } catch (PDOException $e) {
            Log::record($e->getMessage(), 'error', 'sql/');
            if ($sth) {
                Log::record(json_encode($sth->errorInfo()), 'error', 'sql/');
            } else {
                Log::record(json_encode($this->_handle->errorInfo()), 'error', 'sql/');
            }
            if ($sth && $sth->queryString) {
                Log::record($sth->queryString, 'error', 'sql/');
            } else {
                Log::record($sql, 'error', 'sql/');
            }
            Log::record('[pdoMultiInsert]: ' . json_encode($rowsSQL), 'error', 'sql/');
        }
    }
    /**
     * query
     * 执行
     *
     * @access public
     * @param string $sql SQL语句
     * @param array|null $paramenter 参数
     * @return mixed
     */
    public function query($sql, $parameter = null, $style = 'ASSOC')
    {
        //PDO::ATTR_PERSISTENT
        if ($this->_handle === null) {
            $this->connect();
        }
        if ($this->_handle === null) {
            return false;
        }
        if (!$sql) {
            return false;
        }
        $sql = trim($sql);

        $return = null;
        $type = strtoupper(substr($sql, 0, 6));
        $sth = null;

        try {
            $sth = $this->_handle->prepare($sql);
            if ($parameter !== null) {
                $return = $sth->execute($parameter);
            } else {
                $return = $sth->execute();
            }
            $fetch_style = '';
            switch ($style) {
                case 'ASSOC':
                    $fetch_style = PDO::FETCH_ASSOC;
                    break;
                case 'NUM':
                    $fetch_style = PDO::FETCH_NUM;
            }
            switch ($type) {
                case 'SELECT':
                    $return = $sth->fetchAll($fetch_style);
                    break;
                case 'INSERT':
                    $insert_id = $this->_handle->lastInsertId();
                    if ($insert_id) {
                        $return = $insert_id;
                    }
                    break;
                case 'UPDATE':
                case 'DELETE':
                    $row_count = $sth->rowCount();
                    //if ($row_count) {
                    $return = $row_count;
                    //}
                    break;
            }
            $sth->closeCursor();
        } catch (PDOException $e) {
            Log::record($e->getMessage(), 'error', 'sql/');
            if ($sth) {
                Log::record(json_encode($sth->errorInfo()), 'error', 'sql/');
            } else {
                Log::record(json_encode($this->_handle->errorInfo()), 'error', 'sql/');
            }
            if ($sth && $sth->queryString) {
                Log::record($sth->queryString, 'error', 'sql/');
            } else {
                Log::record($sql, 'error', 'sql/');
            }

            if ($parameter) {
                Log::record('[query]: ' . json_encode($parameter), 'error', 'sql/');
            }

        }
        return $return;
    }

    public function multiQuery($sql)
    {
        if ($this->_handle === null) {
            $this->connect();
        }
        if ($this->_handle === null) {
            return false;
        }
        if (!$sql) {
            return false;
        }
        $count = 0;
        try {
            $sql_list = explode(';', $sql);
            foreach ($sql_list as $item) {
                if ($item) {
                    $count += $this->_handle->exec($item);
                }
            }
        } catch (PDOException $e) {
            Log::record($e->getMessage(), 'error', 'sql/');
            Log::record(json_encode($this->_handle->errorInfo()), 'error', 'sql/');
            Log::record('[multiQuery]: ' . $sql, 'error', 'sql/');
        }
        return $count;
    }

    public function multiQuery_new($sql)
    {
        if ($this->_handle === null) {
            $this->connect();
        }
        if ($this->_handle === null) {
            return false;
        }
        if (!$sql) {
            return false;
        }

        try {
            $sql_list = explode(';', $sql);
            foreach ($sql_list as $item) {
                if ($item) {
                    $this->_handle->exec($item);
                }
            }
            return true;
        } catch (PDOException $e) {
            Log::record($e->getMessage(), 'error', 'sql/');
            Log::record(json_encode($this->_handle->errorInfo()), 'error', 'sql/');
            Log::record('[multiQuery_new]: ' . $sql, 'error', 'sql/');
            return false;
        }
        return true;
    }

    /**
     * beginTransaction
     * 事务开始
     *
     * @access public
     * @return void
     */
    public function beginTransaction()
    {
        if ($this->_handle === null) {
            $this->connect();
        }
        $this->_handle->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
        $this->_handle->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
        $this->_handle->setAttribute(PDO::ATTR_TIMEOUT, 90);
        return $this->_handle->beginTransaction();
    }

    /**
     * commit
     * 事务确认
     *
     * @access public
     * @return void
     */
    public function commit()
    {
        $status = $this->_handle->commit();
        if (!$status) {
            Log::record(json_encode($this->_handle->errorInfo()), 'info', 'sql/');
        }
        return $status;
    }

    /**
     * rollBack
     * 事务回滚
     *
     * @access public
     * @return void
     */
    public function rollBack()
    {
        $status = $this->_handle->rollBack();
        if (!$status) {
            Log::record(json_encode($this->_handle->errorInfo()), 'error', 'sql/');
        }
        return $status;
    }

    public function delete($table, $data)
    {
        $sql = array();
        $sql[] = 'INSERT INTO ' . $table . ' SET ';
        foreach ($data as $key => $value) {
            $sql[] = $key . '=\'' . $value . '\',';
        }
        $content = trim(implode('', $sql), ',') . ';';
        Log::record($content, 'info', 'sql/deleted/');
    }
}
