<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 缓存类
 *
 * @package common
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Cache
{
    private $_handle;

    public function __construct()
    {
        $this->_handle = new KgRedis();
    }

    public function get($key)
    {
        return $this->_handle->get($key);
    }

    public function set($key, $value, $time = 0)
    {
        return $this->_handle->set($key, $value, $time);
    }

    public function del($key)
    {
        return $this->_handle->del($key);
    }

    public function expire($key, $time)
    {
        return $this->_handle->expire($key, $time);
    }

    public function exists($key)
    {
        return $this->_handle->exists($key);
    }

    public function hGet($hash, $key)
    {
        return $this->_handle->hGet($hash, $key);
    }

    public function hMGet($hash, $key)
    {
        return $this->_handle->hMGet($hash, $key);
    }

    public function mGet($keys)
    {
        return $this->_handle->mGet($keys);
    }

    public function hSet($hash, $key, $value)
    {
        return $this->_handle->hSet($hash, $key, $value);
    }

    public function hDel($hash, $key)
    {
        return $this->_handle->hDel($hash, $key);
    }

    public function hMset($hash, $array)
    {
        return $this->_handle->hMset($hash, $array);
    }

    public function hGetAll($hash)
    {
        return $this->_handle->hGetAll($hash);
    }

    public function hLen($hash)
    {
        return $this->_handle->hLen($hash);
    }

    public function hExists($hash, $key)
    {
        return $this->_handle->hExists($hash, $key);
    }

    public function lPush($key, $value)
    {
        return $this->_handle->lPush($key, $value);
    }

    public function lPop($key)
    {
        return $this->_handle->lPop($key);
    }

    public function lLen($key)
    {
        return $this->_handle->lLen($key);
    }
}
