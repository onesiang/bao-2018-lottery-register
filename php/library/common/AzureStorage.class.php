<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;

/**
 * Azure封装类
 *
 * @package common
 * @author xxx
 * @final
 */
class AzureStorage
{
    public $_handle;
    public $_contariner;
    public $_connected = false;
    public $_error;

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */

    private function __clone()
    {
    }

    public function __construct()
    {
        $this->connection();
    }

    public function connection()
    {
        if (defined('AZURE_ACCOUNT_NAME') && defined('AZURE_ACCOUNT_KEY') && defined('AZURE_ACCOUNT_CONTARINER') && defined('AZURE_UPLOAD')) {
            try {
                if (AZURE_UPLOAD == true) {
                    $connectionString = 'DefaultEndpointsProtocol=https;AccountName='.AZURE_ACCOUNT_NAME.';AccountKey='.AZURE_ACCOUNT_KEY.';';
                    $this->_handle = BlobRestProxy::createBlobService($connectionString);
                    $this->_contariner = AZURE_ACCOUNT_CONTARINER;
                    $this->_connected = true;
                }
            } catch (ServiceException $e) {
                $this->_error = $e->getMessage();
                $this->_connected = false;
            }
        }
    }

    public function set_contariner($contariner)
    {
        $this->_contariner = $contariner;
    }

    public function upload_blob($blob_name, $content)
    {
        try {
            if ($this->_connected) {
                $this->_handle->createBlockBlob($this->_contariner, $blob_name, $content);
            }
            return 1;
        } catch (ServiceException $e) {
            $this->_error = $e->getMessage();
            return 0;
        }
    }

    public function delete_blob($blob_name)
    {
        try {
            if ($this->_connected) {
                $this->_handle->deleteBlob($this->_contariner, $blob_name);
            }
            return 1;
        } catch (ServiceException $e) {
            $this->_error = $e->getMessage();
            return 0;
        }
    }

    public function download_blob($blob_name, $path='temp.jpg')
    {
        try {
            if ($this->_connected) {
                $blob = $this->_handle->getBlob($this->_contariner, $blob_name);
                file_put_contents($path, $blob->getContentStream());
            }
            return 1;
        } catch (ServiceException $e) {
            $this->_error = $e->getMessage();
            return 0;
        }
    }

    public function get_blob_url($path='')
    {
        $url = '';
        if (defined('AZURE_ACCOUNT_URL') && defined('AZURE_ACCOUNT_CONTARINER') && $path) {
            $tmp = explode('/', $path);
            $url =  AZURE_ACCOUNT_URL .'/'. AZURE_ACCOUNT_CONTARINER .'/'. end($tmp);
        }
        return $url;
    }
}
