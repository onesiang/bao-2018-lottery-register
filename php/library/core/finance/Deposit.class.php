<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 在线存款类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Deposit extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function add($argument)
    {
        global $g_payment_type;

        if ($argument[3] < 0) {
            return false;
        }

        $payment_handle = new Payment();
        $payment = $payment_handle->check_gateway($argument[2]);
        unset($payment_handle);
        if (!$payment) {
            return false;
        }

        $argument[] = '[' . $g_payment_type[$payment['payment_id']] . ']' . $payment['name'];

        $sql = array();
        $sql[] = 'INSERT INTO fnn_deposit SET member_id=?,account=?,payment_id=?';
        $sql[] = ',amount=?,submit_time=?,order_id=?,status=1,payment_summary=?';
        return $this->_db->query(implode('', $sql), $argument);
    }

    public function add_newpay($argument, $type)
    {
        global $g_payment_type;
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        if ($argument[3] < 0) {
            return false;
        }

        $payment_handle = new Payment();
        $payment = $payment_handle->check_gateway($argument[2], $type);
        unset($payment_handle);
        if (!$payment) {
            return false;
        }

        if ($payment['fnn_payment_type'] == 22
            && ($type == 1 || $type == 2 || $type == 4)) {
            if ($type == 1) {
                $mer = '支付宝';
            } elseif ($type == 2) {
                $mer = '微信';
            } else {
                $mer = 'QQ';
            }
            $argument[] = '[' . $g_payment_type[$payment['fnn_payment_type']] . ']' . $mer . '扫码/WAP';
        } else {
            $argument[] = '[' . $g_payment_type[$payment['fnn_payment_type']] . ']' . $payment['name'];
        }

        $sql = array();
        $sql[] = 'INSERT INTO fnn_deposit SET member_id=?,account=?,payment_id=?';
        $sql[] = ',amount=?,submit_time=?,order_id=?,status=1,payment_summary=?';
        return $this->_db->query(implode('', $sql), $argument);
    }

    public function get($deposit_id)
    {
        $sql = 'SELECT * FROM fnn_deposit WHERE deposit_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($deposit_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function untreated($argument)
    {
        $expire_time = date('Y-m-d H:i:s', time() - 900);
        $expire_time0 = date('Y-m-d H:i:s');
        $sql = 'UPDATE fnn_deposit SET status=3 , confirm_time = ? WHERE status=1 AND submit_time < ? ';
        $this->_db->query($sql, array($expire_time0, $expire_time));

        $start_time = date('Y-m-d H:i:s', time() - 60);
        array_unshift($argument, $start_time);
        $sql = 'SELECT count(deposit_id) AS deposit_amount FROM fnn_deposit WHERE status=1 AND submit_time <? AND amount >= ? AND amount <= ?';

        $result = $this->_db->query($sql, $argument);
        return $result;
    }

    public function getByOrderId($order_id)
    {
        $sql = 'SELECT * FROM fnn_deposit WHERE order_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($order_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function confirm($deposit_id, $auto_award = false, $operator = '')
    {
        $deposit = $this->get($deposit_id);
        if (!$deposit) {
            return -1;
        }
        if (1 != $deposit['status']) {
            return -2;
        }
        $award = 0;
        $config_handle = new Config();
        $config = $config_handle->getByName(array('ONLINE_DEPOSIT_AWARD', 'CHECK_RATE'));
        unset($config_handle);
        if ($auto_award) {
            $award = $config['ONLINE_DEPOSIT_AWARD'];
        }
        $time = date('Y-m-d H:i:s');
        $this->_db->beginTransaction();

        $amount_db = money_to_db($deposit['amount']);
        $award_amount = 0;
        $check_bet_amount = ceil(($amount_db + $amount_db * $award * 0.01) * (1 - $config['CHECK_RATE'] * 0.01));

        $member_handle = new Member();
        $member = $member_handle->getByMemberId($deposit['member_id']);
        // 最近一次存款记录时间点
        $last_deposit_time = $member_handle->getLastDepositTime($member['member_id']);
        $params = array(
            'member_id' => $member['member_id'],
            'date_start' => $last_deposit_time,
            'date_stop' => date('Y-m-d H:i:s'),
        );
        $member_valid_data = $member_handle->memberBetReportByMember($params);
        $member_config_handle = new MemberConfig();
        $member_config_data = $member_config_handle->getByMemberId($member['member_id']);
        $win_total = money_to_db($member_valid_data['win_total']);
        $last_need_amount = 0;
        if ($member_config_data['check_bet_amount'] + $win_total > 0) {
            $valid_bet_total = money_to_db($member_valid_data['valid_bet_total']);
            $last_need_amount = $member_config_data['check_bet_amount'] - $valid_bet_total;
        }
        $last_need_amount = $last_need_amount > 0 ? $last_need_amount : 0;
        $check_bet_amount += $last_need_amount;
        if ($member_config_data['check_bet_amount'] == $check_bet_amount) {
            $check_bet_amount += 1;
        }
        Log::record($member['account'] . '|check_bet_amount:' . $check_bet_amount . ',' . $member_config_data['check_bet_amount'] . 'last_need_amount:' . $last_need_amount . '|' . print_r($member_valid_data, true), 'info','check_valid_bet_');
        $flag = $member_config_handle->updateCheckBetAmount($member['member_id'], $check_bet_amount, 0);

        if ($flag) {
            $sql = 'UPDATE fnn_deposit SET status=2,confirm_time=?,operator=? WHERE deposit_id=? AND status=1 LIMIT 1';
            $flag = $this->_db->query($sql, array($time, $operator, $deposit_id));
        }

        if ($flag) {
            if ($award) {
                $award_amount = round($amount_db * $award / 100);
            }

            $flag = $member_handle->updateAmount($deposit['member_id'], $amount_db);
        }

        if ($flag) {
            if ($member_config_data['casino_amount'] + $deposit['amount'] > 100000) {
                /*$notice_content = '会员'.$member['account'].'的可用额度转移点数已经相对较多。为保险起见，本次额度变化未对其可用点数继续上调!如若需要调整，请在会员配置中调整其值。';
            $notice_handle = new Notice();
            $notice_argument = array(
            0,
            1,
            3,
            0,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s', time() + 900),
            '额度点数',
            $notice_content,
            '',
            date('Y-m-d H:i:s')
            );
            $notice_handle->add($notice_argument);
            unset($notice_handle);*/
            } else {
                $member_config_handle->updateCasinoPoint($deposit['amount'], $member['member_id']);
            }
        }

        if ($flag) {
            $argument = array(
                $deposit['member_id'],
                0,
                1,
                $deposit_id,
                $amount_db,
                $time,
                '',
                $member['amount'],
            );
            $finance_handle = new Finance();
            $flag = $finance_handle->add($argument);
            unset($finance_handle);
        }

        if ($award && $award_amount >= 1) {
            $flag = $member_handle->updateAmount($deposit['member_id'], $award_amount);
            if ($flag) {
                $argument = array(
                    $deposit['member_id'],
                    0,
                    12,
                    $deposit_id,
                    $award_amount,
                    $time,
                    '',
                );
                $finance_handle = new Finance();
                $flag = $finance_handle->add($argument);
                unset($finance_handle);
            }
        }

        if ($flag) {
            $flag = $member_handle->updateDeposit($deposit['member_id'], $deposit['amount']);
        }

        if ($flag) {
            $payment_handle = new Payment();
            $payment_handle->updateAmount($deposit['payment_id'], $deposit['amount']);
            unset($payment_handle);
        }

        if ($flag) {
            if ($operator) {
                // 管理员操作日志
                $operate_log_handle = new OperateLog();
                $parameters = array(
                    'manager_account' => $operator,
                    'account' => $member['account'],
                    'account_type' => 1,
                    'module' => 'finance',
                    'argument' => array(),
                    'detail' => '确认在线存款|金额' . $deposit['amount'] . '|单号' . $deposit['order_id'] . '|赠送' . $deposit['amount'] * $award * 0.01,
                );
                $operate_log_handle->add($parameters);
                unset($operate_log_handle);
            }
            $this->_db->commit();
            return 1;
        } else {
            $this->_db->rollBack();
            return -3;
        }
    }

    public function confirm2($deposit_id, $auto_award = false, $operator = '')
    {
        $deposit = $this->get($deposit_id);
        if (!$deposit) {
            return -1;
        }
        if (1 != $deposit['status']) {
            return -2;
        }

        $time = date('Y-m-d H:i:s');
        $this->_db->beginTransaction();

        $sql = 'UPDATE fnn_deposit SET status=2,confirm_time=?,operator=? WHERE deposit_id=? AND status=1 LIMIT 1';
        $flag = $this->_db->query($sql, array($time, $operator, $deposit_id));

        if ($flag) {
            $this->_db->commit();
            return 1;
        } else {
            $this->_db->rollBack();
            return -3;
        }
    }

    public function cancel($deposit_id, $operator = '')
    {
        $deposit = $this->get($deposit_id);
        if (1 != $deposit['status']) {
            return 0;
        }
        $time = date('Y-m-d H:i:s');
        $sql = 'UPDATE fnn_deposit SET status=3,confirm_time=?,operator=? WHERE deposit_id=? LIMIT 1';
        if ($this->_db->query($sql, array($time, $operator, $deposit_id))) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * [getList 獲取清單]
     * @param  [type] $argument [description]
     * @return [array]  [fnn_deposit列表]
     */
    public function getList($argument)
    {
        global $g_payment_type;
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $time_type = isset($argument['time_type']) ? $argument['time_type'] : 0; // 時間排序預選為按照提交時間排序
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // if (1 == $time_zone) {
        //     // 北京时间
        //     if ($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if ($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }

        if (0 == $time_zone) { // 美東時間
            if ($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if ($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $sort = (0 == $time_type) ? 'submit_time' : 'confirm_time';

        $condition = array();
        $parameter = array();
        if ($date_start) {
            if (0 == $time_type) {
                $condition[] = ' AND fnn_deposit.submit_time>=?';
            } else {
                $condition[] = ' AND fnn_deposit.confirm_time>=?';
            }
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            if (0 == $time_type) {
                $condition[] = ' AND fnn_deposit.submit_time<=?';
            } else {
                $condition[] = ' AND fnn_deposit.confirm_time<=?';
            }
            $parameter[] = $date_stop;
        }
        if (isset($argument['account']) && '' != $argument['account']) {

            if (isset($argument['fuzzy']) && $argument['fuzzy']) {
                $condition[] = ' AND fnn_deposit.account Like ?';
                $parameter[] = '%' . $argument['account'] . "%";
            } else {
                $condition[] = ' AND fnn_deposit.account=?';
                $parameter[] = $argument['account'];
            }
        }
        if (isset($argument['operator']) && '' != $argument['operator']) {
            $condition[] = ' AND fnn_deposit.operator=?';
            $parameter[] = $argument['operator'];
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND fnn_deposit.status=?';
            $parameter[] = $argument['status'];
        }
        if (isset($argument['income_view_min']) && $argument['income_view_min']) {
            $condition[] = ' AND fnn_deposit.amount>=?';
            $parameter[] = $argument['income_view_min'];
        }
        if (isset($argument['income_view_max']) && $argument['income_view_max']) {
            $condition[] = ' AND fnn_deposit.amount<=?';
            $parameter[] = $argument['income_view_max'];
        }

        if (isset($argument['payment_list']) &&
            isset($argument['payment_method_list'])) {
            $payment_channel = $argument['payment_list'];
            $payment_method = $argument['payment_method_list'];

            if ($argument['payment_list'] !== 'all' &&
                $argument['payment_method_list'] !== 'all') {
                $condition[] = ' AND fnn_deposit.payment_summary LIKE ? ';
                $parameter[] = '%' . $g_payment_type[$payment_channel] . '%' . $payment_method;
            } elseif ($argument['payment_list'] != 'all' &&
                $argument['payment_method_list'] == 'all') {
                $condition[] = ' AND fnn_deposit.payment_summary LIKE ? ';
                $parameter[] = '%' . $g_payment_type[$payment_channel] . '%';
            }
        }

        if (isset($argument['category']) && $argument['category'] != 0) {
            $condition[] = ' AND uc_member.category=?';
            $parameter[] = $argument['category'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM fnn_deposit ';
        $sql .= ' INNER JOIN uc_member ON uc_member.member_id=fnn_deposit.member_id ';
        $sql .= 'WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        if (isset($argument['countTotal']) && $argument['countTotal']) {

        } else {
            $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
            if ($argument['page'] > $return['total_page']) {
                $argument['page'] = $return['total_page'];
            }
            if ($argument['page'] < 1) {
                $argument['page'] = 1;
            }
            $return['page'] = $argument['page'];
        }

        $sql = 'SELECT fnn_deposit.* FROM fnn_deposit ';
        $sql .= ' INNER JOIN uc_member ON uc_member.member_id=fnn_deposit.member_id ';
        $sql .= 'WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY ' . $sort . ' DESC ';

        if (isset($argument['countTotal']) && $argument['countTotal']) {

        } else {
            if ($argument['page_size']) {
                $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
            }
        }
        $result = $this->_db->query($sql, $parameter); //print_r($result);
        $return['list'] = $result;
        $return['argument'] = $argument;

        return $return;
    }

    /**
     * [getTotalDepositAmount 獲取在線存款總額]
     * @param  [type] $argument [description]
     * @return [integer]   [總金額]
     */
    public function getTotalDepositAmount($argument)
    {
        $data = $this->getList($argument);
        $total_amount = 0;

        if ($data['list']) {
            foreach ($data['list'] as $row) {
                if (2 == $row['status']) {
                    $total_amount += $row['amount'];
                }
            }
        }

        return $total_amount;
    }

    /**
     * [getNumberDeposit 獲取存款總額]
     * @param  [type] $date_start [description]
     * @return [type]             [description]
     */
    public function getNumberDeposit($date_start)
    {
        $date_end = $date_start;
        $date_start .= ' 00:00:00';
        $date_end .= ' 23:59:59';
        $status = 2;

        $sql = "SELECT DISTINCT(fnn_deposit.member_id) AS total_number_deposit
                FROM fnn_deposit
                WHERE submit_time >= ? AND submit_time <= ? AND status=?
                GROUP BY member_id";

        $result = $this->_db->query($sql, array($date_start, $date_end, $status));

        return count($result);
    }
}
