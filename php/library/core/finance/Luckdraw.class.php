<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 公司入款类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Luckdraw extends Wwin {

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}

	public function getDepositSummeryList($argument)  //按条件进行查询个人总入款，合并
	{
		//SELECT member_id,account,SUM(amount) FROM `fnn_deposit` WHERE STATUS=2 AND submit_time BETWEEN '2017-03-01 20:00:00' AND '2017-12-31 23:59:59' GROUP BY member_id

        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';
		if($date_start) {
            $day_start  = $date_start . ' 00:00:00';
            $date_start = strtotime($day_start) ? $day_start : $date_start;
        }
        if($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // if(1 == $time_zone) {
        //     // 北京时间
        //     if($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }

        if(0 == $time_zone) {
            // 美東时间
            if($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND f.submit_time>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND f.submit_time<=?';
            $parameter[] = $date_stop;
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            $condition[] = ' AND u.account=?';
            $parameter[] = $argument['account'];
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND u.status=?';
            $parameter[] = $argument['status'];
        }

        $return = array();


        //$sql = 'SELECT COUNT(DISTINCT member_id) AS count FROM fnn_deposit WHERE member_id>0 ';
        $sql = 'SELECT COUNT(DISTINCT f.member_id) AS count FROM fnn_record f WHERE f.member_id>0  AND f.subtype IN (1,2,13) ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

		$return['sql1'] = $sql;


        //$sql = 'SELECT member_id,account,SUM(amount) as amount FROM fnn_deposit WHERE member_id>0';
        $sql = 'SELECT u.member_id,u.account,SUM(f.amount) AS amount FROM fnn_record f,uc_member u WHERE f.member_id>0 AND f.subtype IN (1,2,13) AND u.member_id=f.member_id ';
        $sql .= implode('', $condition);
        $sql .= ' GROUP BY u.member_id ORDER BY u.account ASC'; //having SUM(f.amount/100)>9
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
		$return['sql2'] = $sql;
        $return['list'] = $result;

		$return['argument'] = $argument;
        return $return;
    }

	public function getDepositDetailList($argument)  //查询个人详细入款记录
	{

        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';
		if($date_start) {
            $day_start  = $date_start . ' 00:00:00';
            $date_start = strtotime($day_start) ? $day_start : $date_start;
        }
        if($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // if(1 == $time_zone) {
        //     // 北京时间
        //     if($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }

        if(0 == $time_zone) {
            // 美東时间
            if($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND f.submit_time>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND f.submit_time<=?';
            $parameter[] = $date_stop;
        }

        if (isset($argument['member_id']) && '' != $argument['member_id']) {
            $condition[] = ' AND f.member_id=?';
            $parameter[] = $argument['member_id'];
        }


		if (isset($argument['account']) && '' != $argument['account']) {
            $condition[] = ' AND u.account=?';
            $parameter[] = $argument['account'];
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND u.status=?';
            $parameter[] = $argument['status'];
        }

        $return = array();


        //$sql = 'SELECT COUNT(member_id) AS count FROM fnn_deposit WHERE member_id>0 ';
        $sql = 'SELECT COUNT(DISTINCT f.member_id) AS count FROM fnn_record f WHERE f.member_id>0  AND f.subtype IN (1,2,13) ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];




        //$sql = 'SELECT * FROM fnn_deposit WHERE member_id>0';
		$sql = 'SELECT u.member_id,u.account,f.amount,f.subtype,f.submit_time,f.note FROM fnn_record f,uc_member u WHERE f.member_id>0 AND f.subtype IN (1,2,13) AND u.member_id=f.member_id ';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY f.submit_time DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
		$return['sql'] = $sql;
        $return['list'] = $result;
		$return['argument'] = $argument;
        return $return;
    }

	public function genLuckDraw($argument)
	{

		$time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;

        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';

        $start_time = isset($argument['start_time']) ? strtotime($argument['start_time'])*1000 : 0;
        $pause_time  = isset($argument['pause_time']) ? strtotime($argument['pause_time'])*1000 : 0;
        $end_time  = isset($argument['end_time']) ? strtotime($argument['end_time'])*1000 : 0;


        $draw_amount  = isset($argument['draw_amount']) ? $argument['draw_amount'] : 0;
        $draw_num  = isset($argument['draw_num']) ? $argument['draw_num'] : 1;
        $draw_max  = isset($argument['draw_max']) ? $argument['draw_max'] : 0;
        $draw_min  = isset($argument['draw_min']) ? $argument['draw_min'] : 0;




		date_default_timezone_set("Asia/Shanghai");
		$return = array();
		$draw_date=date('Y-m-d');
        $sql='INSERT IGNORE INTO fnn_lucktime SET draw_date=?,start_time=?,pause_time=?,end_time=?,draw_amount=?,draw_num=?,draw_max=?,draw_min=?';
		$parameter = array(
			$draw_date,
            $argument['start_time'],
            $argument['pause_time'],
            $argument['end_time'],
            $argument['draw_amount'],
            $argument['draw_num'],
            $argument['draw_max'],
            $argument['draw_min']
        );
		$time_id=$this->_db->query($sql,$parameter);

		$sql = 'DELETE FROM `fnn_luckdraw` WHERE `draw_date`=?';
        $this->_db->query($sql, array($draw_date));


		$gen_data = $this->genDrawResult($draw_amount,$draw_num,$draw_max,$draw_min);
		$result = $this->getDepositSummeryList($argument);
		$return['result'] = $result;
		$return['argument'] = $argument;

		$r=0;
		$submit_time = date('Y-m-d H:i:s');
		foreach ($result["list"] as $item) {
			$times=$this->getDrawTimes($item["amount"]);
			for($i=0;$i<$times;$i++)
			{
				$r=$r+1;
				$money=$gen_data["res"][$r]["money"];

				$sql = 'INSERT INTO fnn_luckdraw SET time_id=?,draw_date=?,member_id=?,account=?,amount=?,submit_time=?,status=1';
				$this->_db->query($sql, array($time_id,$draw_date, $item["member_id"], $item["account"], $money, $submit_time));
			}
		}
		$return['err'] = 0;


		return $return;
	}

	public function genDrawResult($totalAmount,$num,$max,$min)  //根据红包总额，红包总数，最大值和最小值，产生随机红包
	{
		$total=$totalAmount;//红包总额
		//$num=10;// 分成10个红包，支持10人随机领取
		//$min=1;//每个人最少能收到0.01元

		$temp=$min;
		$total=$total-$max-($num-1)*$min;
		if($total<0)
		{
			$arr['msg'] = '每个人最少值有错，无法进行抽奖，请调整最少值。';
			return $arr;
		}

		$m=mt_rand(1,$num);
		$min=0.01;
		for($i=1;$i<=$num;$i++)
		{
			if($i!=$m)
			{
				if($i==$num)
				{
					$arr['res'][$num] = array('money'=>$temp+$total,'total'=>0);
				}
				else
				{
					if($m==$num && $i==$num-1)
					{
						$arr['res'][$i] = array('money'=>$temp+$total,'total'=>0);
					}
					else
					{
						$safe_total=($total-($num-$i)*$min)/($num-$i);//随机安全上限
						$money=mt_rand($min*100,$safe_total*100)/100;
						$total=$total-$money;
						$arr['res'][$i] = array(
							'money' => $temp+$money,
							'total' => $total
						);
					}
				}

			}
			else
			{
				$arr['res'][$i] = array(
					'money' => $max,
					'total' => $total
				);
			}
		}

		$arr['msg'] = 1;
		return $arr;
	}


	public function getLuckDrawSummeryList($argument)  //按条件进行查询个人红包记录，合并
	{


        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';
		if($date_start) {
            $day_start  = $date_start . ' 00:00:00';
            $date_start = strtotime($day_start) ? $day_start : $date_start;
        }
        if($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // if(1 == $time_zone) {
        //     // 北京时间
        //     if($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }
        if(0 == $time_zone) {
            // 美東时间
            if($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND draw_date>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND draw_date<=?';
            $parameter[] = $date_stop;
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            $condition[] = ' AND account=?';
            $parameter[] = $argument['account'];
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND status=?';
            $parameter[] = $argument['status'];
        }

        $return = array();


        $sql = 'SELECT COUNT(DISTINCT member_id) AS count FROM fnn_luckdraw WHERE member_id>0 ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

		$return['sql'] = $sql;

        $sql = 'SELECT member_id,account,SUM(amount) as amount,count(member_id) as times FROM fnn_luckdraw WHERE member_id>0';
        $sql .= implode('', $condition);
        $sql .= ' GROUP BY member_id ORDER BY account ASC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;

		$return['argument'] = $argument;
        return $return;
    }


	public function getLuckDrawSummery($argument)  //查看详情
	{


        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;


        $condition = array();
        $parameter = array();

        if (isset($argument['time_id']) && '' != $argument['time_id']) {
            $condition[] = ' AND time_id=?';
            $parameter[] = $argument['time_id'];
        }


        $return = array();


        $sql = 'SELECT COUNT(DISTINCT member_id) AS count FROM fnn_luckdraw WHERE member_id>0 ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

		$return['sql'] = $sql;

        $sql = 'SELECT member_id,draw_date,account,SUM(amount) AS amount,SUM(CASE WHEN NOT ISNULL(draw_time) THEN amount ELSE 0 END) AS draw_amount,COUNT(member_id) AS times,SUM(CASE WHEN NOT ISNULL(draw_time) THEN 1 ELSE 0 END) AS draw_times FROM fnn_luckdraw WHERE member_id>0';
        $sql .= implode('', $condition);
        $sql .= ' GROUP BY member_id ORDER BY account ASC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);

		$sql = 'SELECT SUM(amount) AS amount,SUM(CASE WHEN NOT ISNULL(draw_time) THEN amount ELSE 0 END) AS draw_amount,COUNT(member_id) AS times,SUM(CASE WHEN NOT ISNULL(draw_time) THEN 1 ELSE 0 END) AS draw_times FROM fnn_luckdraw WHERE member_id>0';
        $sql .= implode('', $condition);
        $summery = $this->_db->query($sql, $parameter);

		$return['summery'] = $summery;

        $return['list'] = $result;

		$return['argument'] = $argument;
        return $return;
    }



	public function getLuckDrawDetail($argument)  //获取个人红包详细记录
	{
        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';

		if(!$draw_date)
		{
		}
		else
		{
			if($date_start) {
				$day_start  = $date_start . ' 00:00:00';
				$date_start = strtotime($day_start) ? $day_start : $date_start;
			}
			if($date_stop) {
				$day_stop  = $date_stop . ' 23:59:59';
				$date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
			}

			// if(1 == $time_zone) {
			// 	// 北京时间
			// 	if($date_start) {
			// 		$date_start = bj_to_et_date($date_start);
			// 	}
			// 	if($date_stop) {
			// 		$date_stop = bj_to_et_date($date_stop);
			// 	}
            // }
            
            if(0 == $time_zone){// 美東時間
                if($date_start){
                    $date_start = et_to_bj_date($date_start);
                }
                if($date_stop){
                    $date_stop = et_to_bj_date($date_stop);
                }
            }

		}


        $condition = array();
        $parameter = array();
		if($draw_date)
		{
			$condition[] = ' AND draw_date=?';
			$parameter[] = $draw_date;
		}
		if ($date_start) {
			$condition[] = ' AND draw_date>=?';
			$parameter[] = substr($date_start,0,10);
		}
		if ($date_stop) {
			$condition[] = ' AND draw_date<=?';
			$parameter[] = substr($date_stop,0,10);
		}
		if (isset($argument['account']) && '' != $argument['account']) {
			$condition[] = ' AND account=?';
			$parameter[] = $argument['account'];
		}
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND status=?';
            $parameter[] = $argument['status'];
        }

        $return = array();


        $sql = 'SELECT * FROM fnn_luckdraw WHERE member_id>0';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY account ASC';
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        //$return['argument'] = $argument;
		//$return['parameter'] = $parameter;
		//$return['sql'] = $sql;
		$return['total_record'] =count($result);
		$return['total_page'] = 1;
		$return['page'] = 1;
        return $return;
    }


	public function add($time_id,$draw_date,$member_id,$account,$amount) {  //产生每天个人的红包
		date_default_timezone_set("Asia/Shanghai");
		$submit_time = date('Y-m-d H:i:s');
        $sql = 'INSERT INTO fnn_luckdraw SET time_id=?,draw_date=?,member_id=?,account=?,amount=?,submit_time=?,status=1';
        return $this->_db->query($sql, array($time_id,$draw_date, $member_id, $account, $amount*100, $submit_time));
    }



	public function deleteDraw($draw_date) {  //删除当天的红包，以备
        $sql = 'DELETE FROM `fnn_luckdraw` WHERE `draw_date`=?';
        return $this->_db->query($sql, array($draw_date));
    }

	public function updateDraw($draw_id,$draw_amount) {  //更新红包
        $sql = 'UPDATE `fnn_luckdraw` SET `amount`=? WHERE `draw_id`=? ';
        return $this->_db->query($sql, array($draw_amount,$draw_id));
    }

	public function selectDraw($account) {  //抽取红包
		date_default_timezone_set("Asia/Shanghai");
		$status=0;//还未到时间
		$draw_date=date('Y-m-d');
		$sql= 'SELECT * FROM `fnn_lucktime` WHERE `draw_date`=? LIMIT 1';
		$result=$this->_db->query($sql, array($draw_date));
		$now_time=time()*1000;
		$start_time=strtotime($result[0]["start_time"])*1000;
		$pause_time=strtotime($result[0]["pause_time"])*1000;
		$end_time=strtotime($result[0]["end_time"])*1000;
		$member_id=0;
		$draw_id=0;
		$draw_amount=0;
		if($end_time>=$now_time)
		{
			if($now_time<$start_time)
			{
				$status=0; //还未到时间
			}
			else
			{
				if($pause_time<$end_time && $start_time<$pause_time) //如果有暂停时间,检查一下是否在抽奖中暂停
				{
					$status=2; //已经暂停
				}
				else
				{
					$status=1; //活动中
					$draw_times=0;
					$sql='SELECT * FROM `fnn_luckdraw` WHERE `draw_date`=? AND `account`=? AND status=1 LIMIT 1'; //查找是否还有没有抽取的红包
					$result=$this->_db->query($sql, array($draw_date,$account));
					if($result) {  //有可抽取的红包，抽取一个红包
						$draw_amount=$result[0]['amount'];
						$draw_id=$result[0]['draw_id'];
						$member_id=$result[0]['member_id'];
						$sql = 'UPDATE fnn_luckdraw SET status=2,draw_time=? WHERE draw_id=?';  //抽取红包，并修改状态为已经抽取
						$this->_db->query($sql, array(date('Y-m-d H:i:s'),$draw_id));
						$sql = 'SELECT COUNT(account) AS draw_times FROM `fnn_luckdraw` WHERE `draw_date`=? AND account=? AND status=1';
						$result=$this->_db->query($sql, array($draw_date,$account));
						$draw_times=$result[0]['draw_times'];
					}
					else //红包已经抽完
					{
						$draw_times=0;
					}
				}
			}

		}
		else
		{
			$status=3;//已经结束
		}





		$time = date('Y-m-d H:i:s');
		if ($draw_amount > 0) {
			$member_handle = new Member();
			$member = $member_handle->getByMemberId($member_id);
			$flag = $member_handle->updateAmount($member_id, $draw_amount);
			unset($member_handle);
            do {
    			if (!$flag) {
    				break;
    			}
            } while (0);

			//type=21  subtype=21 活动彩金
			//$sql = 'INSERT INTO fnn_record SET member_id=?,type=?,subtype=?,table_id=?,amount=?,submit_time=?,note=?,previous_amount=?,current_amount=?,member_category=?';
			$argument = array(
				$member_id,
				21,
				21,
				$draw_id,
				$draw_amount,
				$time,
				'红包' . $draw_id,
				$member['draw_amount']
			);
			$finance_handle = new Finance();
			$flag = $finance_handle->add($argument);
			unset($finance_handle);
		}

		$result_data= array(
            'draw_date' => $draw_date,
            'draw_amount' => $draw_amount,
            'draw_times' => $draw_times,
            'now_time' => $now_time,
            'start_time' => $start_time,
            'pause_time' => $pause_time,
            'end_time' => $end_time,
			'draw_status'=>$status
        );

		return $result_data;
    }



	public function getMemberDrawTimes($account) {  //获取个人的红包次数
		date_default_timezone_set("Asia/Shanghai");
		$draw_date = date('Y-m-d');
        $sql = array();
        $sql[] = 'SELECT COUNT(account) AS total FROM `fnn_luckdraw` WHERE `draw_date`=? AND account=? AND status=1';
		$result=$this->_db->query(implode('', $sql), array($draw_date,$account));
        return $result;
    }

	public function getMemberDrawList($account) {  //获取个人红包记录
		date_default_timezone_set("Asia/Shanghai");
		$draw_date = date('Y-m-d');
        $sql = array();
        $sql[] = 'SELECT * FROM `fnn_luckdraw` WHERE `draw_date`=? AND account=? AND status=2';
		$result=$this->_db->query(implode('', $sql), array($draw_date,$account));
        return $result;
    }


	public function newDrawTime($argument) {  //生成抽奖时间，开始，结束。//增加活动的开始时间，暂停时间，以及结束时间。
		date_default_timezone_set("Asia/Shanghai");
		$draw_date=date('Y-m-d');
        $sql='INSERT IGNORE INTO fnn_lucktime SET draw_date=?,start_time=?,pause_time=?,end_time=?,draw_amount=?,draw_num=?,draw_max=?,draw_min=?';
		$parameter = array(
			$draw_date,
            $argument['start_time'],
            $argument['pause_time'],
            $argument['end_time'],
            $argument['draw_amount'],
            $argument['draw_num'],
            $argument['draw_max'],
            $argument['draw_min']
        );
		$time_id=$this->_db->query($sql,$parameter);
        return $time_id;
    }

	public function getDrawTime() {  //获取当前时间活动的相关时间
		date_default_timezone_set("Asia/Shanghai");
		$draw_date = date('Y-m-d');
        $sql = 'SELECT * FROM `fnn_lucktime` WHERE `draw_date`=? LIMIT 1';
		$result=$this->_db->query($sql, array($draw_date));
		$now_time=time()*1000;
		$start_time=0;
		$pause_time=0;
		$end_time=0;
		if($result)
		{
			$start_time=strtotime($result[0]["start_time"])*1000;
			$pause_time=strtotime($result[0]["pause_time"])*1000;
			$end_time=strtotime($result[0]["end_time"])*1000;
		}


		$status=0; //没有开始
		if($now_time<$start_time)
		{
			$status=0; //没有开始
		}
		else if($now_time>$end_time)
		{
			$status=3; //活动结束
		}
		else if($start_time<$pause_time && $pause_time<$end_time)
		{
			$status=2; //活动暂停
		}
		else
		{
			$status=1; //抽奖活动
		}

		$result_data= array(
            'draw_date' => $draw_date,
            'now_time' => $now_time,
            'start_time' => $start_time,
            'pause_time' => $pause_time,
            'end_time' => $end_time,
			'status'=>$status
        );
        return $result_data;
    }


	public function getLuckTimeList($argument)  //查询活动列表
	{

        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 1;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';
		if($date_start) {
            $day_start  = $date_start . ' 00:00:00';
            $date_start = strtotime($day_start) ? $day_start : $date_start;
        }
        if($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // if(1 == $time_zone) {
        //     // 北京时间
        //     if($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }

        if(0 == $time_zone) {
            // 北京时间
            if($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND draw_date>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND draw_date<=?';
            $parameter[] = $date_stop;
        }


        $return = array();


        $sql = 'SELECT COUNT(time_id) AS count FROM fnn_lucktime WHERE 1=1 ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];




        $sql = 'SELECT * FROM fnn_lucktime WHERE 1=1';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY time_id DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
		$return['sql'] = $sql;
        $return['list'] = $result;
		$return['argument'] = $argument;
        return $return;
    }

	public function getLuckTimeStatus($start_time,$pause_time,$end_time)  //通过传入钱数，获取可以参与多少次抢红包
	{
		date_default_timezone_set("Asia/Shanghai");
		$now_time=time()*1000;
		$start_time=strtotime($start_time)*1000;
		$pause_time=strtotime($pause_time)*1000;
		$end_time=strtotime($end_time)*1000;
		$status="未开始"; //没有开始
		if($now_time<$start_time)
		{
			$status="未开始"; //没有开始
		}
		else if($now_time>=$end_time)
		{
			$status="已结束"; //活动结束
		}
		else if($start_time<$pause_time && $pause_time<$end_time)
		{
			$status="暂停中"; //活动暂停
		}
		else
		{
			$status="抽奖中"; //抽奖活动
		}
		return $status;
	}

	public function updateLuckTimeStatusPause($time_id)    //修改活动状态暂停
	{
		date_default_timezone_set("Asia/Shanghai");
		$now_time = date('Y-m-d H:i:s');
        $sql = 'UPDATE `fnn_lucktime` SET pause_time=? WHERE `time_id`=?';
		$result=$this->_db->query($sql, array($now_time,$time_id));
		$return = array();
		$return['result'] = $result;
		$return['pause_time'] = $now_time;
        return $return;
    }
	public function updateLuckTimeStatusContinue($time_id)    //修改活动状态为继续
	{
		date_default_timezone_set("Asia/Shanghai");
		$now_time = date('0000-00-00 00:00:00');
        $sql = 'UPDATE `fnn_lucktime` SET pause_time=? WHERE `time_id`=?';
		$result=$this->_db->query($sql, array($now_time,$time_id));
        $return = array();
		$return['result'] = $result;
		$return['pause_time'] = $now_time;
        return $return;
    }

	public function updateLuckTime($time_id,$start_time,$end_time)    //修改活动时间
	{
		date_default_timezone_set("Asia/Shanghai");
        $sql = 'UPDATE `fnn_lucktime` SET start_time=?,end_time=? WHERE `time_id`=?';
		$result=$this->_db->query($sql, array($start_time,$end_time,$time_id));
        return $result;
    }

	public function getDrawTimes($amount)  //通过传入钱数，获取可以参与多少次抢红包
	{
		$amount=$amount/100;
		if($amount>=1000000)
		{
			return 30;
		}
		if($amount>=500000)
		{
			return 20;
		}
		if($amount>=100000)
		{
			return 15;
		}
		if($amount>=50000)
		{
			return 12;
		}
		if($amount>=10000)
		{
			return 8;
		}
		if($amount>=5000)
		{
			return 6;
		}
		if($amount>=1000)
		{
			return 4;
		}
		if($amount>=500)
		{
			return 3;
		}
		if($amount>=200)
		{
			return 2;
		}
		if($amount>=10)
		{
			return 1;
		}
		return 0;

	}







}
