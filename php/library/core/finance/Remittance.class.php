<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 公司入款类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Remittance extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function updateRemittance($parameter)
    {
        $sql = ' UPDATE
                fnn_remittance SET
                name=?,deposit_time=?,submit_time=? ,
                 status=? WHERE status=? AND remittance_id=?';
        return ($this->_db->query($sql, $parameter))? 1: -7;
    }

    public function addRemittance($parameter)
    {
        $sql = 'INSERT INTO
                fnn_remittance SET
                member_id=?,account=?,bankcard_id=?,amount=?,bank_name=?,
                deposit_time=?,submit_time=?,order_id=?,
                status=?,bankcard_summary=?';
        return ($this->_db->query($sql, $parameter));
    }

    // public function update($argument)
    // {
    //     global $member;
    //     if (!$member) {
    //         return -1; // 会员不存在
    //     }
    //
    //     if (!$argument['bankcard_id']) {
    //         return -20; // 银行卡不存在
    //     }
    //
    //     $bankcard_handle = new Bankcard();
    //     $bankcard = $bankcard_handle->get($argument['bankcard_id']);
    //     unset($bankcard_handle);
    //
    //     // if (!$bankcard) {
    //     //     return -3; // 银行卡不存在
    //     // }
    //
    //     $config_handle = new Config();
    //     $config = $config_handle->getByName(array('OFFLINE_DEPOSIT_MIN'));
    //     unset($config_handle);
    //
    //     if ($argument['amount'] < $config['OFFLINE_DEPOSIT_MIN']) {
    //         return -30000; // 小于存款最低限额
    //     }
    //
    //     if ($argument['amount'] > 10000000) {
    //         return -6;
    //     }
    //
    //     $sql = array();
    //     $sql[] = 'UPDATE fnn_remittance SET name=?,deposit_time=?,submit_time=?,status=? WHERE status=? AND remittance_id=?';
    //     $parameter = array(
    //         $argument['name'],
    //         $argument['deposit_time'],
    //         date('Y-m-d H:i:s'),
    //         1,
    //         0,
    //         $argument['remittance_id'],
    //     );
    //     if ($this->_db->query(implode('', $sql), $parameter)) {
    //         return 1;
    //     } else {
    //         Log::record('[Remittance.add][line:72] 更新公司入款纪录 parameter: ' . json_encode($parameter), 'error',
    //             'remittance/');
    //         return -4;
    //     }
    // }

    // public function get($remittance_id)
    // {
    //     $sql = 'SELECT * FROM fnn_remittance WHERE remittance_id=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($remittance_id));
    //     if ($result) {
    //         $result = $result[0];
    //     }
    //     return $result;
    // }

    // public function confirm($remittance_id, $auto_award = false, $operator = '', $manager_note = '')
    // {
    //     $remittance = $this->get($remittance_id);
    //     if (!$remittance) {
    //         return -1; // 存款记录不存在
    //     }
    //     if (1 != $remittance['status'] && 4 != $remittance['status']) {
    //         return -2; // 存款已经确认
    //     }
    //     $config_handle = new Config();
    //     $config = $config_handle->getByName(array('OFFLINE_DEPOSIT_AWARD', 'CHECK_RATE'));
    //     unset($config_handle);
    //     $award = 0;
    //     $award_amount = 0;
    //     if ($auto_award) {
    //         $award = $config['OFFLINE_DEPOSIT_AWARD'];
    //     }
    //     $time = date('Y-m-d H:i:s');
    //     $this->_db->beginTransaction();
    //
    //     $remittance_amount = $remittance['amount'];
    //     $check_bet_amount = ceil(($remittance_amount + $remittance_amount * $award * 0.01) * (1 - $config['CHECK_RATE'] * 0.01));
    //     $member_handle = new Member();
    //     $member = $member_handle->getByMemberId($remittance['member_id']);
    //     // 最近一次存款记录时间点
    //     $last_deposit_time = $member_handle->getLastDepositTime($member['member_id']);
    //     $params = array(
    //         'member_id' => $member['member_id'],
    //         'date_start' => $last_deposit_time,
    //         'date_stop' => date('Y-m-d H:i:s'),
    //     );
    //     $member_valid_data = $member_handle->memberBetReportByMember($params);
    //     $member_config_handle = new MemberConfig();
    //     $member_config_data = $member_config_handle->getByMemberId($member['member_id']);
    //     $valid_bet_total = money_to_db($member_valid_data['valid_bet_total']);
    //     $win_total = money_to_db($member_valid_data['win_total']);
    //     $last_need_amount = 0;
    //     // if ($member_config_data['check_bet_amount'] + $win_total > 0) {
    //     //     $last_need_amount = $member_config_data['check_bet_amount'] - $valid_bet_total;
    //     // }
    //     $last_need_amount = $last_need_amount > 0 ? $last_need_amount : 0;
    //     $check_bet_amount += $last_need_amount;
    //     if ($member_config_data['check_bet_amount'] == $check_bet_amount) {
    //         $check_bet_amount += 1;
    //     }
    //     Log::record($member['account'] . '|check_bet_amount:' . $check_bet_amount . ',' . $member_config_data['check_bet_amount'] . 'last_need_amount:' . $last_need_amount . '|' . print_r($member_valid_data, true), 'info', 'check_valid_bet_');
    //     $flag = $member_config_handle->updateCheckBetAmount($member['member_id'], $check_bet_amount, 0);
    //
    //     if ($flag) {
    //         $sql = 'UPDATE fnn_remittance SET status=2,confirm_time=?,operator=?,manager_note=? WHERE remittance_id=? AND status!=2 AND status!=3 LIMIT 1';
    //         $flag = $this->_db->query($sql, array($time, $operator, $manager_note, $remittance_id));
    //     }
    //     if ($flag) {
    //         if ($award) {
    //             $award_amount = round($remittance['amount'] * $award / 100);
    //         }
    //         $flag = $member_handle->updateAmount($remittance['member_id'], $remittance['amount']);
    //     }
    //
    //     if ($flag) {
    //         if ($member_config_data['casino_amount'] + money_from_db($remittance_amount) > 100000) {
    //             /*$notice_content = '会员'.$member['account'].'的可用额度转移点数已经相对较多。为保险起见，本次额度变化未对其可用点数继续上调!如若需要调整，请在会员配置中调整其值。';
    //         $notice_handle = new Notice();
    //         $notice_argument = array(
    //         0,
    //         1,
    //         3,
    //         0,
    //         date('Y-m-d H:i:s'),
    //         date('Y-m-d H:i:s', time() + 900),
    //         '额度点数',
    //         $notice_content,
    //         '',
    //         date('Y-m-d H:i:s')
    //         );
    //         $notice_handle->add($notice_argument);
    //         unset($notice_handle);*/
    //         } else {
    //             $member_config_handle->updateCasinoPoint(money_from_db($remittance_amount), $member['member_id']);
    //         }
    //     }
    //
    //     $finance_handle = new Finance();
    //     if ($flag) {
    //         $argument = array(
    //             $remittance['member_id'],
    //             0,
    //             2,
    //             $remittance_id,
    //             $remittance['amount'],
    //             $time,
    //             '',
    //             $member['amount'],
    //         );
    //         $flag = $finance_handle->add($argument);
    //     }
    //     if ($award) {
    //         $flag = $member_handle->updateAmount($remittance['member_id'], $award_amount);
    //         if ($flag) {
    //             $argument = array(
    //                 $remittance['member_id'],
    //                 0,
    //                 12,
    //                 $remittance_id,
    //                 $award_amount,
    //                 $time,
    //                 '',
    //             );
    //             $flag = $finance_handle->add($argument);
    //         }
    //     }
    //     if ($flag) {
    //         $flag = $member_handle->updateDeposit($remittance['member_id'], $remittance['amount']);
    //     }
    //     unset($member_handle);
    //     unset($finance_handle);
    //
    //     if ($flag) {
    //         $bankcard_handle = new Bankcard();
    //         $bankcard_handle->updateAmount($remittance['bankcard_id'], $remittance['amount']);
    //         unset($bankcard_handle);
    //     }
    //
    //     if ($flag) {
    //         // 管理员操作日志
    //         $operate_log_handle = new OperateLog();
    //         $parameters = array(
    //             'manager_account' => $operator,
    //             'account' => $member['account'],
    //             'account_type' => 1,
    //             'module' => 'finance',
    //             'argument' => array(),
    //             'detail' => '确认公司入款|金额' . money_from_db($remittance_amount) . '|单号' . $remittance['order_id'] . '|赠送' . money_from_db($remittance_amount * $award * 0.01),
    //         );
    //         $operate_log_handle->add($parameters);
    //         unset($operate_log_handle);
    //
    //         $this->_db->commit();
    //         return 1;
    //     } else {
    //         $this->_db->rollBack();
    //         return -3;
    //     }
    // }

    // public function cancel($remittance_id, $operator = '', $manager_note = '')
    // {
    //     $remittance = $this->get($remittance_id);
    //     if (1 != $remittance['status'] && 4 != $remittance['status']) {
    //         return 0;
    //     }
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE fnn_remittance SET status=3, confirm_time=?,operator=?,manager_note=? WHERE remittance_id=? AND status!=3 AND status!=2 LIMIT 1';
    //     if ($this->_db->query($sql, array($time, $operator, $manager_note, $remittance_id))) {
    //         // 管理员操作日志
    //         $member_handle = new Member();
    //         $member = $member_handle->getByMemberId($remittance['member_id']);
    //         unset($member_handle);
    //         $operate_log_handle = new OperateLog();
    //         $parameters = array(
    //             'manager_account' => $operator,
    //             'account' => $member['account'],
    //             'account_type' => 1,
    //             'module' => 'finance',
    //             'argument' => array(),
    //             'detail' => '取消公司入款|金额' . money_from_db($remittance['amount']) . '|单号' . $remittance['order_id'],
    //         );
    //         $operate_log_handle->add($parameters);
    //         unset($operate_log_handle);
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    // public function doing($remittance_id, $operator = '', $manager_note = '')
    // {
    //     $remittance = $this->get($remittance_id);
    //     if (1 != $remittance['status']) {
    //         return 0;
    //     }
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE fnn_remittance SET status=4, confirm_time=?,operator=?,manager_note=? WHERE remittance_id=? AND status=1 LIMIT 1';
    //     if ($this->_db->query($sql, array($time, $operator, $manager_note, $remittance_id))) {
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $time_type = isset($argument['time_type']) ? $argument['time_type'] : 0;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }

        $sort = (0 == $time_type) ? 'submit_time' : 'confirm_time';

        $condition = array();
        $parameter = array();
        if ($date_start) {
            if (0 == $time_type) {
                $condition[] = ' AND fnn_remittance.submit_time>=?';
            } else {
                $condition[] = ' AND fnn_remittance.confirm_time>=?';
            }
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            if (0 == $time_type) {
                $condition[] = ' AND fnn_remittance.submit_time<=?';
            } else {
                $condition[] = ' AND fnn_remittance.confirm_time<=?';
            }
            $parameter[] = $date_stop;
        }
        if (isset($argument['name']) && '' != $argument['name']) {
            $condition[] = ' AND fnn_remittance.name=?';
            $parameter[] = $argument['name'];
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            if (isset($argument['fuzzy']) && $argument['fuzzy']) {
                $condition[] = ' AND fnn_remittance.account like ?';
                $parameter[] = '%' . $argument['account'] . '%';
            } else {
                $condition[] = ' AND fnn_remittance.account=?';
                $parameter[] = $argument['account'];
            }
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND fnn_remittance.status=?';
            $parameter[] = $argument['status'];
        }
        if (isset($argument['operator']) && '' != $argument['operator']) {
            $condition[] = ' AND fnn_remittance.operator=?';
            $parameter[] = $argument['operator'];
        }
        if (isset($argument['income_view_min'])) {
            $condition[] = ' AND fnn_remittance.amount>=?';
            $parameter[] = $argument['income_view_min'] * 100;
        }
        if (isset($argument['income_view_max'])) {
            $condition[] = ' AND fnn_remittance.amount<=?';
            $parameter[] = $argument['income_view_max'] * 100;
        }
        if (isset($argument['category']) && $argument['category'] != 0) {
            $condition[] = ' AND uc_member.category=?';
            $parameter[] = $argument['category'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM fnn_remittance ';
        $sql .= ' INNER JOIN uc_member ON fnn_remittance.member_id=uc_member.member_id';
        $sql .= ' WHERE 1 ';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT fnn_remittance.* FROM fnn_remittance ';
        $sql .= ' INNER JOIN uc_member ON fnn_remittance.member_id=uc_member.member_id';
        $sql .= ' WHERE 1 ';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY ' . $sort . ' DESC ';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function get_free($argument)
    {
        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $time_type = isset($argument['time_type']) ? $argument['time_type'] : 0;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : date('Y-m-d');
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : $date_start;
        // if (1 == $time_zone) {
        //     // 北京时间
        //     if ($date_start) {
        //         $date_start = bj_to_et_date($date_start);
        //     }
        //     if ($date_stop) {
        //         $date_stop = bj_to_et_date($date_stop);
        //     }
        // }

        if (0 == $time_zone) { // 美東時間

            if ($date_start) {
                $date_start = et_to_bj_date($date_start);
            }

            if ($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            if (0 == $time_type) {
                $condition[] = ' AND submit_time>=?';
            } else {
                $condition[] = ' AND confirm_time>=?';
            }
            $parameter[] = $date_start . " 00:00:00";
        }
        if ($date_stop) {
            if (0 == $time_type) {
                $condition[] = ' AND submit_time<=?';
            } else {
                $condition[] = ' AND confirm_time<=?';
            }
            $parameter[] = $date_start . " 23:59:59";
        }
        if (isset($argument['name']) && '' != $argument['name']) {
            $condition[] = ' AND name=?';
            $parameter[] = $argument['name'];
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            $condition[] = ' AND account=?';
            $parameter[] = $argument['account'];
        }

        $return = array();
        $sql = 'SELECT table_id , amount FROM fnn_record WHERE subtype=12';
        $result = $this->_db->query($sql, array());
        return $result;
    }

    /**
     * 未处理的公司入款记录
     */
    // public function untreated($argument)
    // {
    //     $sql = 'SELECT count(remittance_id) AS remittance_amount FROM fnn_remittance WHERE status=1 AND amount >= ? AND amount <= ?';
    //     $result = $this->_db->query($sql, $argument);
    //     return $result;
    // }

    /***
     * 根據時間撈取充值的人數
     * * */
    // public function getNumberRemittance($date_start)
    // {
    //     $date_end = $date_start;
    //     $date_start .= ' 00:00:00';
    //     $date_end .= ' 23:59:59';
    //     $status = 2;
    //
    //     $sql = "SELECT DISTINCT(fnn_remittance.member_id) AS total_number_remittance
    //             FROM fnn_remittance
    //             WHERE submit_time >= ? AND submit_time <= ? AND status=?
    //             GROUP BY member_id";
    //
    //     $result = $this->_db->query($sql, array($date_start, $date_end, $status));
    //
    //     return count($result);
    // }
}
