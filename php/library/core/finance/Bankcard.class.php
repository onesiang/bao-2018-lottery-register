<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 银行卡类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Bankcard extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

//    public function getBankCard($level)
//    {
//        $config_handle = new Config();
//        $sql = 'SELECT * FROM fnn_bankcard WHERE 1';
//        $condition = [];
//        $config = $config_handle->getByName(array('DEPOSIT_LEVEL'));
//        if($config['DEPOSIT_LEVEL']){
//            $sql .= " AND level REGEXP ?";
//            $condition[] = $level;
//        }
//        $sql .= " AND status = 1  ORDER BY last_time DESC";
//        $result = $this->_db->query($sql , $condition);
//        return (count($result) > 0)? $result:[];
//    }
//
//    public function add($argument)
//    {
//        $sql = array();
//        $sql[] = 'INSERT INTO fnn_bankcard (name,bank_name,bank_address,number';
//        $sql[] = ',level,limit_amount,status,submit_time,img_path,notice) VALUES (?,?,?,?,?,?,?,?,?,?)';
//        if ($this->_db->query(implode('', $sql), $argument)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
   public function get($bankcard_id = 0)
   {
       if (0 == $bankcard_id) {
           $config_handle = new Config();
           $config = $config_handle->getByName(array('DEPOSIT_LEVEL'));
           unset($config_handle);
           if ($config['DEPOSIT_LEVEL']) {
               $sql = 'SELECT * FROM fnn_bankcard';
               $sql .= ' WHERE status=1 ORDER BY last_time DESC';
               $all = $this->_db->query($sql);
               $result = array();
               if ($all) {
                   foreach ($all as $row) {
                       $one = explode(',', $row['level']);
                       // if (in_array($member['level'], $one)) {
                       $result[] = $row;
                       // }
                   }
               }
           } else {
               $sql = 'SELECT * FROM fnn_bankcard';
               $sql .= ' WHERE status=1 ORDER BY last_time DESC';
               $result = $this->_db->query($sql);
           }
       } else {
           $sql = 'SELECT * FROM fnn_bankcard WHERE bankcard_id=? LIMIT 1';
           $result = $this->_db->query($sql, array($bankcard_id));
       }
       if ($result) {
           foreach ($result as &$row) {
               $row['number'] .= ' ';
           }
           if (0 != $bankcard_id) {
               $result = $result[0];
           }
       }
       return $result;
   }
//
//    public function edit($argument)
//    {
//        $sql = array();
//        $sql[] = 'UPDATE fnn_bankcard SET name=?,bank_name=?,bank_address=?';
//        $sql[] = ',number=?,level=?,limit_amount=?,status=?,img_path=?, notice=? WHERE bankcard_id=?';
//        if ($this->_db->query(implode('', $sql), $argument)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function delete($bankcard_id)
//    {
//        $bankcard = $this->get($bankcard_id);
//        if ($bankcard) {
//            $this->_db->delete('fnn_bankcard', $bankcard);
//            $sql = 'DELETE FROM fnn_bankcard WHERE bankcard_id=? LIMIT 1';
//            if ($this->_db->query($sql, array($bankcard_id))) {
//                return 1;
//            } else {
//                return 0;
//            }
//        } else {
//            return -1;
//        }
//    }
//
//    public function updateAmount($bankcard_id, $amount)
//    {
//        $sql = 'UPDATE fnn_bankcard SET current_amount=current_amount+?';
//        $sql .= ',last_time=? WHERE bankcard_id=? LIMIT 1';
//        return $this->_db->query($sql, array($amount, date('Y-m-d H:i:s'), $bankcard_id));
//    }
//
//    public function getList($argument)
//    {
//        global $g_page_size;
//        if (!isset($argument['page_size'])) {
//            $argument['page_size'] = $g_page_size['general'];
//        }
//
//        $condition = array();
//        $parameter = array();
//        if (isset($argument['name']) && '' != $argument['name']) {
//            $condition[] = ' AND name=?';
//            $parameter[] = $argument['name'];
//        }
//        /*if (isset($argument['level']) && -1 != $argument['level']) {
//            $condition[] = ' AND level=?';
//            $parameter[] = $argument['level'];
//        }*/
//        if (isset($argument['status']) && -1 != $argument['status']) {
//            $condition[] = ' AND status=?';
//            $parameter[] = $argument['status'];
//        }
//
//        $return = array();
//
//        $sql = 'SELECT COUNT(*) AS count FROM fnn_bankcard WHERE 1';
//        $sql .= implode('', $condition);
//        $result = $this->_db->query($sql, $parameter);
//        $return['total_record'] = $result[0]['count'];
//        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
//        if ($argument['page'] > $return['total_page']) {
//            $argument['page'] = $return['total_page'];
//        }
//        if ($argument['page'] < 1) {
//            $argument['page'] = 1;
//        }
//        $return['page'] = $argument['page'];
//
//        $sql = 'SELECT * FROM fnn_bankcard WHERE 1';
//        $sql .= implode('', $condition);
//        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
//        $result = $this->_db->query($sql, $parameter);
//        $return['list'] = $result;
//        $return['argument'] = $argument;
//        return $return;
//    }
//
//    public function map()
//    {
//        $sql = 'SELECT bankcard_id,name,bank_name FROM fnn_bankcard';
//        $result = $this->_db->query($sql);
//        $map = array();
//        if ($result) {
//            foreach ($result as $row) {
//                $map[$row['bankcard_id']] = $row['name'] . '[' . $row['bank_name'] . ']';
//            }
//        }
//        return $map;
//    }
}
