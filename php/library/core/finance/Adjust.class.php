<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 额度调整类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Adjust extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function add($uid, $argument, $super = false)
    {
        global $g_finance_subtype;

        $member_handle = new Member();
        $member = $member_handle->getByAccount($argument['account']);
        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        if (!$member) {
            Log::record('[Adjust.Add][line:39]新增额度调整失败 找不到会员帐号:' . $argument['account'], 'warning', 'adjust/');
            return -1;
        }
        if (!$argument['type']) {
            Log::record('[Adjust.Add][line:43]新增额度调整失败 argument type 为空', 'warning', 'adjust/');
            return -2;
        }
        if (!is_numeric($argument['amount']) || !$argument['amount']) {
            Log::record('[Adjust.Add][line:47]新增额度调整失败 amount为空亦或是非数值 : ' . $argument['amount'], 'warning', 'adjust/');
            return -3;
        }
        if (!isset($argument['parent_type'])) {
            $argument['parent_type'] = 0;
        }
        $manager_handle = new Manager();
        $manager = $manager_handle->getByUid($uid);
        unset($manager_handle);
        if (!$manager && !$super) {
            Log::record('[Adjust.Add][line:57]新增额度调整失败 manager找不到且 super为true', 'warning', 'adjust/');
            return -4;
        }
        if (!$uid) {
            $manager = array('account' => 'system');
        }

        $amount_db = money_to_db($argument['amount']);
        $award_db = money_to_db($argument['award']);
        $check_amount_db = money_to_db($argument['check_amount']);
        $time = date('Y-m-d H:i:s');

        $flag = 1;
        // $this->_db->beginTransaction();

        $member_config_handle = new MemberConfig();
        $member_config_data = $member_config_handle->getByMemberId($member['member_id']);
        $ProcessUpdateCheckBetAmount = false;
        // 1. updateCheckBetAmount
        if ($check_amount_db > 0) {
            if (13 == $argument['type']) {
                $last_deposit_time = $member_handle->getLastDepositTime($member['member_id']);
                $params = array(
                    'member_id' => $member['member_id'],
                    'date_start' => $last_deposit_time,
                    'date_stop' => date('Y-m-d H:i:s'),
                );
                $member_valid_data = $member_handle->memberBetReportByMember($params);
                $valid_bet_total = $member_valid_data['valid_bet_total'] > 0 ? $member_valid_data['valid_bet_total'] : 0;
                $valid_bet_total = money_to_db($valid_bet_total);
                $win_total = money_to_db($member_valid_data['win_total']);

                //$member_config_data = $member_config_handle->getByMemberId($member['member_id']);
                $member_config_data['check_bet_amount'] = $member_config_data['check_bet_amount'] > 0 ? $member_config_data['check_bet_amount'] : 0;
                $last_need_amount = 0;
                if ($member_config_data['check_bet_amount'] + $win_total > 0) {
                    $last_need_amount = $member_config_data['check_bet_amount'] - $valid_bet_total;
                }
                $last_need_amount = $last_need_amount > 0 ? $last_need_amount : 0;
                $check_amount_db += $last_need_amount;
                $check_amount_db = ceil($check_amount_db);
                if ($member_config_data['check_bet_amount'] == $check_amount_db) {
                    $check_amount_db += 1;
                }

                Log::record('[Adjust.Add][line:102]新增额度调整 account:' . $member['account'] . '|check_bet_amount:' . $check_amount_db . '|member_config_data[check_bet_amount]: ' . $member_config_data['check_bet_amount'] . '|last_need_amount:' . $last_need_amount . '|member_valid_data:' . print_r($member_valid_data, true), 'info', 'adjust/');
                $step1_add = 0;
            } else {
                $check_amount_db = ceil($check_amount_db);
                // 这里的判断是因为有NULL值
                if ($member_config_data['check_bet_amount']) {
                    $step1_add = 1;
                } else {
                    $step1_add = 0;
                }
            }
        }

        $finance_handle = new Finance();
        $operate_log_handle = new OperateLog();
        // 4. add fnn_record
        $argument_finance4 = array(
            $member['member_id'],
            $argument['parent_type'],
            $argument['type'],
            $adjust_id,
            $amount_db,
            $time,
            $argument['note'],
            $member['amount'],
            null,
            null,
            ($uid) ? $manager['manager_id'] : 0,
        );

        // 5. add fnn_record 人工彩金
        $argument_finance5 = array(
            $member['member_id'],
            $argument['parent_type'],
            15,
            $adjust_id,
            $award_db,
            $time,
            $argument['note'],
            $member['amount'] + $amount_db,
            null,
            null,
            ($uid) ? $manager['manager_id'] : 0,
        );

        // 6. member updateDeposit
        if (13 == $argument['type'] && $flag) {
            $flag = $member_handle->updateDeposit($member['member_id'], $amount_db);

            if (!$flag) {
                Log::record('[Adjust.Add][line:152]更新会员存款失败: member_id ' . $member['member_id'] . '|amount_db:' . $amount_db, 'error', 'adjust/');
            }
        }
        // 7. add OperateLog
        $parametersOperate = array(
            'manager_account' => $manager['account'],
            'account' => $member['account'],
            'account_type' => 1,
            'module' => 'finance',
            'argument' => $argument,
            'detail' => $g_finance_subtype[$argument['type']] . '|金额' . $argument['amount'] . '|奖励' . $argument['award'] . '|打码量' . $argument['check_amount'],
        );

        // start db action
        $this->_db->beginTransaction();
        // 1.
        if ($check_amount_db > 0) {
            $flag = $member_config_handle->updateCheckBetAmount($member['member_id'], $check_amount_db, $step1_add);

            if (!$flag) {
                Log::record('[Adjust.Add][line:172]更新会员打码量失败: member_id ' . $member['member_id'] . '|check_amount_db:' . $check_amount_db . ' |step1_add:' . $step1_add, 'error', 'adjust/');
            }
        }

        // 2. insert adjust
        if ($flag) {
            $sql = 'INSERT INTO fnn_adjust SET member_id=?,account=?,amount=?,award=?';
            $sql .= ',check_amount=?,type=?,manager=?,submit_time=?,note=?,description=?';
            $parameter = array($member['member_id'], $member['account'], $amount_db,
                $award_db, $check_amount_db, $argument['type'], $manager['account'], $time,
                $argument['note'],$argument['description']);
            $adjust_id = $this->_db->query($sql, $parameter);
            if ($adjust_id) {
                $flag = true;
            } else {
                $flag = false;
            }

            if (!$flag) {
                Log::record('[Adjust.Add][line:190]新增财务纪录失败: parameter : ' . print_r($parameter, true), 'error', 'adjust/');
            }
        }

        // 3. member updateAmount
        if ($flag) {
            $flag = $member_handle->updateAmount($member['member_id'], $amount_db);

            if (!$flag) {
                Log::record('[Adjust.Add][line:198]更新会员金额失败: member_id : ' . $member['member_id'] . ' |amount_db: ' . $amount_db, 'error', 'adjust/');
            }
        }

        // 4.
        if ($flag) {
            $flag = $finance_handle->add($argument_finance4);

            if (!$flag) {
                Log::record('[Adjust.Add][line:207]新增财务纪录失败: argument : ' . print_r($argument_finance4, true), 'error', 'adjust/');
            }
        }

        // 5.
        if ($flag && $award_db > 0) {
            $flag = $member_handle->updateAmount($member['member_id'], $award_db);
            if ($flag) {
                $flag = $finance_handle->add($argument_finance5);

                if (!$flag) {
                    Log::record('[Adjust.Add][line:218]新增财务纪录失败(人工彩金): argument : ' . print_r($argument_finance5, true), 'error', 'adjust/');
                }
            } else {
                Log::record('[Adjust.Add][line:221]更新会员金额失败: member_id : ' . $member['member_id'] . ' |award_db: ' . $award_db, 'error', 'adjust/');
            }
        }

        // 6.
        if (13 == $argument['type'] && $flag) {
            $flag = $member_handle->updateDeposit($member['member_id'], $amount_db);

            if (!$flag) {
                Log::record('[Adjust.Add][line:229]更新会员金额失败: member_id : ' . $member['member_id'] . ' |amount_db: ' . $amount_db, 'error', 'adjust/');
            }
        }

        // 7.
        if ($flag) {
            if (isset($manager['manager_id'])) {
                $operate_log_handle->add($parametersOperate);
            }
            $this->_db->commit();
            unset($member_config_handle);
            unset($member_handle);
            unset($finance_handle);
            unset($operate_log_handle);
            Log::record('[Adjust.Add][line:242] commit adjust | member account:' . $member['account'], 'info', 'adjust/');
            //echo 1234567;
            return 1;
        } else {
            Log::record('[Adjust.Add][line:245] adjust rollback | member account:' . $member['account'], 'error', 'adjust/');
            $this->_db->rollBack();
            unset($member_config_handle);
            unset($member_handle);
            unset($finance_handle);
            unset($operate_log_handle);
            return -5;
        }
    }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
        if ($date_start) {
            $day_start = $date_start . ' 00:00:00';
            $date_start = strtotime($day_start) ? $day_start : $date_start;
        }
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND submit_time>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND submit_time<=?';
            $parameter[] = $date_stop;
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            if (isset($argument['fuzzy']) && $argument['fuzzy']) {
                $condition[] = ' AND account Like ?';
                $parameter[] = '%' . $argument['account'] . '%';
            } else {
                $condition[] = ' AND account=?';
                $parameter[] = $argument['account'];
            }
        }
        if (isset($argument['manager']) && '' != $argument['manager']) {
            $condition[] = ' AND manager=?';
            $parameter[] = $argument['manager'];
        }
        if (isset($argument['type']) && '' != $argument['type'] && !is_array($argument['type'])) {
            $condition[] = ' AND type=?';
            $parameter[] = $argument['type'];
        }
        if (isset($argument['type']) && '' != $argument['type'] && is_array($argument['type'])) {
            $condition[] = ' AND type IN ( ';
            foreach ($argument['type'] as $type) {
                $condition[] = ' ? ,';
                $parameter[] = $type;
            }
            $lastConditionIndex = count($condition) - 1;
            $condition[$lastConditionIndex] = substr($condition[$lastConditionIndex], 0, strlen($condition[$lastConditionIndex]) - 1);
            $condition[] = ')';
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM fnn_adjust WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM fnn_adjust WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY adjust_id DESC ';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }
}
