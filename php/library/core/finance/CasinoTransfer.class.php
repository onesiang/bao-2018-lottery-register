<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 真人娱乐额度转移记录类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class CasinoTransfer extends Wwin {

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    public function add($argument) {
        $sql = 'INSERT INTO fnn_casino_transfer SET order_id=?,member_id=?';
        $sql .= ',account=?,module=?,method=?,amount=?,submit_time=?';
        return $this->_db->query($sql, $argument);
    }
    
    public function addWithdrawalFail($argument) {
        $sql = 'INSERT INTO fnn_casino_transfer SET order_id=?,member_id=?';
        $sql .= ',account=?,module=?,method=?,amount=?,submit_time=?,qos=1';
        return $this->_db->query($sql, $argument);
    }
    
    public function updateDepositFailByOrderId($order_id, $member_id) {
        $sql = 'UPDATE fnn_casino_transfer SET qos=1 WHERE order_id=? AND member_id=? AND qos=0';
        $result = $this->_db->query($sql, array($order_id, $member_id));
        return $result;
    }
    
    public function qosFail($transfer_id) {
        $sql = 'UPDATE fnn_casino_transfer SET qos=3, qos_time=? WHERE transfer_id=?';
        $result = $this->_db->query($sql, array(date('Y-m-d H:i:s'), $transfer_id));
        return $result;
    }
    
    public function qosStop($transfer_id) {
        $sql = 'UPDATE fnn_casino_transfer SET qos=4 WHERE transfer_id=?';
        $result = $this->_db->query($sql, array($transfer_id));
        return $result;
    }
    
    public function qosSuccess($transfer_id) {
        $sql = 'UPDATE fnn_casino_transfer SET qos=2, qos_time=? WHERE transfer_id=? AND qos=1';
        $result = $this->_db->query($sql, array(date('Y-m-d H:i:s'), $transfer_id));
        return $result;
    }

    public function checkDisconnectTransfer() {
        global $g_casino_module;
        $sql = 'SELECT a.*, b.category FROM fnn_casino_transfer AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE a.qos=1';
        $result = $this->_db->query($sql, array());
        if($result) {
            $config_handle = new Config();
            $config = $config_handle->getByName(array('QOS_TIME'));
            $qos_time_gap = (isset($config['QOS_TIME']) && $config['QOS_TIME']) ? $config['QOS_TIME'] : 1800;
            unset($config_handle);
            foreach($result as $row) {
                $stop_time = time() - strtotime($row['submit_time']);
                if($stop_time > $qos_time_gap) {
                    // 超过$qos_time_gap就不再查询了
                    $this->qosStop($row['order_id']);
                }
                else {
                    $flag = 0;
                    $data = -2;
                    
                    if(7 == $row['module']) {
                        // MG
                        /*$transfer_category = '';
                        if(1 == $row['method']) {
                            $transfer_category = 'deposit';
                        }
                        else if(2 == $row['method']) {
                            $transfer_category = 'withdrawal';
                        }
                        $mg_handle = new Mg();
                        $data      = $mg_handle->qos($row['account'], $row['submit_time'], $transfer_category, $row['order_id'], money_from_db($row['amount']));
                        unset($mg_handle);*/
                    }

                    $this->_db->beginTransaction();
                    if(1 == $row['method']) {
                        // 系统->casino
                        if(1 == $data) {
                             $flag = $this->qosSuccess($row['transfer_id']);
                        }
                        else if(-1 == $data) {
                            // 还钱给系统
                            $flag = $this->qosFail($row['transfer_id']);
                            if($flag) {
                                $member_handle = new Member();
                                $flag = $member_handle->updateAmount($row['member_id'], $row['amount']);
                                unset($member_handle);
                                if ($flag) {
                                    $argument = array(
                                        $row['member_id'],
                                        0,
                                        19,
                                        $row['transfer_id'],
                                        $row['amount'],
                                        date('Y-m-d H:i:s'),
                                        '系统->' . $g_casino_module[$row['module']]
                                    );
                                    $finance_handle = new Finance();
                                    $flag = $finance_handle->add($argument);
                                    unset($finance_handle);
                                }
                            }
                        }
                        else if(4 == $data) {
                            // 结束轮询 可尝试人工确认是否掉单
                            $flag = $this->qosStop($row['order_id']);
                        }
                    }
                    else if(2 == $row['method']) {
                        // casino->系统 
                        if(1 == $data) {
                            $flag = $this->qosSuccess($row['transfer_id']);
                            if($flag) {
                                $member_handle = new Member();
                                $flag = $member_handle->updateAmount($row['member_id'], $row['amount']);
                                unset($member_handle);
                                if ($flag) {
                                    $argument = array(
                                        $row['member_id'],
                                        0,
                                        19,
                                        $row['transfer_id'],
                                        $row['amount'],
                                        date('Y-m-d H:i:s'),
                                        $g_casino_module[$row['module']]. '->系统 '
                                    );
                                    $finance_handle = new Finance();
                                    $flag = $finance_handle->add($argument);
                                    unset($finance_handle);
                                }
                            }
                        }
                        else if(-1 == $data) {
                            $flag = $this->qosFail($row['transfer_id']);
                        }
                        else if(4 == $data) {
                            // 结束轮询 可尝试人工确认是否掉单
                            $flag = $this->qosStop($row['order_id']);
                        }
                    }
    
                    if($flag) {
                        $this->_db->commit();
                    }
                    else {
                        $this->_db->rollBack();
                    }
                }

            }
        }
    }
    
    public function getList($argument) {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }
        
        $time_zone  = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop  = isset($argument['date_stop']) ? $argument['date_stop'] : '';
        if($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if(1 == $time_zone) {
            // 北京时间
            if($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }
        
        $condition = array();
        $parameter = array();
        if ($date_start) {
            $condition[] = ' AND submit_time>=?';
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            $condition[] = ' AND submit_time<=?';
            $parameter[] = $date_stop;
        }
        if (isset($argument['module']) && '' !== $argument['module']) {
            $condition[] = ' AND module=?';
            $parameter[] = $argument['module'];
        }
        if (isset($argument['method']) && '' !== $argument['method']) {
            $condition[] = ' AND method=?';
            $parameter[] = $argument['method'];
        }
        if (isset($argument['account']) && '' !== $argument['account']) {
            $condition[] = ' AND account=?';
            $parameter[] = $argument['account'];
        }
        $return = array();
        
        $sql = 'SELECT COUNT(transfer_id) AS count FROM fnn_casino_transfer WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];
        
        $sql = array();
        $sql[] = 'SELECT * FROM fnn_casino_transfer WHERE 1';
        $sql[] = implode('', $condition);
        $sql[] = ' ORDER BY transfer_id DESC';
        $sql[] = ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query(implode('', $sql), $parameter);

        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function updateAgentTransferAmount($transfer_amount, $casino) {
        $sql = 'SELECT * FROM mdl_agent_config WHERE module_name=? LIMIT 1';
        $result = $this->_db->query($sql, array($casino));
        $flag   = 0;
        if($result) {
            $agent_amount = $result[0]['amount'];
            $id           = $result[0]['id'];
            if($agent_amount + $transfer_amount > 0) {
                $sql = 'UPDATE mdl_agent_config SET amount=amount+? WHERE id=? LIMIT 1';
                $result = $this->_db->query($sql, array($transfer_amount, $id));
                if($result) {
                    $flag = 1;
                }
            }
        }
        return $flag;
    }

    public function getCasinoTransferOne($casino) {
        $sql = 'SELECT * FROM mdl_agent_config WHERE module_name=? LIMIT 1';
        $result = $this->_db->query($sql, array($casino));
        if($result) {
            $result =  $result[0];
        }
        return $result;
    }
    
    public function getCasinoTransferList() {
        $sql = 'SELECT * FROM mdl_agent_config';
        $result = $this->_db->query($sql, array());
        $data   = array();
        if($result) {
            foreach($result as $row) {
                $data[$row['module_name']] = $row;
            }
        }
        return $data;
    }

}