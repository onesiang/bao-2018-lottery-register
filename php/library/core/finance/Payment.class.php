<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 第三方支付类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Payment extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function getBankCard($key, $level, $whichType = '')
    {
        $config_handle = new Config();
        $sql = "SELECT {$key} FROM fnn_bankcard WHERE 1";
        $condition = [];
        $config = $config_handle->getByName(array('DEPOSIT_LEVEL'));
        if ($config['DEPOSIT_LEVEL']) {
            $sql .= " AND level REGEXP ?";
            $condition[] = $level;
        }

        if ($whichType) {
            switch ($whichType) {
                case 'weixin':
                    $sql .= " AND bank_name LIKE '%微信%' ";
                    break;
                case 'alipay':
                    $sql .= " AND bank_name LIKE '%支付宝%' ";
                    break;
                default:
                    $sql .= " AND bank_name NOT LIKE '%支付宝%' AND  bank_name NOT LIKE '%微信%' ";
                    break;
            }
        }
        $sql .= " AND status = 1  ORDER BY last_time DESC";
        $result = $this->_db->query($sql, $condition);
        return (count($result) > 0)? $result:[];
    }

    public function countGateway($level)
    {
        $detect_handle = new Mobile_Detect;
        $is_mobile = $detect_handle->isMobile();

        $device = ($is_mobile)? 'b.h5 = 1' : 'b.web = 1';

        $sql = "SELECT
            sum(b.name like '%微信%') as wx,
            sum(b.name like '%支付宝%') as ali,
            sum(b.name like '%财付通%') as cft,
            sum(b.name like '%银联%') as yl,
            sum(b.name like '%京东%') as jd,
            sum(b.name like '%QQ%') as qq,
            sum(b.type = 'b2c') as b2c,
            sum(b.name like '%美团%') as meituan,
            sum(b.name like '%百度%')  as baidu
            FROM fnn_payment as a inner JOIN fnn_gateway as b
            on payment_id = fnn_payment_id and a.type = fnn_payment_type
            WHERE a.level REGEXP ? AND a.status= 1  AND b.status = 1 AND {$device}
            ORDER BY payment_id ASC";
        return $this->_db->query($sql, array($level))[0];
    }

    public function getBankCardById($bankcard_id = 0)
    {
        $sql = 'SELECT * FROM fnn_bankcard WHERE bankcard_id= ? LIMIT 1';
        $result = $this->_db->query($sql, array($bankcard_id));
        return (count($result) > 0)? $result[0] : [];
    }

    public function getThirdPartyGateway($level, $like, $device)
    {
        $sql = "SELECT concat(a.name,'-',b.name) as name,b.id,
                b.note as description, b.limit_amount, b.lower_limit_amount
                FROM fnn_payment as a INNER JOIN fnn_gateway as b
                ON a.payment_id = b.fnn_payment_id
                WHERE a.level REGEXP ?
                AND a.status = 1
                AND b.status = 1
                AND b.{$device} = 1
                AND {$like}
                ORDER BY b.sort";
        $result = $this->_db->query($sql, array($level));

        return (count($result) > 0)? $result : [];
    }

    public function checkThirdPaymentById($id)
    {
        $sql = 'SELECT a.domain,a.type as payment_type, b.* FROM fnn_payment AS a INNER JOIN fnn_gateway AS b
        ON a.payment_id = b.fnn_payment_id WHERE
        b.id = ? AND a.status = 1 AND b.status = 1';
        $result = $this->_db->query($sql, [$id]);

        return ($result)? $result[0] : [];
    }
//    public function add($argument)
//    {
//        $sql = array();
//        $sql[] = 'INSERT INTO fnn_payment (type,name,company_id,terminal_id,serial,pri_key,pub_key,version,domain';
//        $sql[] = ',level,limit_amount,status,submit_time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';
//        if ($this->_db->query(implode('', $sql), $argument)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function getIdbyCondition($argument)
//    {
//        $sql = 'SELECT payment_id FROM `fnn_payment` WHERE type=? AND name=? AND company_id=? AND terminal_id=? AND serial=? AND pri_key=? AND pub_key=? AND version=? AND domain=? AND level=? AND limit_amount=? AND status=? AND submit_time=?';
//
//        $row = $this->_db->query($sql, $argument);
//
//        $row = $row[0];
//
//        $result = $row['payment_id'];
//
//        return $result;
//    }
//
//    public function getClipboardList($argument)
//    {
//        $sql = 'SELECT * FROM fnn_thirdPartyGateway_Clipboard WHERE payment_id = ? ';
//        $sql .= implode('', $condition);
//
//        $result = $this->_db->query($sql, array($argument));
//
//        $return['list'] = $result;
//
//        $return['argument'] = $argument;
//
//        return $return;
//    }
//
//    public function addtype($argument, $need_id, $type)
//    {
//        $value = array(
//            $need_id,
//            $type,
//            $argument['name'],
//            $argument['type'],
//            $argument['category'],
//        );
//        $sql = array();
//
//        $sql[] = 'INSERT INTO fnn_gateway (fnn_payment_id,fnn_payment_type,name,type, ';
//
//        $sql[] = ' category) VALUES (?,?,?,?,?)';
//
//        if ($this->_db->query(implode('', $sql), $value)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function check($payment_id)
//    {
//        $sql = 'SELECT * FROM fnn_payment WHERE payment_id=? LIMIT 1';
//        $result = $this->_db->query($sql, array($payment_id));
//        if ($result && 1 == $result[0]['status']) {
//            return $result[0];
//        } else {
//            return false;
//        }
//    }
//
    public function check_gateway($payment_id, $type)
    {
        $sql = 'SELECT * FROM fnn_gateway WHERE fnn_payment_id=? AND type = ? LIMIT 1';
        $result = $this->_db->query($sql, array($payment_id, $type));
        if ($result && 1 == $result[0]['status']) {
            return $result[0];
        } else {
            return false;
        }
    }
//
//    public function getId()
//    {
//        $sql = 'SELECT payment_id FROM `fnn_payment` ORDER BY payment_id DESC LIMIT 1';
//
//        $row = $this->_db->query($sql);
//
//        $row = $row[0];
//
//        $result = $row['payment_id'];
//
//        return $result;
//    }
//
    public function get($payment_id = 0)
    {
        global $member;
        if (0 == $payment_id) {
            $config_handle = new Config();
            // 存款启用会员层级，是否启用会员层级，启用后会员只能看到本层级支付资料。
            $config = $config_handle->getByName(array('DEPOSIT_LEVEL'));
            unset($config_handle);
            if ($config['DEPOSIT_LEVEL']) {
                $sql = 'SELECT * FROM fnn_payment WHERE status=1 ORDER BY payment_id ASC';
                $all = $this->_db->query($sql);
                $result = array();
                if ($all) {
                    foreach ($all as $row) {
                        $one = explode(',', $row['level']);
                        if (in_array($member['level'], $one)) {
                            $result[] = $row;
                        }
                    }
                }
            } else {
                $sql = 'SELECT * FROM fnn_payment WHERE status=1 ORDER BY payment_id ASC LIMIT 1';
                $result = $this->_db->query($sql);
            }
        } else {
            $sql = 'SELECT * FROM fnn_payment WHERE payment_id=? LIMIT 1';
            $result = $this->_db->query($sql, array($payment_id));
        }
        if ($result) {
            $result = $result[0];
        }

        return $result;
    }

    public function getGateway($payment_id)
    {
        global $member;

        $sql = 'SELECT * FROM fnn_gateway WHERE id=? LIMIT 1';
        $result = $this->_db->query($sql, array($payment_id));

        if ($result) {
            $result = $result[0];
        }

        return $result;
    }
//
//    public function getByDomain($domain)
//    {
//        $sql = 'SELECT * FROM fnn_payment WHERE domain=? LIMIT 1';
//        $result = $this->_db->query($sql, array($domain));
//        if ($result) {
//            $result = $result[0];
//        }
//        return $result;
//    }
//
//    public function edit($argument)
//    {
//        $sql = array();
//        $sql[] = 'UPDATE fnn_payment SET type=?,name=?,company_id=?,terminal_id=?,serial=?,pri_key=?,pub_key=?';
//        $sql[] = ',version=?,domain=?,level=?,limit_amount=?,status=? WHERE payment_id=?';
//        if ($this->_db->query(implode('', $sql), $argument)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function list_edit($argument)
//    {
//        $sql = 'UPDATE fnn_gateway SET web=?,h5=?,lower_limit_amount=?,limit_amount=?,note=?,status=? WHERE id=?';
//
//        $parameter = array();
//        $parameter[0] = (int)$argument[0];
//        $parameter[1] = (int)$argument[1];
//        $parameter[2] = (int)$argument[2];
//        $parameter[3] = (int)$argument[3];
//        $parameter[4] = $argument[4];
//        $parameter[5] = $argument[5];
//        $parameter[6] = $argument[6];
//
//        if ($this->_db->query($sql, $parameter)) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function delete($payment_id)
//    {
//        $payment = $this->get($payment_id);
//
//        if ($payment) {
//            $sql = 'DELETE a.*, b.* FROM fnn_payment a INNER JOIN fnn_gateway b ON a.payment_id = b.fnn_payment_id WHERE a.payment_id=?;';
//
//            if ($this->_db->query($sql, array($payment_id))) {
//                return 1;
//            } else {
//                return 0;
//            }
//        } else {
//            return -1;
//        }
//    }
//
//    public function deleteType($payment_id)
//    {
//        $sql = 'DELETE FROM fnn_gateway WHERE fnn_payment_id=? AND fixed = 0 ';
//        if ($this->_db->query($sql, array($payment_id))) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
//
//    public function getMember_getPayment($level)
//    {
//        $sql = 'SELECT payment_id , type  , domain , name FROM fnn_payment WHERE level REGEXP ? AND status=1 ORDER BY payment_id ASC';
//        $result = $this->_db->query($sql, [$level]);
//        return $result;
//    }
//
//    public function getMember_getPaymentTab($fnn_payment_id, $fnn_payment_type, $checkWeb)
//    {
//        $condition = ($checkWeb == 1) ? 'AND web = ? ' : 'AND h5 = ? ';
//
//        $sql = 'SELECT * FROM  fnn_gateway WHERE status = 1 ';
//        $sql .= ' AND fnn_payment_id = ? AND fnn_payment_type = ? ';
//        $sql .= $condition;
//        $list = $this->_db->query($sql, array($fnn_payment_id, $fnn_payment_type, 1));
//        return $list;
//    }
//
//    public function updateAmount($payment_id, $amount)
//    {
//        $sql = 'UPDATE fnn_payment SET current_amount=current_amount+?';
//        $sql .= ',last_time=? WHERE payment_id=? LIMIT 1';
//        return $this->_db->query($sql, array($amount, date('Y-m-d H:i:s'), $payment_id));
//    }
//
//    public function getByLevel($level)
//    {
//        $sql = 'SELECT * FROM fnn_payment WHERE level=? ORDER BY last_time LIMIT 1';
//        $result = $this->_db->query($sql, array($level));
//        if ($result) {
//            $result = $result[0];
//        }
//        return $result;
//    }
//
//    public function getPayList($argument)
//    {
//        global $g_page_size;
//
//        if (!isset($argument['page_size'])) {
//            $argument['page_size'] = $g_page_size['general'];
//        }
//
//
//        $condition = array();
//
//        $parameter = array();
//
//        if (isset($argument['payment_id']) && -1 != $argument['payment_id']) {
//            $condition[] = ' AND payment_id=?';
//
//            $parameter[] = $argument['payment_id'];
//        }
//
//        /*if (isset($argument['level']) && -1 !== $argument['level']) {
//
//            $condition[] = ' AND level=?';
//
//            $parameter[] = $argument['level'];
//
//        }*/
//
//        if (isset($argument['status']) && -1 != $argument['status']) {
//            $condition[] = ' AND status=?';
//
//            $parameter[] = $argument['status'];
//        }
//
//
//        $return = array();
//
//
//        $sql = 'SELECT COUNT(*) AS count FROM fnn_gateway WHERE 1';
//
//        $sql .= implode('', $condition);
//
//        $result = $this->_db->query($sql, $parameter);
//
//        $return['total_record'] = $result[0]['count'];
//
//        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
//
//        if ($argument['page'] > $return['total_page']) {
//            $argument['page'] = $return['total_page'];
//        }
//
//        if ($argument['page'] < 1) {
//            $argument['page'] = 1;
//        }
//
//        $return['page'] = $argument['page'];
//
//
//        $sql = 'SELECT * FROM fnn_gateway WHERE 1';
//
//        $sql .= implode('', $condition);
//
//        $sql .= ' ORDER BY sort LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
//
//        $result = $this->_db->query($sql, $parameter);
//
//        $return['list'] = $result;
//
//        $return['argument'] = $argument;
//
//        return $return;
//    }
//
//    public function getList($argument)
//    {
//        global $g_page_size;
//        if (!isset($argument['page_size'])) {
//            $argument['page_size'] = $g_page_size['general'];
//        }
//
//        $condition = array();
//        $parameter = array();
//        if (isset($argument['name']) && $argument['name']) {
//            $condition[] = ' AND name=?';
//            $parameter[] = $argument['name'];
//        }
//        /*if (isset($argument['level']) && -1 !== $argument['level']) {
//            $condition[] = ' AND level=?';
//            $parameter[] = $argument['level'];
//        }*/
//        if (isset($argument['status']) && -1 !== $argument['status']) {
//            $condition[] = ' AND status=?';
//            $parameter[] = $argument['status'];
//        }
//
//        $return = array();
//
//        $sql = 'SELECT COUNT(*) AS count FROM fnn_payment WHERE 1';
//        $sql .= implode('', $condition);
//        $result = $this->_db->query($sql, $parameter);
//        $return['total_record'] = $result[0]['count'];
//        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
//        if ($argument['page'] > $return['total_page']) {
//            $argument['page'] = $return['total_page'];
//        }
//        if ($argument['page'] < 1) {
//            $argument['page'] = 1;
//        }
//        $return['page'] = $argument['page'];
//
//        $sql = 'SELECT * FROM fnn_payment WHERE 1';
//        $sql .= implode('', $condition);
//        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
//        $result = $this->_db->query($sql, $parameter);
//        $return['list'] = $result;
//        $return['argument'] = $argument;
//        return $return;
//    }
//
//    public function getAllList($argument)
//    {
//
//        $sql = 'SELECT * FROM fnn_gateway WHERE fnn_payment_id = ? ';
//        $result = $this->_db->query($sql, array($argument));
//
//        $return['list'] = $result;
//        $return['argument'] = $argument;
//
//        return $return;
//    }
//
//    public function map()
//    {
//        global $g_payment_type;
//        $sql = 'SELECT payment_id,name,type FROM fnn_payment';
//        $result = $this->_db->query($sql);
//        $map = array();
//        if ($result) {
//            foreach ($result as $row) {
//                $map[$row['payment_id']] = $row['name'] . '[' . $g_payment_type[$row['type']] . ']';
//            }
//        }
//        return $map;
//    }
}
