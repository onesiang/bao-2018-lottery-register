<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 取款类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Withdrawal extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * [add 新增 未確認 status =1]
     * @param [type] $argument [description]
     */
    public function add($argument)
    {
        $this->_db->beginTransaction();

        $argument[2] = money_to_db($argument[2]);
        $argument[3] = money_to_db($argument[3]);

        $sql = 'INSERT INTO fnn_withdrawal SET member_id=?,account=?,amount=?,actual_amount=?';
        $sql .= ',submit_time=?,status=1,order_id=?,bankCard_id=?;';
        $withdrawal_id = $this->_db->query($sql, $argument);
        if ($withdrawal_id) {
            $flag = 1;
        } else {
            $flag = 0;
        }

        if ($flag) {
            $member_handle = new Member();
            $member = $member_handle->getByMemberId($argument[0]);
            $flag = $member_handle->updateAmount($argument[0], 0 - $argument[2]);
            unset($member_handle);
        }

        if ($flag) {
            $argument = array(
                $argument[0],
                0,
                3,
                $withdrawal_id,
                0 - $argument[2],
                date('Y-m-d H:i:s'),
                '',
                $member['amount'],
                $member['amount'] - $argument[2],
            );
            $finance_handle = new Finance();
            $flag = $finance_handle->add($argument);
            unset($finance_handle);
        }

        if ($flag) {
            $this->_db->commit();
            return 1;
        } else {
            $this->_db->rollBack();
            return 0;
        }
    }

    public function getWithdrawalByMemberId($member_id, $field = '*')
    {
        $sql = 'SELECT ' . $field . ' FROM fnn_withdrawal WHERE member_id=? ORDER BY submit_time DESC LIMIT 1';
        $result = $this->_db->query($sql, array($member_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    /**
     * [getSuccessCountToday 抓取今日取款次數]
     *
     * @param integer $member_id 會員編號
     * @return void
     */
    public function getSuccessCountToday($member_id)
    {
        $date_start = date('Y-m-d');
        $date_stop = $date_start . ' 23:59:59';

        $count = 0;
        $sql = 'SELECT count(withdrawal_id) AS count FROM fnn_withdrawal WHERE status=2 AND member_id=? AND deal_time>=? AND deal_time<=?';
        $result = $this->_db->query($sql, array($member_id, $date_start, $date_stop));
        if ($result && isset($result[0])) {
            $count = $result[0]['count'];
        }
        return $count;
    }

    /**
     * [getList 獲取取款清單]
     * @param  [type] $argument [description]
     * @return [array]           [description]
     */
    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        $time_type = isset($argument['time_type']) ? $argument['time_type'] : 0; // 時間排序預選為按照提交時間排序
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }

        $condition = array();
        $parameter = array();
        if ($date_start) {
            if (0 == $time_type) {
                $condition[] = ' AND a.submit_time>=?';
            } else {
                $condition[] = ' AND a.deal_time>=?';
            }
            $parameter[] = $date_start;
        }
        if ($date_stop) {
            if (0 == $time_type) {
                $condition[] = ' AND a.submit_time<=?';
            } else {
                $condition[] = ' AND a.deal_time<=?';
            }
            $parameter[] = $date_stop;
        }
        if (isset($argument['account']) && '' != $argument['account']) {
            $condition[] = ' AND a.account=?';
            $parameter[] = $argument['account'];
        }
        if (isset($argument['member_id']) && $argument['member_id']) {
            $condition[] = ' AND a.member_id=?';
            $parameter[] = $argument['member_id'];
        }
        if (isset($argument['order_id']) && $argument['order_id']) {
            $condition[] = ' AND a.order_id=?';
            $parameter[] = $argument['order_id'];
        }
        if (isset($argument['manager']) && '' != $argument['manager']) {
            $condition[] = ' AND manager=?';
            $parameter[] = $argument['manager'];
        }
        if (isset($argument['status']) && '' != $argument['status']) {
            $condition[] = ' AND a.status=?';
            $parameter[] = $argument['status'];
        }
        if (isset($argument['withdraw_view_min']) && $argument['withdraw_view_min']) {
            $condition[] = ' AND a.amount>=?';
            $parameter[] = $argument['withdraw_view_min'];
        }
        if (isset($argument['withdraw_view_max']) && $argument['withdraw_view_max']) {
            $condition[] = ' AND a.amount<=?';
            $parameter[] = $argument['withdraw_view_max'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM fnn_withdrawal a WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        if (isset($argument['countTotal']) && $argument['countTotal']) {
        } else {
            $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
            if ($argument['page'] > $return['total_page']) {
                $argument['page'] = $return['total_page'];
            }
            if ($argument['page'] < 1) {
                $argument['page'] = 1;
            }
            $return['page'] = $argument['page'];
        }

        $sql = 'SELECT a.*,b.real_name,b.bank_name,b.bank_address,b.bank_number ,c.* ';
        $sql .= ' FROM fnn_withdrawal a,uc_member b ,uc_member_bankcard c WHERE a.account=b.account AND a.bankCard_id = c.id  ';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY withdrawal_id DESC ';
        if (isset($argument['countTotal']) && $argument['countTotal']) {
        } else {
            if ($argument['page_size']) {
                $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
            }
        }

        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }
    public function getLastDepositTime($member_id)
    {
        $sql = "SELECT(
                    SELECT modified_time
                        FROM uc_member_config
                        WHERE member_id = ? )
                AS uc_member_config_modified_time,
                (
                    SELECT confirm_time
                        FROM fnn_remittance
                        WHERE status = 2 AND member_id = ?
                        ORDER BY confirm_time DESC LIMIT 1 )
                AS remittance_confirm_time ,
                (
                    SELECT submit_time
                        FROM fnn_adjust
                        WHERE member_id = ? AND type = 13 AND check_amount != 0
                        ORDER BY submit_time DESC LIMIT 1 )
                AS adjust_submit_time,
                (
                    SELECT confirm_time
                    FROM fnn_deposit
                    WHERE status = 2 AND member_id = ?
                    ORDER BY confirm_time DESC LIMIT 1 )
                AS deposit_confirm_time";

        $result = $this->_db->query($sql, array($member_id,$member_id,$member_id,$member_id));
        $last_depost_time = 0;
        if (count($result) > 0) {
            $result = $result[0];
            $last_depost_time = ($result['deposit_confirm_time'])?strtotime($result['deposit_confirm_time']):0;
            $last_remittance_time = ($result['remittance_confirm_time'])?strtotime($result['remittance_confirm_time']):0;
            $last_adjust_time = ($result['adjust_submit_time'])?strtotime($result['adjust_submit_time']):0;
            $last_uc_member_config_time = ($result['uc_member_config_modified_time'])?strtotime($result['uc_member_config_modified_time']):0;

            if ($last_remittance_time > $last_depost_time) {
                $last_depost_time = $last_remittance_time;
            }
            if ($last_adjust_time > $last_depost_time) {
                $last_depost_time = $last_adjust_time;
            }
            if ($last_uc_member_config_time > $last_depost_time) {
                $last_depost_time = $last_uc_member_config_time;
            }
        }
        $last_depost_time = date('Y-m-d H:i:s', $last_depost_time);
        return $last_depost_time;
    }

    public function showCheckData($memberId, $uid)
    {
        $last_deposit_time = $this->getLastDepositTime($memberId);

        $params = array(
            'member_id' => $memberId,
            'date_start' => $last_deposit_time,
            'date_stop' => date('Y-m-d H:i:s'),
        );
        $member_handle = new Member();
        $member_valid_data = $member_handle->memberBetReportByMember($params);
        unset($member_handle);

        $member_config_handle = new MemberConfig();
        $member_config = $member_config_handle->getByMemberId($memberId);
        unset($member_config_handle);

        $withdrawal_count = $this->getSuccessCountToday($memberId);
        $data = array(
            'total_check_amount' => money_from_db($member_config['check_bet_amount']),
            'last_deposit_time' => $last_deposit_time,
            'valid_amount' => $member_valid_data['valid_bet_total'],
            'lottery_bet' => $member_valid_data['lottery_bet'],
            'lottery_valid' => $member_valid_data['lottery_valid'],
            'withdrawal_count_max' => $member_config['withdrawal_count_max'],
            'withdrawal_count' => $withdrawal_count,
        );
        return $data;
    }

    /**
     * [getList 獲取今日已提款]
     * @param  [type] $argument [description]
     * @return [array]           [description]
     */
    // public function getTodaySum($argument)
    // {
    //     $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
    //     $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
    //     if ($date_stop) {
    //         $day_stop = $date_stop . ' 23:59:59';
    //         $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
    //     }
    //
    //     $condition = array();
    //     $parameter = array();
    //     if ($date_start) {
    //         $condition[] = ' AND a.submit_time>=?';
    //         $parameter[] = $date_start;
    //     }
    //     if ($date_stop) {
    //         $condition[] = ' AND a.submit_time<=?';
    //         $parameter[] = $date_stop;
    //     }
    //
    //     if (isset($argument['status']) && '' != $argument['status']) {
    //         $condition[] = ' AND a.status=?';
    //         $parameter[] = $argument['status'];
    //     }
    //     if (isset($argument['withdraw_view_min']) && $argument['withdraw_view_min']) {
    //         $condition[] = ' AND a.amount>=?';
    //         $parameter[] = $argument['withdraw_view_min'] * 100;
    //     }
    //     if (isset($argument['withdraw_view_max']) && $argument['withdraw_view_max']) {
    //         $condition[] = ' AND a.amount<=?';
    //         $parameter[] = $argument['withdraw_view_max'] * 100;
    //     }
    //
    //     $return = array();
    //
    //     $sql = 'SELECT sum(a.amount) as total_withdraw';
    //     $sql .= ' FROM fnn_withdrawal as a ';
    //     $sql .= ' LEFT JOIN uc_member as b ON b.member_id = a.member_id WHERE b.category = 1  '; //只抓有效用戶
    //     $sql .= implode('', $condition);
    //     $result = $this->_db->query($sql, $parameter);
    //
    //     return (isset($result[0]['total_withdraw']) && $result[0]['total_withdraw']) ? $result[0]['total_withdraw'] : 0;
    // }

    // public function untreated($argument)
    // {
    //     $sql = 'SELECT count(withdrawal_id) AS withdrawal_amount FROM fnn_withdrawal WHERE status=1 AND amount >= ? AND amount <= ?';
    //
    //     $result = $this->_db->query($sql, $argument);
    //     return $result;
    // }

    /**
     * [showCheckData 取得打碼資訊]
     *
     * @param string $account 會員帳號
     * @return void
     */

    // public function showcheck($account)
    // {
    //     $member_handle = new Member();
    //     $member = $member_handle->getByAccount($account);
    //     unset($member_handle);
    //
    //     $member_config_handle = new MemberConfig();
    //     $member_config = $member_config_handle->getByMemberId($member['member_id']);
    //     unset($member_config_handle);
    //
    //     /* 上次取款时间点 */
    //     $last_withdrawal_time = $member['register_time'];
    //     $sql = 'SELECT * FROM fnn_withdrawal WHERE status=2 AND account=? ORDER BY deal_time DESC LIMIT 1';
    //     $result = $this->_db->query($sql, array($account));
    //     if ($result) {
    //         $last_withdrawal_time = $result[0]['deal_time'];
    //     }
    //
    //     $config_handle = new Config();
    //     $config = $config_handle->getByName(array('CHECK_RATE'));
    //     unset($config_handle);
    //
    //     $parameter = array($account, $last_withdrawal_time);
    //
    //     // 在线存款
    //     $sql = 'SELECT SUM(amount) AS amount FROM fnn_deposit WHERE status=2 AND account=? AND confirm_time>? ';
    //     $result_deposit = $this->_db->query($sql, $parameter);
    //     $deposit_amount = ($result_deposit && $result_deposit[0]['amount']) ? $result_deposit[0]['amount'] : 0;
    //     $deposit_amount = money_to_db($deposit_amount);
    //     $deposit_check_amount = $deposit_amount;
    //     // 公司存款
    //     $sql = 'SELECT SUM(amount) AS amount FROM fnn_remittance WHERE status=2 AND account=? AND confirm_time>?';
    //     $result_remittance = $this->_db->query($sql, $parameter);
    //     $remittance_amount = ($result_remittance && $result_remittance[0]['amount']) ? $result_remittance[0]['amount'] : 0;
    //     $remittance_check_amount = $remittance_amount;
    //     // 人工入款/人工彩金
    //     $sql = 'SELECT SUM(amount) AS amount, SUM(check_amount) AS check_amount FROM fnn_adjust WHERE account=? AND type IN(13,15,20) AND submit_time>? AND check_amount !=0';
    //     $result_adjust = $this->_db->query($sql, $parameter);
    //     $adjust_amount = ($result_adjust && $result_adjust[0]['amount']) ? $result_adjust[0]['amount'] : 0;
    //     $adjust_check_amount = ($result_adjust && $result_adjust[0]['check_amount']) ? $result_adjust[0]['check_amount'] : 0;
    //
    //     if ($member_config['check_bet_amount']) {
    //         $total_check_amount = $member_config['check_bet_amount'];
    //         $ease_check_amount = $member_config['check_bet_amount'];
    //     } else {
    //         $total_check_amount = $deposit_check_amount + $remittance_check_amount + $adjust_check_amount;
    //         $ease_check_amount = (($deposit_check_amount + $remittance_check_amount) * (100 - $config['CHECK_RATE']) / 100) + $adjust_check_amount;
    //     }
    //     $total_check_amount = $total_check_amount > 0 ? $total_check_amount : 0;
    //     $ease_check_amount = $ease_check_amount > 0 ? $ease_check_amount : 0;
    //
    //     $end_time = date('Y-m-d H:i:s');
    //     $end_time = date('Y-m-d H:i:s', strtotime($end_time)); // 防止有地方美东和北京时间的问题
    //     $valid_amount = $this->subcheck($account, $last_withdrawal_time, $end_time);
    //     $data = array(
    //         'deposit_amount' => $deposit_amount,
    //         'deposit_check_amount' => $deposit_check_amount,
    //         'remittance_amount' => $remittance_amount,
    //         'remittance_check_amount' => $remittance_check_amount,
    //         'adjust_amount' => $adjust_amount,
    //         'adjust_check_amount' => $adjust_check_amount,
    //         'valid_amount' => $valid_amount,
    //         'last_withdrawal_time' => $last_withdrawal_time,
    //         'total_check_amount' => $total_check_amount,
    //         'ease_check_amount' => $ease_check_amount,
    //         'check_bet_amount' => $member_config['check_bet_amount'],
    //     );
    //     return $data;
    // }

    // private function subcheck($account, $start_time, $stop_time)
    // {
    //     $valid_amount = 0;
    //     $handle = new Member();
    //     $member = $handle->getByAccount($account);
    //     if ($member) {
    //         // 体育
    //         $sql = 'SELECT SUM(valid_amount) AS valid_amount FROM spr_bet WHERE member_id=? AND settle_time>? AND settle_time<?';
    //         $parameter = array($member['member_id'], $start_time, $stop_time);
    //         $result_sport = $this->_db->query($sql, $parameter);
    //         $sport_amount = ($result_sport && $result_sport[0]['valid_amount']) ? $result_sport[0]['valid_amount'] : 0;
    //         // 彩票
    //         $sql = 'SELECT SUM(valid_amount) AS valid_amount FROM ltt_bet WHERE member_id=? AND settle_time>? AND settle_time<?';
    //         $parameter = array(
    //             $member['member_id'],
    //             date('Y-m-d H:i:s', strtotime($start_time)),
    //             date('Y-m-d H:i:s', strtotime($stop_time)),
    //         );
    //         $result_lottery = $this->_db->query($sql, $parameter);
    //         $lottery_amount = ($result_lottery && $result_lottery[0]['valid_amount']) ? $result_lottery[0]['valid_amount'] : 0;
    //         // casino
    //         $sql = 'SELECT SUM(valid_amount) AS valid_amount FROM csn_bet WHERE account=? AND bet_time>? AND bet_time<?';
    //         $parameter = array($account, $start_time, $stop_time);
    //         $result_casino = $this->_db->query($sql, $parameter);
    //         $casino_amount = ($result_casino && $result_casino[0]['valid_amount']) ? $result_casino[0]['valid_amount'] : 0;
    //         $casino_amount = money_to_db($casino_amount); // 因为真人的单位和系统的不一样，所以这里要转一下
    //         // electronic
    //         $sql = 'SELECT SUM(valid_amount) AS valid_amount FROM electronic_bet WHERE account=? AND submit_time>? AND submit_time<?';
    //         $parameter = array($account, $start_time, $stop_time);
    //         $result_electronic = $this->_db->query($sql, $parameter);
    //         $electronic_amount = ($result_electronic && $result_electronic[0]['valid_amount']) ? $result_electronic[0]['valid_amount'] : 0;
    //         $electronic_amount = money_to_db($electronic_amount); // 电子的单位和系统的不一样，所以这里要转一下
    //
    //         $valid_amount = $sport_amount + $lottery_amount + $casino_amount + $electronic_amount;
    //     }
    //     return $valid_amount;
    // }

    /**
     * [getTotalWithdrawalAmount 取款總額]
     * @param  [type] $argument [description]
     * @return [type]           [description]
     */
    // public function getTotalWithdrawalAmount($argument)
    // {
    //     $data = $this->getList($argument);
    //     $total_amount = 0;
    //
    //     if ($data['list']) {
    //         foreach ($data['list'] as $row) {
    //             if (1 == $row['status']) {
    //                 $total_amount += $row['amount'];
    //             }
    //         }
    //     }
    //
    //     return $total_amount;
    // }

    // public function get($withdrawal_id)
    // {
    //     $sql = 'SELECT * FROM fnn_withdrawal WHERE withdrawal_id=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($withdrawal_id));
    //     if ($result) {
    //         $result = $result[0];
    //     }
    //     return $result;
    // }


    /**
     * [confirm 確認取款 status = 2]
     * @param  [type] $withdrawal_id [description]
     * @param  [type] $uid           [description]
     * @param  string $note          [description]
     * @param  string $manager_note  [description]
     * @return [type]                [description]
     */
    // public function confirm($withdrawal_id, $uid, $note = '', $manager_note = '')
    // {
    //     $withdrawal = $this->get($withdrawal_id);
    //     if (!$withdrawal) {
    //         return -1;
    //     }
    //     if (1 != $withdrawal['status'] && 4 != $withdrawal['status']) {
    //         return -2;
    //     }
    //     $manager_handle = new Manager();
    //     $manager = $manager_handle->getByUid($uid);
    //     unset($manager_handle);
    //     if (!$manager) {
    //         return -3;
    //     }
    //
    //     $time = date('Y-m-d H:i:s');
    //
    //     $this->_db->beginTransaction();
    //     $sql = 'UPDATE fnn_withdrawal SET manager=?,deal_time=?,status=2,deal_note=?,manager_note=?';
    //     $sql .= ' WHERE withdrawal_id=? AND status!=2 AND status!=3 LIMIT 1';
    //     $flag = $this->_db->query($sql, array($manager['account'], $time, $note, $manager_note, $withdrawal_id));
    //
    //     if ($flag) {
    //         $member_handle = new Member();
    //         $member = $member_handle->getByAccount($withdrawal['account']);
    //         $flag = $member_handle->updateWithdrawal($member['member_id'], $withdrawal['amount']);
    //         unset($member_handle);
    //     }
    //     // if ($flag) {
    //     //     $member_config_handle = new MemberConfig();
    //     //     $member_config_handle->updateCheckBetAmount($member['member_id'], -1);
    //     //     unset($member_config_handle);
    //     // }
    //
    //     if ($flag) {
    //         $sql = "UPDATE fnn_record SET note='完成',manager_id=?";
    //         $sql .= " WHERE note!='取消' AND subtype=3 AND member_id=? AND table_id=?  LIMIT 1";
    //         $flag = $this->_db->query($sql, array($manager['manager_id'], $member['member_id'], $withdrawal_id));
    //     }
    //
    //     if ($flag) {
    //         // 管理员操作日志
    //         $operate_log_handle = new OperateLog();
    //         $parameters = array(
    //             'manager_account' => $manager['account'],
    //             'account' => $member['account'],
    //             'account_type' => 1,
    //             'module' => 'finance',
    //             'argument' => array(),
    //             'detail' => '确认取款|金额' . money_from_db($withdrawal['amount']) . '|单号' . $withdrawal['order_id'],
    //         );
    //         $operate_log_handle->add($parameters);
    //         unset($operate_log_handle);
    //
    //         $this->_db->commit();
    //         return 1;
    //     } else {
    //         $this->_db->rollBack();
    //         return -4;
    //     }
    // }

    /**
     * [doing 處理中 status = 4]
     * @param  [type] $withdrawal_id [description]
     * @param  [type] $uid           [description]
     * @param  string $note          [description]
     * @param  string $manager_note  [description]
     * @return [type]                [description]
     */
    // public function doing($withdrawal_id, $uid, $note = '', $manager_note = '')
    // {
    //     $withdrawal = $this->get($withdrawal_id);
    //     if (!$withdrawal) {
    //         return -1;
    //     }
    //     if (1 != $withdrawal['status']) {
    //         return -2;
    //     }
    //     $manager_handle = new Manager();
    //     $manager = $manager_handle->getByUid($uid);
    //     unset($manager_handle);
    //     if (!$manager) {
    //         return -3;
    //     }
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE fnn_withdrawal SET manager=?,deal_time=?,deal_note=?';
    //     $sql .= ',manager_note=?,status=4 WHERE withdrawal_id=? AND status=1 LIMIT 1';
    //     $result = $this->_db->query($sql, array($manager['account'], $time, $note, $manager_note, $withdrawal_id));
    //     if ($result) {
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    /**
     * [cancel 取消 status = 3]
     * @param  [type] $withdrawal_id [description]
     * @param  [type] $uid           [description]
     * @param  string $note          [description]
     * @param  string $manager_note  [description]
     * @return [type]                [description]
     */
    // public function cancel($withdrawal_id, $uid, $note = '', $manager_note = '')
    // {
    //     $withdrawal = $this->get($withdrawal_id);
    //     if (!$withdrawal) {
    //         return -1;
    //     }
    //     if (1 != $withdrawal['status'] && 4 != $withdrawal['status']) {
    //         return -2;
    //     }
    //     $manager_handle = new Manager();
    //     $manager = $manager_handle->getByUid($uid);
    //     unset($manager_handle);
    //     if (!$manager) {
    //         return -3;
    //     }
    //
    //     $time = date('Y-m-d H:i:s');
    //
    //     $this->_db->beginTransaction();
    //     $sql = 'UPDATE fnn_withdrawal SET manager=?,deal_time=?,deal_note=?';
    //     $sql .= ',manager_note=?,status=3 WHERE withdrawal_id=? AND status!=3 AND status!=2 LIMIT 1';
    //     $flag = $this->_db->query($sql, array($manager['account'], $time, $note, $manager_note, $withdrawal_id));
    //
    //     if ($flag) {
    //         $member_handle = new Member();
    //         $flag = $member_handle->updateAmount($withdrawal['member_id'], $withdrawal['amount']);
    //         unset($member_handle);
    //     }
    //
    //     if ($flag) {
    //         $argument = array(
    //             $withdrawal['member_id'],
    //             0,
    //             3,
    //             $withdrawal_id,
    //             $withdrawal['amount'],
    //             date('Y-m-d H:i:s'),
    //             '取消',
    //         );
    //         $finance_handle = new Finance();
    //         $flag = $finance_handle->add($argument);
    //         unset($finance_handle);
    //     }
    //
    //     /*if ($flag) {
    //     $sql = 'SELECT * FROM fnn_record WHERE type=0 AND subtype=3 AND table_id=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($withdrawal_id));
    //     if($result) {
    //     $finance = $result[0];
    //     $this->_db->delete('fnn_record', $finance);
    //     $sql = 'DELETE FROM fnn_record WHERE record_id=? LIMIT 1';
    //     if ($this->_db->query($sql, array($finance['record_id']))) {
    //     $flag = 1;
    //     }
    //     else {
    //     $flag = 0;
    //     }
    //     }
    //     else {
    //     $flag = 0;
    //     }
    //     }*/
    //
    //     if ($flag) {
    //         $this->_db->commit();
    //         return 1;
    //     } else {
    //         $this->_db->rollBack();
    //         return -4;
    //     }
    // }

    // public function change_acutal_amount($withdrawal_id, $actual_amount)
    // {
    //     $sql = 'UPDATE fnn_withdrawal SET actual_amount=? WHERE withdrawal_id=? AND status !=2 LIMIT 1';
    //     $result = $this->_db->query($sql, array($actual_amount, $withdrawal_id));
    //     return $result;
    // }
}
