<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 流水账类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Finance extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {}

    /**
     * add
     *
     * @access public
     * @param array $argument 参数
     * <code>
     * -- member_id 会员编号
     * -- type 类型
     * ---- 0 系统
     * ---- 1 皇冠体育
     * ---- 2 彩票游戏
     * ---- 3 真人视讯
     * ---- 4 真人视讯-AG
     * ---- 5 真人视讯-LEBO
     * ---- 6 真人视讯-SSS
     * ---- 7 真人视讯-DS
     * ---- 8 真人视讯-AB
     * ---- 9 电子-MG
     * ---- 20 电子
     * -- subtype 子类型
     * ---- 1 线上存款
     * ---- 2 公司入款
     * ---- 3 在线取款
     * ---- 4 转入
     * ---- 5 转出
     * ---- 6 投注
     * ---- 7 投注盈利
     * ---- 8 重新结算
     * ---- 9 投注取消
     * ---- 10 比分/注单重置
     * ---- 11 自动返水
     * ---- 12 自动彩金
     * ---- 13 人工入款
     * ---- 14 人工返水
     * ---- 15 人工彩金
     * ---- 16 其它
     * ---- 17 归档
     * ---- 18 手续费
     * -----19 真人额度转换回补
     * -----20 兑换码充值
     * -- table_id 账目编号
     * -- amount 金额
     * -- submit_time 记录时间
     * -- note 备注
     * -- current_amount 当前额度（操作前）
     * </code>
     * @return bool
     */
    public function add($argument)
    {
        $sql = 'INSERT INTO fnn_record SET member_id=?,type=?,subtype=?';
        $sql .= ',table_id=?,amount=?,submit_time=?,note=?,previous_amount=?';
        $sql .= ',current_amount=?,member_category=?,manager_id=?';
        if (!isset($argument[7]) || !isset($argument[8])) {
            $member_handle = new Member();
            $member = $member_handle->getByMemberId($argument[0]);
            unset($member_handle);
            $argument[7] = $member['amount'] - $argument[4];
            $argument[8] = $member['amount'];
            $argument[9] = $member['category'];
        }
        if (!isset($argument[9])) {
            $member_handle = new Member();
            $member = $member_handle->getByMemberId($argument[0], 'category');
            unset($member_handle);
            $argument[9] = $member['category'];
        }
        if (!isset($argument[10])) {
            $argument[10] = 0; //manager_id
        }
        return $this->_db->query($sql, $argument);
    }

    /**
     * [getList 獲取紀錄清單]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
    public function getList($parameters)
    {
        global $g_page_size;

        $member_id = isset($parameters['member_id']) ? $parameters['member_id'] : 0;
        $start_time = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : $g_page_size['general'];
        $subtypes = isset($parameters['subtypes']) ? $parameters['subtypes'] : '';

        $where = '';
        $sql_parameters = array();
        if ($member_id) {
            $where .= ' AND a.member_id=?';
            $sql_parameters[] = $member_id;
        }
        if ($start_time) {
            $where .= ' AND a.submit_time>=?';
            $sql_parameters[] = $start_time;
        }
        if ($stop_time) {
            $stop_time .= ' 23:59:59';
            $where .= ' AND a.submit_time<=?';
            $sql_parameters[] = $stop_time;
        }
        if (isset($parameters['subtype'])) {
            $where .= ' AND a.subtype=?';
            $sql_parameters[] = $parameters['subtype'];
        } else if ($subtypes) {
            $where .= ' AND a.subtype IN(' . $subtypes . ')';
        } else {
            $where .= ' AND a.subtype<=5';
        }

        $sql = 'SELECT COUNT(record_id) AS `total` FROM fnn_record AS a WHERE 1 ' . $where;
        $result = $this->_db->query($sql, $sql_parameters);
        $return = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ' LIMIT ' . (($page - 1) * $page_size) . ',' . $page_size . ' ';

        $sql = 'SELECT a.record_id, a.type, a.subtype, a.table_id, a.amount, a.note, a.submit_time,a.previous_amount,a.current_amount FROM fnn_record AS a WHERE 1 ';
        $sql .= $where . ' ORDER BY a.record_id DESC ' . $limits;
        $result = $this->_db->query($sql, $sql_parameters);

        $return['list'] = $result;

        return $return;
    }

    private function subtypesValue()
    {
        $subtypes = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            4 => 'change_into', // 转入
            5 => 'change_out', // 转出
            6 => 'bet', // 投注
            7 => 'pay_out', // 派彩
            8 => 're_settle', // 重新结算
            9 => 'bet_cancel', // 投注取消
            10 => 'score_reset', // 比分注单重置
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_withdraw', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            17 => 'node', // 归档
            18 => 'charge', // 手续费
            19 => 'back', // 系统回补
            20 => 'exchange', // 充值码充值
        );
        return $subtypes;
    }

    /**
     * [memberReport 对账]
     *
     * @param array $parameters
     * @return void
     */
    public function memberReport($parameters)
    {
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;
        $status = isset($parameters['status']) ? $parameters['status'] : 0;
        $category = isset($parameters['category']) ? $parameters['category'] : 0;

        $where = '';
        $sql_parameters = array();

        if ($account) {
            $handle = new Member();

            $result = $handle->getByAccountFuzzy($account, 'member_id');
            if ($result) {

                $member_id = array(0);
                foreach ($result as $row) {
                    $member_id[] = $row['member_id'];
                }
                $member_id = implode(',', $member_id);
            } else {
                $member_id = 0;
            }
            unset($handle, $result);
            $where .= ' AND a.member_id in(' . $member_id . ')';
        }

        if ($category != 0) {
            $where .= ' AND a.category=?';
            $sql_parameters[] = $category;
        }

        $limits = '';
        $return = array();

        $sql = 'SELECT
                    a.member_id,
                    a.amount,
                    COUNT(a.member_id) as total,
                    SUM(b.amount) AS total_amount
                FROM
                    uc_member AS a ';
        $sql .= ' LEFT JOIN
                        fnn_record AS b
                  ON
                        a.member_id=b.member_id 
                 WHERE 1 ' . $where;
        $sql .= ' GROUP BY a.member_id ' ;

        $result = $this->_db->query($sql, $sql_parameters);
        
        // 差異
        if (1 == $status) {
            
            $members = array(0);
            foreach ($result as $key => $row) {
                //檢查差異
                if ($row['amount'] != $row['total_amount']) {
                    $members[] = $row['member_id'];
                }
            }
            $total_members = count($members);
            $members = implode(',', $members);
            $where = $where . ' AND a.member_id IN(' . $members . ')';

            $return['total_page'] = ceil($total_members / $page_size);
            $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
            $page = $page > 0 ? $page : 1;
            $page = $page > $return['total_page'] ? $return['total_page'] : $page;
            $return['page'] = $page;
            $limits = ' LIMIT ' . (($page - 1) * $page_size) . ',' . $page_size . ' ';
    
        } else {
            $return['total_page'] = ceil(count($result) / $page_size);
            $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;      
            $page = $page > 0 ? $page : 1;
            $page = $page > $return['total_page'] ? $return['total_page'] : $page;
            $return['page'] = $page;
            $limits = ' LIMIT ' . (($page - 1) * $page_size) . ',' . $page_size . ' ';
        }

        $sql = 'SELECT a.member_id,a.account,a.amount,SUM(b.amount) AS amount_total,';
        $sql .= ' SUM(IF(b.subtype = 1,b.amount,0)) AS online_deposit , '; // 在線存款
        $sql .= ' SUM(IF(b.subtype = 2,b.amount,0)) AS company_money,'; // 公司入款
        $sql .= ' SUM(IF(b.subtype = 3,b.amount,0)) AS online_withdraw,'; // 在線取款
        $sql .= ' SUM(IF(b.subtype = 4,b.amount,0)) AS change_into,'; // 轉入
        $sql .= ' SUM(IF(b.subtype = 5,b.amount,0)) AS change_out,'; // 轉出
        $sql .= ' SUM(IF(b.subtype = 6,b.amount,0)) AS bet,'; // 投注
        $sql .= ' SUM(IF(b.subtype = 7,b.amount,0)) AS pay_out,'; //派彩
        $sql .= ' SUM(IF(b.subtype = 8,b.amount,0)) AS re_settle,'; // 重新結算
        $sql .= ' SUM(IF(b.subtype = 9,b.amount,0)) AS bet_cancel,'; //投注取消
        $sql .= ' SUM(IF(b.subtype = 11,b.amount,0)) AS auto_return_water,'; // 自動反水
        $sql .= ' SUM(IF(b.subtype = 12,b.amount,0)) AS auto_winnings,'; //自動彩金
        $sql .= ' SUM(IF(b.subtype = 13,b.amount,0)) AS hand_withdraw,'; // 人工入款
        $sql .= ' SUM(IF(b.subtype = 14,b.amount,0)) AS hand_return_water,'; // 人工反水
        $sql .= ' SUM(IF(b.subtype = 15,b.amount,0)) AS hand_winnings,'; // 人工彩金
        $sql .= ' SUM(IF(b.subtype = 16,b.amount,0)) AS other,'; // 其他
        $sql .= ' SUM(IF(b.subtype = 17,b.amount,0)) AS node,'; // 歸檔
        $sql .= ' SUM(IF(b.subtype = 18,b.amount,0)) AS charge'; //手續費
        $sql .= ' FROM uc_member a ';
        $sql .= ' LEFT JOIN fnn_record AS b  ON a.member_id = b.member_id ';
        $sql .= ' WHERE 1  ' . $where . ' GROUP BY a.member_id ORDER BY a.member_id DESC ' . $limits;

        $data = array();
        $data = $this->_db->query($sql, $sql_parameters);

        //格式處理
        $new_data = array();
        foreach ($data as $row) {
            if (!isset($new_data[$row['member_id']])) {
                $new_data[$row['member_id']] = $row;
            }
        }

        foreach ($new_data as $key => $item) {

            if (!isset($item['bet'])) {
                $new_data[$key]['bet'] = 0;
            }

            if (!isset($item['pay_out'])) {
                $new_data[$key]['pay_out'] = 0;
            }

            if (!isset($item['score_reset'])) {
                $new_data[$key]['score_reset'] = 0;
            }

        }

        $return['list'] = $new_data;
        return $return;
    }

    /**
     * [managerFinancereport 管理員經手財務報表]
     * @param  [type] $parameters [description]
     * @param  string $default    [description]
     * @return [type]             [description]
     */
    public function managerFinanceReport($parameters, $default = '')
    {
        $account = isset($parameters['account']) ? $parameters['account'] : 0;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 100;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }

        $sql1_parameters = array();
        $where1 = '';
        if ($account) {
            $handle = new Manager();
            $result = $handle->getByAccount($account, 'manager_id');
            if ($result) {
                $manager_id = $result['manager_id'];
            } else {
                $manager_id = 0;
            }
            unset($handle, $result);
            $where1 .= ' AND b.manager_id=?';
            $sql1_parameters[] = $manager_id;
        }

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
        );
        // 計算總數
        $sql = 'SELECT COUNT(DISTINCT a.manager_id) as total ';
        $sql .= 'FROM mng_manager AS a LEFT JOIN fnn_record AS b ON a.manager_id=b.manager_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        // echo $sql;
        $total = $result ? $result[0]['total'] : 0;
        //分頁
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        // 撈出資料
        $sql = 'SELECT a.manager_id, a.manager_id, ';
        $sql .= 'SUM(IF(b.subtype=1,b.amount,0)) as online_deposit,';
        $sql .= 'SUM(IF(b.subtype=2,b.amount,0)) as company_money,';
        $sql .= "SUM(IF(b.subtype=3,IF(b.note='完成',b.amount,0),0)) as online_withdraw,";
        $sql .= 'SUM(IF(b.subtype=11,b.amount,0)) as auto_return_water, ';
        $sql .= 'SUM(IF(b.subtype=12,b.amount,0)) as auto_winnings,';
        $sql .= 'SUM(IF(b.subtype=13,b.amount,0)) as hand_deposit, ';
        $sql .= 'SUM(IF(b.subtype=14,b.amount,0)) as hand_return_water, ';
        $sql .= 'SUM(IF(b.subtype=15,b.amount,0)) as hand_winnings, ';
        $sql .= 'SUM(IF(b.subtype=16,b.amount,0)) as other ';
        $sql .= 'FROM mng_manager AS a LEFT JOIN fnn_record AS b ON a.manager_id=b.manager_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $sql .= ' GROUP BY a.manager_id';
        $sql .= ' ORDER BY a.manager_id DESC LIMIT ' . $limits;
        // echo $sql;
        $result = $this->_db->query($sql, $sql_parameters);
        $return['list'] = $result;

        return $return;
    }

    /**
     * [userFinanceReport 會員財務報表]
     * @param  [type] $parameters [description]
     * @param  string $default    [description]
     * @return [type]             [description]
     */
    public function userFinanceReport($parameters, $default = '')
    {
        $account = isset($parameters['account']) ? $parameters['account'] : 0;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 100;
        $category = isset($parameters['category']) ? $parameters['category'] : 0;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }

        $sql1_parameters = array();
        $where1 = '';
        if ($account) {
            $handle = new Member();

            $result = $handle->getByAccountFuzzy($account, 'member_id');
            if ($result) {
                $member_id = array(0);
                foreach ($result as $res) {
                    $member_id[] = $res['member_id'];
                }
                $member_id = implode(',', $member_id);
            } else {
                $member_id = 0;
            }
            $where1 .= ' AND b.member_id in(' . $member_id . ')';
        }

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        if ($category != 0) {
            $where .= ' AND a.category=?';
            $sql_parameters[] = $category;
        }

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            // 4 => 'change_into', // 转入
            // 5 => 'change_out', // 转出
            //6 => 'bet',                      // 投注
            //7 => 'pay_out',                  // 派彩
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            // 20 => 'recharge', // 充值码
            // 21 => 'luckdraw', // 活动彩金
        );
        // 計算總數
        $sql = 'SELECT COUNT(DISTINCT a.member_id) as total ';
        $sql .= 'FROM uc_member AS a LEFT JOIN fnn_record AS b ON a.member_id=b.member_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        // echo $sql;
        $total = $result ? $result[0]['total'] : 0;
        //分頁
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        // 撈出資料
        $sql = 'SELECT a.member_id, a.account, ';
        $sql .= 'SUM(IF(b.subtype=1,b.amount,0)) as online_deposit,';
        $sql .= 'SUM(IF(b.subtype=2,b.amount,0)) as company_money,';
        $sql .= "SUM(IF(b.subtype=3,IF(b.note='完成',b.amount,0),0)) as online_withdraw,";
        // $sql.= 'SUM(IF(b.subtype=4,b.amount,0)) as change_into, ';
        // $sql.= 'SUM(IF(b.subtype=5,b.amount,0)) as change_out, ';
        $sql .= 'SUM(IF(b.subtype=11,b.amount,0)) as auto_return_water, ';
        $sql .= 'SUM(IF(b.subtype=12,b.amount,0)) as auto_winnings,';
        $sql .= 'SUM(IF(b.subtype=13,b.amount,0)) as hand_deposit, ';
        $sql .= 'SUM(IF(b.subtype=14,b.amount,0)) as hand_return_water, ';
        $sql .= 'SUM(IF(b.subtype=15,b.amount,0)) as hand_winnings, ';
        $sql .= 'SUM(IF(b.subtype=16,b.amount,0)) as other ';
        // $sql.= 'SUM(IF(b.subtype=20,b.amount,0)) as recharge, ';
        // $sql.= 'SUM(IF(b.subtype=21,b.amount,0)) as luckdraw, ';
        $sql .= 'FROM uc_member AS a LEFT JOIN fnn_record AS b ON a.member_id=b.member_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $sql .= ' GROUP BY a.member_id';
        $sql .= ' ORDER BY a.member_id DESC LIMIT ' . $limits;
        // echo $sql;
        // print_r($sql_parameters);
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        $return['list'] = $result;

        return $return;

    }

    /**
     * [memberReportSum 會員管理報表全部顯示]
     *
     * @param [type] $parameters
     * @param string $default
     * @return void
     */
    public function memberReportSum($parameters, $default = '')
    {
        $account = isset($parameters['account']) ? $parameters['account'] : 0;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $category = isset($parameters['category']) ? $parameters['category'] : 0;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }

        $sql1_parameters = array();
        $where1 = '';
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $member_id = 0;
            }
            unset($handle, $result);
            $where1 .= ' AND b.member_id=?';
            $sql1_parameters[] = $member_id;
        }

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        // if ($category != 0) {
        //     $where .= ' AND a.category=?';
        //     $sql_parameters[] = $category;
        // }

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            // 4 => 'change_into', // 转入
            // 5 => 'change_out', // 转出
            //6 => 'bet',                      // 投注
            //7 => 'pay_out',                  // 派彩
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            20 => 'recharge', // 充值码
            // 21 => 'luckdraw', // 活动彩金
        );
        // 撈出資料
        $sql = 'SELECT COUNT(DISTINCT a.member_id) as member_count, ';
        $sql .= 'SUM(IF(b.subtype=1,b.amount,0)) as online_deposit,';
        $sql .= 'SUM(IF(b.subtype=2,b.amount,0)) as company_money,';
        $sql .= "SUM(IF(b.subtype=3,IF(b.note='完成',b.amount,0),0)) as online_withdraw,";
        $sql .= 'SUM(IF(b.subtype=11 OR b.subtype = 14,b.amount,0)) as auto_return_water, ';
        $sql .= 'SUM(IF(b.subtype=12 OR b.subtype = 15,b.amount,0)) as auto_winnings,';
        $sql .= 'SUM(IF(b.subtype=13,b.amount,0)) as hand_deposit, ';
        $sql .= 'SUM(IF(b.subtype=20,b.amount,0)) as recharge_amount, ';
        // $sql .= 'SUM(IF(b.subtype=14 OR b.subtype = 11,b.amount,0)) as hand_return_water, ';
        // $sql .= 'SUM(IF(b.subtype=15,b.amount,0)) as hand_winnings, ';
        $sql .= 'SUM(IF(b.subtype=16,b.amount,0)) as other ';
        $sql .= 'FROM uc_member AS a LEFT JOIN fnn_record AS b ON a.member_id=b.member_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        $return['data'] = $result;
        $return['sql'] = $sql;

        $sql = 'SELECT
                    SUM(b.win_amount) AS win_amount,
                    SUM(b.valid_amount) AS valid_amount,
                    SUM(b.bet_amount) AS bet_amount,
                    b.member_id,
                    a.amount as member_amount
                FROM
                    ltt_bet AS b
                LEFT JOIN
                    uc_member AS a ON b.member_id=a.member_id WHERE bet_time >= ? AND bet_time <= ? AND b.status=2';

        $result_two = $this->_db->query($sql, array($date_start, $date_stop));
        $return['bet'] = $result_two;

        $sql = 'SELECT
                    uc_member.member_id ,
                    sum(uc_member.amount) as total_member_amount
                FROM
                (
                SELECT
                SUM(b.win_amount) AS win_amount,
                SUM(b.valid_amount) AS valid_amount,
                SUM(b.bet_amount) AS bet_amount,
                    b.member_id,
                    a.amount as member_amount
                FROM
                    ltt_bet AS b
                LEFT JOIN
                    uc_member AS a
                ON
                b.member_id=a.member_id
                WHERE
                bet_time >= ? AND bet_time <= ?
                AND b.status=2
                group by
                a.member_id
                ) AS c,
                    uc_member
                WHERE
                    uc_member.member_id IN (c.member_id)';

        $result_three = $this->_db->query($sql, $sql_parameters);

        $return['total_member_amount'] = $result_three;

        $member_params = array();
        $member_params[] = $date_start;
        $member_params[] = $date_stop;
        $member_params[] = $date_start;
        $member_params[] = $date_stop;
        $member_params[] = $date_start;
        $member_params[] = $date_stop;

        if ($category != 0) {
            $condition_str = ' AND a.category=?';
            $member_params[] = $category;
        }

        $sql = 'select count(member_id) as cnt FROM uc_member AS a where ';
        $sql .= '(exists(select 1 from fnn_withdrawal b where a.member_id=b.member_id and b.submit_time>? AND b.submit_time<? AND b.status = 2) or ';
        $sql .= 'exists(select 1 from fnn_record c where a.member_id=c.member_id and c.submit_time>? AND c.submit_time<? AND c.subtype IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21) ) or ';
        $sql .= 'exists(select 1 from ltt_bet d force index (bet_time) where a.member_id=d.member_id and d.bet_time>? AND d.bet_time<?)) ';
        $sql .= $condition_str;

        $result_four = $this->_db->query($sql, $member_params);

        $return['cnt'] = $result_four;

        return $return;

    }

    /**
     * [userFinanceReportSum 會員財務報表總計]
     * @param  [type] $parameters [description]
     * @param  string $default    [description]
     * @return [type]             [description]
     */
    public function userFinanceReportSum($parameters, $default = '')
    {
        $account = isset($parameters['account']) ? $parameters['account'] : 0;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $category = isset($parameters['category']) ? $parameters['category'] : 0;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }

        $sql1_parameters = array();
        $where1 = '';
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $member_id = 0;
            }
            unset($handle, $result);
            $where1 .= ' AND b.member_id=?';
            $sql1_parameters[] = $member_id;
        }

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        if ($category != 0) {
            $where .= ' AND a.category=?';
            $sql_parameters[] = $category;
        }

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            // 4 => 'change_into', // 转入
            // 5 => 'change_out', // 转出
            //6 => 'bet',                      // 投注
            //7 => 'pay_out',                  // 派彩
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            // 20 => 'recharge', // 充值码
            // 21 => 'luckdraw', // 活动彩金
        );
        // 撈出資料
        $sql = 'SELECT COUNT(DISTINCT a.member_id) as member_count, ';
        $sql .= 'SUM(IF(b.subtype=1,b.amount,0)) as online_deposit,';
        $sql .= 'SUM(IF(b.subtype=2,b.amount,0)) as company_money,';
        $sql .= "SUM(IF(b.subtype=3,IF(b.note='完成',b.amount,0),0)) as online_withdraw,";
        $sql .= 'SUM(IF(b.subtype=11,b.amount,0)) as auto_return_water, ';
        $sql .= 'SUM(IF(b.subtype=12,b.amount,0)) as auto_winnings,';
        $sql .= 'SUM(IF(b.subtype=13,b.amount,0)) as hand_deposit, ';
        $sql .= 'SUM(IF(b.subtype=14,b.amount,0)) as hand_return_water, ';
        $sql .= 'SUM(IF(b.subtype=15,b.amount,0)) as hand_winnings, ';
        $sql .= 'SUM(IF(b.subtype=16,b.amount,0)) as other ';
        $sql .= 'FROM uc_member AS a LEFT JOIN fnn_record AS b ON a.member_id=b.member_id';
        $sql .= '  WHERE 1 ' . $where1 . $where;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        $return['data'] = $result;
        $return['sql'] = $sql;

        return $return;

    }

    /**
     * [getTodaySum 獲取今日的各種總額]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
    public function getTodaySum($parameters)
    {
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : date('Y-m-d');
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : date('Y-m-d');
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }

        $sql1_parameters = array();
        $where1 = '';

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它

        );
        // 撈出資料
        $sql = 'SELECT ';
        $sql .= 'SUM(IF(b.subtype=1,b.amount,0)) as online_deposit, ';
        $sql .= 'SUM(IF(b.subtype=2,b.amount,0)) as company_money, ';
        $sql .= "SUM(IF(b.subtype=3,IF(b.note='完成',b.amount,0),0)) as online_withdraw ";

        $sql .= 'FROM uc_member AS a LEFT JOIN fnn_record AS b ON a.member_id=b.member_id ';
        $sql .= 'WHERE 1 ' . $where1 . $where;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        $return['data'] = $result;
        $return['sql'] = $sql;

        return $return;

    }

    /**
     * [managerGetList description]
     * @param  [type] $parameters [description]
     * @param  string $default    [description]
     * @return [type]             [description]
     */
    public function managerGetList($parameters, $default = '')
    {
        $account = isset($parameters['account']) ? $parameters['account'] : 0;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;
        $category = isset($parameters['category']) ? $parameters['category'] : 0;

        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }

        $sql1_parameters = array();
        $where1 = '';
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $member_id = 0;
            }
            unset($handle, $result);
            $where1 .= ' AND b.member_id=?';
            $sql1_parameters[] = $member_id;
        }

        $where = '';
        $sql_parameters = array();

        if ($date_start) {
            $where .= ' AND b.submit_time>=?';
            $sql_parameters[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND b.submit_time<=?';
            $sql_parameters[] = $date_stop;
        }

        if ($category != 0) {
            $where .= ' AND a.category=?';
            $sql_parameters[] = $category;
        }

        $limits = '';

        $fields = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_money', // 公司入款
            3 => 'online_withdraw', // 在线取款
            4 => 'change_into', // 转入
            5 => 'change_out', // 转出
            //6 => 'bet',                      // 投注
            //7 => 'pay_out',                  // 派彩
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            20 => 'recharge', // 充值码
            21 => 'luckdraw', // 活动彩金
        );
        $fields2 = array();
        foreach ($fields as $val) {
            $fields2[$val] = 0;
        }
        //從金流紀錄
        $data = array();
        $sql = 'SELECT a.member_id, a.account, b.amount, b.subtype FROM uc_member AS a ';
        $sql .= ' LEFT JOIN fnn_record AS b ON a.member_id=b.member_id WHERE 1 AND b.subtype IN(1,2,3,4,5,11,12,13,14,15,16,20,21) ' . $where1 . $where;
        $sql .= ' ORDER BY a.member_id DESC ' . $limits;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));

        foreach ($result as $row) {
            if (!isset($data[$row['member_id']])) {
                $data[$row['member_id']] = $fields2; //預設各種金流都為0
                $data[$row['member_id']]['account'] = $row['account'];
            }
            $data[$row['member_id']][$fields[$row['subtype']]] += $row['amount']; //依照金流種類加總給各會員
        }
        unset($result);
        //從提款紀錄
        $sql = 'SELECT b.member_id, b.amount, b.account FROM fnn_withdrawal as b WHERE b.status=2 ' . $where1 . $where . $limits;
        $result = $this->_db->query($sql, array_merge($sql1_parameters, $sql_parameters));
        foreach ($result as $row) {
            if (!isset($data[$row['member_id']])) {
                $data[$row['member_id']] = $fields2;
                $data[$row['member_id']]['account'] = $row['account'];
            }
            $data[$row['member_id']]['online_withdraw'] -= $row['amount']; //在線取款要扣掉提款記錄的金額
        }
        unset($result);

        $return['list'] = $data;

        return $return;
    }

    /**
     * [memberList 會員金流]
     *
     * @param array $argument
     * @return void
     */
    public function memberList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();
        if (isset($argument['member_id']) && $argument['member_id']) {
            $condition[] = ' AND b.member_id=?';
            $parameter[] = $argument['member_id'];
        }

        if (isset($argument['account'])) {
            if (isset($argument['fuzzy']) && $argument['fuzzy']) {
                $condition[] = ' AND a.account Like ?';
                $parameter[] = '%' . $argument['account'] . '%';
            } else if ($argument['account']) {
                $condition[] = ' AND a.account=?';
                $parameter[] = $argument['account'];
            }
        }
        $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
        if ($date_start) {
            $condition[] = ' AND b.submit_time>=?';
            $parameter[] = $date_start;
        }
        $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
            $condition[] = ' AND b.submit_time<=?';
            $parameter[] = $date_stop;
        }
        $time_zone = isset($argument['time_zone']) ? $argument['time_zone'] : 0;
        if (1 == $time_zone) {
            // 北京时间
            if ($date_start) {
                $date_start = bj_to_et_date($date_start);
            }
            if ($date_stop) {
                $date_stop = bj_to_et_date($date_stop);
            }
        }
        $type = isset($argument['type']) ? $argument['type'] : '';

        $category = isset($argument['category']) ? $argument['category'] : 0;
        if ($category != 0) {
            $condition[] = ' AND a.category=?';
            $parameter[] = $category;
        }

        switch ($type) {
            case 1:
                $condition[] = ' AND b.subtype IN (?,?,?)';
                $parameter[] = 1;
                $parameter[] = 2;
                $parameter[] = 13;
                break;
            case 2:
                $condition[] = ' AND b.subtype=?';
                $parameter[] = 3;
                break;
            case 3:
                $condition[] = ' AND b.subtype IN (?,?)';
                $parameter[] = 4;
                $parameter[] = 5;
                break;
            case 4:
                $condition[] = ' AND b.subtype IN (?,?)';
                $parameter[] = 12;
                $parameter[] = 15;
                break;
            case 5:
                $condition[] = ' AND b.subtype IN (?,?)';
                $parameter[] = 11;
                $parameter[] = 14;
                break;
            case 6:
                $condition[] = ' AND b.subtype=?';
                $parameter[] = 16;
                break;
            case 7:
                $condition[] = ' AND b.type=?';
                $parameter[] = 1;
                break;
            case 8:
                $condition[] = ' AND b.type=?';
                $parameter[] = 2;
                break;
            case 9:
                $condition[] = '  AND b.type IN (?,?,?,?)';
                $parameter[] = 3;
                $parameter[] = 4;
                $parameter[] = 5;
                $parameter[] = 6;
                break;
            default:
                break;
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM fnn_record AS b LEFT JOIN uc_member AS a ON a.member_id=b.member_id WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT
                    a.account,b.record_id,b.member_id,
                    b.type,b.subtype,b.table_id,
                    b.amount,b.submit_time,b.note,
                    b.previous_amount,b.current_amount
                FROM
                    fnn_record AS b
                LEFT JOIN
                    uc_member AS a
                ON
                    a.member_id=b.member_id WHERE 1';

        $sql .= implode('', $condition);
        $sql .= ' ORDER BY b.record_id DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function memberDepositTime($member_id, $start_time, $stop_time = '')
    {
        if ('' == $stop_time) {
            $stop_time = date('Y-m-d H:i:s');
        }
        $sql = 'SELECT amount,submit_time FROM fnn_record WHERE subtype IN (1,2,12)';
        $sql .= ' AND member_id=? AND submit_time>=? AND submit_time<=?';
        $result = $this->_db->query($sql, array($member_id, $start_time, $stop_time));
        return $result;
    }

    public function checkDailyReturn($member_id, $note)
    {
        $sql = 'SELECT SUM(amount) AS amount FROM fnn_record WHERE member_id=? AND note=?';
        $result = $this->_db->query($sql, array($member_id, $note));
        $amount = 0;
        if ($result && isset($result[0]) && $result[0]['amount'] > 0) {
            $amount = $result[0]['amount'];
        }
        return $amount;
    }
    public function getDataForShortcut($parameter, $shortcut)
    {
        $data = $this->managerGetList($parameter, $shortcut);

        if ($data['list']) {
            foreach ($data['list'] as $row) {
                if ($shortcut == 1) {
                    $hand_deposit = money_from_db($row['hand_deposit']);
                    $data_result[$shortcut] += $hand_deposit;
                } elseif ($shortcut == 2) {
                    $company_money = money_from_db($row['company_money']);
                    $data_result[$shortcut] += $company_money;
                } elseif ($shortcut == 3) {
                    $online_withdraw = money_from_db($row['online_withdraw']);
                    $data_result[$shortcut] += $online_withdraw;
                }
            }
        } else {
            return 0;
        }

        return $data_result ? $data_result[$shortcut] : 0;
    }

    public function getList_new($parameters) // 前台彩金紀錄取得活動彩金用

    {
        global $g_page_size;

        $member_id = isset($parameters['member_id']) ? $parameters['member_id'] : 0;
        $start_time = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : $g_page_size['general'];
        // $subtypes = isset($parameters['subtypes']) ? $parameters['subtypes'] : '';

        $where = '';
        $sql_parameters = array();
        if ($member_id) {
            $where .= ' AND a.member_id=?';
            $sql_parameters[] = $member_id;
        }
        if ($start_time) {
            $start_time .= ' 00:00:00';
            $where .= ' AND a.submit_time>=?';
            $sql_parameters[] = $start_time;
        }
        if ($stop_time) {
            $stop_time .= ' 23:59:59';
            $where .= ' AND a.submit_time<=?';
            $sql_parameters[] = $stop_time;
        }
        if (isset($parameters['subtype']) && '' != $parameters['subtype'] && !is_array($parameters['subtype'])) {
            $where .= ' AND a.subtype=?';
            $sql_parameters[] = $parameters['subtype'];
        } else if (isset($parameters['subtype']) && '' != $parameters['subtype'] && is_array($parameters['subtype'])) {
            $condition[] = ' AND subtype IN ( ';
            foreach ($parameters['subtype'] as $subtype) {
                $condition[] = ' ? ,';
                $sql_parameters[] = $subtype;
            }
            $lastConditionIndex = count($condition) - 1;
            $condition[$lastConditionIndex] = substr($condition[$lastConditionIndex], 0, strlen($condition[$lastConditionIndex]) - 1);
            $condition[] = ')';
            $where .= implode('', $condition);
        } else {
            $where .= ' AND a.subtype<=5';
        }

        $sql = 'SELECT COUNT(record_id) AS `total` FROM fnn_record AS a WHERE 1 ' . $where;
        $result = $this->_db->query($sql, $sql_parameters);
        $return = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ' LIMIT ' . (($page - 1) * $page_size) . ',' . $page_size . ' ';

        $sql = 'SELECT a.record_id, a.subtype as type , a.amount, a.note, a.submit_time,a.previous_amount,a.current_amount FROM fnn_record AS a WHERE 1 ';
        $sql .= $where . ' ORDER BY a.record_id DESC ' . $limits;
        $result = $this->_db->query($sql, $sql_parameters);
        $return['list'] = $result;

        return $return;
    }
}
