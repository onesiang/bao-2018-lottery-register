<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 公告类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Notice extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function sync($content)
    {
        if (!$content) {
            return false;
        }
        $content = json_decode($content, true);
        if (time() - $content['time'] > 15) {
            return false;
        }
        $notice_list = json_decode($content['data'], true);
        $time = date('H:i:s');
        foreach ($notice_list as $notice) {
            $submit_time = $notice[0] . ' ' . $time;
            $auth = md5($notice[1] . $notice[0]);
            $this->save($notice[1], $submit_time, $auth);
        }
    }

    private function save($content, $time, $auth)
    {
        $sql = 'SELECT notice_id FROM mdl_notice WHERE auth=?';
        $result = $this->_db->query($sql, array($auth));
        $check = 0;
        if (strpos($content, '会员') || strpos($content, '客服') || strpos($content, '网站') || strpos($content, '热线') || strpos($content, '+8525')) {
            $check = 1;
        }

        if (0 == $check) {
            if (!$result) {
                $sql = 'INSERT IGNORE INTO mdl_notice SET module=?,title=?,content=?,auth=?,submit_time=?,user_type=1,method=1';
                $module = 1;
                $title = '体育公告';
                $content = str_replace(array('本公司网站', '网站维护'), array('体育', '体育维护'), $content);
                $parameter = array($module, $title, $content, $auth, $time);
                $this->_db->query($sql, $parameter);
            }
        }
    }

    public function add($argument)
    {
        $sql = 'INSERT IGNORE INTO mdl_notice SET module=?,method=?,user_type=?,level=?';
        $sql .= ',start_time=?,stop_time=?,title=?,content=?,image_path=?,submit_time=?,specified_user_account=?,specified_method=?';
        if ($this->_db->query($sql, $argument)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function get($notice_id)
    {
        $sql = 'SELECT * FROM mdl_notice WHERE notice_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($notice_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function edit($argument)
    {
        $sql = 'UPDATE mdl_notice SET module=?,method=?,user_type=?,level=?';
        $sql .= ',start_time=?,stop_time=?,title=?,content=?,image_path=?,specified_user_account=?,specified_method=? WHERE notice_id=? LIMIT 1';
        if ($this->_db->query($sql, $argument)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function delete($notice_id)
    {
        $notice = $this->get($notice_id);
        if ($notice) {
            $this->_db->delete('mdl_notice', $notice);
            $sql = 'DELETE FROM mdl_notice WHERE notice_id=? LIMIT 1';
            if ($this->_db->query($sql, array($notice_id))) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    public function get_web_notice()
    {
        $now = date('Y-m-d H:i:s');
        $sql = 'SELECT notice_id,module,method,content,title,submit_time FROM mdl_notice ';
        $sql .= 'WHERE user_type=1 AND method=1 ';
        $sql .= 'AND (start_time <= ? AND stop_time >= ?)';
        $sql .= ' AND (specified_method = 1)';
        $sql .= 'ORDER BY sort , notice_id ,start_time DESC;';

        $result = $this->_db->query($sql, array($now, $now));
        return $result;
    }

    /**
     *  更新公告為已讀
     * * */
    public function updateNoticeRead($notice_id, $member_id)
    {
        if ($member_id && $notice_id) {
            $sql = "SELECT notice_id FROM notice_check
            WHERE notice_id = ? AND member_id =?";

            $parameter = array(
                $notice_id, $member_id,
            );

            $result = $this->_db->query($sql, $parameter);

            if (count($result) == 0) {
                $sql = "INSERT INTO notice_check
                         SET is_read = 1 , notice_id = ? ,
                         member_id = ? ";
                $this->_db->query($sql, $parameter);
            }
        }
    }

    /**
     *  公告已讀
     * * */
    public function NoticeRead($member_id)
    {
        if ($member_id) {
            $sql = "SELECT notice_id FROM notice_check
            WHERE member_id =?";

            $parameter = array(
                $member_id['member_id'],
            );
            $result = $this->_db->query($sql, $parameter);
            return $result;
        }
    }

    /**
     *  取得今天公告未讀總數
     * * */
    public function getUnReadTotalNotice($member)
    {
        if ($member) {
            $parameter = array(
                $member['member_id'],
            );

            $date = date('Y-m-d H:i:s');
            $today = new DateTime($date);
            $today_form = $today->format('Y-m-d H:i:s');
            $week_ago = $today->modify('-7 day')->format('Y-m-d H:i:s');

            $parameter[] = $week_ago;
            $parameter[] = $today_form;
            $parameter[] = $member['account'];

            $sql = "SELECT COUNT(*) AS total
               FROM mdl_notice b
               LEFT JOIN notice_check
               ON notice_check.notice_id = b.notice_id AND notice_check.member_id = ?
               WHERE  (  ( b.start_time <= NOW() AND b.stop_time >= NOW() ) OR
                         (b.start_time IS NULL OR b.stop_time IS NULL )
                      )
               AND notice_check.is_read is null
               AND b.method = 1
               AND b.submit_time BETWEEN ? AND ?
               AND ( b.specified_method = 1  OR ( b.specified_method = 2 AND  b.specified_user_account = ? ) )";

            $result = $this->_db->query($sql, $parameter)[0]['total'];
            if ($result) {
                return $result;
            }
            return 0;
        }
        return 0;
    }

    public function top($module, $user_type = 1)
    {
        $sql = 'SELECT method,content FROM mdl_notice WHERE module=? AND user_type=? ORDER BY notice_id DESC LIMIT 1';
        $result = $this->_db->query($sql, array($module, $user_type));
        if ($result) {
            $result = array($result[0]['method'], $result[0]['content']);
        } else {
            $result = array('', '');
        }
        return $result;
    }

    /**
     * [homeNotice 取得首頁輪播公告以及彈窗公告]
     *
     * @param integer $user_type 用戶類型
     * @return array $data
     */
    public function homeNotice($user_type = 1)
    {
        $time = date('Y-m-d H:i:s');
        $sql = 'SELECT
                    notice_id,
                    method,
                    content,
                    title,
                    image_path,
                    module
                FROM
                    mdl_notice
                WHERE
                    user_type=? AND (module=0 OR module =5)
                AND
                    method=1 AND (start_time<? || !start_time)
                AND
                    (stop_time>? || !stop_time) AND specified_method = 1
                ORDER BY
                    notice_id DESC LIMIT 5';
        $result = $this->_db->query($sql, array($user_type, $time, $time));
        $data = array();
        if ($result) {
            $content = '';
            $count = 1;
            foreach ($result as $row) {
                $content .= '<a class="notice-list-inline" href="member/news.php?from=center&id=' . $row['notice_id'] . '&module=' . $row['module'] . '">' . ($row['title'] ? '【' . $row['title'] . '】' : '【公告】') . $row['content'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>';
                $count++;
            }
            $data[] = array(1, $content, $result[0]['image_path']);
        } else {
            $data[] = $this->top(1);
        }
        $sql = 'SELECT method,content,image_path ,title FROM mdl_notice WHERE user_type=? AND (module=0 OR module = 5) AND method=2 AND (start_time<? || !start_time) AND (stop_time>? || !stop_time) ORDER BY notice_id DESC LIMIT 1';
        $result = $this->_db->query($sql, array($user_type, $time, $time));
        if ($result) {
            $data[] = array($result[0]['method'], $result[0]['title'], $result[0]['content'], $result[0]['image_path']);
        }
        return $data;
    }

    /**
     * [homeList 彩票公告]
     *
     * @param integer $method 顯示方式
     * @param integer $user_type 用戶類型
     * @return void
     */
    public function homeList($method = 1, $user_type = 1)
    {
        $sql = 'SELECT
                    notice_id,
                    title,
                    content,
                    DATE_FORMAT(submit_time,"%Y-%m-%d") AS submit_time
                FROM
                    mdl_notice
                WHERE
                    module=0 ';

        $params = array();
        $sql .= ' AND user_type=? ';
        $params[] = $user_type;
        if ($method) {
            $sql .= ' AND method=? ';
            $params[] = $method;
        }
        $sql .= ' AND start_time< ? AND stop_time>? ';
        $sql .= '  ORDER BY notice_id DESC ';
        $now = date('Y-m-d H:i:s');
        $params[] = $now;
        $params[] = $now;
        $result = $this->_db->query($sql, $params);
        return $result;
    }

    /**
     * [simpleList 抓取系統公告]
     *
     * @param string $date
     * @param integer $user_type 用戶類型
     * @return void
     */
    public function simpleList($module = 5, $date = '', $user_type = 1)
    {
        $sql = 'SELECT
                    notice_id,
                    title,
                    content,
                    submit_time
                FROM
                    mdl_notice
                WHERE
                    1 AND module = ? ';

        $parameter = array();
        $parameter[] = $module;
        $sql .= ' AND user_type=?';
        $parameter[] = $user_type;

        if ($date) {
            $sql .= ' AND submit_time LIKE ?';
            $parameter[] = $date . '%';
        } else {
            $sql .= ' AND submit_time > ? ORDER BY submit_time DESC,notice_id DESC LIMIT 100';
            $parameter[] = date('Y-m-d H:i:s', time() - 432000);
        }
        $result = $this->_db->query($sql, $parameter);
        return $result;
    }

    public function agentGetList()
    {
        $sql = 'SELECT content, submit_time, module, title  FROM mdl_notice WHERE (user_type=0 || user_type = 2) AND (module = 0 ||  module = 2) ORDER BY submit_time DESC LIMIT 20';
        $result = $this->_db->query($sql, array());
        return $result;
    }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();

        if (isset($argument['module']) && -1 != $argument['module']) {
            $condition[] = ' AND module=?';
            $parameter[] = $argument['module'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM mdl_notice WHERE 1' . implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        $return['page_size'] = $argument['page_size'];
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM mdl_notice WHERE 1' . implode('', $condition);
        $sql .= ' ORDER BY notice_id DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    /**
     * [getModalAnnoucement 取得彈窗公告]
     *
     * @param integer $notice_id 公告編號
     * @return void
     */
    public function getModalAnnoucement($notice_id)
    {
        //$sql = 'SELECT home_notice_read_index FROM uc_member WHERE '
        $parameter = array();
        $uid = get_session_uid();

        $sql = 'SELECT notice_id,title,content,image_path,start_time
                FROM mdl_notice
                WHERE submit_time BETWEEN ? AND ?
                AND ( ( start_time <= NOW() AND stop_time >= NOW() ) OR
                      ( start_time IS NULL OR stop_time IS NULL )
                    )
                AND method = 2 AND notice_id > ?
                AND ( specified_method = 1   ';

        $date = date('Y-m-d H:i:s');
        $today = new DateTime($date);
        $today_form = $today->format('Y-m-d H:i:s');
        $day_ago = $today->modify('-1 day')->format('Y-m-d H:i:s');

        $parameter[] = $day_ago;
        $parameter[] = $today_form;
        $parameter[] = $notice_id;

        if ($uid) {
            $sql .= ' OR ( specified_method = 2 AND  specified_user_account = ? )';
            $member_handle = new Member();
            $member = $member_handle->checkLogin($uid);
            $parameter[] = $member['account'];
            unset($member_handle);
            unset($member);
        }
        $sql .= ' ) ';
        $sql .= 'ORDER BY  notice_id , sort ASC
                LIMIT 30';

        $result = $this->_db->query($sql, $parameter);
        // print_r($result);

        if (count($result) > 0 && $result) {
            return $result;
        }
        return -1;
    }
}
