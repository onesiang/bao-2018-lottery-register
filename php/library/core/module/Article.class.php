<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 优惠活动类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Article extends Wwin {
    
    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    public function add($argument) {
        if (!$argument['category']) {
            return -1;
        }
        if (!$argument['content']) {
            return -2;
        }
        $sql = 'INSERT INTO mdl_article SET category=?,title=?,content=?,submit_time=?';
        $parameter = array(
            $argument['category'], 
            $argument['title'], 
            $argument['content'], 
            date('Y-m-d H:i:s')
        );
        if ($this->_db->query($sql, $parameter)) {
            return 1;
        }
        else {
            return -3;
        }
    }
    
    public function edit($article_id, $argument) {
        if (!$argument['category']) {
            return -1;
        }
        if (!$argument['content']) {
            return -2;
        }
        $sql = 'UPDATE mdl_article SET category=?,title=?,content=? WHERE article_id=?';
        $parameter = array(
            $argument['category'], 
            $argument['title'], 
            $argument['content'], 
            $argument['article_id']
        );
        if ($this->_db->query($sql, $parameter)) {
            return 1;
        }
        else {
            return -3;
        }
    }
    
    public function get($article_id) {
        $sql = 'SELECT * FROM mdl_article WHERE article_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($article_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }
    
    public function delete($article_id) {
        $article = $this->get($article_id);
        if ($article) {
            $this->_db->delete('mdl_article', $article);
            $sql = 'DELETE FROM mdl_article WHERE article_id=? LIMIT 1';
            if ($this->_db->query($sql, array($article_id))) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return -1;
        }
    }
    
    public function getList($argument) {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }
        
        $condition = array();
        $parameter = array();
        if ($argument['category']) {
            $condition[] = ' AND category=?';
            $parameter[] = $argument['category'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM mdl_article WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        $return['total_page'] = ($return['total_page']) ? $return['total_page'] : 1;
        $return['page_size'] = $argument['page_size'];
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM mdl_article WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY article_id DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function getByCategory($category, $limit = 10, $type = 0) {
        $column = 'content';
        if ($type) {
            $column .= ',title';
        }
        $sql = 'SELECT ' . $column . ' FROM mdl_article WHERE category=?';
        $sql .= ' ORDER BY article_id DESC LIMIT ' . $limit;
        $result = $this->_db->query($sql, array($category));
        return $result;
    }
    
}