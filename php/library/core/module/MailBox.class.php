<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 信箱类
 *
 */
final class MailBox extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    private function getFields()
    {
        return ' a.message_id,a.title,a.content,a.parent_id,a.send_time,a.receiver_read,a.sender_read,a.sender_id,a.sender_nick,a.receiver_id,a.sender_category,a.receiver_category,a.reply_status,a.sender_delete,a.receiver_delete ';
    }

    /**
     * 是否有新消息
     */
    public function getMessageNew($member_id)
    {
        $sql = 'SELECT message_id FROM mdl_message WHERE ((sender_id=? AND sender_read=0) || (receiver_id=? AND receiver_read=0)) AND parent_id=0 AND sender_delete<=1 AND receiver_delete<=1';
        if ($this->_db->query($sql, array($member_id, $member_id))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getNewMessageTotal($member_id)
    {
        $sql = 'SELECT count(message_id) as total FROM mdl_message WHERE ((sender_id=? AND sender_read=0) || (receiver_id=? AND receiver_read=0)) AND parent_id=0 AND sender_delete<=1 AND receiver_delete<=1';
        $result = $this->_db->query($sql, array($member_id, $member_id));
        return $result[0];
    }

    /**
     * 信箱列表数据
     */
    public function getList($parameters)
    {
        global $g_page_size;
        if (!isset($parameters['page_size'])) {
            $page_size = $g_page_size['general'];
        } else {
            $page_size = $parameters['page_size'];
        }

        $member_id = isset($parameters['member_id']) ? floor($parameters['member_id']) : 0;
        $page = isset($parameters['page']) ? floor($parameters['page']) : 1;

        $condition = '((a.sender_id=? AND a.sender_delete<=1 AND a.sender_category=0) OR (a.receiver_id=? AND a.receiver_delete<=1 AND a.receiver_category=0)) AND a.parent_id = 0';
        $sql = 'SELECT COUNT(a.message_id) AS `total` FROM mdl_message as a WHERE ' . $condition;
        $result = $this->_db->query($sql, array($member_id, $member_id));
        $total = $result ? $result[0]['total'] : 0;

        $return = array();
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        $condition .= ' ORDER BY a.message_id DESC LIMIT ' . $limits;
        $sql = 'SELECT ' . $this->getFields() . ',DATE_FORMAT(a.send_time,"%Y-%m-%d") AS send_time FROM mdl_message as a WHERE ' . $condition;
        $result = $this->_db->query($sql, array($member_id, $member_id));
        $return['list'] = $result;

        return $return;
    }

    /***
     *  更改讀取信件的狀態
     * ** */
    public function updateReadStatus($message_id, $member_id)
    {
        if (is_array($message_id)) {
            foreach ($message_id as $value) {
                $sql = 'UPDATE `mdl_message` SET `receiver_read` = 1 WHERE `message_id` = ? AND `receiver_id` = ?';
                //$sql_concat = $sql_concat . ' ' . $sql;
                $result = $this->_db->query($sql, array($value, $member_id));
            }
            //print_r($sql_concat);

            //print_r($result);
        } else {
            $sql = 'UPDATE mdl_message
            SET receiver_read = 1
            WHERE message_id = ?
            AND receiver_id = ? ';
            $result = $this->_db->query($sql, array($message_id, $member_id));
        }
    }

    /**
     * 管理员取信箱列表数据
     */
    public function managerGetList($parameters)
    {
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $order_by = isset($parameters['order_by']) ? $parameters['order_by'] : 0;
        $sender_account = isset($parameters['sender_account']) ? $parameters['sender_account'] : '';
        $condition = ' a.parent_id=0';
        $on1 = '';
        $on2 = '';
        $params = array();
        if ($date_start) {
            $condition .= ' AND a.send_time >=?';
            $params[] = $date_start;
        }
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
            $condition .= ' AND a.send_time <=?';
            $params[] = $date_stop;
        }
        if (0 == $order_by || 1 == $order_by) {
            $condition .= ' AND a.sender_category=?';
            $params[] = $order_by;
        }
        if ($sender_account) {
            if (0 == $order_by) {
                $condition .= ' AND b.account=?';
                $params[] = $sender_account;
            } elseif (1 == $order_by) {
                $condition .= ' AND c.account=?';
                $params[] = $sender_account;
            } elseif (-1 == $order_by) {
                $condition .= ' AND (b.account=? OR c.account=?)';
                $params[] = $sender_account;
                $params[] = $sender_account;
            }
        }
        if (0 == $order_by) {
            $on1 = ' ON a.sender_id=b.member_id ';
            $on2 = ' ON a.receiver_id=c.manager_id ';
        } elseif (1 == $order_by) {
            $on1 = ' ON a.receiver_id=b.member_id ';
            $on2 = ' ON a.sender_id=c.manager_id ';
        }

        $sql_str = ' FROM mdl_message as a LEFT JOIN uc_member as b ' . $on1 . ' LEFT JOIN mng_manager as c ' . $on2 . ' WHERE ';
        $sql = 'SELECT COUNT(a.message_id) AS `total` ' . $sql_str . $condition;
        $result = $this->_db->query($sql, $params);
        $total = $result ? $result[0]['total'] : 0;

        $return = array();
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        $condition .= ' ORDER BY a.message_id DESC LIMIT ' . $limits;
        $sql = 'SELECT ' . $this->getFields() . ',b.account,c.account as m_account ' . $sql_str . $condition;
        $result = $this->_db->query($sql, $params);
        $return['list'] = $result;

        return $return;
    }

    /**
     * 玩家查看回复
     * @param $member_Id
     * @param $message_id
     */
    public function getReplyList($member_id, $message_id)
    {
        $sql = 'SELECT ' . $this->getFields() . ' FROM mdl_message as a WHERE (a.parent_id=? OR a.message_id=?) AND (a.sender_id=? OR a.receiver_id=?)';
        $sql .= ' ORDER BY a.message_id ASC';
        $result = $this->_db->query($sql, array($message_id, $message_id, $member_id, $member_id));
        if ($result) {
            $sql = '';
            if ($member_id == $result[0]['receiver_id']) {
                if (0 == $result[0]['receiver_read']) {
                    $sql = 'UPDATE mdl_message SET receiver_read=1 WHERE message_id=? AND receiver_id=?';
                }
            } elseif ($member_id == $result[0]['sender_id']) {
                if (0 == $result[0]['sender_read']) {
                    $sql = 'UPDATE mdl_message SET sender_read=1 WHERE message_id=? AND sender_id=?';
                }
            }
            if ($sql) {
                $this->_db->query($sql, array($message_id, $member_id));
            }
        }
        return $result;
    }

    /**
     * 管理员查看回复
     * @param $message_id
     */
    public function managerGetReplyList($message_id)
    {
        $sql = 'SELECT ' . $this->getFields() . ' FROM mdl_message as a WHERE a.message_id=? OR a.parent_id=? ';
        $sql .= ' ORDER BY a.message_id ASC';
        $result = $this->_db->query($sql, array($message_id, $message_id));
        $account = '';
        if ($result) {
            $sql = '';
            if (1 == $result[0]['receiver_category']) {
                $member_id = $result[0]['sender_id'];
                if (0 == $result[0]['receiver_read']) {
                    $sql = 'UPDATE mdl_message SET receiver_read=1 WHERE message_id=?';
                }
            } elseif (0 == $result[0]['receiver_category']) {
                $member_id = $result[0]['receiver_id'];
                if (0 == $result[0]['sender_read']) {
                    $sql = 'UPDATE mdl_message SET sender_read=1 WHERE message_id=?';
                }
            }
            if ($sql) {
                $this->_db->query($sql, array($message_id));
            }
            if ($member_id > 0) {
                $sql = 'SELECT account FROM uc_member WHERE member_id=?';
                $member = $this->_db->query($sql, array($member_id));
                $account = $member[0]['account'];
            }
        }
        return array(
            'list' => $result,
            'account' => $account,
        );
    }

    /**
     * 当前时间    格式Y-m-d H:i:s
     */
    private function getNowDateTime($time = 0)
    {
        $now_time = $time ? $time : get_bj_time();
        $now_date = date('Y-m-d H:i:s', $now_time);
        return $now_date;
    }

    /**
     * 新的主题
     * receiver/sender_category 0:玩家 1:管理员 2:代理
     */
    public function createMessage($member_id, $title, $content)
    {
        $sql = 'INSERT INTO mdl_message set sender_id=?, title=?, content=?, send_time=?, sender_read=1, receiver_read=0, parent_id=0, sender_category=0, receiver_category=1';
        $result = $this->_db->query($sql, array($member_id, $title, $content, $this->getNowDateTime()));
        return $result;
    }

    /**
     * 管理员发送新信件
     */
    public function managerCreateMessage($manager_id, $title, $content, $type, $reply_status, $account)
    {
        $now = $this->getNowDateTime();
        if (0 == $type) {
            if ($account) {
                $sql = 'SELECT account,member_id FROM uc_member WHERE account=?';
                $result = $this->_db->query($sql, array($account));
                if ($result) {
                    $sql = 'INSERT INTO mdl_message set receiver_id=?, sender_id=?,title=?, content=?, reply_status=?, send_time=?, sender_read=1, receiver_read=0, parent_id=0, sender_category=1, receiver_category=0';
                    $result = $this->_db->query($sql, array($result[0]['member_id'], $manager_id, $title, $content, $reply_status, $now));
                    if ($result) {
                        return true;
                    }
                }
            } else {
                $find_time = time() - 86400 * 30; // 30天内有登陆过的会员
                $find_time = $this->getNowDateTime($find_time);
                $sql = 'SELECT member_id FROM uc_member WHERE login_time > ?';
                $result = $this->_db->query($sql, array($find_time));
                foreach ($result as $value) {
                    $sql = 'INSERT INTO mdl_message set receiver_id=?, title=?, content=?, reply_status=?, send_time=?, sender_read=1, receiver_read=0, parent_id=0, sender_category=1, receiver_category=0';
                    $this->_db->query($sql, array($value['member_id'], $title, $content, $reply_status, $now));
                }
                return true;
            }
        }
        return false;
    }

    /**
     * [updateFatherReplyTime 更新父訊息的最後回復時間]
     *
     * @param integer $father_message_id
     * @return void
     */
    public function updateFatherReplyTime($father_message_id)
    {
        $sql = 'UPDATE `mdl_message` AS father_table
                        INNER JOIN
                            (SELECT parent_id ,max(`mdl_message`.send_time) as replay_time_last
                            FROM `mdl_message`
                            WHERE `parent_id` = ?
                            GROUP BY `parent_id`
                            ) child_table
                        ON father_table.message_id = child_table.parent_id
                        SET father_table.reply_time = child_table.replay_time_last';
        $result = $this->_db->query($sql, array($father_message_id));
    }

    /**
     * 回复
     */
    public function replyMessage($member_id, $message_id, $content)
    {
        $sql = 'SELECT sender_category,sender_nick,stop_time,send_type FROM mdl_message WHERE message_id=? AND (sender_id=? OR receiver_id=?) AND reply_status=1';
        $result = $this->_db->query($sql, array($message_id, $member_id, $member_id));
        if ($result) {
            $sender_category = $result[0]['sender_category'];
            $sender_nick = $result[0]['sender_nick'];
            $stop_time = $result[0]['stop_time'];
            $send_type = $result[0]['send_type'];
            $now = $this->getNowDateTime();
            $sql = 'INSERT INTO mdl_message set sender_id=?, receiver_id=0, content=?, parent_id=?, send_time=?, sender_category=0, receiver_category=1,sender_nick =  ?, stop_time = ?, send_type = ? ';
            $result = $this->_db->query($sql, array($member_id, $content, $message_id, $now, $sender_nick, $stop_time, $send_type));
            if ($result) {
                $sql = '';
                if (0 == $sender_category) {
                    $sql = 'UPDATE mdl_message set receiver_read=0 WHERE message_id=?';
                } else {
                    $sql = 'UPDATE mdl_message set sender_read=0 WHERE message_id=?';
                }
                if ($sql) {
                    $this->_db->query($sql, array($message_id));
                }
                $this->updateFatherReplyTime($message_id);
                return array(
                    'content' => $content,
                    'send_time' => $now,
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 管理员回复
     *
     */
    public function managerReplyMessage($manager_id, $message_id, $content)
    {
        $sql = 'SELECT sender_category,receiver_id,sender_id,sender_delete,receiver_delete FROM mdl_message WHERE message_id=?';
        $result = $this->_db->query($sql, array($message_id));
        if ($result && 0 == $result[0]['receiver_delete'] && 0 == $result[0]['sender_delete']) {
            $sender_category = $result[0]['sender_category'];
            if (1 == $sender_category) {
                $receiver_id = $result[0]['receiver_id'];
            } else {
                $receiver_id = $result[0]['sender_id'];
            }
            $now = $this->getNowDateTime();
            $sql = 'INSERT INTO mdl_message set sender_id=?, receiver_id=?, content=?, parent_id=?, send_time=?, sender_category=1, receiver_category=0';
            $result = $this->_db->query($sql, array($manager_id, $receiver_id, $content, $message_id, $now));
            if ($result) {
                $sql = '';
                if (1 == $sender_category) {
                    $sql = 'UPDATE mdl_message set receiver_read=0 WHERE message_id=?';
                } else {
                    $sql = 'UPDATE mdl_message set sender_read=0 WHERE message_id=?';
                }
                if ($sql) {
                    $this->_db->query($sql, array($message_id));
                }
                return array(
                    'content' => $content,
                    'send_time' => $now,
                );
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 删除
     */
    public function delMessage($member_id, $message_id)
    {
        $sql = 'SELECT sender_id,receiver_id FROM mdl_message WHERE message_id=? AND (sender_id=? OR receiver_id=?)';
        $result = $this->_db->query($sql, array($message_id, $member_id, $member_id));
        if ($result) {
            $sender_id = $result[0]['sender_id'];
            if ($sender_id == $member_id) {
                $sql = 'UPDATE mdl_message set sender_delete=2 WHERE message_id=?';
            } else {
                $sql = 'UPDATE mdl_message set receiver_delete=2 WHERE message_id=?';
            }
            $result = $this->_db->query($sql, array($message_id));
            return $result;
        } else {
            return false;
        }
    }

    public function managerCloseMessage($message_id)
    {
        $sql = 'UPDATE mdl_message SET sender_delete=1, receiver_delete=1 WHERE message_id=?';
        $result = $this->_db->query($sql, array($message_id));
        return $result;
    }

    public function untreated($argument)
    {
        $sql = 'SELECT count(*) AS quantity FROM mdl_message WHERE receiver_read = 0 AND sender_category = 0 AND  receiver_category = 1 AND sender_read = ?';

        $result = $this->_db->query($sql, array(1));

        return $result;
    }

    public function getManageNewMessageTotal()
    {
        $sql = 'SELECT COUNT(*) as total FROM `mdl_message` WHERE `sender_category` = 1 AND `sender_read` = 0 AND `parent_id` = 0';

        $result = $this->_db->query($sql);
        return $result[0];
    }
}
