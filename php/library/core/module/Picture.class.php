<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 图片类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Picture extends Wwin
{
    public $_oldpath;
    public $_oldpath_pc;

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function add($argument)
    {
        $sql = 'INSERT INTO mdl_picture SET name=?,path=?,description=?,link=?,fun=?';
        $sql .= ',display=?,sort=?,activity_id=?';
        $parameter = array($argument['name'], $argument['path'], $argument['description'],
         $argument['link'], $argument['fun'], $argument['display'], $argument['sort'], $argument['activity']);

        if ($argument['path_pc']){
            $sql .= ',path_pc=?';
            $parameter[] = $argument['path_pc'];
        }

        if ($this->_db->query($sql, $parameter)) {
            return 1;
        } else {
            @unlink(ROOT . DS . 'sport_official' . DS . 'mobile' . DS . 'images'. DS . $argument['path']);
            return 0;
        }
    }

    public function addqr($argument)
    {
        $sql = 'INSERT INTO qrcode_picture SET name=?,path=?,description=?,link=?';
        $sql .= ',display=?';
        $parameter = array($argument['name'], $argument['path'], $argument['description'],
         $argument['link'], $argument['display']);
        if ($this->_db->query($sql, $parameter)) {
            return 1;
        } else {
            @unlink(ROOT . DS . 'sport_official' . DS . 'mobile' . DS .'images' . DS . $argument['path']);
            return 0;
        }
    }

    public function get($picture_id)
    {
        $sql = 'SELECT * FROM mdl_picture WHERE picture_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($picture_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function getqr($picture_id)
    {
        $sql = 'SELECT * FROM qrcode_picture WHERE picture_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($picture_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function edit($picture_id, $argument)
    {
        $picture = $this->get($picture_id);

        $sql = 'UPDATE mdl_picture SET name=?,description=?,link=?,fun=?,display=?,sort=?,activity_id=?';
        $parameter = array($argument['name'], $argument['description'],
            $argument['link'], $argument['fun'], $argument['display'], $argument['sort'], $argument['activity']);

        if (isset($argument['path'])) {
            $sql .= ',path=?';
            $parameter[] = $argument['path'];
        }        

        if ($argument['path_pc']){
            $sql .= ',path_pc=?';
            $parameter[] = $argument['path_pc'];
        }

        $sql .= ' WHERE picture_id=? LIMIT 1';
        $parameter[] = $picture_id;
        if ($this->_db->query($sql, $parameter)) {
            if (isset($argument['path']) && $picture['path'] != $argument['path']) {
                @unlink(ROOT . DS . 'sport_official' . DS .'mobile' . DS .'images' . DS . $picture['path']);
            }
            $this->_oldpath = $picture['path'];
            if (isset($argument['path_pc']) && $picture['path_pc'] != $argument['path_pc']) {
                @unlink(ROOT . DS . 'sport_official' . DS .'mobile' . DS .'images' . DS . $picture['path_pc']);
            }
            $this->_oldpath_pc = $picture['path_pc'];
            return 1;
        } else {
            if (isset($argument['path'])) {
                @unlink(ROOT . DS . 'sport_official' . DS .'mobile' . DS .'images' . DS . $argument['path']);
            }
            $this->_oldpath = $picture['path'];
            if (isset($argument['path']) && $picture['path'] != $argument['path']) {
                @unlink(ROOT . DS . 'sport_official' . DS .'mobile' . DS .'images' . DS . $picture['path']);
            }
            $this->_oldpath = $picture['path'];
            return 0;
        }
    }

    public function qredit($picture_id, $argument)
    {
        $picture = $this->getqr($picture_id);

        $sql = 'UPDATE qrcode_picture SET name=?,description=?,link=?,display=? ';
        $parameter = array($argument['name'], $argument['description'],
            $argument['link'], $argument['display'],);
        if (isset($argument['path'])) {
            $sql .= ',path=?';
            $parameter[] = $argument['path'];
        }
        $sql .= ' WHERE picture_id=? LIMIT 1';
        $parameter[] = $picture_id;
        if ($this->_db->query($sql, $parameter)) {
            if (isset($argument['path']) && $picture['path'] != $argument['path']) {
                @unlink(ROOT . DS . 'sport_official' .
                        DS . 'mobile' .
                        DS . 'images' .
                        DS . $picture['path']);
            }
            $this->_oldpath = $picture['path'];
            return 1;
        } else {
            if (isset($argument['path'])) {
                @unlink(ROOT . DS . 'sport_official' . DS . 'mobile' . DS . 'images' . DS . $argument['path']);
            }
            $this->_oldpath = $picture['path'];
            return 0;
        }
    }

    public function qrdelete($picture_id)
    {
        $picture = $this->getqr($picture_id);
        if ($picture) {
            $this->_db->delete('qrcode_picture', $picture);

            @unlink(ROOT . DS . 'sport_official' .
                    DS . 'mobile' .
                    DS .'images'.
                    DS . $picture['path']);
            Log::record('delete picuture => ' . ROOT . DS . 'sport_official' .
            DS . 'mobile' .
            DS .'images'.
            DS . $picture['path'], 'error', 'upload');
            $sql = 'DELETE FROM qrcode_picture WHERE picture_id=? LIMIT 1';
            if ($this->_db->query($sql, array($picture_id))) {
                $this->_oldpath = $picture['path'];
                return 1;
            } else {
                $this->_oldpath = $picture['path'];
                return 0;
            }
        } else {
            return -1;
        }
    }

    public function delete($picture_id)
    {
        $picture = $this->get($picture_id);
        if ($picture) {
            $this->_db->delete('mdl_picture', $picture);

            @unlink(ROOT . DS . 'sport_official' . DS . 'mobile' . DS .'images' . DS . $picture['path']);

            $sql = 'DELETE FROM mdl_picture WHERE picture_id=? LIMIT 1';
            if ($this->_db->query($sql, array($picture_id))) {
                $this->_oldpath = $picture['path'];
                $this->_oldpath_pc = $picture['path_pc'];
                return 1;
            } else {
                $this->_oldpath = $picture['path'];
                $this->_oldpath_pc = $picture['path_pc'];
                return 0;
            }
        } else {
            return -1;
        }
    }

    public function showList()
    {
        $sql = 'SELECT path,description,link,fun,picture_id,activity_id,path_pc FROM mdl_picture WHERE display=1 ORDER BY sort';
        $result = $this->_db->query($sql);
        return $result;
    }

    public function qrshowList()
    {
        $sql = 'SELECT path,description,link FROM qrcode_picture WHERE display=1 ';
        $result = $this->_db->query($sql);
        return $result;
    }

    public function qrshowListwith($mobile)
    {
        $sql = 'SELECT * FROM qrcode_picture WHERE display=1 AND fun = ?';
        $result = $this->_db->query($sql, array( $mobile ));
        return $result;
    }

    public function getFunList()
    {
        $fun_list = array(
            0  => '无',
            // 1  => '跳转注册页面',
            // 2  => '跳转体育页面',
            // 3  => '跳转彩票页面',
            // 4  => '跳转真人页面',
            // 5  => '跳转电子页面',
            // 6  => '跳转代理页面',
            7  => '优惠活动',
            // 10 => '跳转AG大厅',
            // 11 => '跳转AG竞咪',
            // 12 => '跳转AG电子',
            // 13 => '跳转MG电子'
        );
        return $fun_list;
    }

    public function getqrFunList()
    {
        $fun_list = array(
            0  => '无',
            1  => '苹果 IOS',
            2  => '安卓 ANDROID'
        );
        return $fun_list;
    }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();
        if (isset($argument['display']) && '' !== $argument['display']) {
            $condition[] = ' AND display=?';
            $parameter[] = $argument['display'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM mdl_picture WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM mdl_picture WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function qrcode_picture($argument=null)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();
        if (isset($argument['display']) && '' !== $argument['display']) {
            $condition[] = ' AND display=?';
            $parameter[] = $argument['display'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM qrcode_picture WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM qrcode_picture WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);

        $return['list'] = $result;

        $return['argument'] = $argument;
        return $return;
    }
}
