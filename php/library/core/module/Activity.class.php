<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 优惠活动类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Activity extends Wwin {
    
    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    /**
     * [add 新增優惠公告]
     *
     * @param array $argument
     * @return void
     */
    public function add($argument) {
        if(!strrpos($argument['date_stop'], ':')) {
            $argument['date_stop'] = $argument['date_stop'] . " 00:00:00";
        }
        if(!strrpos($argument['date_stop'], ':')) {
            $argument['date_stop'] = $argument['date_stop'] . " 23:59:59";
        }

        $insert_col = '';
        $parameter = array($argument['name'], $argument['activity_type'], 
                           $argument['activity_member'], $argument['description'], 
                           $argument['small_image'], $argument['display'], 
                           $argument['sort'], $argument['date_start'], 
                           $argument['date_stop']);
        
        if ($argument['big_image'] && isset($argument['big_image'])) {
            $insert_col .= ' ,big_image =? ';
            $parameter[] = $argument['big_image'];
        }
  
        $sql .= ' INSERT INTO mdl_activity';
        $sql .= ' SET name =?, activity_type =?, activity_member =?,';
        $sql .= ' description = ?, small_image = ?, display = ?,sort =?,';
        $sql .= ' date_start = ?, date_stop = ? ' . $insert_col;
        
         if ($this->_db->query($sql, $parameter)) {
            return 1;
        }
        else {
            @unlink(ROOT . DS . 'home' . DS . $argument['big_image']);
            @unlink(ROOT . DS . 'home' . DS . $argument['small_image']);
            return 0;
        }
    }
    
    public function get($device = null) {
        $date = date('Y-m-d h:i:s');
        $sql = "SELECT * FROM mdl_activity WHERE (activity_type=? OR activity_type=?) AND display=1 AND date_start<=? AND date_stop>=? ORDER BY `sort` ASC";
        $result = $this->_db->query($sql,array($device, 'A', $date, $date));
        return $result;
    }

    public function getDetail($activity_id){
        $date = date('Y-m-d h:i:s');        
        $sql = 'SELECT * FROM mdl_activity WHERE activity_id=? AND date_start<=? AND date_stop>=? LIMIT 1';
        $result = $this->_db->query($sql, array($activity_id, $date, $date));
        if ($result) {
            $result = $result[0];
        }
        return $result;        
    }

    public function getData($activity_id){
        $sql = 'SELECT * FROM mdl_activity WHERE activity_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($activity_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;        
    }

    public function managerGet($activity_id) {
        $sql = 'SELECT * FROM mdl_activity WHERE activity_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($activity_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    /**
     * [edit 編輯優惠公告]
     *
     * @param integer $activity_id
     * @param array $argument
     * @return void
     */
    public function edit($activity_id, $argument) {
        $activity = $this->getData($activity_id);

        if (!$argument['date_start']){
            $argument['date_start'] = date('Y-m-d') . ' 00:00:00';
        }
        if (!$argument['date_stop']){
            $argument['date_stop'] = date('Y-m-d') . ' 23:59:59';
        }

        if(!strrpos($argument['date_start'], ':')) {
            $argument['date_start'] = $argument['date_start'] . " 00:00:00";
        }
        if(!strrpos($argument['date_stop'], ':')) {
            $argument['date_stop'] = $argument['date_stop'] . " 23:59:59";
        }
        
        $sql = 'UPDATE mdl_activity SET name=?,activity_type=?,activity_member=?,description=?,display=?,sort=?,date_start=?,date_stop=?';
        $parameter = array($argument['name'], $argument['activity_type'], $argument['activity_member'], $argument['description'],
            $argument['display'], $argument['sort'], $argument['date_start'], $argument['date_stop']);

        if (isset($argument['big_image'])) {
            $sql .= ',big_image=?';
            $parameter[] = $argument['big_image'];
            
            if ($activity['big_image']) {
                $azurestorage_handle = new AzureStorage();
                list($contariner, $blob_big_image) = explode('/', $activity['big_image']);
                $azurestorage_handle->delete_blob($blob_big_image);
                unset($azurestorage_handle);
            }
           
        }
        if (isset($argument['small_image'])) {
            $sql .= ',small_image=?';
            $parameter[] = $argument['small_image'];

            if($activity['small_image']) {
                $azurestorage_handle = new AzureStorage();
                list($contariner2, $blob_small_image) = explode('/', $activity['small_image']);
                $azurestorage_handle->delete_blob($blob_small_image);
                unset($azurestorage_handle);         
            }     
        }
        $sql .= ' WHERE activity_id=? LIMIT 1';
        $parameter[] = $activity_id;

        if ($this->_db->query($sql, $parameter)) {
            if (isset($argument['big_image']) && $activity['big_image'] != $argument['big_image']) {
                @unlink(ROOT . DS . 'home' . DS . $activity['big_image']);
            }
            if (isset($argument['small_image']) && $activity['small_image'] != $argument['small_image']) {
                @unlink(ROOT . DS . 'home' . DS . $activity['small_image']);
            }
            return 1;
        }
        else {            
            if (isset($argument['big_image'])) {
                @unlink(ROOT . DS . 'home' . DS . $argument['big_image']);
            }
            if (isset($argument['small_image'])) {
                @unlink(ROOT . DS . 'home' . DS . $argument['small_image']);
            }
            return 0;
        }
    }
    
    /**
     * [delete 刪除優惠公告]
     *
     * @param integer $activity_id 優惠公告編號
     * @return void
     */
    public function delete($activity_id) {
        $activity = $this->getData($activity_id);
        if ($activity) {
            // $this->_db->delete('mdl_activity', $activity);
            
            unlink(ROOT . DS . 'home' . DS . $activity['big_image']);
            unlink(ROOT . DS . 'home' . DS . $activity['small_image']);
            $sql = 'DELETE FROM mdl_activity WHERE activity_id=? ';
            if ($this->_db->query($sql, array($activity_id))) {
                
                $azurestorage_handle = new AzureStorage();
                if ($activity['big_image']) {
                    list($contariner, $blob_big_image) = explode('/', $activity['big_image']);
                    $azurestorage_handle->delete_blob($blob_big_image);
                }
                if ($activity['small_image']) {
                    list($contariner2, $blob_small_image) = explode('/', $activity['small_image']);
                    $azurestorage_handle->delete_blob($blob_small_image);
                }
                unset($azurestorage_handle);
               
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return -1;
        }
    }
    
    public function showList() {
        $sql = 'SELECT * FROM mdl_activity WHERE display=1 ORDER BY sort';
        $result = $this->_db->query($sql);
        return $result;
    }
    
    public function getList($argument) {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }
		if($argument['page_size']==0){$argument['page_size']=20;}
        $condition = array();
        $parameter = array();
        if (isset($argument['display']) && '' !== $argument['display']) {
            $condition[] = ' AND display=?';
            $parameter[] = $argument['display'];
        }
		
		 if (isset($argument['activity_type']) && '' !== $argument['activity_type']) {
            $condition[] = ' AND activity_type=?';
            $parameter[] = $argument['activity_type'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM mdl_activity WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM mdl_activity WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' ORDER BY sort';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }
}