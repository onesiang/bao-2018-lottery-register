<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 *
 * @package Wwin
 * @final
 */
final class Domain extends Wwin {
    
    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    public function getList($argument) {
        $category = isset($argument['category']) ? $argument['category'] : 0;
        $status   = isset($argument['status']) ? $argument['status'] : -1;
        $params = array();
        $condition = '';
        if($category) {
            $condition .= ' AND category=? ';
            $params[]  = $category;
        }
        if($status >= 0) {
            $condition .= ' AND status=? ';
            $params[]  = $status;
        }
        $sql = 'SELECT * FROM mdl_domain WHERE 1 ' . $condition;
        $result = $this->_db->query($sql, $params);
        return $result;
    }
    
    public function addDomain($domain) {
        $flag   = 0;
        $now_date = date('Y-m-d H:i:s');
        
        $result = 0;
        $this->_db->beginTransaction();
        foreach($domain as $value) {
            $sql = 'SELECT domain_id FROM mdl_domain WHERE content=?';
            $is_have = $this->_db->query($sql, array($value[1]));
            if(!$is_have) {
                $sql = 'INSERT INTO mdl_domain SET category=?,content=?,submit_time=?,update_time=?,status=? ';
                $result = $this->_db->query($sql, array($value[0], $value[1], $now_date, $now_date, 1));
                if(!$result) {
                    break;
                }
            }
        }
        
        if($result) {
            $flag = 1;
            $this->_db->commit();
        }
        else {
            $this->_db->rollBack();
        }
        return $flag;
    }
    
    public function updateStatus($domain_id, $status) {
        $sql    = 'UPDATE mdl_domain SET status=?,update_time=? WHERE domain_id=?';
        $result = $this->_db->query($sql, array($status, date('Y-m-d H:i:s'), $domain_id));
        return $result;
    }
    
}