<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 优惠活动类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class ArticleCategory extends Wwin {
    
    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    public function add($name) {
        if (!$name) {
            return -1;
        }
        $sql = 'SELECT * FROM mdl_article_category WHERE name=?';
        $result = $this->_db->query($sql, array($name));
        if ($result) {
            return -2;
        }
        $sql = 'INSERT INTO mdl_article_category SET name=?';
        if ($this->_db->query($sql, array($name))) {
            return 1;
        }
        else {
            return -3;
        }
    }
    
    public function edit($category_id, $name) {
        if (!$name) {
            return -1;
        }
        $sql = 'SELECT * FROM mdl_article_category WHERE name=? AND category_id!=?';
        $result = $this->_db->query($sql, array($name, $category_id));
        if ($result) {
            return -2;
        }
        $sql = 'UPDATE mdl_article_category SET name=? WHERE category_id=?';
        if ($this->_db->query($sql, array($name))) {
            return 1;
        }
        else {
            return -3;
        }
    }
    
    public function get($category_id) {
        $sql = 'SELECT * FROM mdl_article_category WHERE category_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($category_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }
    
    public function delete($category_id) {
        $category = $this->get($category_id);
        if ($category) {
            $this->_db->delete('mdl_article_category', $category);
            $sql = 'DELETE FROM mdl_article_category WHERE category_id=? LIMIT 1';
            if ($this->_db->query($sql, array($category_id))) {
                return 1;
            }
            else {
                return 0;
            }
        }
        else {
            return -1;
        }
    }
    
    public function getList($argument) {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM mdl_article_category WHERE 1';
        $result = $this->_db->query($sql);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        $return['total_page'] = ($return['total_page']) ? $return['total_page'] : 1;
        $return['page_size'] = $argument['page_size'];
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM mdl_article_category WHERE 1';
        $sql .= ' ORDER BY category_id DESC';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }
    
    public function select($name, $default = '', $style = '') {
        $sql = 'SELECT * FROM mdl_article_category ORDER BY category_id DESC';
        $result = $this->_db->query($sql);
        $content = array();
        if ($style) {
            $style = ' class="' . $style . '"';
        }
        $content[] = '<select id="' . $name . '" name="' . $name . '"' . $style . '>';
        $content[] = '<option value="0">请选择</option>';
        if ($result) {
            foreach ($result as $row) {
                $content[] = '<option value="' . $row['category_id'] . '"';
                if ($row['category_id'] == $default) {
                    $content[] = ' selected';
                }
                $content[] = '>' . $row['name'] . '</option>';
            }
        }
        $content[] = '</select>';
        return implode('', $content);
    }
    
    public function map() {
        $sql = 'SELECT category_id,name FROM mdl_article_category';
        $result = $this->_db->query($sql);
        $map = array();
        if ($result) {
            foreach ($result as $row) {
                $map[$row['category_id']] = $row['name'];
            }
        }
        return $map;
    }
    
}