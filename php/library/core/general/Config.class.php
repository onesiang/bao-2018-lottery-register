<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 配置类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Config extends Wwin
{
    private $hash = CUSTOMER_NAME . '_config';

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * getAll
     * 获取全部配置
     *
     * @access public
     * @return array
     */
    public function getAll()
    {
        $list = array();
        $sql = 'SELECT item,value FROM g_config';
        $result = $this->_db->query($sql);
        if ($result) {
            foreach ($result as $row) {
                $list[$row['item']] = $row['value'];
            }
        }
        return $list;
    }

    /**
     * getSiteStatus
     * 获取站点状态
     *
     * @access public
     * @return int
     */
    public function getSiteStatus($type)
    {
        $status = 1;
        /**
         * 系统时间为美东时间
         * 数据库中的维护时间为北京时间
         * 两者时差为12小时
         */
        $time = get_bj_time();

        // 先检查本站配置
        $sql = 'SELECT item,value FROM g_config WHERE item IN ("MAINTAIN_START","MAINTAIN_STOP")';
        $result = $this->_db->query($sql);
        $maintain = change_array_index($result, 'item');
        if ($maintain['MAINTAIN_START']['value'] && $maintain['MAINTAIN_STOP']['value']) {
            $maintain_start = explode(',', $maintain['MAINTAIN_START']['value']);
            $maintain_stop = explode(',', $maintain['MAINTAIN_STOP']['value']);
            foreach ($type as $index) {
                if (!empty($maintain_start[$index]) && !empty($maintain_stop[$index]) && strtotime($maintain_start[$index]) <= $time && strtotime($maintain_stop[$index]) >= $time) {
                    $status = 0;
                }
            }
        }
        return $status;
    }

    /**
     * getByName
     * 通过指定名称获取配置
     *
     * @access public
     * @param mixed $name 名称，多个名称以数组传递
     * @return string|array 单个名称直接返回值，多个名称返回数组，名称为键名。
     */
    public function getByName($name)
    {
        if (!is_array($name)) {
            $name = explode(',', $name);
        }
        $redis_result = $this->get_cache_by_name($name);
        if ($redis_result) {
            return $redis_result;
        }
        $result = null;
        if (is_array($name) && $name) {
            $sql_mark = str_repeat('?,', count($name) - 1) . '?';
            $sql = 'SELECT item,value FROM g_config WHERE item IN (' . $sql_mark . ')';
            $result = $this->_db->query($sql, $name);
            if ($result) {
                $result = change_array_index($result, 'item');
                foreach ($result as $key => $value) {
                    $result[$key] = $value['value'];
                }
            }
        } elseif (is_string($name)) {
            $sql = 'SELECT value FROM g_config WHERE item=?';
            $result = $this->_db->query($sql, array($name));
            if ($result) {
                $result = $result[0]['value'];
            }
        }
        return $result;
    }

    /**
     * [get_cache_by_name get redis data by name]
     * @param  array  $name [redis keys]
     * @return array        [list]
     */
    public function get_cache_by_name($name = array())
    {
        $this->check_cache_exists_by_hash();
        return $this->_cache->hMGet($this->hash, $name);
    }

    public function get_cache_by_all($name)
    {
        return $this->_cache->hGetAll($name);
    }

    /**
     * [check_cache_exists_by_hasg ,if redis data not exist,then push config to redis from db]
     * @return null
     */
    public function check_cache_exists_by_hash()
    {
        if (!$this->_cache->exists($this->hash)) {
            $this->db_to_cache();
        }
    }

    /**
     * [db_to_cache update all config data to redis from db]
     * @return [null] [description]
     */
    private function db_to_cache()
    {
        $this->update_cache_by_name($this->getAll());
    }

    public function getDetailByItem($item)
    {
        $sql = 'SELECT * FROM g_config WHERE item=?';
        $result = $this->_db->query($sql, array($item));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function registerOptions($register_options_value)
    {
        $register_options = explode(',', $register_options_value);
        ;
        return $register_options;
    }

    public function withdrawOptions($register_options_value=null)
    {
        if (!$register_options_value) {
            $this->addConfig('WITHDRAW_OPTIONS', '会员提款选填项目', '0,0,0,0', 'mobile,email,qq,wx  0不填1可选2必填');
            $register_options_value = '0,0,0,0';
        }
        $register_options = explode(',', $register_options_value);
        ;
        return $register_options;
    }

    public function getCasinoSortData($casino_setting)
    {
        $casino_setting = explode('|', $casino_setting);
        $casino_data = array();
        foreach ($casino_setting as $key => $value) {
            $casino_data[] = explode(',', $value);
        }
        $sorts = array();
        foreach ($casino_data as $row) {
            $sorts[] = $row[1];
        }
        array_multisort($sorts, SORT_ASC, $casino_data);
        return $casino_data;
    }

    public function getCasinoData($casino_setting)
    {
        $casino_setting = explode('|', $casino_setting);
        $casino_data = array();
        foreach ($casino_setting as $key => $value) {
            $one_data = explode(',', $value);
            $casino_data[$one_data[0]] = $one_data;
        }

        return $casino_data;
    }

    public function addConfig($item, $name, $value, $description = '')
    {
        $sql = 'INSERT INTO g_config SET item=?,name=?,value=?,description=? ';
        $result = $this->_db->query($sql, array($item, $name, $value, $description));
        return $result;
    }

    /**
     * updateByName
     * 通过名称更新配置
     *
     * @access public
     * @param array $config 配置
     * <code>
     * $config = array(
     *     name1 => value1,
     *     name2 => value2
     * );
     * </code>
     * @return mixed 无错时返回1，否则返回出错的配置项
     */
    public function updateByName($config)
    {
        $db_return = $this->update_db_by_name($config);
        $cache_return = $this->update_cache_by_name($config);

        if ($db_return and $ache_return) {
            return 1;
        } else {
            return array_merge($db_return, $cache_return);
        }
    }

    /**
     * [update_db_by_name update config data by name]
     * @param  array $config [post config]
     * @return mixed         [description]
     */
    private function update_db_by_name($config)
    {
        $error = array();
        if (is_array($config) && $config) {
            foreach ($config as $key => $value) {
                $sql = 'UPDATE g_config SET value=?, update_time = ?  WHERE item=? LIMIT 1';
                if (!$this->_db->query($sql, array($value,date('Y-m-d H:i:s'), $key))) {
                    $error[] = $key . ' => ' . $value;
                }
            }
        }

        if (!$error) {
            return 1;
        } else {
            return $error;
        }
    }

    /**
     * [update_cache_by_name description]
     * @param  array $config [post config]
     * @return mixed         [description]
     */
    private function update_cache_by_name($config)
    {
        $error = array();
        if (is_array($config) && $config) {
            $values = array();
            $error[] = $this->_cache->hMset($this->hash, $config);
        }

        if (!$error) {
            return 1;
        } else {
            return $error;
        }
    }

    public function getMaintaince()
    {
        $config = $this->get_cache_by_name(array('MAINTAIN_START', 'MAINTAIN_STOP', 'MAINTAIN_TOKEN', 'MAINTAIN_MEMBER_LEVEL'));
        $start_time = explode(',', $config['MAINTAIN_START']);
        $stop_time = explode(',', $config['MAINTAIN_STOP']);
        $token = explode(',', $config['MAINTAIN_TOKEN']);
        $member_level = explode(',', $config['MAINTAIN_MEMBER_LEVEL']);
        $now = get_bj_time();
        $data = array();

        for ($i=0; $i<count($start_time); $i++) {
            $one_start_time = isset($start_time[$i]) ? $start_time[$i] : '';
            $one_stop_time = isset($stop_time[$i]) ? $stop_time[$i] : '';
            $one_token = isset($token[$i]) ? $token[$i] : '';
            $one_member_level = isset($member_level[$i]) ? $member_level[$i] : '';
            $maintenance = ($now > strtotime($one_start_time) && $now < strtotime($one_stop_time)) ? 1 : 0;
            $data[] = array(
                'start_time'  => $one_start_time,
                'stop_time'   => $one_stop_time,
                'maintenance' => $maintenance,
                'token' => $one_token,
                'member_level' => $one_member_level,
            );
        }
        return $data;
    }

    public function getLotteryMaintaince($lottery_maintain, $lottery = 0)
    {
        $lottery_maintain = $lottery_maintain;
        $lottery_maintain_data = array();
        if ($lottery_maintain) {
            $lottery_maintain = explode(',', $lottery_maintain);
            foreach ($lottery_maintain as $row) {
                $one = explode(':', $row);
                $lottery_maintain_data[$one[0]] = $one[1];
            }
        }
        if ($lottery) {
            if ($lottery_maintain_data && isset($lottery_maintain_data[$lottery]) && $lottery_maintain_data[$lottery]) {
                $lottery_maintain = 1;
            } else {
                $lottery_maintain = 0;
            }
            return $lottery_maintain;
        }
        return $lottery_maintain_data;
    }

    public function updateConfig($config)
    {
        $error = array();
        if (is_array($config) && $config) {
            foreach ($config as $key => $value) {
                $sql = 'UPDATE g_config SET value=? WHERE item=? LIMIT 1';
                $this->_db->query($sql, array($value, $key));
            }
        }
        return 1;
    }

    public function updateMaintaince($content)
    {
        if (!$content) {
            return false;
        }
        if (!is_array($content)) {
            $content = json_decode($content, true);
        }
        if (time() - $content['time'] > 30) {
            return false;
        }
        $content['data'] = json_decode($content['data'], true);
        if (isset($content['data']['start']) && isset($content['data']['stop'])) {
            $start = date('Y-m-d H:i:s', strtotime($content['data']['start']));
            $stop = date('Y-m-d H:i:s', strtotime($content['data']['stop']));
            $config_handle = new Config();
            $config_data   = $config_handle->getByName(array('MAINTAIN_START', 'MAINTAIN_STOP'));
            $config_maintain_start = explode(',', $config_data['MAINTAIN_START']);
            $config_maintain_stop  = explode(',', $config_data['MAINTAIN_STOP']);
            for ($i = 0; $i < 5; $i++) {
                if (!isset($config_maintain_start[$i])) {
                    $config_maintain_start[$i] = '';
                }
                if (!isset($config_maintain_stop[$i])) {
                    $config_maintain_stop[$i] = '';
                }
            }
            $config_maintain_start[1] = $start;
            $config_maintain_stop[1]  = $stop;
            $config_maintain_start = implode(',', $config_maintain_start);
            $config_maintain_stop  = implode(',', $config_maintain_stop);
            $config = array(
                'MAINTAIN_START' => $config_maintain_start,
                'MAINTAIN_STOP'  => $config_maintain_stop
            );
        } else {
            Log::record(json_encode($content), 'info', 'maintaince_');
        }
        return $this->updateByName($config);
    }

    /**
     * [getConfig] 取得g_config的資料
     *
     * @param string $name g_config裡的item名稱
     * @return void
     */
    public function getConfig($name)
    {
        $sql = ' SELECT value,update_time FROM g_config WHERE item = ? ';
        $result = $this->_db->query($sql, array($name));
        return $result;
    }


    /**
     *[getLotteryList] 取得欲要显示的彩種
     *
     * @return array
     */
    public function getLotteryList()
    {
        $sql = 'SELECT category, active from ltt_list';
        $result = $this->_db->query($sql);
        return $result;
    }
}
