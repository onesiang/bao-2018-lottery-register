<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 站点信息类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Site extends Wwin {

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    public function get() {
        $sql = 'SELECT * FROM g_site LIMIT 1';

        $result = $this->_db->query($sql);
        if ($result) {
            $result = $result[0];
        }
        //echo $result;
        return $result;
    }
    
    public function checkModule($module) {
        $site = $this->get();
        if (isset($site[$module]) && $site[$module]) {
            return 1;
        }
        else {
            return 0;
        }
    }
    
    public function checkExpire() {

        $site = $this->get();
       //return 1;
        if (strtotime($site['expire']) < time()) {
            return 0;
        }
        else {
            return 1;
        }
    }
    
    public function updateExpire($expire) {
        $sql = 'UPDATE g_site SET expire=?';
        $result = $this->_db->query($sql, array($expire));
        return $result;
    }
}