<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 操作日志类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class OperateLog extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * add
     * 添加操作日志
     *
     * @access public
     * @param array $argument 操作项
     * <code>
     * -- manager_account   管理员帐号
     * -- account      被操作帐号
     * -- account_type 被操作身份类型  1管理员 2代理 3会员
     * -- amount       操作金额变化
     * -- module       模块(manager user finance notice)
     * -- argument     变动的参数
     * -- detail       文字详细
     * </code>
     * @return bool
     */
    public function add($parameters)
    {
        $manager_account = isset($parameters['manager_account']) ? $parameters['manager_account'] : 0;
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $account_type = isset($parameters['account_type']) ? $parameters['account_type'] : 0;
        $amount = isset($parameters['amount ']) ? $parameters['amount '] : 0;
        $module = isset($parameters['module']) ? $parameters['module'] : '';
        $argument = isset($parameters['argument']) ? $parameters['argument'] : '';
        $detail = isset($parameters['detail']) ? $parameters['detail'] : '';
        $submit_time = date('Y-m-d H:i:s');
        $submit_ip = get_client_ip();
        $data = array(
            'argument' => $argument,
            'detail' => $detail,
        );
        $data = json_encode($data);
        $set = ' manager_account=?, account=?, account_type=?, amount=?, module=?, data=?, submit_time=?, submit_ip=? ';
        $sql = 'INSERT INTO mng_operate_log SET ' . $set;
        $result = $this->_db->query($sql, array($manager_account, $account, $account_type, $amount, $module, $data, $submit_time, $submit_ip));
        return $result;
    }

    /**
     * [getListByManager 以manager來獲取數據,暫時放棄不用因為group_concat_limit的問題]
     * @param  [array] $parameters [description]
     * @return [array]             [description]
     */
    public function getListByManager($parameters)
    {
        // 過濾條件
//        $manager_account = isset($parameters['operate_account']) ? $parameters['operate_account'] : '';
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $account_type = isset($parameters['account_type']) ? $parameters['account_type'] : 0;
        $module = isset($parameters['module']) ? $parameters['module'] : '';
        $start_time = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;
        $params = array();
        $where = '';
        if ($manager_account) {
            $where .= ' AND manager_account=?';
            $params[] = $manager_account;
        }
        if ($account) {
            $where .= ' AND account=?';
            $params[] = $account;
        }
        if ($account_type) {
            $where .= ' AND account_type=?';
            $params[] = $account_type;
        }
        if ($module) {
            $where .= ' AND module=?';
            $params[] = $module;
        }
        if ($start_time) {
            $where .= ' AND submit_time>?';
            $params[] = $start_time;
        }
        if ($stop_time) {
            $where .= ' AND submit_time<=?';
            $params[] = $stop_time . ' 23:59:59';
        }

        // 計算manager_account總數
        $sql = 'SELECT COUNT(DISTINCT manager_account) AS `total` FROM mng_operate_log  WHERE 1 ' . $where;
        // echo $sql.'/n';
        $result = $this->_db->query($sql, $params);
        $total = $result ? $result[0]['total'] : 0;

        $return = array();
        // 計算頁數
        $return['total_row'] = $total;
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;
        // 輸出正式資料
        // SET SESSION group_concat_max_len = 1000000;
        // SET @@group_concat_max_len = value_numeric;
        //substring_index(group_concat(data separator '**') as data, ',', 1000)
        $sql = "SELECT manager_account, group_concat(data separator '**') as data FROM mng_operate_log" .
            ' WHERE 1 ' . $where .
            ' GROUP BY manager_account ORDER BY manager_account DESC LIMIT ' . $limits;
        $result = $this->_db->query($sql, $params);
        $return['list'] = $result;

        return $return;
    }

    /**
     * [getList 獲取營運紀錄]
     * @param  [type] $parameters [description]
     * @return [array]             [description]
     */
    public function getList($parameters)
    {
        $manager_account = isset($parameters['operate_account']) ? $parameters['operate_account'] : '';
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $account_type = isset($parameters['account_type']) ? $parameters['account_type'] : 0;
        $module = isset($parameters['module ']) ? $parameters['module '] : '';
        $start_time = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;

        $params = array();
        $where = '';

        if ($manager_account) {
            if (isset($parameters['fuzzy']) && $parameters['fuzzy']) {
                $where .= ' AND manager_account Like ?';
                $params[] = '%' . $manager_account . '%';
            } else {
                $where .= ' AND manager_account=?';
                $params[] = $manager_account;
            }
        }
        if ($account) {
            if (isset($parameters['fuzzy']) && $parameters['fuzzy']) {
                $where .= ' AND account Like ?';
                $params[] = '%' . $account . '%';
            } else {
                $where .= ' AND account=?';
                $params[] = $account;
            }
        }
        if ($account_type) {
            $where .= ' AND account_type=?';
            $params[] = $account_type;
        }
        if ($module) {
            $where .= ' AND module=?';
            $params[] = $module;
        }
        if ($start_time) {
            $where .= ' AND submit_time>?';
            $params[] = $start_time;
        }
        if ($stop_time) {
            $where .= ' AND submit_time<=?';
            $params[] = $stop_time . ' 23:59:59';
        }
        $sql = 'SELECT COUNT(log_id) AS `total` FROM mng_operate_log  WHERE 1 ' . $where;

        $result = $this->_db->query($sql, $params);
        $total = $result ? $result[0]['total'] : 0;

        $return = array();
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        $sql = 'SELECT * FROM mng_operate_log WHERE 1 ' . $where . ' ORDER BY log_id DESC LIMIT ' . $limits;
        $result = $this->_db->query($sql, $params);
        $return['list'] = $result;

        return $return;
    }

    public function getListNew($parameters)
    {
        $manager_account = isset($parameters['operate_account']) ? $parameters['operate_account'] : '';
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $account_type = isset($parameters['account_type']) ? $parameters['account_type'] : 0;
        $module = isset($parameters['module ']) ? $parameters['module '] : '';
        $start_time = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;

        $params = array();
        $where = '';
        /*if($operate_account) {
        $handle  = new Manager();
        $manager = $handle->getByAccount($operate_account, 'manager_id');
        $opt_manager_id = $manager['manager_id'];
        $where    = ' AND a.manager_id=?';
        $params[] = $opt_manager_id;
        unset($handle, $manager);
        }*/
        if ($manager_account) {
            $where .= ' AND manager_account=?';
            $params[] = $manager_account;
        }
        if ($account) {
            if (isset($parameters['fuzzy']) && $parameters['fuzzy']) {
                $where .= ' AND account Like ?';
                $params[] = '%'.$account.'%';
            } else {
                $where .= ' AND account=?';
                $params[] = $account;
            }
        }
        if ($account_type) {
            $where .= ' AND account_type=?';
            $params[] = $account_type;
        }
        if ($module) {
            $where .= ' AND module=?';
            $params[] = $module;
        }
        if ($start_time) {
            $where .= ' AND submit_time>?';
            $params[] = $start_time. ' 00:00:00';
        }
        if ($stop_time) {
            $where .= ' AND submit_time<=?';
            $params[] = $stop_time . ' 23:59:59';
        }
        $sql = 'SELECT COUNT(log_id) AS `total` FROM mng_operate_log  WHERE 1 ' . $where;
        $result = $this->_db->query($sql, $params);
        $total = $result ? $result[0]['total'] : 0;

        $return = array();
        $return['total_page'] = ceil($total / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        $sql = 'SELECT *   FROM mng_operate_log ' .
        '   WHERE 1 ' . $where .
        ' ORDER BY log_id DESC LIMIT ' . $limits;
        $result = $this->_db->query($sql, $params);
        $return['list'] = $result;

        return $return;
    }
}
