<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

final class Gdklsf extends Lottery {
        
    function __construct($lottery = 0) {
        parent::__construct();
        
        $lottery = $lottery > 0 ? $lottery : 11;
        $this->lottery_type = $lottery; // 广东快乐十分
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers) {
        //  0输  1赢  2和
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );

        $this->settleOneBet($bet, $win);
    }
	
	protected function methods2($bet, $numbers) {
        //  0输  1赢
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );
		return $win;
		
    }
    
    /**
     * 采集结果 号码位数
     * 
     */
    protected function getNumberCount() {
        return 8;
    }
    
    /**
     * 玩法: 单球小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun3($bet, $nums) {
        $val = $this->checkBig($nums[$bet['position'] - 1], 11);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 单球尾小尾大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun4($bet, $nums) {
        $val = $this->checkEndBig($nums[$bet['position'] - 1], 5);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 单球 中发白
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun5($bet, $nums) {
        $val = $this->getZFB($nums[$bet['position'] - 1]);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 单球 东南西北
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun6($bet, $nums) {
        $val = $this->getDNXB($nums[$bet['position'] - 1]);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 总和 单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun7($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 总和 小大和
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun8($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->checkBigTieSmall($sum, 84);
        $win = $val == 3 ? 2 : ($bet['number'] == $val ? 1 : 0);
        return $win;
    }
    
    /**
     * 总和 尾小 尾大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun9($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->checkEndBig($sum, 5);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 龙虎
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun10($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->dragonTiger($nums[0], $nums[7]);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 中发白
     * 
     */
    protected function getZFB($number) {
        $val = 0;
        if($number >= 1 && $number <= 7) {
            $val = 1;
        }
        else if($number >= 8 && $number <= 14) {
            $val = 2;
        }
        else if($number >= 15 && $number <= 20) {
            $val = 3;
        }
        return $val;
    }
    
    /**
     * 东南西北
     * 
     */
    protected function getDNXB($num) {
        $arr = array(
            1  => 1,
            5  => 1,
            9  => 1,
            13 => 1,
            17 => 1,
            2  => 2,
            6  => 2,
            10 => 2,
            14 => 2,
            18 => 2,
            3  => 3,
            7  => 3,
            11 => 3,
            15 => 3,
            19 => 3,
            4  => 4,
            8  => 4,
            12 => 4,
            16 => 4,
            20 => 4
        );
        
        return $arr[$num];
    }

}