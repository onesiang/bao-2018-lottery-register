<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

final class SixMark extends Lottery
{

    public function __construct($lottery = 0)
    {
        parent::__construct();

        $this->lottery_type = $lottery ? $lottery : 9;
        $this->finance_type = 2;
        $this->module_amount = 'marksix';
    }

    /**
     * 采集结果 号码位数
     *
     */
    protected function getNumberCount()
    {
        return 7;
    }

    protected function betTypeTogether()
    {
        $data = array(
            101 => '100', // 连肖
        );
        return $data;
    }

    protected function betTypeComb()
    {
        $data = array(
            18 => '20', // 合肖
        );
        return $data;
    }

    /**
     * 过关
     *
     */
    protected function betTypeThrough()
    {
        $data = array(
            80 => '81,82,83,84',
        );
        return $data;
    }

    /**
     * 连码
     *
     */
    protected function betTypeAttached()
    {
        $data = array(
            105 => '110', // 连码 三中二
            106 => '110', // 连码 三全中
            107 => '110', // 连码二全中
            108 => '110', // 连码二中特
            109 => '110', // 连码特串
            113 => '110', // 全不中
        );
        return $data;
    }

    /**
     * 连码   子odds
     *
     */
    protected function betTypeAttachedChild()
    {
        $data = array(
            105 => '111', // 中二  中三,
            108 => '112', // 中特  中二
        );
        return $data;
    }

    /**
     * 连码   子odds
     *
     */
    protected function betTypeAttachedChildOnly()
    {
        $data = array(
            106 => '106', // 三全中
            107 => '107', // 二全中
            109 => '109', // 特串
            113 => '113', // 全不中
        );
        return $data;
    }

    /**
     * 覆盖Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers)
    {
        //  0输  1赢  2和
        $win = call_user_func_array(array($this, $bet['settle_method']), array($bet, $numbers));

        if (is_array($win)) {
            $this->settleOneBet($bet, $win['win'], $win['win_amount']);
        } else {
            $this->settleOneBet($bet, $win);
        }
    }

    /**
     * 玩法: 特码号码
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun1($bet, $nums)
    {
        $num = $nums[6];
        $win = ($bet['number'] == $num) ? 1 : 0;
        return $win;
    }

    /**
     * 玩法: 特码单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun2($bet, $nums)
    {
        $num = $nums[6];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkDouble($num);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 特码小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun3($bet, $nums)
    {
        $num = $nums[6];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkBig($num, 25);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 尾小尾大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun5($bet, $nums)
    {
        $num = substr($nums[6], -1, 1);
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkBig($num, 5);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 和单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun6($bet, $nums)
    {
        $num = $nums[6];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            if ($num > 9) {
                $num = substr($num, -2, 1) + substr($num, -1, 1);
            }
            $val = $this->checkDouble($num);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 和小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun7($bet, $nums)
    {
        $num = $nums[6];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            if ($num > 9) {
                $num = substr($num, -2, 1) + substr($num, -1, 1);
            }
            $val = $this->checkBig($num, 7);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 特码   红波 蓝波 绿波
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun8($bet, $nums)
    {
        $num = $nums[6];
        $win = ($this->checkBall($num, $bet['number'])) ? 1 : 0;
        return $win;
    }

    /**
     * 玩法: 特码生肖
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun9($bet, $nums)
    {
        $num = $nums[6];
        $bet_balls = $this->getOneZodiacNumber($bet['number'] - 1);
        $win = ($this->checkBall($num, $bet_balls)) ? 1 : 0;
        return $win;
    }

    /**
     * 特码合肖
     *
     */
    protected function fun18($bet, $nums)
    {
        $num = $nums[6];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $child_bets = $this->getByOrderId($bet['order_id']);
            $bet_balls = '';
            $zodiac = $this->getZodiacNumber();
            foreach ($child_bets as $value) {
                $bet_balls .= $zodiac[$value['number'] - 1] . ',';
            }
            unset($child_bets);
            $bet_balls = trim($bet_balls, ',');
            $win = ($this->checkBall($num, $bet_balls)) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 特码半波
     *
     */
    protected function fun22($bet, $nums)
    {
        $num = $nums[6];
        $win = ($this->checkBall($num, $bet['number'])) ? 1 : 0;
        return $win;
    }

    /**
     * 正码 (前面六个号码为正码 下注号码如在六个正码号码里为中奖)
     *
     */
    protected function fun50($bet, $nums)
    {
        unset($nums[6]);
        $win = (in_array($bet['number'], $nums)) ? 1 : 0;
        return $win;
    }

    /**
     * 总分  单双
     *
     */
    protected function fun60($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }

    /**
     * 总分  小大
     *
     */
    protected function fun61($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkBig($sum, 175);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }

    /**
     * 正码特
     *
     */
    protected function fun70($bet, $nums)
    {
        $pos = $bet['position'];
        $number = $bet['number'];
        $win = ($nums[$pos - 1] == $number) ? 1 : 0;
        return $win;
    }

    /**
     * 玩法: 正码1-6 单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun71($bet, $nums)
    {
        $num = $nums[$bet['position'] - 1];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkDouble($num);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 正码1-6 小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun72($bet, $nums)
    {
        $num = $nums[$bet['position'] - 1];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkBig($num, 25);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 正码1-6 红波 蓝波 绿波
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun73($bet, $nums)
    {
        $color_balls = array(
            '1,2,12,13,23,24,34,35,45,46,7,8,18,19,29,30,40', // 红波
            '31,41,42,3,4,14,15,25,26,36,37,47,48,9,10,20', // 蓝波
            '11,21,22,32,33,43,44,5,6,16,17,27,28,38,39,49', // 绿波
        );
        $bet_numbers = $color_balls[$bet['number'] - 1];
        $num = $nums[$bet['position'] - 1];
        $win = ($this->checkBall($num, $bet_numbers)) ? 1 : 0;
        return $win;
    }

    /**
     * 玩法: 正码1-6 和单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun74($bet, $nums)
    {
        $num = $nums[$bet['position'] - 1];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            if ($num > 9) {
                $num = substr($num, -2, 1) + substr($num, -1, 1);
            }
            $val = $this->checkDouble($num);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 正码1-6 和小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun75($bet, $nums)
    {
        $num = $nums[$bet['position'] - 1];
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            if ($num > 9) {
                $num = substr($num, -2, 1) + substr($num, -1, 1);
            }
            $val = $this->checkBig($num, 7);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 玩法: 正码1-6 尾小尾大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun76($bet, $nums)
    {
        $num = $nums[$bet['position'] - 1];
        $num = substr($num, -1, 1);
        if ($this->checkTie($num)) {
            $win = 2;
        } else {
            $val = $this->checkBig($num, 5);
            $win = ($bet['number'] == $val) ? 1 : 0;
        }
        return $win;
    }

    /**
     * 正码过关
     *
     */
    protected function fun80($bet, $nums)
    {
        $child_bets = $this->getByOrderId($bet['order_id']);
        $win_amount = $bet['bet_amount'];
        $win = 0;
        foreach ($child_bets as $value) {
            if ('fun80' != $value['settle_method']) {
                $win = call_user_func_array(array($this, $value['settle_method']), array($value, $nums));
                if (1 == $win) {
                    $win_amount *= $value['odds'];
                } else if (0 == $win) {
                    // 输
                    $win_amount = 0;
                    break;
                } else {
                    // 和做赔率 = 1 当胜利来处理
                    $win = 1;
                }
            }
        }
        unset($child_bets);

        return array('win' => $win, 'win_amount' => $win_amount);
    }

    /**
     * 一肖
     *
     */
    protected function fun90($bet, $nums)
    {
        $one_sx_balls = $this->getOneZodiacNumber($bet['number'] - 1);
        $one_sx_balls = explode(',', $one_sx_balls);
        $win = ($this->intersectBall($one_sx_balls, $nums) >= 1) ? 1 : 0;
        return $win;
    }

    /**
     * 尾数
     *
     */
    protected function fun91($bet, $nums)
    {
        $end_nums = array();
        foreach ($nums as $value) {
            $end_nums[] = substr($value, -1, 1);
        }
        $win = in_array($bet['number'], $end_nums) ? 1 : 0;
        return $win;
    }

    /**
     * 连肖
     *
     */
    protected function fun101($bet, $nums)
    {
        $win = 0;
        $pos = $bet['position'];
        $child_bets = $this->getByOrderId($bet['order_id']);
        $bet_amount = $bet['bet_amount'];
        $total_odds_value = $bet['odds'];
        $odds_value = $total_odds_value;
        $win_count = 0;
        $new_child_bets = array();
        foreach ($child_bets as $key => $value) {
            // 过滤同一个odds_id 防止有非正常数据 比如在一个注单内投同一个生肖
            $new_child_bets[$value['odds_id']] = $value;
        }
        $count = count($child_bets);
        unset($child_bets);

        foreach ($new_child_bets as $value) {
            $one_sx_balls = $this->getOneZodiacNumber($value['number'] - 1);
            $one_sx_balls = explode(',', $one_sx_balls);
            if ($this->intersectBall($one_sx_balls, $nums) >= 1) {
                $win_count++;
                //$odds_value -= ($total_odds_value - $value['odds']);
                // 赔率算法更改为 使用最低赔率
                if ($value['odds'] < $odds_value) {
                    $odds_value = $value['odds'];
                }
            } else {
                $win = 0;
                break;
            }
        }
        if ($pos > 1 && $count == $pos) {
            if ($count == $win_count) {
                $win = 1;
            }
        } else {
            // log error
        }
        $win_amount = ($win == 1) ? floor($bet_amount * $odds_value) : 0;

        return array('win' => $win, 'win_amount' => $win_amount);
    }

    /**
     * 连码  三中二
     *
     */
    protected function fun105($bet, $nums)
    {
        unset($nums[6]);
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }

    /**
     * 连码  三全中
     *
     */
    protected function fun106($bet, $nums)
    {
        unset($nums[6]);
        $data = $this->checkPrizeBalls($bet, $nums, 3);
        return $data;
    }

    /**
     * 连码  二全中
     *
     */
    protected function fun107($bet, $nums)
    {
        unset($nums[6]);
        $data = $this->checkPrizeBalls($bet, $nums, 2);
        return $data;
    }

    /**
     * 连码  二中特
     *
     */
    protected function fun108($bet, $nums)
    {
        $child_bets = $this->getByOrderId($bet['order_id']);
        $all_nums = $nums;
        unset($nums[6]);
        foreach ($child_bets as $value) {
            // 二中特之中特
            if (1 == $value['number']) {
                $value['bet_number'] = $bet['number'];
                $data = $this->checkTwoPrizeSpecil($value, $all_nums);
                if (1 == $data['win']) {
                    return $data;
                    break;
                }
            }
            // 二中特之中二
            else if (2 == $value['number']) {
                $value['bet_number'] = $bet['number'];
                $data = $this->checkPrizeBalls($value, $nums, 2);
                if (1 == $data['win']) {
                    return $data;
                    break;
                }
            }
        }
        return array('win' => 0, 'win_amount' => 0);
    }

    /**
     * 连码  特串
     *
     */
    protected function fun109($bet, $nums)
    {
        $data = $this->checkPrizeSpecil($bet, $nums);
        return $data;
    }

    /**
     * 全不中
     *
     */
    protected function fun110($bet, $nums)
    {
        $data = $this->checkPrizeBalls($bet, $nums, 0, 1);
        return $data;
    }

    /**
     * 连码 两中特 同时中正码和特码
     *
     */
    protected function checkTwoPrizeSpecil($bet, $nums)
    {
        $win = 0;
        $child_bets = $this->getByOrderId($bet['order_id']);
        $bet_amount = $bet['bet_amount'];
        $odds_value = $bet['odds'];
        $special_ball = $nums[6];
        unset($nums[6]);
        $bet_balls = isset($bet['bet_number']) ? $bet['bet_number'] : '';
        $bet_balls = explode(',', $bet_balls);
        $bet_count = count($child_bets);
        foreach ($child_bets as $key => $value) {
            $child_bets[$value['odds_id']] = $value;
            unset($child_bets[$key]);
        }

        $count = count($child_bets);
        if ($bet_count == $count && 2 == $count) {
            // 每二个号码为一组，如二个号码其中一个在正码，另一个在特码，视为中奖
            if ($this->intersectBall($nums, $bet_balls) >= 1 && $this->checkBall($special_ball, $bet_balls)) {
                $win = 1;
            }
        } else {
            // log error
            $win = 0;
        }

        $win_amount = ($win == 1) ? floor($bet_amount * $odds_value) : 0;

        return array('win' => $win, 'win_amount' => $win_amount);
    }

    /**
     * 连码 特串  同时中正码和特码
     *
     */
    protected function checkPrizeSpecil($bet, $nums)
    {
        $win = 0;
        $bet_amount = $bet['bet_amount'];
        $odds_value = $bet['odds'];
        $special_ball = $nums[6];
        unset($nums[6]);
        $bet_balls = $bet['number'];
        $bet_balls = explode(',', $bet_balls);

        $count = count($bet_balls);
        if (2 == $count) {
            // 一个在正码，另一个在特码，视为中奖
            if (1 == $this->intersectBall($nums, $bet_balls) && $this->checkBall($special_ball, $bet['number'])) {
                $win = 1;
            }
        } else {
            // log error
            $win = 0;
        }

        $win_amount = ($win == 1) ? floor($bet_amount * $odds_value) : 0;

        return array('win' => $win, 'win_amount' => $win_amount);
    }

    protected function checkTie($num)
    {
        if (49 == $num) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param $ball int
     * @param $nums String
     */
    protected function checkBall($ball, $nums)
    {
        if (is_string($nums)) {
            $nums = explode(',', $nums);
        }
        if (in_array($ball, $nums)) {
            return true;
        }
        return false;
    }

    /**
     *  生肖对应的号码
     *
     */
    public function getZodiacNumber()
    {
        // 羊年
        //$arr = array('8,20,32,44', '7,19,31,43', '6,18,30,42', '5,17,29,41', '4,16,28,40', '3,15,27,39', '2,14,26,38', '1,13,25,37,49', '12,24,36,48', '11,23,35,47', '10,22,34,46', '9,21,33,45');

        // 猴年(等到猴年就把上面的注释，然后启用下面的)
        //$arr = array( '10,22,34,46', '9,21,33,45', '8,20,32,44', '7,19,31,43', '6,18,30,42', '5,17,29,41', '4,16,28,40', '3,15,27,39', '2,14,26,38', '1,13,25,37,49', '12,24,36,48', '11,23,35,47');

        // 狗年,下一期就把最後一個放到第一個
        //$arr = array('11,23,35,47', '10,22,34,46', '9,21,33,45', '8,20,32,44', '7,19,31,43', '6,18,30,42', '5,17,29,41', '4,16,28,40', '3,15,27,39', '2,14,26,38', '1,13,25,37,49', '12,24,36,48');

        $shengxiaoBall = array(
            1 => [
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
            ], // 鼠
            2 => [
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
            ], // 牛
            3 => [
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
            ], // 虎
            4 => [
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
            ], // 兔
            5 => [
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
            ], // 龍
            6 => [
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
            ], // 蛇
            7 => [
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
            ], // 馬
            8 => [
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
            ], // 羊
            9 => [
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
            ], // 猴
            10 => [
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
                '11,23,35,47',
            ], // 雞
            11 => [
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
                '12,24,36,48',
            ], // 狗
            12 => [
                '12,24,36,48',
                '11,23,35,47',
                '10,22,34,46',
                '9,21,33,45',
                '8,20,32,44',
                '7,19,31,43',
                '6,18,30,42',
                '5,17,29,41',
                '4,16,28,40',
                '3,15,27,39',
                '2,14,26,38',
                '1,13,25,37,49',
            ], // 豬
        );

        $year = date('Y');
        $shengxiao = $year % 12 - 3;
        if ($shengxiao < 0) {
            $shengxiao += 12;
        }

        return $shengxiaoBall[$shengxiao];
    }

    protected function getOneZodiacNumber($n)
    {
        $arr = $this->getZodiacNumber();
        return $arr[$n];
    }

}
