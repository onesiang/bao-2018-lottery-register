<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

final class LotteryProcess extends Wwin
{
    private $_lottery;
    private $playType;

    public function __construct($lottery)
    {
        parent::__construct();
        if ($lottery) {
            switch ($lottery) {
                case 29: // 極速賽車
                    $this->playType = 'pk10';
                    break;
                case 31: // 超級快3
                    $this->playType = 'k3';
                    break;
                case 34: // 超級28
                case 35: // 美國28
                    $this->playType = 'pcdd';
                    break;
                case 36: // 極速六合彩
                    $this->playType = 'lhc';
                    break;
                case 37: // 福利時時彩
                    $this->playType = 'ssc';
                    break;
            }
            $this->_lottery = $lottery;
        }
    }

    public function settleAllBets()
    {
        $sql = 'SELECT a.*, b.result FROM ltt_bet AS a LEFT JOIN ltt_round AS b ON a.round_id=b.round_id WHERE a.status=1 AND a.parlay=0 AND b.status=2 ORDER BY a.category DESC LIMIT 1000';
        $result = $this->_db->query($sql, array());

        $data = array();
        foreach ($result as $row) {
            if (!isset($data[$row['category']])) {
                $data[$row['category']] = array();
            }
            $data[$row['category']][] = $row;
        }
        unset($result);
        foreach ($data as $lottery => $bets) {
            $handle = null;
            switch ($lottery) {
                case 10:
                    $handle = new Cqssc();
                    break;
                case 11:
                    $handle = new Gdklsf();
                    break;
                case 12:
                    $handle = new Bjpk();
                    break;
                case 13:
                    $handle = new Gdklsf($lottery);
                    break;
                case 14:
                    $handle = new Cqssc($lottery);
                    break;
                case 15:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 16:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 17:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 18:
                    $handle = new HappyEight($lottery);
                    break;
                case 98:
                    $handle = new HappyEight($lottery);
                    break;
                case 19:
                    $handle = new ThreeBall($lottery);
                    break;
                case 20:
                    $handle = new ThreeBall($lottery);
                    break;
                case 21: //幸运飞艇
                    $handle = new Bjpk($lottery);
                    break;
                case 22: //幸运28 ,同北京28一样，只是结果由自己
                    $handle = new Bj28($lottery);
                    break;
                case 23: //福利三分彩
                    $handle = new Bjpk($lottery);
                    break;
                case 24: //跑马
                    $handle = new Bjpk($lottery);
                    break;
                case 25: //福利五分彩
                    $handle = new Cqssc($lottery);
                    break;
                case 28: //北京28
                    $handle = new Bj28($lottery);
                    break;
                case 99: //北京28
                    $handle = new Bj28($lottery);
                    break;
                case 64: //安徽快3
                    $handle = new K3($lottery);
                    break;
                case 70: //湖北快3
                    $handle = new K3($lottery);
                    break;
            }
            if ($handle) {
                $handle->settleProcessBets($bets);
                unset($handle);
            }
        }
    }

    public function settle6Bets()
    {
        $sql = 'SELECT a.*, b.result FROM ltt_bet AS a LEFT JOIN ltt_round AS b ON a.round_id=b.round_id WHERE a.status=1 AND a.parlay=0 AND b.status=2 AND b.category = 9 ORDER BY a.category DESC LIMIT 2000';
        $result = $this->_db->query($sql, array());
        $data = array();
        foreach ($result as $row) {
            if (!isset($data[$row['category']])) {
                $data[$row['category']] = array();
            }
            $data[$row['category']][] = $row;
        }
        unset($result);
        foreach ($data as $lottery => $bets) {
            $handle = null;
            switch ($lottery) {
                case 9:
                    $handle = new SixMark();
                    break;
            }
            if ($handle) {
                $handle->settleProcessBets($bets);
                unset($handle);
            }
        }
    }

    /**
     * [getPrivateLotteryRound 抓私彩的最後一期]
     *
     * @return void
     */
    public function getPrivateLotteryRound($category)
    {
        $sql = 'SELECT
                    ltt_round.round_id,
                    ltt_round.number,
                    status
                FROM
                    ltt_list
                INNER JOIN
                    ltt_round ON ltt_list.category = ltt_round.category
                WHERE
                    species = 2
                AND
                    ltt_round.category = ?
                ORDER BY
                    ltt_round.number DESC
                LIMIT 1';
        $result = $this->_db->query($sql, array($category));
        return $result;
    }

    /**
     * [addNewPrivateLotteryRound 開新的一盤]
     * @param [array] $result
     * @return void
     */
    public function addNewPrivateLotteryRound($result, $lottery)
    {
        $play_type = $this->playType;
        $type = $this->_lottery;
        $round = substr(date('Ymd'), 2) . '001';

        switch ($type) {
            case 29: // 極速賽車
                $lottery_handle = new Bjpk($type);
                break;
            case 31: // 超級快3
                $lottery_handle = new K3($type);
                break;
            case 34: // 幸運28
            case 35: // 美國28
                $lottery_handle = new Bj28($type);
                break;
            case 36: // 超級六合彩
                $lottery_handle = new SixMark($type);
                break;
            case 37: // 福利時時彩
                $lottery_handle = new Cqssc($type);
                break;
        }

        // $lottery_handle = new Bjpk($type);
        $date = date('Y-m-d H:i:s');
        $today = new DateTime($date);

        $closing_time = '+2 minutes';
        $cold_time = '+45 seconds';

        if ($lottery == 36) {
            $closing_time = '+9 minutes';
            $cold_time = '+0 seconds';
        }
        $stop_time = $today->modify($closing_time)->modify($cold_time)->format('Y-m-d H:i:s');

        if (count($result) == 0) {
            $lottery_handle->managerAddPosition($round, $type, $stop_time);
        } else {
            $status = $result[0]['status'];
            $today = substr(date('Ymd'), 2);

            $current = $today . '001';

            // if ($current - $round >= 1000 || ($current - $result[0]['number']) >= 1000) {
            //     $next_round = $current;
            // } else {
            //     $next_round = $result[0]['number'] + 1;
            // }
            $lastRoundDay = substr((string) $result[0]['number'], 0, 6);

            if ($today != $lastRoundDay) {
                $next_round = $current;
            } else {
                $next_round = $result[0]['number'] + 1;
            }

            $last_round = $result[0]['round_id'];
            $code = $lottery_handle->managerAddPosition($next_round, $type, $stop_time);
            if (!$code['error']) {
                Log::record('private lottery add success', 'error', 'private_lottery/');
            }
        }
        unset($lottery_handle);
    }

    /**
     * [settlePrivateLottery 私彩結算]
     *
     * @param [array] $result
     * @return void
     */
    public function settlePrivateLottery($result)
    {
        $play_type = $this->playType;
        $type = $this->_lottery;
        $round = 10000;
        if (count($result) != 0) {
            $status = $result[0]['status'];
            $last_round = $result[0]['round_id'];
            if ($status == 1) { // 上一期未結算

                $url = 'http://' . LotteryKjIp . ':' . LotteryKJPort . '/token/kj/' . $play_type . '/' . $last_round . '/b' . '/' . $type . '/1?demo=0&dbName=' . DB_NAME . '&customerName=' . CUSTOMER_NAME;
                $result = curl_kj($url, false);
                $result = json_decode($result, true);
                $token = $result['token'];
                Log::record('get private kj token url => ' . $url, 'error', 'private_lottery/');

                if ($token) {
                    $url = 'http://' . LotteryKjIp . ':' . LotteryKJPort . '/' . 'kj/' . $play_type . '/' . $last_round . '/b' . '/' . $type . '/1?demo=0&dbName=' . DB_NAME . '&customerName=' . CUSTOMER_NAME . '&token=' . $token;
                    Log::record('private kj url => ' . $url, 'error', 'private_lottery/');
                    curl_kj($url, true);
                }
            }
        }
    }
}
