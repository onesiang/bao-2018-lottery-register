<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

class LotteryOpenRound
{

    /**
     * 安徽快3
     * 10分钟一期，每天开奖80期
     * 開盤：08:40 - 21:50
     * 開獎：08:50 - 22:00
     * 盤口規則：yymmdd001 - yymmdd080
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function createAhk3Round($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';
        $data = $this->buildDates($round, $date . ' 08:40:00', $stop_time . ' 22:00:00', 600);
        return $data;
    }

    /**
     * 湖北快3
     * 10分钟一期，销售时间：9:00~22:00，每天开奖78期
     * 開盤：09:00 - 21:50
     * 開獎：09:10 - 22:00
     * 盤口規則：yymmdd001 - yymmdd078
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function createHubk3Round($start_time = '', $stop_time = '')
    {

        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';
        $data = $this->buildDates($round, $date . ' 09:00:00', $stop_time . ' 22:00:00', 600);
        return $data;
    }

    /**
     * 广东11选5
     * 10分钟一期，每天开奖84次
     * 開盤：09:00 - 22:50
     * 開獎：09:10 - 23:00
     * 盤口規則：yymmdd01 - yymmdd84
     *
     * 江西11选5¶
     * 10分钟一期，每天开奖84次
     * 開盤：09:00 - 22:50
     * 開獎：09:10 - 23:00
     * 盤口規則：yymmdd01 - yymmdd84
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function create11x5Round($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '01';
        $data = $this->buildDates($round, $date . ' 09:00:00', $stop_time . ' 23:00:00', 600);
        return $data;
    }

    /**
     * 广东快乐十分
     *
     * 10 分钟一期，共 84 期， 每天 09:11 至 23:01 开奖
     *  每日 09:00開第一期，22:50最後一期
     *  盤口規則：yymmdd001 - yymmdd084
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function createGdklsfRound($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';
        $data = $this->buildDates($round, $date . ' 09:00:00', $stop_time . ' 23:00:00', 600);
        return $data;
    }

    /**
     * 北京赛车pk10 每天从9:00-24:00，每五分钟开奖一次，每天共179期
     *
     * 開獎時間為2,7分鐘結尾。開盤時間亦同。
     * 09:02開第一期 09:07開獎
     * 23:52開最後一期 23:57開獎
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function createPk10Round($yesterday_number = '', $category = 12, $start_date = '')
    {

        if ($yesterday_number) {
            $round = $yesterday_number + 1; //北京賽車期數為累積的，因此拿昨天的期數 + 1;
        } else if ($start_date) {
            $round = $this->calculateNewRoundDateDiff($category, 179, $start_date) + 1;
        }

        $date = $start_date ? date('Ymd', strtotime($start_date)) : date('Ymd');

        $data = $this->buildDates($round, $date . ' 09:02:00', $date . ' 23:57:00');

        return $data;
    }

    /**
     * 重庆时时彩 10:00-22:00（72期）10分钟一期，22:00-02:00（48期）5分钟一期，一天120期
     * 10分：09:50 第一期, 23:50最後一期
     * 5分：00:00第一期, 01:50最後一期
     * 盤口規則：yymmdd001 - yymmdd120
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array
     */
    public function createSscRound($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd') + 1;
        $stop_time = $this->generateStopTime($start_time, $stop_time, 2);

        $round = (int) substr($date, 2) . '001';

        // 產今天 (for本機測試)
        // $data1 = $this->buildDates($round, $date . ' 00:00:00', $date . ' 01:55:00', 300);
        // $data2 = $this->buildDates($round, $date . ' 09:50:00', $date . ' 21:50:00', 600);
        // $data3 = $this->buildDates($round, $date . ' 21:55:00', ($date + 1) . ' 00:00:00', 300);

        // 產未來
        $data1 = $this->buildDates($round, $date . ' 00:00:00', $date . ' 01:55:00', 300);
        $data2 = $this->buildDates($round, $date . ' 09:50:00', $date . ' 21:50:00', 600);
        $data3 = $this->buildDates($round, $date . ' 21:55:00', $stop_time . ' 00:00:00', 300);

        return $data1 + $data2 + $data3;
    }

    /**
     * 幸运飞艇
     *
     * 白天从中午13:04 开到凌晨04:04，每5分钟开一次奖, 每天开奖180期
     *
     * 開盤：13:04 - 03:59
     * 開獎：13:09 - 04:04
     * 盤口規則：yymmdd001 - yymmdd180
     *
     * @param $start_time 開始時間
     * @param $stop_time 結束時間
     * @return array -> 期數陣列
     */
    public function createMlaftRound($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time, 1);

        $round = (int) substr($date, 2) . '001';

        $date = $this->buildDates($round, $date . ' 13:04:00', $stop_time . ' 04:04:00', 300);

        return $date;
    }

    /**
     * 幸运28
     *
     * 5分钟一期，每天开奖179次
     * 開盤：09:00 - 23:50
     * 開獎：09:05 - 23:55
     * 盤口規則：連續性盤口
     *
     * @param $yesterday_number -> 昨日的號碼
     * @param $category -> 彩種代號
     * @param $start_date -> 開始時間
     * @return array -> 期數陣列
     */
    public function createPcddRound($yesterday_number = '', $category = 18, $start_date = '')
    {
        if ($yesterday_number) {
            $round = $yesterday_number + 1; //昨天的最後一期期數+1
        } else if ($start_date) {
            $round = $this->calculateNewRoundDateDiff($category, 180, $start_date) + 1;
        }

        $date = $start_date ? date('Ymd', strtotime($start_date)) : date('Y-m-d');

        $data = $this->buildDates($round, $date . ' 09:00:00', $date . ' 23:55:00', 300);

        return $data;
    }

    /**
     * 重慶十分
     *
     * 每日开奖时间：10分钟一期，每天开奖97次(第一期前一天23:54開)
     * 開盤時間：23:54 - 23:44 先開到02:04 要休息到09:54
     * 開獎時間：00:04 - 23:54
     * 盤口規則：yymmdd01 - yymmdd79
     *
     * @return array -> 期數陣列
     */
    public function createCqklsfRound($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time, 1);

        $round = (((int) substr($date, 2)) + 1) . '001';

        // 產今天 (for 本機測試)
        // $data1 = $this->buildDates($round, ($date - 1) . ' 23:54:00', $date . ' 02:04:00', 600);
        // $data2 = $this->buildDates($round, $date . ' 09:54:00', $date . ' 23:54:00', 600);

        // 產未來
        $data1 = $this->buildDates($round, $date . ' 23:54:00', $stop_time . ' 02:04:00', 600);
        $data2 = $this->buildDates($round, $stop_time . ' 09:54:00', $stop_time . ' 23:54:00', 600);

        return $data1 + $data2;
    }

    /**
     * 江苏快3
     *
     * 每日开奖时间：10分钟一期，每天开奖82次
     * 開盤時間： 08:29 - 21:59
     * 開獎時間： 08:39 - 22:09
     * 盤口規則：yymmdd001 - yymmdd082
     *
     * @return array -> 期數陣列
     */
    public function createJsk3Round($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';

        $data = $this->buildDates($round, $date . ' 08:29:00', $stop_time . ' 22:09:00', 600);
        return $data;
    }

    /**
     * 广西快3
     *
     * 每日开奖时间：10分钟一期，每天开奖78次
     * 開盤時間： 09:27 - 22:17
     * 開獎時間： 09:37 - 22:27
     * 盤口規則：yymmdd001 - yymmdd078
     *
     * @return array -> 期數陣列
     */
    public function createGxk3Round($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';

        $data = $this->buildDates($round, $date . ' 09:27:00', $stop_time . ' 22:27:00', 600);
        return $data;
    }

    /**
     * 新疆时时彩
     *
     * 每日开奖时间：10分钟一期，每天开奖96次
     * 開盤時間： 10:00 - 01:50
     * 開獎時間： 10:10 - 02:00
     * 盤口規則：yymmdd001 - yymmdd096
     *
     * @return array
     */
    public function createXjsscRound($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time, 1);

        $round = (int) substr($date, 2) . '001';

        $data = $this->buildDates($round, $date . ' 10:00:00', $stop_time . ' 02:00:00', 600);
        return $data;
    }

    /**
     *河北快3
     *
     * 每日开奖时间：10分钟一期，每天开奖81次
     * 開盤時間: 08:30 - 21:50
     * 開獎時間：08:40 - 22:00
     * 盤口規則：yymmdd001 - yymmdd081
     *
     * @return array -> 期數陣列
     */
    public function createHbk3Round($start_time = '', $stop_time = '')
    {
        $date = !(empty($start_time)) ? date('Ymd', strtotime($start_time)) : date('Ymd');
        $stop_time = $this->generateStopTime($start_time, $stop_time);

        $round = (int) substr($date, 2) . '001';

        $data = $this->buildDates($round, $date . ' 08:30:00', $stop_time . ' 22:00:00', 600);
        return $data;
    }

    /**
     * 產生期數陣列
     *
     * @param $round -> 起始期數
     * @param $start_time -> 開始時間(第一期開盤時間)
     * @param $stop_time -> 停止時間(最後一期結算時間)
     * @param int $gap_time -> 間隔時間(每期間隔)
     * @param int $close_time -> 要提早的時間(封盤時間)
     * @return array
     */
    public function buildDates(&$round, $start_time, $stop_time, $gap_time = 300, $close_time = -9)
    {
        $end_time = $gap_time - 1;

        $start_date = new DateTime($start_time);
        $end_date = new DateTime($stop_time);
        $interval = DateInterval::createFromDateString($gap_time . ' seconds');
        $periods = new DatePeriod($start_date, $interval, $end_date);

        $dates = array();
        foreach ($periods as $period) {
            $period_begin = $period->format('Y-m-d H:i:s');
            $period_end = $period->modify($end_time . ' seconds');
            $now = new DateTime();

            if ($period_end < $now) {
                $round++;
                continue;
            }

            $dates[$round]['start_time'] = $period_begin;
            $dates[$round]['stop_time'] = $period_end->format('Y-m-d H:i:s');
            $dates[$round]['close_round_time'] = $period->modify($close_time . ' seconds')->format('Y-m-d H:i:s');
            $dates[$round]['bet_round'] = $round;
            $round++;
        }

        return $dates;
    }

    /**
     * [generateStopTime 檢查結束時間]
     *
     * @param date $start_time 開始時間
     * @param date $stop_time 結束時間
     * @param integer $plus   增減多少
     * @return date
     */
    private function generateStopTime($start_time, $stop_time, $plus = 0)
    {
        if ($start_time && !$stop_time) {
            return date('Ymd', strtotime($start_time)) + $plus;
        } else if (!$start_time && !$stop_time) {
            return date('Ymd') + $plus;
        } else if ($stop_time) {
            return date('Ymd', strtotime($stop_time));
        }
    }

    /**
     * [calculateDateDiff 計算新的期數]
     *
     * @param [type] $category 彩種代號
     * @param [type] $total_round 一天共開幾期
     * @param [type] $start_date 開始時間
     * @return void
     */
    private function calculateNewRoundDateDiff($category, $total_round, $start_date)
    {
        if ($category && $start_date && $total_round) {
            $lottery_handle = new Lottery();
            $last_round = $lottery_handle->getLastLotteryRound($category);

            if (!$last_round) {
                return;
            }

            $yesterday = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));

            $start_date = new DateTime($start_date);
            $yesterday = new DateTime($yesterday);
            $interval = $start_date->diff($yesterday);
            $diff_day = $interval->d - 1;

            $round = $diff_day * $total_round + $last_round;

            unset($lottery_handle);

            return $round;
        }
    }
}
