<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

final class K3 extends Lottery {
        
    function __construct($lottery = 0) {
        parent::__construct();
        
        $lottery = $lottery > 0 ? $lottery : 64;
        $this->lottery_type = $lottery; // 安徽快3
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers) {
        //  0输  1赢  2和
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );

        $this->settleOneBet($bet, $win);
    }
	
	protected function methods2($bet, $numbers) {
        //  0输  1赢
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );
		return $win;
		
    }
    
    /**
     * 采集结果 号码位数
     * 
     */
    protected function getNumberCount() {
        return 3;
    }
	
	/**
     * 玩法: 单球号码 位投
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun0($bet, $nums) {
        $win = ($bet['number'] == $nums[$bet['position'] - 1]) ? 1 : 0;
        return $win;
    }
	
	/**
     * 玩法: 总和 点数
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun1($bet, $nums) {
		$sum = array_sum($nums);
        $win = ($bet['number'] ==$sum) ? 1 : 0;
        return $win;
    }
	
	/**
     * 玩法:  大 小
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
	 * 大：三粒骰子之点数总和由11点至17点为【大】。
	 * 小：三粒骰子之点数总和由4点至10点为【小】。
     */
    protected function fun2($bet, $nums) {
		if($nums[0]==$nums[1] && $nums[0]==$nums[2])
		{
			return 0;
		}
		$sum = array_sum($nums);
        $val = $this->checkBig($sum, 11);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 单 双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 单：三个位置的数值相加和为单就为【单】。
     * 双：三个位置的数值相加和为双就为【双】。
     */
    protected function fun3($bet, $nums) {
		if($nums[0]==$nums[1] && $nums[0]==$nums[2])
		{
			return 0;
		}
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
	
	
	/**豹子，围骰：三粒骰子平面与选定点数相同；
     * 全骰：在一点至六点内，三粒骰子平面点数相同；
     * 玩法: 豹子
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 豹子：当期开出三个数字相同即为【豹子】,投全骰，只要有一个豹子就中了
     */
	 /*
    protected function fun4($bet, $nums) {
		$val=0;
        if($nums[0]==$nums[1] && $nums[1]==$nums[2])
		{
			$val=$nums[0];
		}
		
        $win = ($bet['number'] == $val) ? 1 : 0;
		if($bet['number']=="7"&&$val>0){$win=1;}
        return $win;
    }
	*/
	protected function fun4($bet, $nums) {
		$win=0;
		if($bet['number']=="7")
		{
			if($nums[0]==$nums[1] && $nums[1]==$nums[2])
			{
				$win=1;
			}
		}
		else
		{
			$number=explode(",",$bet['number']);
			$val=0;
			if($nums[0]==$nums[1] && $nums[1]==$nums[2])
			{
				$val=$nums[0];
			}
			
			$win = ($number[0] == $val) ? 1 : 0;
			
			
		}
		
		
        return $win;
    }
	
    
    /**
     * 玩法: 三军：任何一粒骰子出现选定之平面点数
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 大单（三个数值和）：15,17,19,21,23,25,27为【大单】。
     * 小单（三个数值和）：01,03,05,07,09,11,13为【小单】。
     */
    protected function fun5($bet, $nums) {		
        $win = (in_array($bet['number'],$nums)) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 短牌：选定两粒骰子之平面点数11,22,33,44,55,66
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和   
     */
	/*protected function fun6($bet, $nums) {
		$val = 0;
		if($nums[0]==$nums[1] || $nums[0]==$nums[2])
		{
			$val=$nums[0];
		}
		else if($nums[1]==$nums[2])
		{
			$val=$nums[1];
		}
		
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
	*/
	protected function fun6($bet, $nums) {
		$val = 0;
		$number=explode(",",$bet['number']);
		
		if($nums[0]==$nums[1] || $nums[0]==$nums[2])
		{
			$val=$nums[0];
		}
		else if($nums[1]==$nums[2])
		{
			$val=$nums[1];
		}
		
         $win = ($number[0] == $val) ? 1 : 0;
        return $win;
    }
	
	

    
    /**
     * 玩法: 长牌：任两粒骰子之平面点数；
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun7($bet, $nums) {
		$val = 0;
		$number=explode(",",$bet['number']);
		
		foreach($number as $v){
			if(in_array($v,$nums)){
				$val = $val+1;
			}
		}
		/*
		if(in_array($number[0],$nums))
		{
			$val = $val+1;
		}
		if(in_array($number[1],$nums))
		{
			$val = $val+1;
		}
		*/
        $win = (2 == $val) ? 1 : 0;
        return $win;
    }
    
   
    
}