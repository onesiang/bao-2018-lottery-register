<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

final class Bj28 extends Lottery
{
    public function __construct($lottery = 0)
    {
        parent::__construct();
        
        $lottery = $lottery > 0 ? $lottery : 28;
        $this->lottery_type = $lottery; // 北京28
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers)
    {
        //  0输  1赢  2和
        $win = call_user_func_array(array($this, $bet['settle_method']), array($bet, $numbers));

        $this->settleOneBet($bet, $win);
    }
    
    protected function methods2($bet, $numbers)
    {
        //  0输  1赢
        $win = call_user_func_array(array($this, $bet['settle_method']), array($bet, $numbers));
        return $win;
    }
    
    /**
     * 采集结果 号码位数
     *
     */
    protected function getNumberCount()
    {
        return 3;
    }
    
    /**
     * 玩法: 单球号码 位投
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun1($bet, $nums)
    {
        $win = ($bet['number'] == $nums[$bet['position'] - 1]) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 总和
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun2($bet, $nums)
    {
        $sum = array_sum($nums);
        $win = ($bet['number'] ==$sum) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法:  大 小
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 大：三个位置的数值相加和大于等于14,15,16,17,18,19,20,21,22,23,24,25,26,27为【大】。
     * 小：三个位置的数值相加和小于等于00,01,02,03,04,05,06,07,08,09,10,11,12,13为【小】。
     */
    protected function fun3($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkBig($sum, 14);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 单 双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 单：三个位置的数值相加和为单就为【单】。
     * 双：三个位置的数值相加和为双就为【双】。
     */
    protected function fun4($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 大单 小单
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 大单（三个数值和）：15,17,19,21,23,25,27为【大单】。
     * 小单（三个数值和）：01,03,05,07,09,11,13为【小单】。
     */
    protected function fun5($bet, $nums)
    {
        $sum = array_sum($nums);
        $bigOdd = array("15", "17", "19", "21", "23", "25", "27");
        $smallOdd = array("1", "3", "5", "7", "9", "11", "13");
        $val = 0;
        if (in_array($sum, $bigOdd)) {
            $val = 2;
        }
        if (in_array($sum, $smallOdd)) {
            $val = 1;
        }
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 大双 小双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     *大双（三个数值和）：14,16,18,20,22,24,26为【大双】。
     *小双（三个数值和）：00,02,04,06,08,10,12为【小双】。
     */
    protected function fun6($bet, $nums)
    {
        $sum = array_sum($nums);
        $bigEven = array("14", "16", "18", "20", "22", "24", "26");
        $smallEven = array("0", "2", "4", "6", "8", "10", "12");
        $val = 0;
        if (in_array($sum, $bigEven)) {
            $val = 2;
        }
        if (in_array($sum, $smallEven)) {
            $val = 1;
        }
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    

    
    /**
     * 玩法: 极大 极小
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 极大（三个数值和）：23,24,25,26,27为【极大】。
     * 极小（三个数值和）：00,01,02,03,04为【极小】。
     */
    protected function fun7($bet, $nums)
    {
        $sum = array_sum($nums);
        $veryBig = array("23", "24", "25", "26", "27");
        $verySmall = array("0", "1", "2", "3", "4");
        $val = 0;
        if (in_array($sum, $veryBig)) {
            $val = 2;
        }
        if (in_array($sum, $verySmall)) {
            $val = 1;
        }
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 豹子
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     * 豹子：当期开出三个数字相同即为【豹子】
     */
    protected function fun8($bet, $nums)
    {
        $val=0;
        if ($nums[0]==$nums[1] && $nums[1]==$nums[2]) {
            $val=1;
        }
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
}
