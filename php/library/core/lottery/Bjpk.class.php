<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

final class Bjpk extends Lottery {

    function __construct($lottery = 0) {
        parent::__construct();        
 
		$lottery = $lottery > 0 ? $lottery : 12;
        $this->lottery_type = $lottery; // 北京pk10
    }
	
	
    
    /**
     * 采集结果 号码位数
     * 
     */
    protected function getNumberCount() {
        return 10;
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers) {
        //  0输  1赢  2和
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );

        $this->settleOneBet($bet, $win);
    }
	
	protected function methods2($bet, $numbers) {
        //  0输  1赢
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );
		return $win;
		
    }
    
    /**
     * 玩法: 单球小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun3($bet, $nums) {
        $val = $this->checkBig($nums[$bet['position'] - 1], 6);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 冠亚和  值
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun4($bet, $nums) {
        $sum = $nums[0] + $nums[1];  
        $win = ($bet['number'] == $sum) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 冠亚和  单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun5($bet, $nums) {
        $sum = $nums[0] + $nums[1];
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 冠亚和  小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun6($bet, $nums) {
        $sum = $nums[0] + $nums[1];
        $val = $this->checkBig($sum, 12);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 龙虎
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun7($bet, $nums) {
        $val = 0;
        switch($bet['position']) {
            case 1:
                $val = $this->dragonTiger($nums[0], $nums[9]);
                break;
            case 2:
                $val = $this->dragonTiger($nums[1], $nums[8]);
                break;
            case 3:
                $val = $this->dragonTiger($nums[2], $nums[7]);
                break;
            case 4:
                $val = $this->dragonTiger($nums[3], $nums[6]);
                break;
            case 5:
                $val = $this->dragonTiger($nums[4], $nums[5]);
                break;
        }
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }

}