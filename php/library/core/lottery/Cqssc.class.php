<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

final class Cqssc extends Lottery {
        
    function __construct($lottery = 0) {
        parent::__construct();
        
        $lottery = $lottery > 0 ? $lottery : 10;
        $this->lottery_type = $lottery; // 重庆时时彩  彩票类型 以10为起始
    }
    
    /**
     * 采集结果 号码位数
     * 
     */
    protected function getNumberCount() {
        return 5;
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers) {
        //  0输  1赢  2和
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );

        $this->settleOneBet($bet, $win);
    }
	
	
	protected function methods2($bet, $numbers) {
        //  0输  1赢
        $win = call_user_func_array( array($this, $bet['settle_method']), array($bet, $numbers) );
		return $win;
		
    }
    
    /**
     * 玩法: 单球小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun3($bet, $nums) {
        $val = $this->checkBig($nums[$bet['position'] - 1], 5);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 总和单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun4($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 总和小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun5($bet, $nums) {
        $sum = array_sum($nums);
        $val = $this->checkBig($sum, 23);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 1龙 2虎  3和
     * @param $num 投注值
     * @param $res_num 开奖值
     * @return int 0输  1赢  2和
     */
    protected function fun6($bet, $nums) {
        $val = $this->dragonTiger($nums[0], $nums[4]); // 第一个球跟第五个球比
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 组合: 1豹子 2顺子 3对子 4半顺  5杂六
     * @param $num 投注值
     * @param $res_num 开奖值
     * @return int 0输  1赢  2和
     */
    protected function fun7($bet, $nums) {
        $val = $this->threeBalls($bet['position'], $nums);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 判断组合
     * @param $position int   1前三  2中三  3后三
     * @param $balls    array 开奖号码数组
     * @return int  1豹子  2顺子  3对子  4半顺  5杂六
     */
    protected function threeBalls($position, $balls) {
        $arr = array();
        $val = 0;
        
        if($position == 1) {
            $arr = array($balls[0], $balls[1], $balls[2]);
        }
        else if($position == 2) {
            $arr = array($balls[1], $balls[2], $balls[3]);
        }
        else if($position == 3) {
            $arr = array($balls[2], $balls[3], $balls[4]);
        }
        
        sort($arr);
        
        if($arr[0] == $arr[1] && $arr[1] == $arr[2]) {
            $val = 1; // 豹子
        }
        else if(($arr[2] - $arr[1]) == 1 && ($arr[1] - $arr[0]) == 1) {
            $val = 2; // 顺子
        }
        else if(in_array(0, $arr) && in_array(9, $arr) && (in_array(1, $arr) || in_array(8, $arr))) {
            $val = 2; // 0 1 9 和 0 8 9三个数字一起算顺子
        }
        else if($arr[0] == $arr[1] || $arr[1] == $arr[2]) {
            $val = 3; // 对子
        }
        else if(in_array(0, $arr) && in_array(9, $arr)) {
            $val = 4; // 0 9算半顺(没有豹子 顺子 对子之后)
        }
        else if(($arr[2] - $arr[1]) == 1 || ($arr[1] - $arr[0]) == 1) {
            $val = 4; // 半顺
        }
        else {
            $val = 5; // 杂六
        }
        
        return $val;
    }

}