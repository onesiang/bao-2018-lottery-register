<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

final class LotteryOdds extends Wwin
{

    /**
     * 查询字段
     * @return String
     */
    private function selectFields()
    {
        $fields = ' odds_id, site_id, category, type, position, name, number, settle_method, odds_a, odds_b, return_a, return_b, cold_hot, amount_min, amount_max ';
        return $fields;
    }

    /**
     * @param $odds_id int
     * @return Number
     */
    public function getOneOddsById($odds_id)
    {
        $sql = 'SELECT ' . $this->selectFields() . ' FROM ltt_odds WHERE odds_id=?';
        $result = $this->_db->query($sql, array($odds_id));
        if ($result && isset($result[0])) {
            return $result[0];
        }
        return $result;
    }

    /**
     * 获取某个彩票的所有赔率
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsByLottery($lottery_type)
    {
        $sql = 'SELECT ' . $this->selectFields() . ' FROM ltt_odds WHERE category=? ORDER BY odds_id ASC';
        $result = $this->_db->query($sql, array($lottery_type));

        return $result;
    }

    /**
     * 获取某个彩票的所有赔率
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsColdHot($lottery_type)
    {
        $sql = "SELECT type,position,odds_id,settle_method,cold_hot,number,name FROM ltt_odds WHERE category=? AND settle_method<>''";
        $result = $this->_db->query($sql, array($lottery_type));

        return $result;
    }

    /**
     * 获取某个彩票的所有赔率
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsByLotteryColdHot($lottery_type)
    {
        $sql = "SELECT name,cold_hot,type,position,settle_method FROM ltt_odds WHERE category=? AND settle_method<>'' AND cold_hot<>0 ORDER BY cold_hot DESC";
        $result = $this->_db->query($sql, array($lottery_type));

        return $result;
    }

    /**
     * 获取某个彩票的赔率，以odds_id为键值
     * @param $lottery_type
     * @return array
     */
    final public function getOddsGroup($lottery_type)
    {
        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);
        foreach ($result as $val) {
            $odds[$val['odds_id']] = $val;
        }
        return $odds;
    }

    final public function getOddsByBetType($lottery_type, $type)
    {
        $odds = array();
        $types = explode(',', $type);
        $in = str_repeat('?,', count($types) - 1) . '?';
        $sql = 'SELECT ' . $this->selectFields() . ' FROM ltt_odds WHERE category=? AND type IN(' . $in . ')';
        $parameter = array_merge(array($lottery_type), $types);
        $result = $this->_db->query($sql, $parameter);
        foreach ($result as $val) {
            $odds[$val['odds_id']] = $val;
        }
        return $odds;
    }

    /**
     * 获取某个彩票的所有赔率并按照一定规则组合成对应的数组
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsList($lottery_type)
    {
        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);
        if ($result) {
            foreach ($result as $value) {
                $type = $value['type'];
                $position = $value['position'];
                $v = $value['number'];
                if (!isset($odds[$type])) {
                    $odds[$type] = array();
                }
                if (!isset($odds[$type][$position])) {
                    $odds[$type][$position] = array();
                }
                $odds[$type][$position][$v] = array(
                    'odds_a' => $value['odds_a'],
                    'odds_b' => $value['odds_b'],
                    'return_a' => $value['return_a'],
                    'return_b' => $value['return_b'],
                    'odds_id' => $value['odds_id'],
                    'name' => $value['name'],
                    'fun' => $value['settle_method'],
                    'cold_hot' => $value['cold_hot'],
                    'number' => $value['number'],
                    'amount_min' => $value['amount_min'],
                    'amount_max' => $value['amount_max'],
                    'type' => $value['type'],
                    'position' => $value['position'],
                );
                $odds[$type][$position]['number'] = $value['number'];
            }
        }
        unset($result);
        return $odds;
    }

    final public function getOddsListForSixMark($lottery_type)
    {
        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);
        return ($result)? $result : [];
    }

    /**
     * 获取某个彩票的所有赔率 by會員 格式不同
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsLotteryByMember($lottery_type, $member_id = 0, $bet_round = 0)
    {
        $member_round_bet = array();
        if ($member_id > 0) {
            if ($bet_round > 0) {
                $lottery_handle = new Lottery();
                $member_round_bet = $lottery_handle->check_member_round_bet($bet_round, $lottery_type, $member_id);
            }

            $bet_limit_handle = new LotteryBetLimit();
            $member_limit_data = $bet_limit_handle->getMemberBetAmountLimit($member_id, $lottery_type);
            if ($member_limit_data) {
                foreach ($member_limit_data as $val) {
                    if ($val['amount_min'] > 0) {
                        if (isset($member_round_bet[$val['odds_id']]['bet_amount'])) {
                            $amount_min = $val['amount_min'] - money_from_db($member_round_bet[$val['odds_id']]['bet_amount']);
                        } else {
                            $amount_min = $val['amount_min'];
                        }
                        $member_round_bet[$val['odds_id']]['amount_min'] = ($amount_min > 0) ? $amount_min : 0;
                    }

                    if ($val['amount_max'] > 0) {
                        if (isset($member_round_bet[$val['odds_id']]['bet_amount'])) {
                            $amount_max = $val['amount_max'] - money_from_db($member_round_bet[$val['odds_id']]['bet_amount']);
                        } else {
                            $amount_max = $val['amount_max'];
                        }
                        $member_round_bet[$val['odds_id']]['amount_max'] = ($amount_max > 0) ? $amount_max : 0;
                    }
                }
            }
        }

        $sql = 'SELECT ' . $this->selectFields() . ' FROM ltt_odds WHERE category=? ORDER BY odds_id ASC';
        $result = $this->_db->query($sql, array($lottery_type));

        $odds = array();
        foreach ($result as $val) {
            if (isset($member_round_bet[$val['odds_id']]['amount_min'])) {
                $val['amount_min'] = $member_round_bet[$val['odds_id']]['amount_min'];
            } elseif (isset($member_round_bet[$val['odds_id']]['bet_amount'])) { //沒限額 有投注
                $val['amount_min'] = 0;
            }

            if (isset($member_round_bet[$val['odds_id']]['amount_max'])) {
                $val['amount_max'] = $member_round_bet[$val['odds_id']]['amount_max'];
            } elseif (isset($member_round_bet[$val['odds_id']]['bet_amount'])) { //沒限額 有投注
                $val['amount_max'] = $val['amount_max'] - money_from_db($member_round_bet[$val['odds_id']]['bet_amount']);
            }

            $odds[$val['odds_id']] = $val;
        }
        unset($result);

        return $odds;
    }

    /**
     * 获取會員某个彩票的所有赔率限額 前端使用 因參數顯示方式不同故拆出 加入已投注金額
     * @param $lottery_type int
     * @return array
     */
    final public function getOddsListByMember($lottery_type, $member_id = 0, $bet_round = 0)
    {
        $member_round_bet = array();
        if ($member_id > 0) {
            if ($bet_round > 0) {
                $lottery_handle = new Lottery();
                $member_round_bet = $lottery_handle->check_member_round_bet($bet_round, $lottery_type, $member_id);
            }

            $bet_limit_handle = new LotteryBetLimit();
            $member_limit_data = $bet_limit_handle->getMemberBetAmountLimit($member_id, $lottery_type);
            if ($member_limit_data) {
                foreach ($member_limit_data as $val) {
                    if ($val['amount_min'] > 0) {
                        if (isset($member_round_bet[$val['odds_id']]['bet_amount'])) {
                            $amount_min = $val['amount_min'] - money_from_db($member_round_bet[$val['odds_id']]['bet_amount']);
                        } else {
                            $amount_min = $val['amount_min'];
                        }
                        $member_round_bet[$val['odds_id']]['amount_min'] = ($amount_min > 0) ? $amount_min : 0;
                    }

                    if ($val['amount_max'] > 0) {
                        if (isset($member_round_bet[$val['odds_id']]['bet_amount'])) {
                            $amount_max = $val['amount_max'] - money_from_db($member_round_bet[$val['odds_id']]['bet_amount']);
                        } else {
                            $amount_max = $val['amount_max'];
                        }
                        $member_round_bet[$val['odds_id']]['amount_max'] = ($amount_max > 0) ? $amount_max : 0;
                    }
                }
            }
        }

        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);
        if ($result) {
            foreach ($result as $value) {
                $type = $value['type'];
                $position = $value['position'];
                $v = $value['number'];
                if (!isset($odds[$type])) {
                    $odds[$type] = array();
                }
                if (!isset($odds[$type][$position])) {
                    $odds[$type][$position] = array();
                }

                $amount_min = $value['amount_min']; //沒限額 沒投注
                if (isset($member_round_bet[$value['odds_id']]['amount_min'])) { //有限額 有投注(沒投注)
                    $amount_min = $member_round_bet[$value['odds_id']]['amount_min'];
                } elseif (isset($member_round_bet[$value['odds_id']]['bet_amount'])) { //沒限額 有投注
                    $amount_min = $value['amount_min'] - money_from_db($member_round_bet[$value['odds_id']]['bet_amount']);
                }

                $amount_max = $value['amount_max'];
                if (isset($member_round_bet[$value['odds_id']]['amount_max'])) {
                    $amount_max = $member_round_bet[$value['odds_id']]['amount_max'];
                } elseif (isset($member_round_bet[$value['odds_id']]['bet_amount'])) {
                    $amount_max = $value['amount_max'] - money_from_db($member_round_bet[$value['odds_id']]['bet_amount']);
                }

                $odds[$type][$position][$v] = array(
                    'odds_a' => $value['odds_a'],
                    'odds_b' => $value['odds_b'],
                    'return_a' => $value['return_a'],
                    'return_b' => $value['return_b'],
                    'odds_id' => $value['odds_id'],
                    'name' => $value['name'],
                    'fun' => $value['settle_method'],
                    'cold_hot' => $value['cold_hot'],
                    'number' => $value['number'],
                    'amount_min' => $amount_min,
                    'amount_max' => $amount_max,
                    'type' => $value['type'],
                    'position' => $value['position'],
                );
                $odds[$type][$position]['number'] = $value['number'];
            }
        }
        unset($result);


        return $odds;
    }

    /**
     * 获取某个彩票的所有赔率并按照一定规则组合成对应的数组 後台使用
     * @param $lottery_type int
     * @return array
     */
    final public function getMemberBetLimitList($lottery_type, $member_id="-1")
    {
        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);

        if ($member_id != "-1") {
            $bet_limit_handle = new LotteryBetLimit();
            $member_limit_data = $bet_limit_handle->getMemberBetAmountLimit($member_id, $lottery_type);
            if ($member_limit_data) {
                foreach ($member_limit_data as $val) {
                    $member_limit[$val['odds_id']]['amount_min'] = ($val['amount_min']) ? $val['amount_min'] : '';
                    $member_limit[$val['odds_id']]['amount_max'] = ($val['amount_max']) ? $val['amount_max'] : '';
                }
            }
        }

        if ($result) {
            foreach ($result as $value) {
                $type = $value['type'];
                $position = $value['position'];
                $v = $value['number'];
                if (!isset($odds[$type])) {
                    $odds[$type] = array();
                }
                if (!isset($odds[$type][$position])) {
                    $odds[$type][$position] = array();
                }
                $odds[$type][$position][$v] = array(
                    'odds_a' => $value['odds_a'],
                    'odds_b' => $value['odds_b'],
                    'return_a' => $value['return_a'],
                    'return_b' => $value['return_b'],
                    'odds_id' => $value['odds_id'],
                    'name' => $value['name'],
                    'fun' => $value['settle_method'],
                    'cold_hot' => $value['cold_hot'],
                    'number' => $value['number'],
                    'amount_min' => ($member_limit[$value['odds_id']]['amount_min']) ? $member_limit[$value['odds_id']]['amount_min'] : '',
                    'amount_max' => ($member_limit[$value['odds_id']]['amount_max']) ? $member_limit[$value['odds_id']]['amount_max'] : '',
                    'type' => $value['type'],
                    'position' => $value['position'],
                );
                $odds[$type][$position]['number'] = $value['number'];
            }
        }
        unset($result);
        return $odds;
    }

    /**
     * 计算两面长龙
     * @param $lottery_type int
     * @numbers char 开奖结果
     * @return array
     */
    final public function getColdHotList($lottery_type, $numbers)
    {
        $odds = array();
        $result = $this->getOddsByLottery($lottery_type);
        if ($result) {
            $handle = null;
            switch ($lottery) {
                case 9:
                    $handle = new SixMark();
                    break;
                case 10:
                    $handle = new Cqssc();
                    break;
                case 11:
                    $handle = new Gdklsf();
                    break;
                case 12:
                case 29:
                    $handle = new Bjpk($lottery);
                    break;
                case 13:
                    $handle = new Gdklsf($lottery);
                    break;
                case 14:
                    $handle = new Cqssc($lottery);
                    break;
                case 15:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 16:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 17:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 18:
                    $handle = new HappyEight($lottery);
                    break;
                case 19:
                    $handle = new ThreeBall($lottery);
                    break;
                case 20:
                    $handle = new ThreeBall($lottery);
                    break;
                case 21: //幸运飞艇
                    $handle = new Bjpk($lottery);
                    break;
                case 22: //幸运28 ,同北京28一样，只是结果由自己
                    $handle = new Bj28($lottery);
                    break;
                case 23: //福利三分彩
                    $handle = new Bjpk($lottery);
                    break;
                case 24: //跑马
                    $handle = new Bjpk($lottery);
                    break;
                case 25: //福利五分彩
                    $handle = new Cqssc($lottery);
                    break;
                case 28: //北京28
                    $handle = new Bj28($lottery);
                    break;
                case 64: //安徽快3
                    $handle = new K3($lottery);
                    break;
                case 70: //湖北快3
                    $handle = new K3($lottery);
                    break;
            }
            if ($handle) {
                //$handle->settleProcessBets($bets);

                foreach ($result as $value) {
                    $bet = array(
                        'position' => $value['position'],
                        'odds_id' => $value['odds_id'],
                        'fun' => $value['settle_method'],
                        'cold_hot' => $value['cold_hot'],
                        'number' => $value['number'],
                    );

                    $win = call_user_func_array(array($handle, $value['settle_method']), array($bet, $numbers));

                    if ($win == 1) {
                        if ($value['cold_hot'] >= 0) {
                            $sql = 'UPDATE ltt_odds SET cold_hot=cold_hot+1 WHERE odds_id=?';
                        } else {
                            $sql = 'UPDATE ltt_odds SET cold_hot=1 WHERE odds_id=?';
                        }
                    } else {
                        if ($value['cold_hot'] >= 0) {
                            $sql = 'UPDATE ltt_odds SET cold_hot=-1 WHERE odds_id=?';
                        } else {
                            $sql = 'UPDATE ltt_odds SET cold_hot=cold_hot-1 WHERE odds_id=?';
                        }
                    }

                    $params = array($value['odds_id']);
                    $result_update = $this->_db->query($sql, $params);
                }
            }
            unset($handle);
        }
        unset($result);
        return $odds;
    }

    /**
     * 更新赔率
     * @param $odds = array(odds_id => array('odds_a' => value, 'odds_b' => value, ...))
     * @return Boolean
     */
    public function updateOdds($odds)
    {
        $status = true;

        $this->_db->beginTransaction();
        foreach ($odds as $key => $value) {
            $set = '';
            $params = array();
            foreach ($value as $k => $v) {
                $params[] = $v;
                $set .= $k . '=?,';
            }
            $set = trim($set, ',');
            $params[] = $key;

            $sql = 'UPDATE ltt_odds SET ' . $set . ' WHERE odds_id=?';

            $result = $this->_db->query($sql, $params);
            if (!$result) {
                $status = false;
                break;
            }
        }
        if (true == $status) {
            $this->_db->commit();
        } else {
            $this->_db->rollBack();
        }

        return $status;
    }

    /**
     * 更新限制金額
     * @param $odds = array(odds_id => array('odds_a' => value, 'odds_b' => value, ...))
     * @return Boolean
     */
    public function updateBetLimit($data)
    {
        $category = $data['category'];
        $bet_limit = $data['bet_limit'];

        $sql = '';
        foreach ($bet_limit as $key => $value) {
            if (is_numeric($value['bet_min']) && is_numeric($value['bet_max'])) {
                if ($value['bet_min'] <= $value['bet_max']) {
                    $sql .= 'UPDATE `ltt_odds` SET `amount_min` = ' . $value['bet_min'] . ', `amount_max` = ' . $value['bet_max'] . ' WHERE `odds_id` = '. $value['odds_id'] .';' ;
                } else {
                    return '最低限额不得高于最高限额';
                }
            } else {
                return '最低限额和最高限额其一不得为空';
            }
        }

        $result = $this->_db->multiQuery_new($sql);
        if (!$result) {
            return '更新资料失败';
        }
        return $result;
    }

    public function dbOddsToRedis($key, $value)
    {
        $this->_cache->set($key, $value);
    }

    public function getRedisData($key)
    {
        return $this->_cache->get($key);
    }
}
