<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

final class LotteryBetLimit extends Wwin {
//     /**
//      * @return array
//      */
	public function getBetLimits() {
		$fields = 'bet_limit_id, site_id, category, name, min, max';
		$sql =  "SELECT $fields FROM ltt_bet_limit";

		$result = $this->_db->query($sql);

		return $result;
	}

	public function getBetLimitByCategory($category) {
		$fields = 'bet_limit_id, site_id, category, name, min, max';
		$sql =  "SELECT $fields FROM ltt_bet_limit WHERE category=? LIMIT 1";

		$result = $this->_db->query($sql, array($category));

		if($result && $result[0]) {
			return $result[0];
		}

		return $result;
	}

	// public function updateBetLimits($betLimits) {
	// 	foreach($betLimits as $betId => $minMax) {
    //     	$sql = 'UPDATE ltt_bet_limit SET ';
	// 		$params = [];

	// 		if(isset($minMax->min) && isset($minMax->max)) {
	// 			$sql.= 'min=?,max=? ';
	// 			$params[] = $minMax->min;
	// 			$params[] = $minMax->max;
	// 		} else if(isset($minMax->min)) {
	// 			$sql.= 'min=? ';
	// 			$params[] = $minMax->min;
	// 		} else if(isset($minMax->max)) {
	// 			$sql.= 'max=? ';
	// 			$params[] = $minMax->max;
	// 		}

	// 		$sql.= 'WHERE bet_limit_id=?';
	// 		$params[] = $betId;

    //     	$result = $this->_db->query($sql, $params);

    //     	return $result;
	// 	}
	// }

	// public function getBetAmountLimits() {
	// 	$fields = 'category, site_id, min, max';
	// 	$sql =  "SELECT $fields FROM ltt_bet_amount_limit";
	// 	$result = $this->_db->query($sql);

	// 	return $result;
	// }

	// public function getBetAmountLimit($category) {
	// 	$fields = 'category, site_id, min, max';
	// 	$sql =  "SELECT $fields FROM ltt_bet_amount_limit WHERE category=?";
	// 	$result = $this->_db->query($sql, array($category));

	// 	if($result && $result[0]) {
	// 		return $result[0];
	// 	}
		
	// 	return $result;
	// }

	// public function getBetTypeAmountLimits($category) {
	// 	$fields = 'category, type, name, min, max';
	// 	$sql =  "SELECT $fields FROM ltt_bet_type_amount_limit WHERE category=?";

	// 	$result = $this->_db->query($sql, array($category));

	// 	return $result;
	// }

	public function getBetTypeAmountLimit($category, $type) {
		$fields = 'category, type, name, min, max';
		$sql =  "SELECT $fields FROM ltt_bet_type_amount_limit WHERE category=? and type=?";

		$result = $this->_db->query($sql, array($category, $type));

		if($result && $result[0]) {
			return $result[0];
		} else {
			return array(
				'category' => $category,
				'type' => $type,
				'name' => 'unknown',
				'min' => 1,
				'max' => 1000000
			);
		}

		return $result;
	}

	public function isValidBetAmountLimits($limits) {

		$amount = 0;
		foreach($limits['types'] as $type) {
			$amount += $type['max'];
		}

		return $limits['max'] >= $amount;
	}

	public function updateBetAmountLimits($limits) {
		$sql = 'UPDATE ltt_bet_amount_limit SET ';

		$sql.= 'min=?,max=? ';
		$sql.= 'WHERE category=?';
		$params = [];
		$params[] = $limits['min'];
		$params[] = $limits['max'];
		$params[] = $limits['category'];
		$result = $this->_db->query($sql, $params);
		

		foreach($limits['types'] as $type => $limit) {
			$sql = 'UPDATE ltt_bet_type_amount_limit SET ';
			$sql.= 'min=?, max=? ';
			$sql.= 'WHERE category=? AND type=?';
			$params = [];
			$params[] = $limit['min'];
			$params[] = $limit['max'];
			$params[] = $limits['category'];
			$params[] = $type;
			
			$result = $this->_db->query($sql, $params);
		}
	}

	public function getTypeAmountBalance($member_id, $category, $round_number, $type) {
		$sql = 'SELECT sum(ltt_bet.bet_amount) as total_bet_amount
		FROM ltt_bet, ltt_round
		WHERE
			ltt_bet.detail IS NOT NULL and
			ltt_bet.member_id=? and
			ltt_bet.category=? and
			ltt_bet.type=? and
			ltt_bet.round_id=ltt_round.round_id and
			ltt_round.number=?
		GROUP BY member_id;';
		$params[] = $member_id;
		$params[] = $category;
		$params[] = $type;
		$params[] = $round_number;

		$result = $this->_db->query($sql, $params);
		
		$total_bet_amount = 0;
		if($result && isset($result[0])) {
			$total_bet_amount = money_from_db($result[0]['total_bet_amount']);
		} else {
			$total_bet_amount = 0;
		}

		$limit = $this->getMemberBetTypeAmountLimit($member_id, $category, $type);
		
		return $limit['max'] - $total_bet_amount;
	}

	public function getMemberBetAmountLimit($member_id, $category) {
		$sql = 'SELECT *
			FROM ltt_member_bet_amount_limit
			WHERE
				member_id=? and
				category=?';
		$params[] = $member_id;
		$params[] = $category;
		$result = $this->_db->query($sql, $params);

		if(count($result)>0) {
			return $result;
		}else {
			return false;
		}
	}

	// public function getMemberBetTypeAmountLimits($member_id, $category) {
	// 	$sql = 'SELECT *
	// 		FROM ltt_member_bet_type_amount_limit
	// 		WHERE
	// 			member_id=? and
	// 			category=?';
	// 	$params[] = $member_id;
	// 	$params[] = $category;
	// 	$result = $this->_db->query($sql, $params);

	// 	$bet_type_amount_limits = $this->getBetTypeAmountLimits($category);
	// 	if($result) {
	// 		foreach($bet_type_amount_limits as $index=>$bet_type_amount_limit) {
	// 			foreach($result as $member_bet_type_amount_limit) {
	// 				if($bet_type_amount_limits[$index]['type'] == $member_bet_type_amount_limit['type']) {
	// 					$bet_type_amount_limits[$index]['min'] = $member_bet_type_amount_limit['min'];
	// 					$bet_type_amount_limits[$index]['max'] = $member_bet_type_amount_limit['max'];
	// 				}
	// 			}
	// 		}
	// 	}

	// 	return $bet_type_amount_limits;
	// }

	public function getMemberBetTypeAmountLimit($member_id, $category, $type) {
		$limit = $this->getBetTypeAmountLimit($category, $type);
		
		$sql = 'SELECT category, type, min, max FROM ltt_member_bet_type_amount_limit
			WHERE
				member_id=? and
				category=? and
				type=?';
		$params[] = $member_id;
		$params[] = $category;
		$params[] = $type;

		$result = $this->_db->query($sql, $params);
		if($result && isset($result[0])) {
			$limit['min'] = $result[0]['min'];
			$limit['max'] = $result[0]['max'];
		}

		return $limit;
	}

	public function updateMemberBetAmountLimits($data) {
		$member_id = $data['member_id'];
		$category = $data['category'];
        $bet_limit = $data['bet_limit'];
		
		$member_limit_data = $this->getMemberBetAmountLimit($member_id, $category);
		if ($member_limit_data){
			foreach ($member_limit_data as $val) {
				$member_limit[$val['odds_id']]= true;
			}
		}

		$sql = '';
		if ($bet_limit){
			foreach ($bet_limit as $key => $value) {
				if (is_numeric($value['bet_min']) && is_numeric($value['bet_max'])){
					if ($value['bet_min'] <= $value['bet_max']){
						if ($member_limit[$value['odds_id']]){
							$sql .=  'UPDATE `ltt_member_bet_amount_limit` SET `member_id` = ' . $member_id . ', `category` = ' . $category . ', `odds_id` = ' . $value['odds_id'] . ', `position` = ' . $value['position'] . ', `type` = ' . $value['type'] . ', `number` = ' . $value['number'] . ', `amount_min` = ' . $value['bet_min'] . ', `amount_max` = ' . $value['bet_max'] .' WHERE `member_id` = ' . $member_id . ' AND ' . ' `odds_id` = ' . $value['odds_id'] . ';' ;
						}else{
							$sql .=  'INSERT `ltt_member_bet_amount_limit` SET `member_id` = ' . $member_id . ', `category` = ' . $category . ', `odds_id` = ' . $value['odds_id'] . ', `position` = ' . $value['position'] . ', `type` = ' . $value['type'] . ', `number` = ' . $value['number'] . ', `amount_min` = ' . $value['bet_min'] . ', `amount_max` = ' . $value['bet_max'] .';' ;
						}
					} else {
						return '最低限额不得高于最高限额';
					}
				}
				else{
					return '最低限额和最高限额其一不得为空';
				}
			}

			$result = $this->_db->multiQuery_new($sql);
		} else {
			return '更新资料失败';
		}

		if (!$result){
            return '更新资料失败';
        }
		return $result;
	}
}
?>

