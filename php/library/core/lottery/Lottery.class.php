<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

class Lottery extends Wwin
{
    protected $error;
    protected $now_time;
    protected $now_date;
    protected $est_time;
    protected $est_date;
    protected $lottery_type;
    protected $odds_type;
    protected $odds_bet;
    protected $return_type;
    protected $cost_amount;
    protected $now_money_amount;
    protected $uid;                 // uid这里实际上指member_id
    protected $bet_round_id;        // 盘口编号
    protected $bet_round;
    protected $options;
    protected $finance_type;        // 金钱变化记录Finance时的类型
    protected $valid_bet_rate;      // 有效投注比例  float
    protected $losing_effective;    // 是否输了才算有效流水并返水


    public function __construct()
    {
        parent::__construct();

        $this->now_time  = get_bj_time();  // 北京时间
        $this->now_date  = date('Y-m-d H:i:s', $this->now_time);
//        $this->now_date  = date('Y-m-d H:i:s', time()); //預設server 為北京時間不需轉換
        $this->est_time  = time();    // 美东时间
        $this->est_date  = date('Y-m-d H:i:s', $this->est_time);
        $this->error     = '';
        $this->finance_type  = 2;
    }

    /**
     * @param $u_id      int 用户id
     * @param $bet_round int 期数
     * @param $bets     array(odds_id1 => bet_amonut1, odds_id2 => bet_amount2, ... )
     * @return array('error' => String, 'data' => Array)
     */
    public function bet($u_id, $bet_round, $bets, $position_type, $lottery_type=0)
    {
        $this->uid           = $u_id;
        $this->options       = array();
        $this->cost_amount   = 0;
        $this->bet_round     = $bet_round;

        if (!$this->uid || $this->uid <= 0) {
            $this->setError('bet user id error!');
        }

        if (!$this->error) {
            $round_result = $this->getBetRound($bet_round, $this->lottery_type);
            if ($round_result) {
                $this->bet_round_id = $round_result[0]['round_id'];
                unset($round_result);
            } else {
                $this->setError('the round is forbidden');
            }
        }

        if (!$this->error) {
            //检查是否存在同一期     开始
            //取出赔率列表
            $odds_handle    = new LotteryOdds();
            $odds      = $odds_handle->getOddsListByMember($this->lottery_type,$u_id);
            unset($odds_handle);
            //kg_echo(json_encode($odds));

            $odds_number_length=0; //找出第一个类型每位数的长度，有些0-9，有些1-10，有些1-20
            $odds_number=$odds["1"];
            $odds_number_length=count($odds_number["1"], COUNT_NORMAL);
            if (array_key_exists("number", $odds_number["1"])) {
                $odds_number_length=$odds_number_length-1;
            }

            $odds_bet = null;

            //找出已经下注的交易
            $options1 = array(
                'category' => $this->lottery_type,
                'member_id' => $u_id,
                'round_id' => $this->bet_round_id,
                'status'   => 1,
                'parlay'   => 0
            );

            $already_bets = $this->getBets($options1);

            if ($odds_number_length>1) { //只有多个数字的时候才要进行限制
                $odds_number_max_bets=floor($odds_number_length/2);

                $result="";
                if ($already_bets) {
                    foreach ($already_bets as $key => $value) {
                        $odds_bet[]=intval($value["odds_id"]);
                    }
                }

                $new_bets   = json_decode($bets, true);
                if ($odds_bet) {
                    foreach ($new_bets as $key => $value) {
                        if (!in_array(intval($key), $odds_bet)) {
                            $odds_bet[]=intval($key);
                        }
                    }

                    $odds_1_bets_count=0;
                    // foreach ($odds_number as $key1=>$value1) {
                    //     $odds_1_bets_count=0;
                    //     foreach ($value1 as $key2=>$value2) {
                    //         if (($key2.'')!="number") {
                    //             if (in_array(intval($value2['odds_id']), $odds_bet)) {    //if($bets[$value2['odds_id']]>0)
                    //                 //$result=$result.intval($value2['odds_id']);
                    //                 $odds_1_bets_count=$odds_1_bets_count+1;
                    //                 if ($odds_1_bets_count>$odds_number_max_bets) {
                    //                     $this->setError('number,'.$odds_number_max_bets);
                    //                     break;
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }
                }
            }

            if ($this->lottery_type==12) { //北京PK10冠亚和 ( 冠军车号 + 亚军车号 )
                $odds_number_length=0; //找出第一个类型每位数的长度，有些0-9，有些1-10，有些1-20
                $odds_number=$odds["4"];
                $odds_number_length=count($odds_number[0], COUNT_NORMAL);
                //if(array_key_exists("number", $odds_number)){$odds_number_length=$odds_number_length-1;}
                $odds_number_length=$odds_number_length-1;
                $odds_bet;

                //找出已经下注的交易
                if ($odds_number_length>1) { //只有多个数字的时候才要进行限制
                    $odds_number_max_bets=floor($odds_number_length/2);
                    //$result="";
                    //$result=$result."len:".$odds_number_length."-";
                    //$result=$result."max:".$odds_number_max_bets."-";
                    if ($already_bets) {
                        foreach ($already_bets as $key => $value) {
                            $odds_bet[]=intval($value["odds_id"]);
                        }
                    }

                    $new_bets   = json_decode($bets, true);
                    if ($odds_bet) {
                        foreach ($new_bets as $key => $value) {
                            if (!in_array(intval($key), $odds_bet)) {
                                $odds_bet[]=intval($key);
                            }
                        }

                        $odds_1_bets_count=0;
                        // foreach ($odds_number as $key1=>$value1) {
                        //     $odds_1_bets_count=0;
                        //     foreach ($value1 as $key2=>$value2) {
                        //         if (($key2.'')!="number") {
                        //             if (in_array(intval($value2['odds_id']), $odds_bet)) {    //if($bets[$value2['odds_id']]>0)
                        //                 //$result=$result.intval($value2['odds_id'])."|";
                        //                 $odds_1_bets_count=$odds_1_bets_count+1;

                        //                 if ($odds_1_bets_count>$odds_number_max_bets) {
                        //                     //$this->setError($result);
                        //                     $this->setError('number,'.$odds_number_max_bets);
                        //                     break;
                        //                 }
                        //             }
                        //         }
                        //     }
                        // }
                    }
                }
            }
        }//检查是否存在同一期     结束

        if (!$this->error) {
            $this->setOddsType($position_type);
            $this->betParse($bets);
        }

        if (!$this->error) {
            if (!$this->checkMoney($this->cost_amount)) {
                $this->setError('money not enough');
            }
        }

        if (!$this->error) {
            $this->_db->beginTransaction();
            // 扣钱
            if (1 != $this->minusMoney($this->cost_amount, $this->uid)) {
                $this->setError('minus money fail');
            }

            if (!$this->error) {
                $finance_handle = new Finance();
                $previous_amount = $this->now_money_amount;

                $member_handle = new Member();
                $member = $member_handle->getByMemberId($this->uid, 'category');
                unset($member_handle);
                $category = $member['category'];
                $sql = array();
                $i = 0;
                foreach ($this->options as $key => $value) {
                    $current_amount = $previous_amount - $value['bet_amount'];
                    $this->options[$key]['type'] =  $lottery_type;
                    $fnn_record[$key]['member_id'] = $this->uid;
                    $fnn_record[$key]['type'] = $this->finance_type;
                    $fnn_record[$key]['subtype'] = 6;
                    $fnn_record[$key]['amount'] = 0 - $value['bet_amount'];
                    $fnn_record[$key]['submit_time'] = $this->est_date;
                    $fnn_record[$key]['note'] = '单号' . $value['order_id'];
                    $fnn_record[$key]['previous_amount'] = $previous_amount;
                    $fnn_record[$key]['current_amount'] = $current_amount;
                    $fnn_record[$key]['member_category'] = $category;

                    $previous_amount = $current_amount;
                }
                unset($finance_handle);
                $get_id = $this->_db->pdoMultiInsert('ltt_bet', $this->options);

                if(!$get_id) {
                    $this->setError('bet error!');
                } else {
                    foreach ($fnn_record as $key => $value) {
                        $fnn_record[$key]['table_id'] = $get_id++;
                    }
                    $get_id = $this->_db->pdoMultiInsert('fnn_record', $fnn_record);

                    if(!$get_id) {
                        $this->setError('bet error!');
                    }
                }

            }
            if ($this->error) {
                $this->_db->rollBack();
            } else {
                $this->_db->commit();
            }
        }
        return array('error' => $this->error, 'data' => '');
    }

    /**
     * @param $u_id      int 用户id
     * @param $bet_round int 期数
     * @param $bets      array()
     * @return array('error' => String, 'data' => Array)
     */
    public function bet2($u_id, $bet_round, $bets, $position_type, $lottery_type=0)
    {
        $this->uid           = $u_id;
        $this->options       = array();
        $this->cost_amount   = 0;

        if (!$this->uid || $this->uid <= 0) {
            $this->setError('bet user id error!');
        }

        if (!$this->error) {
            $round_result = $this->getBetRound($bet_round, $this->lottery_type);
            if ($round_result) {
                $this->bet_round_id = $round_result[0]['round_id'];
                unset($round_result);
            } else {
                $this->setError('the round is forbidden!');
            }
        }

        if (!$this->error) {
            $this->setOddsType($position_type);
            $this->betParse2($bets);
            /*$handle = new Config();
            $config = $handle->getByName(array('LOTTERY_BET_MIN', 'LOTTERY_BET_MAX'));
            if($this->cost_amount < ($config['LOTTERY_BET_MIN'] * 100)) {
                $this->setError('bet min');
            }
            else if($this->cost_amount > ($config['LOTTERY_BET_MAX'] * 100)) {
                $this->setError('bet max');
            }*/
        }

        if (!$this->error) {
            if (!$this->checkMoney($this->cost_amount)) {
                $this->setError('money not enough!');
            }
        }
        if (!$this->error) {
            $this->_db->beginTransaction();
            // 扣钱
            if (1 != $this->minusMoney($this->cost_amount, $this->uid)) {
                $this->setError('minus money fail!');
            }

            if (!$this->error) {
                $finance_handle = new Finance();
                $previous_amount = $this->now_money_amount;
                $member_handle = new Member();
                $member = $member_handle->getByMemberId($this->uid, 'category');
                unset($member_handle);
                $category = $member['category'];
                $sql = array();
                $i = 0;
                foreach ($this->options as  $key => $value) {
                    $current_amount = $previous_amount - $value['bet_amount'];
                    $this->options[$key]['type'] =  $lottery_type;
                    $fnn_record[$key]['member_id'] = $this->uid;
                    $fnn_record[$key]['type'] = $this->finance_type;
                    $fnn_record[$key]['subtype'] = 6;
                    $fnn_record[$key]['amount'] = 0 - $value['bet_amount'];
                    $fnn_record[$key]['submit_time'] = $this->est_date;
                    $fnn_record[$key]['note'] = '单号' . $value['order_id'];
                    $fnn_record[$key]['previous_amount'] = $previous_amount;
                    $fnn_record[$key]['current_amount'] = $current_amount;
                    $fnn_record[$key]['member_category'] = $category;
                    $previous_amount = $current_amount;
                }
                unset($finance_handle);

                foreach (array_chunk($this->options, 500) as $row) {
                    $get_id = $this->_db->pdoMultiInsert('ltt_bet', $row);

                    if(!$get_id) {
                        $this->setError('bet error!');
                        break;
                    }
                }

                foreach ($fnn_record as $key => $value) {
                    $fnn_record[$key]['table_id'] = $get_id++;
                }

                foreach (array_chunk($fnn_record, 500) as $row) {
                    $get_id = $this->_db->pdoMultiInsert('fnn_record', $row);

                    if(!$get_id) {
                        $this->setError('bet error!');
                        break;
                    }
                }
            }
            if ($this->error) {
                $this->_db->rollBack();
            } else {
                $this->_db->commit();
            }
        }
        return array('error' => $this->error, 'data' => '');
    }

    protected function setOddsType($position_type)
    {
        $this->odds_type   = (0 == $position_type) ? 'odds_a' : 'odds_b';
        $this->return_type = (0 == $position_type) ? 'return_a' : 'return_b';
    }

    protected function oddsTypeName()
    {
        return $this->odds_type;
    }

    protected function returnWaterTypeName()
    {
        return $this->return_type;
    }

    protected function getCreateOrderId()
    {
        //$code = kg_md5($p1 . $p2 . microtime(true) . rand(1, 999));
        list($usec, $sec) = explode(' ', microtime());
        $rand_id = '100' . rand(1, 999);
        $rand_id = substr($rand_id, -3);
        $code = $sec . ($usec * 1000000) . $rand_id;
        return $code;
    }

    /**
     * @param array = array(odds_id1 => bet_amonut1, odds_id2 => bet_amount2 )
     */
    protected function betParse($data)
    {
        $data   = json_decode($data, true);

        $handle = new LotteryOdds();
        $odds   = $handle->getOddsLotteryByMember($this->lottery_type,$this->uid,$this->bet_round);
        unset($handle);

        $handle = new Config();
        $config = $handle->getByName(array('LOTTERY_BET_MIN', 'LOTTERY_BET_MAX'));
        unset($handle);
        $member_config_handle = new MemberConfig();
        $member_config        = $member_config_handle->getByMemberId($this->uid, 'lottery_min,lottery_max,check_bet_amount');
        unset($member_config_handle);
//        $bet_min = $member_config['lottery_min'] ? $member_config['lottery_min'] : $config['LOTTERY_BET_MIN'];
//        $bet_max = $member_config['lottery_max'] ? $member_config['lottery_max'] : $config['LOTTERY_BET_MAX'];
        $check_bet_amount = $member_config['check_bet_amount'];

        $bet_amount     = 0;
        $datetime       = $this->now_date;
        $odds_type_name = $this->oddsTypeName();
        $water_type_name = $this->returnWaterTypeName();
        foreach ($data as $key => $value) {
            $odds_id    = intval($key);
            $odds_value = 0;
            if (isset($odds[$odds_id])) {
                $odds_value = $odds[$odds_id][$odds_type_name];
                $water_value = $odds[$odds_id][$water_type_name];
                $bet_min = $odds[$odds_id]['amount_min'];
                $bet_max = $odds[$odds_id]['amount_max'];
                $bet_amount = money_to_db(intval($value));
                 Log::record('[debug] odds_id=' . $odds_id . ' $bet_amount=' . $bet_amount . 'bet_min = '. $bet_min , 'info', 'daily_return/');

                if ($bet_amount < ($bet_min * 100)) {
                    $this->setError('bet min');
                } elseif ($bet_amount > ($bet_max * 100)) {
                    $this->setError('bet max');
                } elseif ($bet_amount > 0) {
                    $status        = 1;                                                // 状态  投注时为1
                    $win_amount    = floor(round($bet_amount * $odds_value, 1)) - $bet_amount;   // 盈利  投注时为可赢金额
                    $return_amount = $this->getReturnAmount($this->uid, $bet_amount, $water_value);  // 返水
                    $method        = $odds[$odds_id]['settle_method'];                 // 执行方法
                    $position      = $odds[$odds_id]['position'];                      // 位置
                    $number        = $odds[$odds_id]['number'];                        // 号码
                    $bet_time      = $datetime;                                        // 投注时间
                    $settle_time   = $datetime;                                        // 结算时间  投注时为0
                    $title         = $odds[$odds_id]['name'];                          // 项目名称
                    $detail        = $title . '@' . $odds_value;                       // 内容
                    $odds_type     = 0;                                                // 水位
                    $order_id      = $this->getCreateOrderId();
                    // 加密串
                    $auth_code = $this->betAuthCode($this->uid, $this->bet_round_id, $this->lottery_type, $bet_time, $bet_amount, $win_amount, $return_amount, $odds_id, $odds_value, $method, $position, $number, $status, $settle_time);

                    $this->options[] = array(
                        'round_id'       => $this->bet_round_id,
                        'member_id'      => $this->uid,
                        'category'       => $this->lottery_type,
                        'bet_time'       => $bet_time,
                        'bet_amount'     => $bet_amount,
                        'win_amount'     => $win_amount,
                        'return_amount'  => $return_amount,
                        'odds_id'        => $odds_id,
                        'title'          => $title,
                        'detail'         => $detail,
                        'odds'           => $odds_value,
                        'odds_type'      => $odds_type,
                        'settle_method'  => $method,
                        'position'       => $position,
                        'number'         => $number,
                        'status'         => $status,
                        'settle_time'    => $settle_time,
                        'auth'           => $auth_code,
                        'order_id'       => $order_id,
                        'current_check_bet_amount' => $check_bet_amount
                    );

                    $this->cost_amount +=$bet_amount;
                } else {
                    $this->setError('bet money error!');
                }
            } else {
                $this->setError('odds id error!');
            }

            if ($this->error) {
                break;
            }
        }
    }

    // 连
    protected function betTypeTogether()
    {
        return array();
    }

    // 合
    protected function betTypeComb()
    {
        return array();
    }

    /**
     * 过关
     *
     */
    protected function betTypeThrough()
    {
        return array();
    }

    /**
     * 多选
     *
     */
    protected function betTypeAttached()
    {
        return array();
    }

    /**
     * 多选   子odds
     *
     */
    protected function betTypeAttachedChild()
    {
        return array();
    }

    protected function betTypeAttachedChildOnly()
    {
        return array();
    }

    protected function betParse2($data)
    {
        $data = json_decode($data, true);

        $datetime       = $this->now_date;
        $bet_time       = $datetime;
        $settle_time    = $datetime;
        $odds_type      = 0;
        $odds_type_name = $this->oddsTypeName();
        $water_type_name = $this->returnWaterTypeName();
        $handle         = new LotteryOdds();
        $odds   = $handle->getOddsLotteryByMember($this->lottery_type,$this->uid,$this->bet_round);

        $config_handle  = new Config();
        $config = $config_handle->getByName(array('LOTTERY_BET_MIN', 'LOTTERY_BET_MAX'));
        unset($config_handle);
        $member_config_handle = new MemberConfig();
        $member_config        = $member_config_handle->getByMemberId($this->uid, 'check_bet_amount');
        unset($member_config_handle);
//        $bet_min = $member_config['lottery_min'] ? $member_config['lottery_min'] : $config['LOTTERY_BET_MIN'];
//        $bet_max = $member_config['lottery_max'] ? $member_config['lottery_max'] : $config['LOTTERY_BET_MAX'];
        $check_bet_amount = $member_config['check_bet_amount'];

        $oldOddID = null;
        $old_bet_type = null;
        $old_bet_odds = null;
        $old_lottery_type = null;

        $old_comb_data_bet_type = null;
        $old_comb_data_child_bet_type = null;

        /**
         *  儲存六合彩連碼參數的資料
         */
        $old_attached_bet_type = null;
        $old_attached_child_bet_type = null;


        foreach ($data as $key => $value) {
            //$odds_id            = intval($key);
            $odds_id            = intval($value['odds_id']);
            $oldOddID = ($oldOddID)? $oldOddID : $odds_id;
            $bet_amount         = money_to_db($value['amount']);
            $bet_min = $odds[$odds_id]['amount_min'];
            $bet_max = $odds[$odds_id]['amount_max'];

            if ($bet_amount < ($bet_min * 100)) {
                $this->setError('bet min');
                break;
            } elseif ($bet_amount > ($bet_max * 100)) {
                $this->setError('bet max');
                break;
            }

            $this->cost_amount +=$bet_amount;
            //$return_amount      = $this->getReturnAmount($this->uid, $bet_amount);    // 返水
            if ($bet_amount <= 0) {
                $this->setError('bet amount error!');
                break;
            }

            /**
             *  判斷新的 odds_id 與 舊的 odds_id 是否相同
             *  假如不一樣的話就要在撈取一次
             * * */
            if (($odds_id == $oldOddID) && $old_bet_odds) {
                $bet_odds = $old_bet_odds;
            } else {
                $bet_odds = $handle->getOneOddsById($odds_id);
                $old_bet_odds = $bet_odds;
                $oldOddID = $odds_id;
            }
            //$bet_odds = $handle->getOneOddsById($odds_id);

            if (!$bet_odds) {
                $this->setError('bet odds id error!');
                break;
            }

            if (!isset($bet_odds[$odds_type_name])) {
                $this->setError('odds type error');
                break;
            }

            $water_value  = $bet_odds[$water_type_name];
            $return_amount = $this->getReturnAmount($this->uid, $bet_amount, $water_value);    // 返水


            $bet_type = $bet_odds['type'];
            if (($this->lottery_type != $old_lottery_type ||
                $bet_odds['type'] != $old_bet_type)) {
                $odds  = $handle->getOddsByBetType(intval($this->lottery_type), $bet_type);
            }
            $old_lottery_type = $this->lottery_type;
            $old_bet_type = $bet_odds['type'];

            //$bet_type    = $bet_odds['type'];
            // 主注单odds
            //$odds        = $handle->getOddsByBetType($this->lottery_type, $bet_type);

            $most_win_amount = 0;

            $through_data      = $this->betTypeThrough();
            $comb_data         = $this->betTypeComb();
            $attached_data     = $this->betTypeAttached();
            $together_data     = $this->betTypeTogether();

            // 过关
            if (isset($through_data[$bet_type])) {
                $this->cost_amount = 0; // 如果是过关 投注金额再这里置为0
                $child_odds = $handle->getOddsByBetType($this->lottery_type, $through_data[$bet_type]);
                if (!$child_odds) {
                    $this->setError('bet error!');
                    break;
                }
                $groups = $value['groups'];
                //foreach($groups as $row) {
                $order_id = $this->getCreateOrderId();
                //$bet_count = count($row);
                $bet_count = count($groups);
                $new_row   = array();
                $detail    = '';
                if (!$this->checkNoSameOdds($groups)) {
                    $this->setError('child odds count error!');
                    break;
                }
                $most_win_amount = $bet_amount;
                $this->cost_amount +=$bet_amount;
                foreach ($groups as $val) {
                    $child_odds_id = $val;
                    if (!isset($child_odds[$child_odds_id])) {
                        $this->setError('child odds id error!');
                        break;
                    }
                    $odds_value = $child_odds[$child_odds_id][$odds_type_name];
                    $most_win_amount *=$odds_value;

                    // 子注单详细
                    $title           = $child_odds[$child_odds_id]['name'];
                    $odds_value      = $child_odds[$child_odds_id][$odds_type_name];
                    $win_amount      = floor(round($bet_amount * $odds_value, 1)) - $bet_amount;
                    $method          = $child_odds[$child_odds_id]['settle_method'];
                    $position        = $child_odds[$child_odds_id]['position'];
                    $number          = $child_odds[$child_odds_id]['number'];
                    ;
                    $status          = 1;
                    $parlay          = 1;
                    $auth_code       = $this->betAuthCode(
                            $this->uid,
                            $this->bet_round_id,
                                            $this->lottery_type,
                            $bet_time,
                            $bet_amount,
                                            $win_amount,
                            $return_amount,
                            $child_odds_id,
                                            $odds_value,
                            $method,
                            $position,
                            $number,
                                            $status,
                            $settle_time
                        );
                    $this->options[] = array(
                            'round_id'       => $this->bet_round_id,
                            'member_id'      => $this->uid,
                            'category'       => $this->lottery_type,
                            'bet_time'       => $bet_time,
                            'bet_amount'     => $bet_amount,
                            'win_amount'     => $win_amount,
                            'return_amount'  => $return_amount,
                            'odds_id'        => $child_odds_id,
                            'title'          => $title,
                            'odds'           => $odds_value,
                            'odds_type'      => $odds_type,
                            'settle_method'  => $method,
                            'position'       => $position,
                            'number'         => $number,
                            'status'         => $status,
                            'settle_time'    => $settle_time,
                            'parlay'         => $parlay,
                            'order_id'    => $order_id,
                            'auth'           => $auth_code,
                            'current_check_bet_amount' => $check_bet_amount
                        );

                    $detail .= ' ' . $title . '@' . $odds_value . ' ';
                    //$detail .= $child_odds_id . ',';
                }

                $most_win_amount = floor(round($most_win_amount, 1));

                // 主注单详细
                    $status        = 1;                                         // 状态  投注时为1
                    $method        = $odds[$odds_id]['settle_method'];          // 执行方法
                    $position      = $odds[$odds_id]['position'];               // 位置
                    $number        = $odds[$odds_id]['number'];                 // 号码
                    $bet_time      = $datetime;                                 // 投注时间
                    $settle_time   = $datetime;                                 // 结算时间
                    $title         = $odds[$odds_id]['name'];                   // 项目名称
                    $win_amount    = $most_win_amount - $bet_amount;                          // 可赢最大金额
                    $odds_value    = $odds[$odds_id][$odds_type_name];          // 赔率值
                    $odds_type     = 0;                                         // 水位
                    $parlay        = 0;                                         // 主注单
                    $auth_code     = $this->betAuthCode(
                        $this->uid,
                        $this->bet_round_id,
                        $this->lottery_type,
                                            $bet_time,
                        $bet_amount,
                        $win_amount,
                        $return_amount,
                                            $odds_id,
                        $odds_value,
                        $method,
                        $position,
                                            $number,
                        $status,
                        $settle_time
                    );
                $bet_options = array(
                        'round_id'       => $this->bet_round_id,
                        'member_id'      => $this->uid,
                        'category'       => $this->lottery_type,
                        'bet_time'       => $bet_time,
                        'bet_amount'     => $bet_amount,
                        'win_amount'     => $win_amount,
                        'return_amount'  => $return_amount,
                        'odds_id'        => $odds_id,
                        'title'          => $title,
                        'detail'         => $detail,
                        'odds'           => $odds_value,
                        'odds_type'      => $odds_type,
                        'settle_method'  => $method,
                        'position'       => $position,
                        'number'         => $number,
                        'status'         => $status,
                        'settle_time'    => $settle_time,
                        'auth'           => $auth_code,
                        'parlay'         => $parlay,
                        'order_id'    => $order_id,
                        'current_check_bet_amount' => $check_bet_amount
                    );
                $this->options[] = $bet_options;
            //}
            } elseif (isset($comb_data[$bet_type])) {
                // 合肖
                if (($old_comb_data_bet_type != $comb_data[$bet_type]) ||
                    ($old_lottery_type != $this->lottery_type)) {
                    $child_odds = $handle->getOddsByBetType($this->lottery_type, $comb_data[$bet_type]);
                }
                $old_comb_data_bet_type = $comb_data[$bet_type];
                $old_lottery_type = $this->lottery_type;
                //$child_odds = $handle->getOddsByBetType($this->lottery_type, $comb_data[$bet_type]);
                if (!$child_odds) {
                    $this->setError('bet error!');
                    break;
                }
                $groups = $value['groups'];
                $most_win_amount = $bet_amount;
                //foreach($groups as $row) {
                $order_id = $this->getCreateOrderId();
                //$bet_count = count($row);
                $bet_count = count($groups);
                $new_row   = array();
                $detail    = '';
                $shengxiaoNumbers = array();
                if (!$this->checkNoSameOdds($groups)) {
                    $this->setError('child odds count error!');
                    break;
                }
                foreach ($groups as $val) {
                    $child_odds_id = $val;
                    if (!isset($child_odds[$child_odds_id])) {
                        $this->setError('child odds id error!');
                        break;
                    }

                    // 子注单详细
                    $title           = $child_odds[$child_odds_id]['name'];
                    $odds_value      = $child_odds[$child_odds_id][$odds_type_name];
                    $win_amount      = floor(round($bet_amount * $odds_value, 1)) - $bet_amount;
                    $method          = $child_odds[$child_odds_id]['settle_method'];
                    $position        = $child_odds[$child_odds_id]['position'];
                    $number          = $child_odds[$child_odds_id]['number'];
                    ;
                    $status          = 1;
                    $parlay          = 1;
                    $shengxiaoNumbers[] = $number;
                    $auth_code       = $this->betAuthCode(
                            $this->uid,
                            $this->bet_round_id,
                                            $this->lottery_type,
                            $bet_time,
                            $bet_amount,
                                            $win_amount,
                            $return_amount,
                            $child_odds_id,
                                            $odds_value,
                            $method,
                            $position,
                            $number,
                                            $status,
                            $settle_time
                        );
                    $this->options[] = array(
                            'round_id'       => $this->bet_round_id,
                            'member_id'      => $this->uid,
                            'category'       => $this->lottery_type,
                            'bet_time'       => $bet_time,
                            'bet_amount'     => $bet_amount,
                            'win_amount'     => $win_amount,
                            'return_amount'  => $return_amount,
                            'odds_id'        => $child_odds_id,
                            'title'          => $title,
                            'detail'         => null,
                            'odds'           => $odds_value,
                            'odds_type'      => $odds_type,
                            'settle_method'  => $method,
                            'position'       => $position,
                            'number'         => $number,
                            'status'         => $status,
                            'settle_time'    => $settle_time,
                            'parlay'         => $parlay,
                            'order_id'    => $order_id,
                            'auth'           => $auth_code,
                            'current_check_bet_amount' => $check_bet_amount
                        );

                    $detail .= ' ' . $title . ',';
                }
                // 主注单详细
                    $status        = 1;                                         // 状态  投注时为1
                    $method        = $odds[$odds_id]['settle_method'];          // 执行方法
                    $position      = $position;//$odds[$odds_id]['position'];               // 位置
                    $number        = count($shengxiaoNumbers) > 0 ? join(',', $shengxiaoNumbers) : $odds[$odds_id]['number'];//$odds[$odds_id]['number'];                 // 号码
                    $bet_time      = $datetime;                                 // 投注时间
                    $settle_time   = $datetime;                                 // 结算时间
                    $title         = $odds[$odds_id]['name'];                   // 项目名称
                    $odds_value    = $odds[$odds_id][$odds_type_name];          // 赔率值
                    $win_amount    = floor(round($bet_amount * $odds_value, 1)) - $bet_amount;          // 可赢最大金额
                    $odds_type     = 0;                                         // 水位
                    $parlay        = 0;                                         // 主注单
                    $auth_code     = $this->betAuthCode(
                        $this->uid,
                        $this->bet_round_id,
                        $this->lottery_type,
                                            $bet_time,
                        $bet_amount,
                        $win_amount,
                        $return_amount,
                                            $odds_id,
                        $odds_value,
                        $method,
                        $position,
                                            $number,
                        $status,
                        $settle_time
                    );
                $detail = $title . '|' . trim($detail, ',') . ' @' . $odds_value . ' ';
                $bet_options = array(
                        'round_id'       => $this->bet_round_id,
                        'member_id'      => $this->uid,
                        'category'       => $this->lottery_type,
                        'bet_time'       => $bet_time,
                        'bet_amount'     => $bet_amount,
                        'win_amount'     => $win_amount,
                        'return_amount'  => $return_amount,
                        'odds_id'        => $odds_id,
                        'title'          => $title,
                        'detail'         => $detail,
                        'odds'           => $odds_value,
                        'odds_type'      => $odds_type,
                        'settle_method'  => $method,
                        'position'       => $position,
                        'number'         => $number,
                        'status'         => $status,
                        'settle_time'    => $settle_time,
                        'parlay'         => $parlay,
                        'order_id'    => $order_id,
                        'auth'           => $auth_code,
                        'current_check_bet_amount' => $check_bet_amount
                    );
                $this->options[] = $bet_options;
            //}
            } elseif (isset($together_data[$bet_type])) {
                // 连肖
                $child_odds = $handle->getOddsByBetType($this->lottery_type, $together_data[$bet_type]);
                if (!$child_odds) {
                    $this->setError('bet error!');
                    break;
                }
                $groups = $value['groups'];
                //foreach($groups as $row) {
                $order_id = $this->getCreateOrderId();
                //$bet_count = count($row);
                $bet_count = count($groups);
                $new_row   = array();
                $detail    = '';
                if (!$this->checkNoSameOdds($groups)) {
                    $this->setError('child odds count error!');
                    break;
                }
                $big_odds   = $odds[$odds_id][$odds_type_name];
                $total_odds = $big_odds;
                $shengxiaoNumbers = array();
                foreach ($groups as $val) {
                    $child_odds_id = $val;
                    if (!isset($child_odds[$child_odds_id])) {
                        $this->setError('child odds id error!');
                        break;
                    }

                    // 子注单详细
                    $title           = $child_odds[$child_odds_id]['name'];
                    $odds_value      = $child_odds[$child_odds_id][$odds_type_name];
                    $win_amount      = floor(round($bet_amount * $odds_value, 1)) - $bet_amount;
                    $method          = $child_odds[$child_odds_id]['settle_method'];
                    $position        = $child_odds[$child_odds_id]['position'];
                    $number          = $child_odds[$child_odds_id]['number'];
                    ;
                    $status          = 1;
                    $parlay          = 1;
                    $auth_code       = $this->betAuthCode(
                            $this->uid,
                            $this->bet_round_id,
                                            $this->lottery_type,
                            $bet_time,
                            $bet_amount,
                                            $win_amount,
                            $return_amount,
                            $child_odds_id,
                                            $odds_value,
                            $method,
                            $position,
                            $number,
                                            $status,
                            $settle_time
                        );
                    $this->options[] = array(
                            'round_id'       => $this->bet_round_id,
                            'member_id'      => $this->uid,
                            'category'       => $this->lottery_type,
                            'bet_time'       => $bet_time,
                            'bet_amount'     => $bet_amount,
                            'win_amount'     => $win_amount,
                            'return_amount'  => $return_amount,
                            'odds_id'        => $child_odds_id,
                            'title'          => $title,
                            'detail'         => null,
                            'odds'           => $odds_value,
                            'odds_type'      => $odds_type,
                            'settle_method'  => $method,
                            'position'       => $position,
                            'number'         => $number,
                            'status'         => $status,
                            'settle_time'    => $settle_time,
                            'parlay'         => $parlay,
                            'order_id'    => $order_id,
                            'auth'           => $auth_code,
                            'current_check_bet_amount' => $check_bet_amount
                        );
                    $shengxiaoNumbers[] = $number;
                    $detail .= ' ' . $title . '@' . $odds_value;
                    //$total_odds -= ($big_odds - $odds_value);
                    // 赔率算法调整为使用最低赔率
                    if ($odds_value < $total_odds) {
                        $total_odds = $odds_value;
                    }
                }
                $total_odds = $total_odds > 0 ? $total_odds : 0;
                $most_win_amount = floor(round($bet_amount * $total_odds, 1));

                // 主注单详细
                    $status        = 1;                                         // 状态  投注时为1
                    $method        = $odds[$odds_id]['settle_method'];          // 执行方法
                    $position      = $odds[$odds_id]['position'];               // 位置
                    $number        = count($shengxiaoNumbers) > 0 ? join(',', $shengxiaoNumbers) : $odds[$odds_id]['number']; //$odds[$odds_id]['number'];                 // 号码
                    $bet_time      = $datetime;                                 // 投注时间
                    $settle_time   = $datetime;                                 // 结算时间
                    $title         = $odds[$odds_id]['name'];                   // 项目名称
                    $win_amount    = $most_win_amount - $bet_amount;                          // 可赢最大金额
                    $odds_value    = $odds[$odds_id][$odds_type_name];          // 赔率值
                    $odds_type     = 0;                                         // 水位
                    $parlay        = 0;                                         // 主注单
                    $auth_code     = $this->betAuthCode(
                        $this->uid,
                        $this->bet_round_id,
                        $this->lottery_type,
                                            $bet_time,
                        $bet_amount,
                        $win_amount,
                        $return_amount,
                                            $odds_id,
                        $odds_value,
                        $method,
                        $position,
                                            $number,
                        $status,
                        $settle_time
                    );
                $detail = $title . '|' .  $detail;
                $bet_options = array(
                        'round_id'       => $this->bet_round_id,
                        'member_id'      => $this->uid,
                        'category'       => $this->lottery_type,
                        'bet_time'       => $bet_time,
                        'bet_amount'     => $bet_amount,
                        'win_amount'     => $win_amount,
                        'return_amount'  => $return_amount,
                        'odds_id'        => $odds_id,
                        'title'          => $title,
                        'detail'         => $detail,
                        'odds'           => $odds_value,
                        'odds_type'      => $odds_type,
                        'settle_method'  => $method,
                        'position'       => $position,
                        'number'         => $number,
                        'status'         => $status,
                        'settle_time'    => $settle_time,
                        'parlay'         => $parlay,
                        'order_id'    => $order_id,
                        'auth'           => $auth_code,
                        'current_check_bet_amount' => $check_bet_amount
                    );
                $this->options[] = $bet_options;
            //}
            } elseif (isset($attached_data[$bet_type])) {
                // 连码
                if (($old_lottery_type != $this->lottery_type)  ||
                     ($old_attached_bet_type  != $attached_data[$bet_type])) {
                    $child_odds = $handle->getOddsByBetType($this->lottery_type, $attached_data[$bet_type]);
                }
                $old_lottery_type = $this->lottery_type;
                $old_attached_bet_type = $attached_data[$bet_type];
                //$child_odds = $handle->getOddsByBetType($this->lottery_type, $attached_data[$bet_type]);
                if (!$child_odds) {
                    $this->setError('bet error!');
                    break;
                }

                // 可赢最大金额
                $attached_child_data      = $this->betTypeAttachedChild();
                $attached_child_data_only = $this->betTypeAttachedChildOnly();
                if (isset($attached_child_data[$bet_type])) {
                    if (($old_lottery_type != $this->lottery_type) ||
                        ($old_attached_child_bet_type != $attached_child_data[$bet_type])) {
                        $child_odds2 = $handle->getOddsByBetType($this->lottery_type, $attached_child_data[$bet_type]);
                    }

                    $old_lottery_type  = $this->lottery_type;
                    $old_attached_child_bet_type = $attached_child_data[$bet_type];

                    //$child_odds2 = $handle->getOddsByBetType($this->lottery_type, $attached_child_data[$bet_type]);
                    foreach ($child_odds2 as $child2_row) {
                        $child_odds2_id  = $child2_row['odds_id'];
                        $odds_value      = $child_odds2[$child_odds2_id][$odds_type_name];
                        $win_amount      = floor(round($bet_amount * $odds_value, 1));
                        $most_win_amount = $most_win_amount < $win_amount ? $win_amount : $most_win_amount;
                    }
                } elseif (isset($attached_child_data_only[$bet_type])) {
                    $odds_value      = $odds[$odds_id][$odds_type_name];
                    $win_amount      = floor(round($bet_amount * $odds_value, 1));
                    $most_win_amount = $most_win_amount < $win_amount ? $win_amount : $most_win_amount;
                } else {
                    $this->setError('bet data error!');
                    break;
                }

                // 组合注单
                $groups = $value['groups'];
                //foreach($groups as $row) {
                $order_id = $this->getCreateOrderId();
                if (!$this->checkNoSameOdds($groups)) {
                    $this->setError('child odds count error!');
                    break;
                }
                // 子注单number里的内容
                $child_num = array();
                //$bet_count = count($row);
                $bet_count = count($groups);
                $new_row   = array();
                foreach ($groups as $val) {
                    $child_odds_id = $val;
                    if (isset($child_odds[$child_odds_id])) {
                        $child_num[] = $child_odds[$child_odds_id]['number'];
                    } else {
                        $this->setError('child odds id error!');
                        break;
                    }
                    $new_row[$child_odds_id] = $val;
                }
                /*foreach($row as $val) {
                    $child_odds_id = $val;
                    if(isset($child_odds[$child_odds_id])) {
                        $child_num[] = $child_odds[$child_odds_id]['number'];
                    }
                    else {
                        $this->setError('child odds id error!');
                        break;
                    }
                    $new_row[$child_odds_id] = $val;
                }*/
                $child_num = implode(',', $child_num);

                // 拼主注单的投注内容
                $detail = $child_num . ' ';

                // 判断子注单是否合法  有没有重复的  是否符合子注单数量要求
                $bet_number = $odds[$odds_id]['number'];
                if ($bet_count == count($new_row) && $bet_count == $bet_number) {
                    $lm_odds_str  = array();
                    // 子注单详细
                    if (isset($child_odds2) && $child_odds2) {
                        foreach ($child_odds2 as $val) {
                            $child_odds2_id  = $val['odds_id'];
                            $title           = $child_odds2[$child_odds2_id]['name'];
                            $odds_value      = $child_odds2[$child_odds2_id][$odds_type_name];
                            $win_amount      = floor(round($odds_value * $bet_amount, 1)) - $bet_amount;
                            $method          = $child_odds2[$child_odds2_id]['settle_method'];
                            $position        = $child_odds2[$child_odds2_id]['position'];
                            //$number          = $child_num;
                            $number          = $child_odds2[$child_odds2_id]['number'];
                            $status          = 1;
                            $parlay          = 1;
                            $auth_code       = $this->betAuthCode(
                                    $this->uid,
                                    $this->bet_round_id,
                                    $this->lottery_type,
                                                        $bet_time,
                                    $bet_amount,
                                    $win_amount,
                                    $return_amount,
                                    $child_odds2_id,
                                                        $odds_value,
                                    $method,
                                    $position,
                                    $number,
                                    $status,
                                    $settle_time
                                );
                            $this->options[] = array(
                                    'round_id'       => $this->bet_round_id,
                                    'member_id'      => $this->uid,
                                    'category'       => $this->lottery_type,
                                    'bet_time'       => $bet_time,
                                    'bet_amount'     => $bet_amount,
                                    'win_amount'     => $win_amount,
                                    'return_amount'  => $return_amount,
                                    'odds_id'        => $child_odds2_id,
                                    'title'          => $title,
                                    'detail'         => null,
                                    'odds'           => $odds_value,
                                    'odds_type'      => $odds_type,
                                    'settle_method'  => $method,
                                    'position'       => $position,
                                    'number'         => $number,
                                    'status'         => $status,
                                    'settle_time'    => $settle_time,
                                    'parlay'         => $parlay,
                                    'order_id'    => $order_id,
                                    'auth'           => $auth_code,
                                    'current_check_bet_amount' => $check_bet_amount
                                );

                            // 拼主注单的投注内容
                            $detail .= $title . '@' . $odds_value . ' ';
                            $lm_odds_str[] = $odds_value;
                        }
                    }

                    // 主注单详细
                        $status        = 1;                                         // 状态  投注时为1
                        $method        = $odds[$odds_id]['settle_method'];          // 执行方法
                        $position      = $odds[$odds_id]['position'];               // 位置
                        //$number        = $odds[$odds_id]['number'];                 // 号码
                        $number        = $child_num;
                    $bet_time      = $datetime;                                 // 投注时间
                        $settle_time   = $datetime;                                 // 结算时间
                        $title         = $odds[$odds_id]['name'];                   // 项目名称
                        $win_amount    = $most_win_amount - $bet_amount;                          // 可赢最大金额
                        $odds_value    = $odds[$odds_id][$odds_type_name];          // 赔率值
                        $odds_type     = 0;                                         // 水位
                        $parlay        = 0;                                         // 主注单
                        $auth_code     = $this->betAuthCode(
                            $this->uid,
                            $this->bet_round_id,
                            $this->lottery_type,
                                                $bet_time,
                            $bet_amount,
                            $win_amount,
                            $return_amount,
                                                $odds_id,
                            $odds_value,
                            $method,
                            $position,
                                                $number,
                            $status,
                            $settle_time
                        );
                    $detail = $title . '|' . $detail;
                    $this->options[] = array(
                            'round_id'       => $this->bet_round_id,
                            'member_id'      => $this->uid,
                            'category'       => $this->lottery_type,
                            'bet_time'       => $bet_time,
                            'bet_amount'     => $bet_amount,
                            'win_amount'     => $win_amount,
                            'return_amount'  => $return_amount,
                            'odds_id'        => $odds_id,
                            'title'          => $title,
                            'detail'         => $detail,
                            'odds'           => count($lm_odds_str) > 0 ? join(',', $lm_odds_str) : $odds_value,//$odds_value,
                            'odds_type'      => $odds_type,
                            'settle_method'  => $method,
                            'position'       => $position,
                            'number'         => $number,
                            'status'         => $status,
                            'settle_time'    => $settle_time,
                            'parlay'         => $parlay,
                            'order_id'       => $order_id,
                            'auth'           => $auth_code,
                            'current_check_bet_amount' => $check_bet_amount
                        );

                // 总投注额
                        //$this->cost_amount +=$bet_amount;
                } else {
                    $this->setError('child odds id count error!');
                    break;
                }

                //}
            }

            if ($this->error) {
                break;
            }
        }
    }

    /**
     * 组合查询条件
     * @param $options1 = array('member_id' => int, 'bet_id' => int, ...) 等于
     * @param $options2 = array('bet_time' => date, ...)  大于等于
     * @param $options3 = array('bet_time' => date, ...)  小于等于
     * @param $orders = arrray('round_id' => 'DESC', 'bet_id' => 'ASC', ...)
     * @param $limits = String '100' '0,10'
     * @return array('sql' => String, 'parameters' => array)
     */
    private function getConditions($options1 = array(), $options2 = array(), $options3 = array(), $orders = array(), $limits ='', $where = true)
    {
        $str = '';
        $arr = array();
        if ($where) {
            $str = ' WHERE 1 ';
        }

        if ($options1) {
            foreach ($options1 as $key => $value) {
                $str .= ' AND ' . $key . '=? ';
                $arr[] = $value;
            }
        }

        if ($options2) {
            foreach ($options2 as $key => $value) {
                $str .= ' AND ' . $key . '>=? ';
                $arr[] = $value;
            }
        }
        if ($options3) {
            foreach ($options3 as $key => $value) {
                $str .= ' AND ' . $key . '<=? ';
                $arr[] = $value;
            }
        }

        $str = trim($str, ',');
        if ($orders) {
            $str .= ' ORDER BY ';
            foreach ($orders as $key => $value) {
                $str .= $key . ' ' . $value . ',';
            }
            $str = trim($str, ',');
        }
        if ($limits) {
            $str .= ' LIMIT ' . $limits;
        }

        return array('sql' => $str, 'parameters' => $arr);
    }

    private function betFields()
    {
        $sql = 'SELECT bet_id,category,round_id,member_id,parlay,order_id,odds_id,title,odds,odds_type,position,number,settle_method,bet_amount,win_amount,valid_amount,return_amount,bet_time,settle_time,auth,status FROM ltt_bet';
        return $sql;
    }

    private function betLeftJoinFields()
    {
        $sql = 'SELECT a.bet_id,a.title,a.detail,a.odds,' .
                'a.bet_amount,a.win_amount,a.valid_amount,' .
                'a.return_amount,a.bet_time,a.settle_time,a.order_id,a.status,a.category, ' .
                'b.account,c.number as round ' .
                ' FROM ltt_bet AS a LEFT JOIN uc_member as b ON a.member_id=b.member_id ' .
                ' LEFT JOIN ltt_round as c ON a.round_id=c.round_id ';
        return $sql;
    }

    /**
     * 根据条件获取注单记录
     * @param $options1 = array('member_id' => int, 'bet_id' => int, ...) 等于
     * @param $options2 = array('bet_time' => date, ...)  大于等于
     * @param $options3 = array('bet_time' => date, ...)  小于等于
     * @param $orders = array('round_id' => 'DESC', 'bet_id' => 'ASC', ...)
     * @param $limits = String like '100' '0,10'
     * @return array
     */
    private function getBets($options1 = array(), $options2 = array(), $options3 = array(), $orders = array(), $limits = '')
    {
        $conditions = $this->getConditions($options1, $options2, $options3, $orders, $limits);
        $sql        = $this->betFields() . $conditions['sql'];
        $result     = $this->_db->query($sql, $conditions['parameters']);
        return $result;
    }

    public function dailyReturn()
    {
        // $time_day   = date('Y-m-d', $this->est_time);
        // $time_start = strtotime($time_day) ;
        // $time_stop  = date('Y-m-d H:i:s', $time_start);
        // $time_start = date('Y-m-d H:i:s', $time_start - 86400);  // 对美东时间昨天返水

        $time_start = date('Y-m-d', time() - 86400);
        $time_stop  = $time_start . ' 23:59:59';

        // $sql = "SELECT id,status FROM daily_return WHERE DATE_FORMAT(start_time,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d')";
        // $dailyReturnCheck = $this->_db->query($sql);
        // if (!empty($dailyReturnCheck)) {
             Log::record('[dailyReturn] 每日反水統計執行', 'info', 'daily_return/');
        //     return;
        // }

        // $sql = 'INSERT INTO daily_return set start_time=? ,status =?';
        // $insertId = $this->_db->query($sql, array(date('Y-m-d H:i:s'),'0'));

        $sql  = 'SELECT member_id,SUM(valid_amount) AS valid_amount FROM ltt_bet';
        $sql .= ' WHERE bet_time>=? AND bet_time<=? AND status=2 AND return_amount=0 GROUP BY member_id';
        $result = $this->_db->query($sql, array($time_start, $time_stop));
        if ($result) {
            foreach ($result as $row) {
                $this->dailyMemberReturn($row['member_id'], $row['valid_amount'], $time_start);
            }
        }
        // $sql = 'UPDATE daily_return SET status=1 , end_time=? WHERE id=?';
        // $this->_db->query($sql, array(date('Y-m-d H:i:s'),$insertId));
        Log::record('[dailyReturn] 每日反水統計結束！！', 'info', 'daily_return/');
    }

    private function dailyMemberReturn($member_id, $valid_amount, $date)
    {
        $member_handle = new Member();
        $member = $member_handle->getByMemberId($member_id, 'account');
        if (!$member) {
            Log::record('Can found memberid: ' . $member_id, 'error','lottery/');
            return;
        }

        $account = $member['account'];

        $mc_handle = new MemberConfig();
        $member_config = $mc_handle->getByMemberId($member_id);
        unset($mc_handle);

        $return_rate = 0;
        if (2 == $member_config['lottery_return_type']) {
            $rate_handle = new MemberLevel();
            $return_rate = $rate_handle->getRateId($valid_amount);
            unset($rate_handle);
        }
        if (!$return_rate) {
            return; // 无需返水
        }

        $note = $account . ' ' . $date . ' 彩票天天返水';

        $finance_handle = new Finance();
        $check = $finance_handle->checkDailyReturn($member_id, $note);
        if ($check > 0) {
            return; // 已经返水
        }

        $actual_return_amount = ($valid_amount * $return_rate / 100);
        //$actual_return_amount -= $return_amount;
        $actual_return_amount = money_from_db($actual_return_amount);

        if ($actual_return_amount > 0) {
            $argument_adjust = array(
                'account'      => $account,
                'parent_type'  => 2,    // 表示Finance中type值  彩票类型
                'type'         => 11,   // 表示Finance中subtype值  自动返水
                'amount'       => $actual_return_amount,
                'award'        => 0,
                'check'        => 0,
                'check_amount' => $actual_return_amount,
                'description' => '',
                'note'         => $note
            );
            $adjust_handle = new Adjust();
            $adjust_handle->add('', $argument_adjust, true);
            unset($adjust_handle);
        }
    }

    /**
     * 会员注单统计
     * @param $lottery int
     * @return array
     */
    public function getUserBetsStatis($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $lottery       = isset($parameters['lottery']) ? intval($parameters['lottery']) : 0;
        $account       = isset($parameters['account']) ? $parameters['account'] : '';
        $start_time    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone     = isset($parameters['time_zone']) ? $parameters['time_zone'] : 1;  // 彩票默认北京时间
        $order         = isset($parameters['order']) ? intval($parameters['order']) : 0;
        $page          = isset($parameters['page']) ? intval($parameters['page']) : '';
        $page_size     = isset($parameters['page_size']) ? $parameters['page_size'] : 20;
        $category      = isset($parameters['category']) ? $parameters['category'] : 0;
        $page_size     = 20;

        $con = ($lottery == 9999)? ' category != 9' : ' category = 9' ;

        if ($stop_time) {
            $day_stop  = $stop_time . ' 23:59:59';
            $stop_time = strtotime($day_stop) ? $day_stop : $stop_time;
        }
        if (0 == $time_zone) {
            // 美东时间
            if ($start_time) {
                $start_time = et_to_bj_date($start_time);
            }
            if ($stop_time) {
                $stop_time = et_to_bj_date($stop_time);
            }
        }

        $options1 = array();
        $options1['b.parlay'] = 0;
        if ($round > 0) { // 撈取期數
            //$options1['b.number'] = $round;
            $ltt_round_parameter = array();
            if ($lottery == 0) {
                $sql = 'SELECT round_id FROM ltt_round WHERE number = ? AND '.$con ;
                $ltt_round_parameter = array($round,10);
            } else {
                $sql = 'SELECT round_id FROM ltt_round WHERE number = ? AND '.$con;
                $ltt_round_parameter = array($round);
            }
            $result = $this->_db->query($sql, $ltt_round_parameter);
            unset($ltt_round_parameter);
            $options1['b.round_id'] = $result[0]['round_id'];
        }

        if ($lottery > 0) {
            $options1['b.category'] = $lottery;
        }

        $member_id = 0;
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $options1['b.member_id'] = 0; // 说明没有该用户
            }
            unset($handle, $result);
        }

        if ($member_id > 0) {
            $options1['b.member_id'] = $member_id;
        }

        if ($category > 0) {
            $options1['a.category'] = $category;
        }

        $options2 = array();
        $options2['b.bet_amount'] = 1;
        if ($start_time) {
            $options2['b.bet_time'] = $start_time;
        }

        // 如果0则取>=10的所有
        if (0 == $lottery) {
            $options2['b.category'] = 10;
        }

        $options3 = array();
        if ($stop_time) {
            $options3['b.bet_time'] = $stop_time;
        }

        if ($order > 0) {
            switch ($order) {
                case 1:
                    $orders['win_total'] = 'DESC';
                    break;
                case 2:
                    $orders['win_total'] = 'ASC';
                    break;
                case 3:
                    $orders['bet_total'] = 'DESC';
                    break;
                case 4:
                    $orders['bet_total'] = 'ASC';
                    break;
                case 5:
                    $orders['return_total'] = 'DESC';
                    break;
                case 6:
                    $orders['return_total'] = 'ASC';
                    break;
            }
        } else {
            $orders = array(
                'a.member_id' => 'DESC'
            );
        }

        $condition_total = $this->getConditions($options1, $options2, $options3, '', '', false);
        $condition2     = $this->getConditions(array(), array(), array(), $orders, '', false);

        $sql       = 'SELECT a.member_id, a.account, a.note, a.register_time, a.level, a.amount,';
        $sql      .= ' COUNT(b.bet_id) AS bet_record_total, SUM(b.bet_amount) AS bet_total, SUM(b.valid_amount) AS valid_total,';
        $sql      .= ' SUM(b.win_amount) AS win_total, SUM(b.return_amount) AS return_total ';
        $sql      .= ' FROM uc_member as a LEFT JOIN ltt_bet AS b ON a.member_id=b.member_id WHERE b.status<=2 ' . $condition_total['sql'];
        $sql      .= ' GROUP BY a.member_id ' . $condition2['sql'];
        $set_result    = $this->_db->query($sql, $condition_total['parameters']);
        if (!$set_result) {
            $set_result = array();
        }

        $sql       = 'SELECT a.member_id, COUNT(b.bet_id) AS unsettle_bet_record_total, SUM(b.bet_amount) AS bet_total, SUM(b.valid_amount) AS valid_total, ';
        $sql      .= ' SUM(b.win_amount) AS win_total, SUM(b.return_amount) AS return_total ';
        $sql      .= ' FROM uc_member as a LEFT JOIN ltt_bet AS b ON a.member_id=b.member_id WHERE b.status<=1 ' . $condition_total['sql'];
        $sql      .= ' GROUP BY a.member_id ' . $condition2['sql'];
        $unset_result    = $this->_db->query($sql, $condition_total['parameters']);
        if (!$unset_result) {
            $unset_result = array();
        }

        $unset_data = array();
        foreach ($unset_result as $row) {
            $unset_data[$row['member_id']] = $row;
        }
        unset($unset_result);

        $data = array();
        foreach ($set_result as $key => $row) {
            if (isset($unset_data[$row['member_id']])) {
                $set_result[$key]['bet_total']       -= $unset_data[$row['member_id']]['bet_total'];
                $set_result[$key]['valid_total']     -= $unset_data[$row['member_id']]['valid_total'];
                $set_result[$key]['win_total']       -= $unset_data[$row['member_id']]['win_total'];
                $set_result[$key]['return_total']    -= $unset_data[$row['member_id']]['return_total'];
                $set_result[$key]['unsettle_bet_total']  = $unset_data[$row['member_id']]['bet_total'];
                $set_result[$key]['unsettle_bet_record_total'] = $unset_data[$row['member_id']]['unsettle_bet_record_total'];
                $set_result[$key]['bet_record_total'] = $row['bet_record_total']-$unset_data[$row['member_id']]['unsettle_bet_record_total'];
                unset($unset_data[$row['member_id']]);
            } else {
                $set_result[$key]['unsettle_bet_total'] = 0;
                $set_result[$key]['unsettle_bet_record_total'] = 0;
            }
            $data[$row['member_id']] = $set_result[$key];
        }
        unset($set_result);

        foreach ($unset_data as $row) {
            if (!isset($data[$row['member_id']])) {
                $data[$row['member_id']] = array(
                    'bet_total'       => 0,
                    'valid_total'     => 0,
                    'win_total'       => 0,
                    'return_total'    => 0,
                    'unsettle_bet_record_total' => $row['unsettle_bet_record_total'],
                    'unsettle_bet_total' => $row['bet_total']
                );
            }
        }

        return $data;
    }

    /**
     * [getBetsStatisGroupByUser 查詢會員的下注狀況]
     * @param  [type]  $parameters [description]
     * @return boolean             [description]
     */
    public function getBetsStatisGroupByUser($parameters)
    {

        //1. 蒐集過濾條件
        $account       = isset($parameters['account']) ? $parameters['account'] : '';
        $start_time    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone     = isset($parameters['time_zone']) ? $parameters['time_zone'] : 1;  // 彩票默认北京时间
        $order         = isset($parameters['order_by']) ? intval($parameters['order_by']) : 0;
        $page          = isset($parameters['page']) ? intval($parameters['page']) : 1;
        $category      = isset($parameters['category']) ? $parameters['category'] : 0;
        $page_size     = isset($parameters['page_size']) ? $parameters['page_size'] : 20;
        $fuzzy     = isset($parameters['fuzzy']) ? $parameters['fuzzy'] : 0;

        $result = array();

        if ($stop_time) {
            $day_stop  = $stop_time . ' 23:59:59';
            $stop_time = strtotime($day_stop) ? $day_stop : $stop_time;
        }
        if (1 == $time_zone) {
            // 美东时间
            if ($start_time) {
                $start_time = et_to_bj_date($start_time);
            }
            if ($stop_time) {
                $stop_time = et_to_bj_date($stop_time);
            }
        }
        // a=會員,b=注單
        //        $member_id = 0;
        //        if ($account) {
        //            $handle = new Member();
        //            $result = $handle->getByAccount($account, 'member_id');
        //            if ($result) {
        //                $member_id = $result['member_id'];
        //            } else {
        //                $options1['b.member_id'] = 0; // 说明没有该用户
        //            }
        //            unset($handle, $result);
        //        }
        //        // 查詢特定使用者
        //        if ($member_id > 0) {
        //            $options1['b.member_id'] = $member_id;
        //        }
        $where = '';
        if (isset($account) && $account) {
            if (isset($fuzzy) && $fuzzy) {
                $where .= ' AND a.account Like "%'.$account.'%" ';
            } else {
                $options1['a.account'] = $account;
            }
        }

        $options2 = array();
        $options2['b.bet_amount'] = 1;
        if ($start_time) {
            $options2['b.submit_time'] = $start_time;//下單時間 開始
        }

        $options3 = array();
        if ($stop_time) {
            $options3['b.submit_time'] = $stop_time;//下單時間 結束
        }

        if ($category != 0) {
            $options1['a.category'] = $category;//會員種類
        }

        if ($order > 0) {
            switch ($order) {
                case 1:
                    $orders['win_total'] = 'DESC';
                    break;
                case 2:
                    $orders['win_total'] = 'ASC';
                    break;
                case 3:
                    $orders['bet_total'] = 'DESC';
                    break;
                case 4:
                    $orders['bet_total'] = 'ASC';
                    break;
                case 5:
                    $orders['return_total'] = 'DESC';
                    break;
                case 6:
                    $orders['return_total'] = 'ASC';
                    break;
            }
        } else {
            $orders = array(
                'a.member_id' => 'DESC'
            );
        }

        $condition_total = $this->getConditions($options1, $options2, $options3, '', '', false);
        $condition2     = $this->getConditions(array(), array(), array(), $orders, '', false);

        //1.2組合基本sql
        $sqlRaw = 'SELECT DISTINCT a.member_id , amount ';
        // 過濾條件
        $sqlRaw .= ' FROM uc_member as a RIGHT JOIN ltt_bet as b ON a.member_id = b.member_id WHERE 1 ' .$where. $condition_total['sql'];
        //        $sqlRaw .= ' ' . $condition2['sql']; 這邊不能oeder by bet_total 因為尚未產生欄位

        //echo print_r($sqlRaw);
        //2. 找出總會員數 與 金額
        // echo $sqlRaw;
        $result_total    = $this->_db->query($sqlRaw, $condition_total['parameters']);
        //         print_r($result_total);

        $result['total_record'] = 0;
        foreach ($result_total as $index=>$raw) {
            $result['amount'] += $raw['amount'];
            $result['total_record'] ++;
        }
        //3. 製作分頁資訊
        $result['total_page'] = ceil($result['total_record'] / $parameters['page_size']);
        if ($parameters['page'] > $result['total_page']) {
            $parameters['page'] = $result['total_page'];
        }
        if ($parameters['page'] < 1) {
            $parameters['page'] = 1;
        }
        $result['page'] = $parameters['page'];

        //4. 輸出資料
        $sqlRaw = 'SELECT a.member_id, a.account, a.note, a.register_time, a.level, a.amount, ';
        // 已結算部分
        $sqlRaw .= 'SUM(IF(settle=1, 1, 0)) as bet_record_total, SUM(IF(settle=1,bet_amount, 0)) as bet_total, SUM(IF(settle=1,valid_amount, 0)) as valid_total, SUM(IF(settle=1, win_amount, 0)) as win_total, SUM(IF(settle=1, return_amount, 0)) as return_total, ';
        // 未結算部分
        $sqlRaw .= 'SUM(IF(settle=0, 1, 0)) as unsettle_bet_record_total, SUM(IF(settle=0, bet_amount, 0)) as unsettle_bet_total';
        // 過濾條件
        $sqlRaw .= ' FROM uc_member as a INNER JOIN ltt_bet as b ON a.member_id = b.member_id WHERE 1  '. $where . $condition_total['sql']. ' group by a.member_id ';
        $sqlRaw .= ' ' . $condition2['sql'];
        // 分頁
        $sqlRaw .= ' LIMIT ' . ($parameters['page']-1) * $parameters['page_size'] . ',' . $parameters['page_size'];

        $result['list'] = $this->_db->query($sqlRaw, $condition_total['parameters']);
        $result['argument'] = $parameters;
        // $result['sql'] = $sqlRaw;
        return $result;
    }

    /**
     * 注单统计
     * @param lottery int
     * @return array
     */
    public function getBetsStatis($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $lottery       = isset($parameters['lottery']) ? intval($parameters['lottery']) : 0;
        $account       = isset($parameters['account']) ? $parameters['account'] : '';
        $start_time    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $stop_time     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';

        //$today_start = get_bj_day();
        $where = ' AND parlay=0 ';
        $params = array();

        if (0 == $lottery) {
            $where .= ' AND category >= 10';
        } elseif ($lottery > 0) {
            $where .= ' AND category=?';
            $params[] = $lottery;
        }
        if ($start_time) {
            $where .= ' AND bet_time >=? ';
            $params[] = $start_time;
        }
        if ($stop_time) {
            $where .= ' AND bet_time <=?';
            $params[] = $stop_time. ' 23:59:59';
        }
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            $member_id = $result['member_id'];
            unset($handle, $result);
        }
        if (isset($member_id)) {
            $where .= ' AND member_id=?';
            $params[] = $member_id;
        }

        // 已结算
        /*$sql = 'SELECT count(bet_id) as total, sum(win_amount) as win_total, sum(bet_amount) as bet_total,'
             . ' sum(return_amount) as return_total FROM ltt_bet WHERE status=2 AND parlay=0 ' . $where; */
        $select = 'SELECT SUM(bet_amount) AS bet_total, SUM(valid_amount) AS valid_total, SUM(win_amount) AS win_total, SUM(return_amount) AS return_total, '
              . ' COUNT(bet_id) AS total, TRUNCATE(SUM(win_amount)/SUM(bet_amount), 4) AS win_rate '
              . ' FROM ltt_bet ';

        // 未结算
        $sql = $select . ' WHERE status=1 ' .$where;
        $un_settle = $this->_db->query($sql, $params);
        if ($un_settle) {
            $un_settle = $un_settle[0];
            if ($un_settle['bet_total'] > 0) {
                $un_settle['bet_total'] = money_from_db($un_settle['bet_total']);
            } else {
                $un_settle['bet_total'] = 0;
            }
        }

        // 已结算
        $settled = '';
        $settled_params = array();
        if ($stop_time) {
            $settled .= ' AND settle_time <=?';
            $settled_params[] = $stop_time. ' 23:59:59';
        }

        $sql = $select . ' WHERE status=2 ' .$where;
        $settle = $this->_db->query($sql.$settled, array_merge($params, $settled_params));
        if ($settle) {
            $settle = $settle[0];
            $settle['bet_total']    = money_from_db($settle['bet_total']);
            $settle['valid_total']  = money_from_db($settle['valid_total']);
            $settle['win_total']    = money_from_db($settle['win_total']);
            $settle['return_total'] = money_from_db($settle['return_total']);
        }


        return array(
            'settle'    => $settle,
            'un_settle' => $un_settle
        );
    }

    public function getByBetId($bet_id)
    {
        $sql    = $this->betFields() . ' WHERE bet_id=?';
        $result = $this->_db->query($sql, array($bet_id));

        return $result;
    }

    protected function getByOrderId($order_id, $parlay = 1)
    {
        $sql    = $this->betFields() . ' WHERE order_id=? AND parlay=?';
        $result = $this->_db->query($sql, array($order_id, $parlay));
        return $result;
    }

    public function getRigtNowBetTotal($parameters){
        $sql = 'SELECT ROUND(SUM(bet_amount),2) AS bet_amount,ROUND(SUM(win_amount),2) AS win_amount
                FROM (
                SELECT (bet_amount/100)AS bet_amount ,0 AS win_amount FROM `ltt_bet`
                WHERE bet_time BETWEEN ? AND ? AND member_id = ? AND parlay ="0" AND status=1
                UNION ALL
                SELECT 0 AS bet_amount,(win_amount/100) AS win_amount FROM `ltt_bet`
                WHERE bet_time BETWEEN ? AND ? AND member_id = ? AND parlay ="0" AND status=2
                ) AS t';
        $arrgument =array(
            $parameters['date_start'],
            $parameters['date_stop'],
            $parameters['member_id'],
            $parameters['date_start'],
            $parameters['date_stop'],
            $parameters['member_id']
        );
        $result = $this->_db->query($sql,$arrgument);

        return $result;
    }

    public function getRigtNowBetDetail($parameters){
        $sql= 'SELECT category AS tag,ROUND(SUM(bet_amount/100),0) AS bet_amount,ROUND(SUM(win_amount/100),0) AS win_amount,COUNT(*) AS count FROM `ltt_bet`
               WHERE member_id = ? AND parlay =0 AND status=?
               GROUP BY category';

        $result = $this->_db->query($sql, array($parameters['member_id'],$parameters['status']));
        return $result;
    }

    public function getBetListFront($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $lottery       = isset($parameters['lottery']) ? intval($parameters['lottery']) : 0;
        $account       = isset($parameters['account']) ? $parameters['account'] : '';
        $status        = isset($parameters['bet_status']) ? $parameters['bet_status'] : 0;
        $date_start    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone     = isset($parameters['time_zone']) ? $parameters['time_zone'] : 1;  // 彩票默认北京时间
        $order_by      = isset($parameters['order_by']) ? intval($parameters['order_by']) : 0;
        $order_id      = isset($parameters['order_id']) ? $parameters['order_id'] : 0;
        $page          = isset($parameters['page']) ? intval($parameters['page']) : '';
        $page_size     = isset($parameters['page_size']) ? intval($parameters['page_size']) : 20;
        $is_settle     = isset($parameters['is_settle']) ? $parameters['is_settle'] : 1;

        if ($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (0 == $time_zone) {
            // 美东时间  因为存的时间是北京时间 所以如果选择美东时间查询则要将美东时间换成北京时间查询
            if ($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if ($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $options1 = array();
        $options1['a.parlay'] = 0;
        if ($round > 0) {
            $options1['c.number'] = $round;
        }

        if ($lottery > 0) {
            $options1['a.category'] = $lottery;
        }
        if ($status > 0) {
            $options1['a.status'] = $status;
        }
        if ($order_id) {
            $options1['a.order_id'] = $order_id;
        }
        $member_id = 0;
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $options1['a.member_id'] = 0;
            }
            unset($handle, $result);
        }
        if ($member_id > 0) {
            $options1['a.member_id'] = $member_id;
        }

        $options2 = array();
        if ($date_start) {
            $options2['a.bet_time'] = $date_start;
        }
        // 如果0则取>=9的所有
        if (0 == $lottery) {
            $options2['a.category'] = 9;
        }

        if($is_settle) {
            $options2['a.status'] = 2;
        } else {
            $options1['a.status'] = 1;
        }

        $options3 = array();
        if ($date_stop) {
            $options3['a.bet_time'] = $date_stop;
        }

        if ($order_by > 0) {
            switch ($order_by) {
                case 1:
                    $orders['a.bet_time'] = 'DESC';
                    break;
                case 2:
                    $orders['a.bet_time'] = 'ASC';
                    break;
                case 3:
                    $orders['a.bet_amount'] = 'DESC';
                    break;
                case 4:
                    $orders['a.bet_amount'] = 'ASC';
                    break;
                case 5:
                    $orders['a.win_amount'] = 'DESC';
                    break;
                case 6:
                    $orders['a.win_amount'] = 'ASC';
                    break;
            }
        } else {
            $orders = array(
                'a.bet_id' => 'DESC'
            );
        }
        $condition = $this->getConditions($options1, $options2, $options3, $orders);
        $sql       = 'SELECT COUNT(bet_id) AS `total` FROM ltt_bet AS a LEFT JOIN ltt_round AS c ON a.round_id=c.round_id ' . $condition['sql'];
        $result    = $this->_db->query($sql, $condition['parameters']);
        $return               = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page                 = $page > 0 ? $page : 1;
        $page                 = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page']       = $page;
        $limits               = ($page - 1) * $page_size . ',' . $page_size;

        $condition      = $this->getConditions($options1, $options2, $options3, $orders, $limits);
        $sql            = $this->betLeftJoinFields() . $condition['sql'];
        $result         = $this->_db->query($sql, $condition['parameters']);
        $return['list'] = $result;

        return $return;
    }

    public function getBetList($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $round         = (isset($parameters['round2']) && $parameters['round2'] != 0)  ? intval($parameters['round2']) : $round;
        $lottery       = isset($parameters['lottery']) ? intval($parameters['lottery']) : 0;
        $account       = isset($parameters['account']) ? $parameters['account'] : '';
        $status        = isset($parameters['bet_status']) ? $parameters['bet_status'] : 0;
        $date_start    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $time_zone     = isset($parameters['time_zone']) ? $parameters['time_zone'] : 1;  // 彩票默认北京时间
        $order_by      = isset($parameters['order_by']) ? intval($parameters['order_by']) : 0;
        $order_id      = isset($parameters['order_id']) ? $parameters['order_id'] : 0;
        $page          = isset($parameters['page']) ? intval($parameters['page']) : '';
        $page_size     = isset($parameters['page_size']) ? intval($parameters['page_size']) : 20;
        $category     = isset($parameters['category']) ? $parameters['category'] : 0;

        if ($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        if (0 == $time_zone) {
            // 美东时间  因为存的时间是北京时间 所以如果选择美东时间查询则要将美东时间换成北京时间查询
            if ($date_start) {
                $date_start = et_to_bj_date($date_start);
            }
            if ($date_stop) {
                $date_stop = et_to_bj_date($date_stop);
            }
        }

        $options1 = array();
        $options1['a.parlay'] = 0;
        if ($round > 0) {
            $options1['c.number'] = $round;
        }

        if ($lottery > 0) {
            $options1['a.category'] = $lottery;
        }
        if ($status > 0) {
            $options1['a.status'] = $status;
        }
        if ($order_id) {
            $options1['a.order_id'] = $order_id;
        }
        $member_id = 0;
        if ($account) {
            $handle = new Member();
            $result = $handle->getByAccount($account, 'member_id');
            if ($result) {
                $member_id = $result['member_id'];
            } else {
                $options1['a.member_id'] = 0;
            }
            unset($handle, $result);
        }
        if ($member_id > 0) {
            $options1['a.member_id'] = $member_id;
        }

        if ($category > 0) {
            $options1['b.category'] = $category;
        }

        $options2 = array();
        if ($date_start) {
            $options2['a.bet_time'] = $date_start;
        }
        // 如果0则取>=9的所有
        if (0 == $lottery) {
            $options2['a.category'] = 10;
        }

        $options3 = array();
        if ($date_stop) {
            $options3['a.bet_time'] = $date_stop;
        }

        if ($order_by > 0) {
            switch ($order_by) {
                case 1:
                    $orders['a.bet_time'] = 'DESC';
                    break;
                case 2:
                    $orders['a.bet_time'] = 'ASC';
                    break;
                case 3:
                    $orders['a.bet_amount'] = 'DESC';
                    break;
                case 4:
                    $orders['a.bet_amount'] = 'ASC';
                    break;
                case 5:
                    $orders['a.win_amount'] = 'DESC';
                    break;
                case 6:
                    $orders['a.win_amount'] = 'ASC';
                    break;
            }
        } else {
            $orders = array(
                'a.bet_id' => 'DESC'
            );
        }
        $condition = $this->getConditions($options1, $options2, $options3, $orders);
        $sql       = 'SELECT COUNT(bet_id) AS `total` FROM ltt_bet AS a ';
        $sql      .= 'INNER JOIN uc_member AS b ON a.member_id=b.member_id ';
        $sql      .= 'LEFT JOIN ltt_round AS c ON a.round_id=c.round_id ' . $condition['sql'];
        $result    = $this->_db->query($sql, $condition['parameters']);
        $return               = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page                 = $page > 0 ? $page : 1;
        $page                 = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page']       = $page;
        $limits               = ($page - 1) * $page_size . ',' . $page_size;

        //如果是已結 有跨日問題 故重組條件 改為結算日期
        if ($status == 2) {
            $options3 = array();
            if ($date_stop) {
                $options3['a.settle_time'] = $date_stop;
            }
        }

        $condition      = $this->getConditions($options1, $options2, $options3, $orders, $limits);
        $sql            = $this->betLeftJoinFields() . $condition['sql'];

        $result         = $this->_db->query($sql, $condition['parameters']);
        $return['list'] = $result;

        return $return;
    }

    public function getNearFutureBets($member_id, $lottery = 0)
    {
        $settled    = array();
        $un_settled = array();
        $lottery    = $lottery > 0 ? $lottery : $this->lottery_type;
        $limit      = 2;
        $data       = array();
        if ($member_id) {
            $sql        = 'SELECT round_id,number FROM ltt_round WHERE status=? AND category=? ORDER BY round_id DESC, round_id DESC LIMIT ' . $limit;
            $result     = $this->_db->query($sql, array(1, $lottery));
            if ($result) {
                $in_params = array();
                foreach ($result as $value) {
                    $in_params[] = $value['round_id'];
                }
                $in        = str_repeat('?,', count($in_params) - 1) . '?';
                $sql       = 'SELECT a.bet_id,a.order_id,a.title,a.detail,a.odds,a.bet_amount,a.win_amount,b.number FROM ltt_bet AS a LEFT JOIN ltt_round as b ON a.round_id=b.round_id WHERE a.member_id=? AND a.status=1 AND a.parlay=0 AND a.round_id IN(' . $in . ') ORDER BY a.bet_id DESC';
                $parameter = array_merge(array($member_id), $in_params);
                $result    = $this->_db->query($sql, $parameter);
                if ($result) {
                    foreach ($result as $value) {
                        if (!isset($data[$value['number']])) {
                            $data[$value['number']] = array();
                        }
                        //$value['win_amount'] = money_from_db($value['win_amount']);
                        $data[$value['number']][] = $value;
                    }
                }
            }
        }

        return $data;
    }

    private function positionFields()
    {
        $sql = 'SELECT round_id,category,number,start_time,stop_time,settle_time,result,status FROM ltt_round';
        return $sql;
    }

    /**
     * 根据期数和彩票类型获取盘口
     * @param $round        int 期数
     * @param $lottery_type int 类型
     * @return array
     */
    private function getPosition($round, $lottery_type)
    {
        $fields = $this->positionFields();
        $sql    = $fields . ' WHERE number=? AND category=? ORDER BY round_id DESC';
        $result = $this->_db->query($sql, array($round, $lottery_type));
        return $result;
    }

    public function getPositionList($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $lottery       = isset($parameters['lottery']) ? $parameters['lottery'] : 0;
        $date_start    = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop     = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $page          = isset($parameters['page']) ? $parameters['page'] : '';
        $page_size     = 20;

        $options1 = array();
        if ($round > 0) {
            $options1['number'] = $round;
        }
        if ($lottery > 0) {
            $options1['category'] = $lottery;
        }
        $options2 = array();
        if ($date_start) {
            $options2['start_time'] = $date_start;
        }
        $options3 = array();
        if ($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
            $options3['start_time'] = $date_stop;
        }
        $condition = $this->getConditions($options1, $options2, $options3);
        $sql       = 'SELECT COUNT(round_id) AS `total` FROM ltt_round ' . $condition['sql'];
        $result    = $this->_db->query($sql, $condition['parameters']);

        $return               = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page                 = $page > 0 ? $page : 1;
        $page                 = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page']       = $page;
        $limits               = ($page - 1) * $page_size . ',' . $page_size;
        $orders = array(
            'number'   => 'DESC',
            'round_id' => 'DESC'
        );

        $condition      = $this->getConditions($options1, $options2, $options3, $orders, $limits);
        $sql            = $this->positionFields() . $condition['sql'];
        $result         = $this->_db->query($sql, $condition['parameters']);
        $return['list'] = $result;

        return $return;
    }

    public function getPositionListNew($parameters)
    {
        $round         = isset($parameters['round']) ? intval($parameters['round']) : 0;
        $lottery       = isset($parameters['lottery']) ? $parameters['lottery'] : 0;
        $date_start    = isset($parameters['date_start']) ? $parameters['date_start'] : '';

        $options1 = array();
        if ($round > 0) {
            $options1['number'] = $round;
        }
        if ($lottery > 0) {
            $options1['category'] = $lottery;
        }
        $options2 = array();
        $options2['start_time'] = $date_start;

        $options3 = array();
        $date_stop  = date('Y-m-d H:i:s', strtotime($date_start) + 97200);  // 有的彩票从早上8点开到第二天凌晨2点 所有多加3个小时的时间
        $options3['stop_time'] = $date_stop;

        $orders = array(
            'number'   => 'ASC'
        );

        $condition      = $this->getConditions($options1, $options2, $options3, $orders);
        $sql            = $this->positionFields() . $condition['sql'];
        $result         = $this->_db->query($sql, $condition['parameters']);

        return $result;
    }

    /**
     * 根据编号获取
     * @param $rid int 盘口编号
     * @return array
     */
    final public function getPositionById($rid)
    {
        $fields = $this->positionFields();
        $sql    = $fields . ' WHERE round_id=?';
        $result = $this->_db->query($sql, array($rid));
        return $result;
    }

    /**
     * 添加盘口
     * @param $round   int 期数
     * @param $lottery_type int 类型
     * @param $start_time date
     * @param $stop_time date
     * @param $numbers String 开奖号码
     * @return int
     */
    private function addPosition($round, $lottery_type, $start_time, $stop_time, $numbers = '')
    {
        $sql        = 'INSERT INTO ltt_round SET number=?,category=?,start_time=?,stop_time=?,settle_time=?,result=?,status=?';
        $parameters = array($round, $lottery_type, $start_time, $stop_time, $this->now_date, $numbers, 1);
        $result     = $this->_db->query($sql, $parameters);
        return $result;
    }

    private function updatePositionResult($round, $type, $settle_time, $numbers)
    {
        $sql    = 'UPDATE ltt_round SET settle_time=?,result=?,status=? WHERE number=? AND category=?';
        $result = $this->_db->query($sql, array($settle_time, $numbers, 2, $round, $type));
        return $result;
    }

    private function updatePositionStopTime($round_id, $stop_time)
    {
        $sql       = 'UPDATE ltt_round SET stop_time=? WHERE round_id=?';
        $result    = $this->_db->query($sql, array($stop_time, $round_id));
        return $result;
    }

    private function getRecordResult($round, $lottery)
    {
        $sql    = 'SELECT result_id FROM ltt_result WHERE number=? AND category=?';
        $result = $this->_db->query($sql, array($round, $lottery));
        return $result;
    }

    public function getRecordList($lottery = 0, $limit = 200)
    {
        $sql    = 'SELECT submit_time, content, number FROM ltt_result WHERE category=? ORDER BY number DESC LIMIT ?';
        $result = $this->_db->query($sql, array($lottery, $limit));
        return $result;
    }

    public function getRecordList2($lottery = 0, $index = 0, $pagesize = 20, $result_id=0, $result_date='')
    {
        if (!$result_date){
            $result_date = date("Y-m-d") ;
        }
        if ($result_id>0) {
            $sql    = 'SELECT result_id,submit_time, content, number FROM ltt_result WHERE category=? and result_id<=? AND submit_time like ? ORDER BY number DESC LIMIT ?,?';
            $result = $this->_db->query($sql, array($lottery, $result_id, $result_date. ' %', $index, $pagesize));
        } else {
            $sql    = 'SELECT result_id,submit_time, content, number FROM ltt_result WHERE category=? AND submit_time like ? ORDER BY number DESC LIMIT ?,?';
            $result = $this->_db->query($sql, array($lottery, $result_date. ' %', $index, $pagesize));
        }

        return $result;
    }

    /**
     * 添加开奖结果
     * @param $round        int      期数
     * @param $lottery_type int      类型
     * @param $time         date     时间
     * @param $numbers      String   开奖号码
     */
    private function addRecordResult($round, $lottery_type, $time, $numbers)
    {
        $sql    = 'INSERT INTO ltt_result SET number=?,category=?,submit_time=?,content=?';
        $result = $this->_db->query($sql, array($round, $lottery_type, $time, $numbers));

        return $result;
    }

    private function updateRecordResult($round, $lottery, $time, $numbers)
    {
        $sql = 'UPDATE ltt_result SET submit_time=?,content=? WHERE number=? AND category=?';
        $result = $this->_db->query($sql, array($time, $numbers, $round, $lottery));

        return $result;
    }

    /**
     * 采集结果 号码位数
     *
     */
    protected function getNumberCount()
    {
        return 0;
    }

    /**
     * @param $bet_round
     * @param $stop_time(上海时区的时间戳)
     * @param $type 彩票类型
     *
     */
    public function createNewRound($bet_round, $start_bet_time, $stop_bet_time, $type = 0)
    {
        $type = $type > 0 ? $type : $this->lottery_type;
        if (!$this->checkInt($bet_round)) {
            $this->setError('new round error!');
        }

        if (!$this->checkInt($start_bet_time)) {
            $this->setError('start_bet_time error!');
        }

        if (!$this->checkInt($stop_bet_time)) {
            $this->setError('stop_bet_time error!');
        }

        if (!$this->error) {
            $start_time_str = date('Y-m-d H:i:s', get_bj_time($start_bet_time));
            $stop_time_str = date('Y-m-d H:i:s', get_bj_time($stop_bet_time));

            $result = $this->getPosition($bet_round, $type);

            $cache_handle = new Cache();
            $lottery_info = $cache_handle->hGet(CUSTOMER_NAME.'-lottery',$type);
            $lottery_info = json_decode($lottery_info,true);
            $current_timestamp = strtotime(date('Y-m-d H:i:s'));
            $date_diff = 0 ;
            if ($lottery_info['open_term_time']) {
                $date_diff = $current_timestamp - $lottery_info['open_term_time'];
            }
            unset($cache_handle);

            if (!$result && $date_diff >= 0) {

                //$result = $this->addPosition($bet_round, $type, $this->now_date, $stop_time_str);
                $result = $this->addPosition($bet_round, $type, $start_time_str, $stop_time_str);

                if (!$result) {
                    $this->setError('add position fail!');
                    $this->setError('bet_round:'.$bet_round.'  type:'.$type.'  start_time:'.$start_time_str.'  stop_time:'.$stop_time_str);
                } else {
                    $this->freshCache();
                }
            } else {
                if ($result[0]['stop_time'] != $stop_time_str) {
                    $round_id = $result[0]['round_id'];
                    $result   = $this->updatePositionStopTime($round_id, $stop_time_str);
                    if (!$result) {
                        // log stop time update fail!
                        $this->setError('update position fail!');
                    } else {
                        $this->freshCache($round_id);
                    }
                }
            }
        }


        $data = array(
            'bet_round'     => $bet_round,
            'start_bet_time' => $start_bet_time,
            'stop_bet_time' => $stop_bet_time,
            'lottery_type'  => $type,
        );

        return array('error' => $this->error, 'data' => $data);
    }

    /**
     * @param $round int 期数
     * @param $numbers String 采集到的结果 '1,3,8,9,5,...'
     * @param $type int 彩票类型
     */
    public function setCollectedResult($round, $numbers, $type = 0)
    {
        $type = $type > 0 ? $type : $this->lottery_type;

        if (!$this->checkInt($round)) {
            $this->setError('collect round error');
        }
        if (!$this->checkInt($type)) {
            $this->setError('collect lottery type error');
        }
        if (!$this->error) {
            $num_data = explode(',', $numbers);
            if (count($num_data) == $this->getNumberCount()) {
                foreach ($num_data as $key => $val) {
                    if (!$this->checkInt($val) || intval($val) < 0 || intval($val) > 80) {
                        $this->setError('collectd result number error');
                        break;
                    } else {
                        $num_data[$key] = intval($val);
                    }
                }
            } else {
                $this->setError('collectd result number count error');
            }
        }
        if (!$this->error) {
            $result = $this->getPosition($round, $type);
            if ($result && (1 != $result[0]['status'] || $result[0]['result'])) {
                $this->setError('collectd result already done');
            }
        }
        if (!$this->error) {
            $numbers = implode(',', $num_data);
            $round_id = $this->settleResult($round, $type, $numbers);
            $this->freshCache($round_id);
        }

        $data = array(
            'round'   => $round,
            'numbers' => $numbers,
            'lottery' => $this->lottery_type
        );

        return array('error' => $this->error, 'data' => $data);
    }

    /**
     * 盘口结果：最後呼叫排程結算注單
     * @param $round int 期数
     * @param $type  int 类型
     * @param $numbers String 号码 '1,8,12,5,6,...'
     */
    private function settleResult($round, $type, $numbers)
    {
        $cache_handle = new Cache();
        $cache_handle->hSet(CUSTOMER_NAME."-lotteryResult".$type, $round, $numbers);
        unset($cache_handle);

        $result = $this->getPosition($round, $type); //ltt_round
        $rid    = 0;
        if ($result) {
            // 更新盘口结果
            $update = $this->updatePositionResult($round, $type, $this->now_date, $numbers);  //ltt_round
            if ($update) {
                $rid = $result[0]['round_id'];
            } else {
                // error log
            }
        } else {
            // 插入盘口结果
            $rid = $this->addPosition($round, $type, $this->now_date, $this->now_date, $numbers);  //ltt_round
            if (!$rid || 0 == $rid) {
                // error log
            }
        }

        if ($result) {
            // 插入结果
            $record_result = $this->getRecordResult($round, $type); //ltt_result
            if ($record_result) {
                $record_result = $this->updateRecordResult($round, $type, $this->now_date, $numbers);  //ltt_result
            } else {
                $record_result = $this->addRecordResult($round, $type, $this->now_date, $numbers);  //ltt_result
            }

            if (!$record_result) {
                // error log
            }
        }
        //计算两面长龙
        if ($rid>0) {
            $this->setColdHotList($type, $numbers);
        }

        // 注单结算放到后台process进程里面去
        if ($rid > 0) {
            switch ($type) {
                case 9:
                case 36:
                    $play_type = 'lhc';
                    break;
                case 12:
                case 21:
                case 29:
                    $play_type = 'pk10';
                    break;
                case 10:
                case 38:
                case 37:
                    $play_type = 'ssc';
                    break;
                case 11:
                case 13:
                    $play_type = 'klsf';
                    break;
                case 18:
                case 98:
                    $play_type = 'bjkl8';
                    break;
                case 28:
                case 99:
                case 34:
                case 35:
                    $play_type = 'pcdd';
                    break;
                case 15:
                case 17:
                    $play_type = '11x5';
                    break;
                case 64:
                case 70:
                case 39:
                case 33:
                case 32:
                case 31:
                    $play_type = 'k3';
                    break;
                default:
                    $play_type = '';
                    break;
            }

            $url = '';
            $sql = "SELECT stop_time FROM ltt_round WHERE number=" . $round . ", category=" . $type . " LIMIT 1;";
            $result = $this->_db->query($sql);
            $stop_time = strtotime($result[0]['stop_time']);
            $now = new DateTime();
            if ($play_type) {
                if($now >= $stop_time){
                    $numbers = str_replace(',', ' ', $numbers);
                    $url = 'http://' . LotteryKjIp . ':' . LotteryKJPort .'/token/kj/'. $play_type .'/'.$rid.'/'.$numbers.'/'.$type.'/1?demo=0&dbName='.DB_NAME;
                    $result = curl_kj($url, false);
                    $result = json_decode($result, true);
                    $token = $result['token'];
                    Log::record('get play_type: ' . $play_type . ' | kj token url: ' . $url, 'info', 'settle/');

                    if ($token) {
                        $url = 'http://' . LotteryKjIp .':' . LotteryKJPort .'/'.'kj/' . $play_type . '/'.$rid.'/'. $numbers.'/'. $type.'/1?demo=0&dbName='.DB_NAME.'&token='.$token  ;
                        Log::record('kj url => ' . $url, 'info', 'settle/');
                        curl_kj($url, true);
                    }
                }
                // $numbers = str_replace(',', ' ', $numbers);
                // $url = 'http://' . LotteryKjIp . ':' . LotteryKJPort .'/token/kj/'. $play_type .'/'.$rid.'/'.$numbers.'/'.$type.'/1?demo=0&dbName='.DB_NAME;
                // $result = curl_kj($url, false);
                // $result = json_decode($result, true);
                // $token = $result['token'];
                // Log::record('get play_type: ' . $play_type . ' | kj token url: ' . $url, 'info', 'settle/');
                //
                // if ($token) {
                //     $url = 'http://' . LotteryKjIp .':' . LotteryKJPort .'/'.'kj/' . $play_type . '/'.$rid.'/'. $numbers.'/'. $type.'/1?demo=0&dbName='.DB_NAME.'&token='.$token  ;
                //     Log::record('kj url => ' . $url, 'info', 'settle/');
                //     curl_kj($url, true);
                // }
            }
            //$this->settleBets($rid);
        }
        return $rid;
    }
    /**
     * 计算两面长龙
     * @param $lottery int
     * @numbers char 开奖结果
     * @return array
     */
    public function setColdHotList($lottery, $numbers)
    {
        Log::record('Start setColdHotList lottery:' . $lottery.'---numbers:'.$numbers,'info','settle/');
        //取出赔率列表
        $odds_handle    = new LotteryOdds();
        $result      = $odds_handle->getOddsColdHot($lottery);
        unset($odds_handle);

        if ($result) {
            $handle = null;
            $funArray=array();//把不要计算的FUN写进来,用来排除
            switch ($lottery) {
                case 9:
                    $handle = new SixMark();
                    $funArray[]="fun10";//半小单，半小双，半大单，半大双
                    $funArray[]="fun11";//特码|家野
                    $funArray[]="fun101";//几连肖
                    $funArray[]="fun105";//三中二
                    $funArray[]="fun106";//三全中
                    $funArray[]="fun107";//二全中
                    $funArray[]="fun108";//二中特
                    $funArray[]="fun109";//特串
                    $funArray[]="fun80";//正码过关
                    $funArray[]="fun18";//连肖
                    $funArray[]="fun110";//全不中
                    break;
                case 10:
                    $handle = new Cqssc();
                    break;
                case 11:
                    $handle = new Gdklsf();
                    break;
                case 12:
                case 29:
                    $handle = new Bjpk($lottery);
                    break;
                case 13:
                    $handle = new Gdklsf($lottery);
                    break;
                case 14:
                    $handle = new Cqssc($lottery);
                    break;
                case 15:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 16:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 17:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 18:
                    $handle = new HappyEight($lottery);
                    $funArray[]="fun1";//选一
                    $funArray[]="fun2";//选二
                    $funArray[]="fun3";//选三
                    $funArray[]="fun4";//选四
                    $funArray[]="fun5";//选五
                    break;
                case 19:
                    $handle = new ThreeBall($lottery);
                    break;
                case 20:
                    $handle = new ThreeBall($lottery);
                    break;
                case 21: //幸运飞艇
                    $handle = new Bjpk($lottery);
                    break;
                case 22: //幸运28 ,同北京28一样，只是结果由自己
                    $handle = new Bj28($lottery);
                    break;
                case 23: //福利三分彩
                    $handle = new Bjpk($lottery);
                    break;
                case 24: //跑马
                    $handle = new Bjpk($lottery);
                    break;
                case 25: //福利五分彩
                    $handle = new Cqssc($lottery);
                    break;
                case 28: //北京28
                    $handle = new Bj28($lottery);
                    break;
                case 64: //安徽快3
                    $handle = new K3($lottery);
                    break;
                case 70: //湖北快3
                    $handle = new K3($lottery);
                    break;
                case 98:
                    $handle = new HappyEight($lottery);
                    $funArray[]="fun1";//选一
                    $funArray[]="fun2";//选二
                    $funArray[]="fun3";//选三
                    $funArray[]="fun4";//选四
                    $funArray[]="fun5";//选五
                    break;
                 case 99: //湖北快3
                    $handle = new Bj28($lottery);
                    break;
            }

            if ($handle) {
                foreach ($result as $value) {
                    if ($value['settle_method']) {
                        if (in_array($value['settle_method'], $funArray)) {
                        } else {
                            $bet = array(
                                'type'  => $value['type'],
                                'position'  => $value['position'],
                                'odds_id' => $value['odds_id'],
                                'settle_method'     => $value['settle_method'],
                                'cold_hot'=> $value['cold_hot'],
                                'number'  => $value['number']
                            );
                            $win = call_user_func_array(array($handle, $bet['settle_method']), array($bet, explode(",", $numbers)));
                            //$win=$this->winProcessBet($bet, $numbers);


                            if ($win==1) {
                                if ($value['cold_hot']>=0) {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=cold_hot+1 WHERE odds_id=?';
                                } else {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=1 WHERE odds_id=?';
                                }
                            } else {
                                if ($value['cold_hot']>=0) {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=-1 WHERE odds_id=?';
                                } else {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=cold_hot-1 WHERE odds_id=?';
                                }
                            }
                            $params = array($value['odds_id']);
                            $result_update= $this->_db->query($sql, $params);
                        }
                    }
                    //kg_echo("result_update:".$result_update);
                }
            }
            unset($handle);
        }
        unset($result);


        $coldhot_handle    = new LotteryOdds();
        $result_coldhot      = $coldhot_handle->getOddsByLotteryColdHot($lottery);
        unset($coldhot_handle);
        if ($result_coldhot) {
            $temp=array();
            foreach ($result_coldhot as $value) {
                //name,cold_hot,type,position,settle_method
                $temp[]=array($value['name'],$value['cold_hot'],$value['type'],$value['position'],$value['settle_method']);
            }

            $cache_handle = new Cache();
            $cache_handle->hSet(CUSTOMER_NAME.'-codehot', $lottery, json_encode($temp));
            unset($cache_handle);
            unset($temp);
        }
        unset($result_coldhot);
    }


    public function setColdHotList2($lottery, $numbers)
    {
        Log::record('Start setColdHotList lottery:' . $lottery.'---numbers:'.$numbers,'info','settle/');
        //取出赔率列表
        $odds_handle    = new LotteryOdds();
        $result      = $odds_handle->getOddsColdHot($lottery);
        unset($odds_handle);

        $cache_data=array();
        $cache_handle = new Cache();
        $cache_data   = $cache_handle->hGet(CUSTOMER_NAME.'-coldhot', $lottery);
        Log::record('cache_data_'.$lottery.':' . $cache_data,'info','settle/');
        $cache_data = $cache_data ? json_decode($cache_data) : array();
        unset($cache_handle);
        foreach ($cache_data as $key=>$value) {
            　　Log::record($key."=>".$value,'info','settle/');
        }
        if ($result) {
            $handle = null;
            $funArray=array();//把不要计算的FUN写进来,用来排除
            switch ($lottery) {
                case 9:
                    $handle = new SixMark();
                    $funArray[]="fun10";//半小单，半小双，半大单，半大双
                    $funArray[]="fun11";//特码|家野
                    $funArray[]="fun101";//几连肖
                    $funArray[]="fun105";//三中二
                    $funArray[]="fun106";//三全中
                    $funArray[]="fun107";//二全中
                    $funArray[]="fun108";//二中特
                    $funArray[]="fun109";//特串
                    $funArray[]="fun80";//正码过关
                    $funArray[]="fun18";//连肖
                    $funArray[]="fun110";//全不中
                    break;
                case 10:
                    $handle = new Cqssc();
                    break;
                case 11:
                    $handle = new Gdklsf();
                    break;
                case 12:
                case 29:
                    $handle = new Bjpk($lottery);
                    break;
                case 13:
                    $handle = new Gdklsf($lottery);
                    break;
                case 14:
                    $handle = new Cqssc($lottery);
                    break;
                case 15:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 16:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 17:
                    $handle = new ElevenSsc($lottery);
                    break;
                case 18:
                    $handle = new HappyEight($lottery);
                    $funArray[]="fun1";//选一
                    $funArray[]="fun2";//选二
                    $funArray[]="fun3";//选三
                    $funArray[]="fun4";//选四
                    $funArray[]="fun5";//选五
                    break;
                case 19:
                    $handle = new ThreeBall($lottery);
                    break;
                case 20:
                    $handle = new ThreeBall($lottery);
                    break;
                case 21: //幸运飞艇
                    $handle = new Bjpk($lottery);
                    break;
                case 22: //幸运28 ,同北京28一样，只是结果由自己
                    $handle = new Bj28($lottery);
                    break;
                case 23: //福利三分彩
                    $handle = new Bjpk($lottery);
                    break;
                case 24: //跑马
                    $handle = new Bjpk($lottery);
                    break;
                case 25: //福利五分彩
                    $handle = new Cqssc($lottery);
                    break;
                case 28: //北京28
                    $handle = new Bj28($lottery);
                    break;
                case 64: //安徽快3
                    $handle = new K3($lottery);
                    break;
                case 70: //湖北快3
                    $handle = new K3($lottery);
                    break;
                 case 98:
                    $handle = new HappyEight($lottery);
                    $funArray[]="fun1";//选一
                    $funArray[]="fun2";//选二
                    $funArray[]="fun3";//选三
                    $funArray[]="fun4";//选四
                    $funArray[]="fun5";//选五
                    break;
                case 99: //北京28
                    $handle = new Bj28($lottery);
                    break;

            }

            if ($handle) {
                $tempArray=array();
                foreach ($result as $value) {
                    if ($value['settle_method']) {
                        if (in_array($value['settle_method'], $funArray)) {
                        } else {
                            $bet = array(
                                'type'  => $value['type'],
                                'position'  => $value['position'],
                                'odds_id' => $value['odds_id'],
                                'settle_method'     => $value['settle_method'],
                                'cold_hot'=> $value['cold_hot'],
                                'number'  => $value['number']
                            );
                            $win = call_user_func_array(array($handle, $bet['settle_method']), array($bet, explode(",", $numbers)));
                            //$win=$this->winProcessBet($bet, $numbers);

                            $name=$value['name'];
                            //Log::record('cache_name-'.$lottery.':' . $name);
                            Log::record('$cache_data[$name]'.$cache_data[$name],'info','settle/');
                            if ($win==1) {
                                if (isset($cache_data[$name])) {
                                    if ($cache_data[$name]>=0) {
                                        $tempArray[]=array($name=>($cache_data[$name]+1));
                                    } else {
                                        $tempArray[]=array($name=>1);
                                    }
                                } else {
                                    $tempArray[]=array($name=>1);
                                }


                                if ($value['cold_hot']>=0) {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=cold_hot+1 WHERE odds_id=?';
                                } else {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=1 WHERE odds_id=?';
                                }
                            } else {
                                if (isset($cache_data[$name])) {
                                    if ($cache_data[$name]>=0) {
                                        $tempArray[]=array($name=>-1);
                                    } else {
                                        $tempArray[]=array($name=>($cache_data[$name]-1));
                                    }
                                } else {
                                    $tempArray[]=array($name=>-1);
                                }


                                if ($value['cold_hot']>=0) {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=-1 WHERE odds_id=?';
                                } else {
                                    $sql    = 'UPDATE ltt_odds SET cold_hot=cold_hot-1 WHERE odds_id=?';
                                }
                            }
                            $params = array($value['odds_id']);
                            $result_update= $this->_db->query($sql, $params);
                        }
                    }
                    //kg_echo("result_update:".$result_update);
                }

                $cache_data = json_encode($tempArray);
                Log::record('cache_data-'.$lottery.':' . $cache_data,'info','settle/');
                $cache_handle = new Cache();
                $cache_handle->hSet(CUSTOMER_NAME.'-coldhot', $lottery, $cache_data);
                unset($cache_handle);
            }
            unset($handle);
        }
        unset($result);





        /*
        $coldhot_handle    = new LotteryOdds();
        $result_coldhot      = $coldhot_handle->getOddsByLotteryColdHot($lottery);
        unset($coldhot_handle);
        if($result_coldhot) {

            $temp=array();
            foreach($result_coldhot as $value) {
                $temp[]=array($value['name'],$value['cold_hot']);
            }

            $cache_handle = new Cache();
            $cache_handle->hSet('codehot', $lottery, json_encode($temp));
            unset($cache_handle);
            unset($temp);
        }
        unset($result_coldhot);
        */
    }








    /**
     * 获取两面长龙
     * @param $lottery int
     * @numbers char 开奖结果
     * @return array
     */
    public function getColdHotList($lottery)
    {
        $cache_handle = new Cache();
        $cache_data   = $cache_handle->hGet(CUSTOMER_NAME.'-codehot', $lottery);
        unset($cache_handle);
        return cache_data;
    }

    public function managerSettleBet($bet_id)
    {
        $options1 = array(
            'bet_id' => $bet_id,
            'status' => 1
        );
        $bet = $this->getBets($options1, array(), array(), array(), '1');
        if ($bet) {
            $round_id = $bet[0]['round_id'];
            if ($round_id > 0) {
                $result = $this->getPositionById($round_id);
                if ($result) {
                    $numbers = explode(',', $result[0]['result']);
                    $this->methods($bet[0], $numbers);
                }
                unset($result, $bet);
            }
        } else {
            $this->setError('can not operate this bet!');
        }

        return array('error' => $this->error);
    }

    /**
     * 用户注单结算
     *
     * @param $rid     int 盘口编号 唯一  (注意是盘口编号 不是期数)
     * @param $bet_id  int 注单编号
     */
    protected function settleBets($rid, $bet_id = 0)
    {
        $result = $this->getPositionById($rid);
        if ($result) {
            $options1 = array(
                'round_id' => $rid,
                'status'   => 1,
                'parlay'   => 0
            );
            if ($bet_id > 0) {
                $options1['bet_id'] = $bet_id;
            }
            $bets = $this->getBets($options1);
            if ($bets) {
                $numbers = explode(',', $result[0]['result']);
                foreach ($bets as $info) {
                    $this->methods($info, $numbers);
                }
            }
        }
    }

    public function settleProcessBets($bets)
    {
        foreach ($bets as $row) {
            $numbers = explode(',', $row['result']);
            $this->methods($row, $numbers);
            unset($numbers);
        }

        unset($bets);
    }

    public function winProcessBet($bet, $numbers)
    {
        return $this->methods2($bet, $numbers);
    }

    protected function set_config_rate()
    {
        if (!is_numeric($this->losing_effective)) {
            $handle = new Config();
            $config = $handle->getByName(array('VALID_BET_RATE', 'LOTTERY_LOSING_EFFECTIVE'));
            $this->valid_bet_rate = $config['VALID_BET_RATE'] * 0.01;
            $this->losing_effective =  $config['LOTTERY_LOSING_EFFECTIVE'];
        }
    }

    protected function get_valid_bet_rate()
    {
        if (!$this->valid_bet_rate) {
            $handle = new Config();
            $config = $handle->getByName(array('VALID_BET_RATE'));
            $this->valid_bet_rate = $config['VALID_BET_RATE'] * 0.01;
        }
        return $this->valid_bet_rate;
    }

    /**
     * 单个注单结算
     * @param $bet array
     * @param $win int 0输  1赢  2平
     * @param $win_amount int 赢金额
     */
    protected function settleOneBet($bet, $win, $win_amount = false)
    {
        $bet_id = $bet['bet_id'];
        $code = $this->betAuthCode(
            $bet['member_id'],
            $bet['round_id'],
            $bet['category'],
                                   $bet['bet_time'],
            $bet['bet_amount'],
            $bet['win_amount'],
                                   $bet['return_amount'],
            $bet['odds_id'],
            $bet['odds'],
                                   $bet['settle_method'],
            $bet['position'],
            $bet['number'],
                                   $bet['status'],
            $bet['settle_time']
        );


        if ($code == $bet['auth']) {
            $pay_out       = (false != $win_amount) ? $win_amount : $this->getWinAmount($bet, $win);  // 派彩
            $bet_amount    = $bet['bet_amount'];
            $win_amount    = $pay_out - $bet_amount;


            // 返水  非平局才返水
            $return_amount = 0;
            if (0 != $win_amount) {
                $return_amount = $bet['return_amount'];
            }
            $this->set_config_rate();
            $valid_rate    = $this->valid_bet_rate;
            $losing_effect = $this->losing_effective;
            // 有效投注 > 本金 * valid_rate倍数才有效  平局无效(平局时win_amount为0)
            $valid_amount  = 0;
            if ($win_amount < 0) {
                $valid_amount = $bet_amount;
            } else {
                if ($win_amount > $bet_amount * $valid_rate) {
                    $valid_amount = $bet_amount;
                }
            }
            // 如果设置了只有输才算有效流水并反水
            if ($losing_effect && $win_amount > 0) {
                $valid_amount  = 0;
                $return_amount = 0;
            }

            // member表要更新的金额
            $add_money     = $pay_out  + $return_amount;

            $status        = 2;
            $settle_time   = $this->now_date;
            $auth_code     = $this->betAuthCode($bet['member_id'], $bet['round_id'], $bet['category'], $bet['bet_time'], $bet['bet_amount'], $win_amount, $return_amount, $bet['odds_id'], $bet['odds'], $bet['settle_method'], $bet['position'], $bet['number'], $status, $settle_time);

            $sql_array = array() ;

            $sql = ' UPDATE `ltt_bet` SET `status` = '. $status .', `win_amount`='.$win_amount.',' ;
            $sql .= ' `valid_amount`='.$valid_amount.', `return_amount`='.$return_amount.',' ;
            $sql .= ' `settle_time`="'.$settle_time.'",`auth`="'.$auth_code.'" WHERE ' ;
            $sql .= ' `bet_id`='.$bet_id. ' AND `status` = 1 ; ' ;
            $sql .= ' UPDATE `uc_member_config` SET `check_bet_amount` = CASE ' ;
            $sql .= ' WHEN (`check_bet_amount` -'.$bet['bet_amount'].') < 0 THEN 0 ' ;
            $sql .= ' ELSE `check_bet_amount` -'.$bet['bet_amount'] ;
            $sql .= ' END WHERE `member_id` ='.$bet['member_id'].';';
            $sql_array[0] = $sql ;
            $result = $this->_db->nativeQuery($sql_array);

            $error     = '';
            if ($result) {
                $handle = new Member();
                $member = $handle->getByMemberId($bet['member_id'], 'amount,category');
                $previous_amount = $member['amount'];
                unset($handle);
                // 派彩
                if (0 != $pay_out) {
                    $current_amount = $previous_amount + $pay_out;
                    $replaceprevious_amount = $previous_amount ;
                    $sqll .= 'INSERT INTO fnn_record SET `member_id` = '.$bet['member_id'].',type='.$this->finance_type.',subtype=7';
                    $sqll .= ',table_id='.$bet_id.',amount='.$pay_out.',submit_time="'.$this->est_date.'",note="单号'.$bet['order_id'].'",previous_amount='.$replaceprevious_amount;
                    $sqll .= ',current_amount='.$current_amount.',member_category='.$member['category'].'; ';
                    $previous_amount = $current_amount;
                }
                if ($add_money > 0) {
                    $add_money = $member['amount'] + $add_money ;
                    $sqll .= ' UPDATE uc_member SET amount='.$add_money.' WHERE member_id='.$bet['member_id'].'; ';

                    if ($return_amount > 0) {
                        $current_amount = $previous_amount + $return_amount;
                        $sqll .= 'INSERT INTO fnn_record SET `member_id` = '.$bet['member_id'].',type='.$this->finance_type.',subtype=11';
                        $sqll .= ',table_id='.$bet_id.',amount='.$return_amount.',submit_time="'.$this->est_date.'",note="",previous_amount='.$previous_amount;
                        $sqll .= ',current_amount='.$current_amount.',member_category='.$member['category'].';';
                    }
                }
                if ($sqll) {
                    $this->_db->beginTransaction();
                    $sql_array[0] = $sqll ;
                    $result = $this->_db->nativeQuery($sql_array);

                    if (!$result) {
                        $this->_db->rollBack();
                    } else {
                        $this->_db->commit();
                    }
                }
            }
        }
    }

    public function managerAddPosition($round, $lottery, $stop_time = '', $numbers = '')
    {
        $result = $this->getPosition($round, $lottery);
        Log::record('line 3238 => ' . count($result) . 'round => ' . $round .' | lottery =>' . $lottery,'debug','create/');
        if (!$result) {
            $stop_time = $stop_time ? $stop_time : $this->now_date;
            $result = $this->addPosition($round, $lottery, $this->now_date, $stop_time, $numbers);
            if ($result) {
                if ($numbers) {
                    $this->settleResult($round, $this->lottery_type, $numbers);
                }
                // 更新redis cache
                $this->freshCache();
            } else {
                $this->setError('add position fail!');
            }
        }
        return array('error' => $this->error);
    }

    /**
     * [getSixMarkLastRound 取得最後一期的六合彩]
     *
     * @return void
     */
    public function getSixMarkLastRound()
    {
        $sql = ' SELECT number, round_id FROM ltt_round WHERE category = 9 ORDER BY round_id DESC LIMIT 1';
        return $this->_db->query($sql);
    }

    /**
     * [updateLttRoundStatus 更新該期狀態]
     *
     * @param [type] $ltt_number 彩種期數
     * @param [type] $status    彩種狀態
     * @return void
     */
    public function updateLttRoundStatus($ltt_number, $status, $category)
    {
        $sql = ' UPDATE ltt_round SET status = ? WHERE number = ? AND category = ? ';
        return $this->_db->query($sql, array($status, $ltt_number, $category));
    }

    /**
     * 管理更新盤口
     * @param  [type] $round_id  [description]
     * @param  string $numbers   [description]
     * @param  string $stop_time [description]
     * @return [type]            [description]
     */
    public function managerChangePosition($round_id, $numbers = '', $stop_time = '')
    {
        $result = $this->getPositionById($round_id);
        if ($result && $result[0]) {
            $round = $result[0]['number'];
            if ($stop_time) {
                $update_result = $this->updatePositionStopTime($round_id, $stop_time);
                if (!$update_result) {
                    $this->setError('change position update time fail!');
                }
            }
            if (!$this->error) {
                // 有輸入結果並且還沒過結算就跑結算排程
                if ($numbers) {
                    if ($result[0]['status'] <= 2) {
                        $this->settleResult($round, $this->lottery_type, $numbers);

                        if (2 == $result[0]['status']) {
                            // 把已经结算过的取消至未结算状态
                            $this->cancelBets($round_id, 0);
                        }
                    } else {
                        $this->setError('change position status fail!');
                    }
                }
                if (!$this->error) {
                    // 更新缓存
                    $this->freshCache($round_id);
                }
            }
        } else {
            $this->setError('change position data fail!');
        }

        return array('error' => $this->error);
    }
    /**
     * [managerCancelPosition description]
     * @param  [type] $round_id [description]
     * @return [type]           [description]
     */

    public function managerCancelPosition($round_id)
    {
        $result = $this->getPositionById($round_id);
        if ($result) {
            $sql    = 'UPDATE ltt_round SET status=? WHERE round_id=? AND status != 3';
            $result = $this->_db->query($sql, array(3, $round_id));
            if ($result) {
                $this->cancelBets($round_id, 1);
            } else {
                $this->setError('cancel position update status error!');
            }
        } else {
            $this->setError('cancel position round id error!');
        }

        return array('error' => $this->error);
    }

    public function managerDeletePosition($round_id)
    {
        $result = $this->getPositionById($round_id);
        if ($result && 3 == $result[0]['status']) {
            $number = $result[0]['number'];
            $category = $result[0]['category'];
            $sql    = 'DELETE FROM ltt_round WHERE round_id=? AND status=3';
            $result = $this->_db->query($sql, array($round_id));
            if ($result) {
                $sql = 'DELETE FROM ltt_result WHERE number=? AND category=? LIMIT 1';
                $this->_db->query($sql, array($number, $category));
            } else {
                $this->setError('delete position update status error!');
            }
        } else {
            $this->setError('delete position round id error!');
        }

        return array('error' => $this->error);
    }

    public function managerCancelByBetId($bet_id)
    {
        $options1 = array('bet_id' => $bet_id);       // =bet_id
        $options3 = array('status' => 2);             // <=2
        $result   = $this->getBets($options1, array(), $options3, array(), '1');
        if ($result) {
            $this->cancelOneBet($result[0], 1);
        } else {
            $this->setError('cancel bet id error');
        }
        unset($result);

        return array('error' => $this->error);
    }
    /*
    更新或新增redis cache: 盤口資料
    */
    private function freshCache($round_id=0)
    {
        global $g_auto_create_round;
        // 1.撈出尚未結算的最後一筆盤口資料，寫入到redis
        if (0==$round_id) {
            $sql = $this->positionFields() . ' WHERE status=1 AND category=? ORDER BY number DESC, round_id DESC LIMIT 1 ';
            $result = $this->_db->query($sql, array($this->lottery_type));
        } else {
            $sql = $this->positionFields() . ' WHERE status=1 AND category=? AND round_id=? ORDER BY number DESC, round_id DESC LIMIT 1 ';
            $result = $this->_db->query($sql, array($this->lottery_type,$round_id));
        }

        $cache_handle = new Cache();
        $cache_data   = $cache_handle->hGet(CUSTOMER_NAME.'-lottery', $this->lottery_type);
        $cache_data = $cache_data ? json_decode($cache_data, true) : array();

        if ($result) {
            $bet_round = $result[0]['number'];
            $stop_time = bj_strtotime($result[0]['stop_time']);

            if(!in_array($this->lottery_type, $g_auto_create_round)) {
                $cache_data[0]['bet_stop_time']  = $stop_time;
                if ($result[0]['category']==9) {
                    $cache_data[0]['open_term_time'] = $stop_time + 300;
                } elseif ($result[0]['category']==98 || $result[0]['category']==99) {
                    $cache_data[0]['open_term_time'] = $stop_time + 30;
                } elseif ($result[0]['category'] == 29 || $result[0]['category'] == 30 || $result[0]['category'] == 31 || $result[0]['category'] ==34 || $result[0]['category'] ==35 || $result[0]['category'] ==37) {
                    $cache_data[0]['open_term_time'] = $stop_time + 15;
                } else {
                    $cache_data[0]['open_term_time'] = $stop_time + 60;
                }
                //$cache_data['open_term_time'] = $stop_time + 120;  六合彩提前5分钟封盘
                $cache_data[0]['bet_round']      = $bet_round;
            }
        }

        if($this->lottery_type != '31' && $this->lottery_type != '29' && $this->lottery_type != '34' && $this->lottery_type != '35' && $this->lottery_type != '36' && $this->lottery_type != '37'){
            // 2.撈出剛結算的盤口，寫入到redis(上期開獎)
            $sql = $this->positionFields() . ' WHERE status=2 AND category=? ORDER BY number DESC, round_id DESC LIMIT 1 ';
            $result = $this->_db->query($sql, array($this->lottery_type));
            if ($result) {
                $cache_data[0]['round']   = $result[0]['number'];
                $cache_data[0]['numbers'] = $result[0]['result'];
            }
        }
        
        $cache_data = json_encode($cache_data);
        Log::record('cache: ' .$this->lottery_type.'::'. $cache_data,'info','settle/');
        $cache_handle->hSet(CUSTOMER_NAME.'-lottery', $this->lottery_type, $cache_data);
    }


    /**
     * 取消盘口下的注单
     * @param $round_id 盘口编号
     * @param $operate 0:取消至未结算状态 不返回本金，但收回盈利  1:永久取消，返回本金 收回盈利
     *
     */
    protected function cancelBets($round_id, $operate = 0)
    {
        // 将已结算的注单重置回未结算状态
        if (0 == $operate) {
            $options1 = array(
                'round_id' => $round_id,
                'status'   => 2,
                'parlay'   => 0
            );
            $result = $this->getBets($options1, array(), array());
        }
        // 永久取消 已结算和未结算的注单皆取消
        elseif (1 == $operate) {
            $options1 = array(
                'round_id' => $round_id,
                'parlay'   => 0
            );
            $options3 = array(
                'status' => 2
            );
            $result = $this->getBets($options1, array(), $options3);
        }

        if ($result) {
            foreach ($result as $bet) {
                $this->cancelOneBet($bet, $operate);
            }
        }
    }

    /**
     * 取消某一注单
     * @param $bet array 注单数据
     * @param $operate int 操作类型 0:取消至未结算状态，但不返回本金，但收回盈利  1:永久取消，返回本金 收回盈利
     */
    private function cancelOneBet($bet, $operate)
    {
        $bet_id = $bet['bet_id'];
        $code   = $this->betAuthCode($bet['member_id'], $bet['round_id'], $bet['category'], $bet['bet_time'], $bet['bet_amount'], $bet['win_amount'], $bet['return_amount'], $bet['odds_id'], $bet['odds'], $bet['settle_method'], $bet['position'], $bet['number'], $bet['status'], $bet['settle_time']);
        if ($code == $bet['auth']) {
            $status    = (1 == $operate) ? 3 : 1;
            $settle_time = $this->now_date;
            $auth_code = $this->betAuthCode($bet['member_id'], $bet['round_id'], $bet['category'], $bet['bet_time'], $bet['bet_amount'], $bet['win_amount'], $bet['return_amount'], $bet['odds_id'], $bet['odds'], $bet['settle_method'], $bet['position'], $bet['number'], $status, $settle_time);

            $this->_db->beginTransaction();
            $error  = '';
            $sql    = 'UPDATE ltt_bet SET status=?, settle_time=?, auth=? WHERE bet_id=? AND status != ?';
            $result = $this->_db->query($sql, array($status, $settle_time, $auth_code, $bet_id, $status));
            if ($result) {
                $money_amount  = 0;
                $return_amount = 0;
                $valid_amount  = 0;
                if (1 == $status) {
                    // status = 1 取消至未结算状态
                    $return_amount = $bet['return_amount'] > 0 ? -($bet['return_amount']) : 0;
                    $money_amount  = -($bet['bet_amount'] + $bet['win_amount']);
                    $valid_amount  = -($bet['valid_amount']);
                } elseif (3 == $status) {
                    // status = 3 永久取消(即该注单作废)
                    if (1 == $bet['status']) {
                        // 如果注单还未被结算
                        $money_amount = $bet['bet_amount'];
                    } elseif (2 == $bet['status']) {
                        // 如果注单已结算
                        $return_amount = $bet['return_amount'] > 0 ? -($bet['return_amount']) : 0;
                        $money_amount  = -($bet['win_amount']);
                        $valid_amount  = -($bet['valid_amount']);
                    }
                }

                $change_amount = $money_amount + $return_amount;
                if ($change_amount) {
                    $handle = new Member();
                    $member = $handle->getByMemberId($bet['member_id'], 'amount,category');
                    $previous_amount = $member['amount'];
                    unset($handle);
                    if ($this->updateValidAndMoney($bet['member_id'], $change_amount, $valid_amount)) {
                        $current_amount = $previous_amount + $change_amount;
                        $finance_handle = new Finance();
                        $argument = array(
                            $bet['member_id'],
                            $this->finance_type,
                            9,
                            $bet_id,
                            $change_amount,
                            $this->est_date,
                            $status . ' 单号' . $bet['order_id'],
                            $previous_amount,
                            $current_amount,
                            $member['category']
                        );
                        $finance_handle->add($argument);
                        unset($finance_handle);
                    } else {
                        $error = 'update money error';
                    }
                }
            } else {
                $error = 'update status error';
            }
            if ($error) {
                $this->_db->rollBack();
            // set error log
            } else {
                $this->_db->commit();
                // 给用户写一条取消记录到信箱
                // todo
            }
        }
    }

    /**
     * 计算盈利金额
     * @param $bet array 注单数组
     * @param $win int 0输  1赢  2平
     * @return int 金额
     */
    protected function getWinAmount($bet, $win)
    {
        $win_amount = (1 == $win) ? floor(round($bet['odds'] * $bet['bet_amount'], 1)) : (2 == $win ? $bet['bet_amount'] : 0);
        unset($bet, $win);
        return $win_amount;
    }

    /**
     * 返水
     * @param $member_id
     * @param $bet_amount
     * @param $water_value 赔率对应的返水
     * @return int
     */
    protected function getReturnAmount($member_id, $bet_amount, $water_value)
    {
        $return_amount = 0;
        if (0 == $water_value) {
            $handle = new MemberConfig();
            $result = $handle->getByMemberId($member_id);
            if ($result) {
                // 实时返水
                if (1 == $result['lottery_return_type']) {
                    $rate = $result['lottery_return_rate'];
                    if (!$rate) {
                        $handle = new Config();
                        $config = $handle->getByName(array('LOTTERY_REAL_TIME_RETURN_RATE'));
                        $rate   = $config['LOTTERY_REAL_TIME_RETURN_RATE'];
                        unset($handle, $config);
                    }
                    $return_amount = floor(round($bet_amount * $rate * 0.01, 1));
                }
            }
        } else {
            $return_amount = ceil($bet_amount * $water_value * 0.01);
        }
        return $return_amount;
    }

    /**
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers)
    {
    }

    /**
     * 玩法: 单球号码
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun1($bet, $nums)
    {
        $win = ($bet['number'] == $nums[$bet['position'] - 1]) ? 1 : 0;
        return $win;
    }

    /**
     * 玩法: 单球单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int
     */
    protected function fun2($bet, $nums)
    {
        $val = $this->checkDouble($nums[$bet['position'] - 1]);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }

    /**
     * 判断单双
     * @param $num int
     * @return int  1单  2双
     */
    protected function checkDouble($num)
    {
        $double = (0 == $num % 2) ? 2 : 1;
        return $double;
    }

    /**
     * 判断大小
     * @param $num int  原值
     * @param $diff int 拿来做比较的值
     * @return int 1小 2大
     */
    protected function checkBig($num, $diff)
    {
        $val = ($num >= $diff) ? 2 : 1;
        return $val;
    }

    /**
     * 判断大小和
     * @param $num int  原值
     * @param $diff int 拿来做比较的值
     * @return int 1小 2大 3和
     */
    protected function checkBigTieSmall($num, $diff)
    {
        $val = ($num > $diff) ? 2 : (($num == $diff) ? 3 : 1);
        return $val;
    }

    /**
     * 判断尾小尾大
     * @param $num int  原值
     * @param $diff int 拿来做比较的值
     * @return int 1小 2大
     */
    protected function checkEndBig($num, $diff)
    {
        $num = substr($num, -1);
        $val = ($num >= $diff) ? 2 : 1;
        return $val;
    }

    /**
     * 龙虎和
     * @param $dragon int 龙值
     * @param $tiger int 虎值
     * @return int 1龙  2虎  3和
     */
    protected function dragonTiger($dragon, $tiger)
    {
        $val = 0;
        if ($dragon > $tiger) {
            $val = 1;
        } elseif ($dragon < $tiger) {
            $val = 2;
        } elseif ($dragon == $tiger) {
            $val = 3;
        }
        return $val;
    }

    /**
     *
     * @param $bet     注单信息
     * @param $num     开奖结果
     * @param $correct 交集元素数量
     * @param $method 0:大于等于交集 1小于等于交集
     *
     */
    protected function checkPrizeBalls($bet, $nums, $correct = 3, $method = 0)
    {
        $win        = 0;
        $win_amount = floor(round($bet['bet_amount'] * $bet['odds'], 1));
        $bet_balls  = isset($bet['bet_number']) ? $bet['bet_number'] : $bet['number'];
        $bet_balls  = explode(',', $bet_balls);

        if (1 == $method) {
            if ($this->intersectBall($nums, $bet_balls) <= $correct) {
                $win = 1;
            }
        } else {
            if ($this->intersectBall($nums, $bet_balls) >= $correct) {
                $win = 1;
            }
        }

        $win_amount = ($win == 1) ? $win_amount : 0;
        return array('win' => $win, 'win_amount' => $win_amount);
    }

    protected function checkNoSameOdds($data)
    {
        $new_row = array();
        $count   = count($data);
        foreach ($data as $val) {
            $id = $val;
            $new_row[$id] = $val;
        }
        if ($count != count($new_row)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param  $balls array
     * @param  $nums array
     * @return int 交集元素数量
     */
    protected function intersectBall($balls, $nums)
    {
        $intersect = array_intersect($balls, $nums);
        $count     = count($intersect);
        return $count;
    }

    /**
     *
     * @param $balls array
     * @param $nums array
     * @param $correct int 交集元素数量
     */
    protected function checkIntersectBall($balls, $nums, $correct = 1)
    {
        $intersect = array_intersect($balls, $nums);
        if ($intersect && count($intersect) >= $correct) {
            return true;
        }
        return false;
    }

    protected function moreBalls($bet, $nums)
    {
        $child_bets = $this->getByOrderId($bet['order_id']);
        $sort_odds  = array();
        $sort_betid = array();
        foreach ($child_bets as $key => $value) {
            $sort_odds[$key]  = $value['odds'];
            $sort_betid[$key] = $value['bet_id'];
        }
        array_multisort($sort_odds, SORT_DESC, $sort_betid, SORT_DESC, $child_bets);

        foreach ($child_bets as $value) {
            $value['bet_number'] = $bet['number'];
            $data = $this->checkPrizeBalls($value, $nums, $value['number']);
            if (1 == $data['win']) {
                return $data;
                break;
            }
        }

        return array('win' => 0, 'win_amount' => 0);
    }

    /**
     *
     * @param $uid
     * @param $round_id 盘口编号
     * @param $lottery_type
     * @param $bet_time
     * @param $bet_amount
     * @param $win_amount
     * @param $return_amount
     * @param $odds_id
     * @param $odds
     * @param $method
     * @param $position
     * @param $number
     * @param $status
     * @param $settle_time
     * @return String
     */
    protected function betAuthCode(
       $uid,
       $round_id,
       $lottery_type,
       $bet_time,
       $bet_amount,
       $win_amount,
       $return_amount,
       $odds_id,
       $odds,
       $method,
       $position,
       $number,
       $status,
       $settle_time
    ) {
        $code = kg_md5($uid.$round_id.$lottery_type.$bet_time.$bet_amount.$win_amount.$return_amount.$odds_id.$odds.$method.$position.$number.$status.$settle_time);
        return $code;
    }

    protected function checkMoney($bet_money)
    {
        $check  = false;
        $handle = new Member();
        $result = $handle->getByMemberId($this->uid, 'amount');
        if ($result) {
            $lottery_amount = $result['amount'];
            $this->now_money_amount = $result['amount'];
            if ($lottery_amount >= $bet_money) {
                $check = true;
            }
        }
        unset($handle, $result);

        return $check;
    }

    protected function getBetRound($bet_round, $lottery_type)
    {
        $sql    = 'SELECT round_id FROM ltt_round WHERE number=? AND category=? AND stop_time>? AND status=1';
        $result = $this->_db->query($sql, array($bet_round, $lottery_type, $this->now_date));
        return $result;
    }

    /**
     * 检查是否存在该round_id
     * @param $round_id
     * @return Boolean
     */
    private function checkRoundId($round_id)
    {
        $sql    = 'SELECT round_id FROM ltt_round WHERE round_id=?';
        $result = $this->_db->query($sql, array($round_id));
        $check  = $result ? true : false;
        return $check;
    }

    protected function checkInt($val)
    {
        $check = false;
        if (is_numeric($val)) {
            $check = ((floor($val).'') == ($val.'')) ? true : false;
        }
        return $check;
    }

    /**
     * 扣钱
     * @param $value 减少金额数量
     * @param $uid member_id
     */
    protected function minusMoney($value, $uid)
    {
        //kg_echo($value);
        //kg_echo($uid);
        $success = false;
        if ($value > 0) {
            $member  = new Member();
            $success = $member->updateAmount($uid, 0 - $value);
            unset($member);
        }
        return $success;
    }

    /**
     * 金额变化
     * @param $value 增加金额数量
     * @param $uid member_id
     */
    protected function plusMoney($value, $uid)
    {
        $success = false;

        $member  = new Member();
        $success = $member->updateAmount($uid, $value);
        unset($member);

        return $success;
    }

    protected function updateValidAndMoney($uid, $value, $bet_valid, $force = true)
    {
        $success = false;

        $member  = new Member();
        $success = $member->updateValidAmount($uid, $value, $bet_valid, $force);
        unset($member);

        return $success;
    }

    /**
     * 生成盘口数据
     * 目前沒看到有使用上
     */
    public function generatePositionData($lottery)
    {
        $generate_setting = array(
            // cqssc
            10 => array(
                array(
                    'begin_time'   => '10:00:00',
                    'end_time'     => '22:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 60,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 72,
                    'round_mode'   => 1,
                    'substr_len'   => -3
                ),
                array(
                    'begin_time'   => '22:00:00',
                    'end_time'     => '01:55:00',
                    'gap_second'   => 300,
                    'delay_second' => 30,
                    'begin_round'  => 73,
                    'round_total'  => 48,
                    'round_mode'   => 1,
                    'substr_len'   => -3
                )
            ),
            // gdklsf
            11 => array(
                array(
                    'begin_time'   => '09:00:00',
                    'end_time'     => '23:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 90,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 84,
                    'round_mode'   => 1,
                    'substr_len'   => -2
                )
            ),
            // bjpk10
            12 => array(
                array(
                    'default_date' => '2016-02-18',
                    'begin_time'   => '09:02:30',
                    'end_time'     => '23:57:30',
                    'gap_second'   => 300,  // 每期间隔时间 秒
                    'delay_second' => 30,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 538240,
                    'round_total'  => 179,
                    'round_mode'   => 2
                )
            ),
            // cqklsf
            13 => array(
                array(
                    'begin_time'   => '10:00:00',
                    'end_time'     => '02:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 90,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 97,
                    'round_mode'   => 1,
                    'substr_len'   => -3
                )
            ),
            // jxssc
            14 => array(
                array(
                    'begin_time'   => '09:00:00',
                    'end_time'     => '23:10:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 60,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 84,
                    'round_mode'   => 1,
                    'substr_len'   => -3
                )
            ),
            // gd11x5
            15 => array(
                array(
                    'begin_time'   => '09:00:00',
                    'end_time'     => '23:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 60,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 84,
                    'round_mode'   => 1,
                    'substr_len'   => -2
                )
            ),
            // cq11x5 (暂停)
            16 => array(
                array(
                    'begin_time'   => '09:00:00',
                    'end_time'     => '23:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 60,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 84,
                    'round_mode'   => 1,
                    'substr_len'   => -2
                )
            ),
            // jx11x5
            16 => array(
                array(
                    'begin_time'   => '09:00:00',
                    'end_time'     => '22:00:00',
                    'gap_second'   => 600,  // 每期间隔时间 秒
                    'delay_second' => 60,   // 停止投注可以往后延迟时间 秒
                    'begin_round'  => 1,
                    'round_total'  => 84,
                    'round_mode'   => 1,
                    'substr_len'   => -2
                )
            ),
            //BJKL8
            18 => array(
                array(
                    'default_date' => '2016-02-18',
                    'begin_time'   => '09:00:00',
                    'end_time'     => '23:59:59',
                    'gap_second'   => 300,
                    'delay_second' => 30,
                    'round_total'  => 179,
                    'begin_round'  => 743669,
                    'round_mode'   => 2
                )
            ),
            //FC3D
            19 => array(
                array(
                    'default_date' => '2016-02-15',
                    'begin_time'   => '08:00:00',
                    'end_time'     => '20:00:00',
                    'round_total'  => 1,
                    'begin_round'  => 16039,
                    'round_mode'   => 3
                )
            ),
            //PL3
            20 => array(
                array(
                    'default_date' => '2016-02-15',
                    'begin_time'   => '08:00:00',
                    'end_time'     => '20:00:00',
                    'round_total'  => 1,
                    'begin_round'  => 16039,
                    'round_mode'   => 3
                )
            ),
        );
        if ($lottery && isset($generate_setting[$lottery])) {
            $one_data = $generate_setting[$lottery];
            $position_data = array();
            $bj_time = get_bj_time();
            foreach ($one_data as $row) {
                $one_begin_time = strtotime(date('Y-m-d', $bj_time) . $row['begin_time']);
                if (1 == $row['round_mode']) {
                    $fix = substr(date('Ymd', $bj_time), -6);
                    for ($i=0;$i<$row['round_total'];$i++) {
                        $now_round = '00' . ($row['begin_round'] + $i);
                        $now_round = $fix . substr($now_round, $row['substr_len']);
                        $bet_begin_time = $one_begin_time + ($i * $row['gap_second']);
                        $bet_stop_time  = $bet_begin_time + $row['gap_second'] + $row['delay_second'];
                        $bet_begin_time = date('Y-m-d H:i:s', $bet_begin_time);
                        $bet_stop_time  = date('Y-m-d H:i:s', $bet_stop_time);
                        $position_data[] = array(
                            'round'      => $now_round,
                            'lottery'    => $lottery,
                            'start_time' => $bet_begin_time,
                            'stop_time'  => $bet_stop_time
                        );
                    }
                } elseif (2 == $row['round_mode']) {
                    $start_round = $row['begin_round'] + $row['round_total'] * floor((time() - strtotime($row['default_date'])) / 86400);
                    for ($i=0;$i<$row['round_total'];$i++) {
                        $now_round = $start_round + $i;
                        $bet_begin_time = $one_begin_time + ($i * $row['gap_second']);
                        $bet_stop_time  = $bet_begin_time + $row['gap_second'] + $row['delay_second'];
                        $bet_begin_time = date('Y-m-d H:i:s', $bet_begin_time);
                        $bet_stop_time  = date('Y-m-d H:i:s', $bet_stop_time);
                        $position_data[] = array(
                            'round'      => $now_round,
                            'lottery'    => $lottery,
                            'start_time' => $bet_begin_time,
                            'stop_time'  => $bet_stop_time
                        );
                    }
                } elseif (3 == $row['round_mode']) {
                    $now_round = $row['begin_round'] + floor((time() - strtotime($row['default_date'])) / 86400);
                    $position_data[] = array(
                        'round'      => $now_round,
                        'lottery'    => $lottery,
                        'start_time' => $row['begin_time'],
                        'stop_time'  => $row['end_time'],
                    );
                }
            }
            // 插入数据
            if ($position_data) {
                foreach ($position_data as $row) {
                    print_r($row);
                    echo '<br>';
                    //$this->addPosition($row['round'], $row['lottery'], $row['start_time'], $row['stop_time'], '');
                }
            }
        }
    }

    protected function setError($err)
    {
        $this->error = $err;
    }

    public function check_bet_on_cap(){
        $sql = 'SELECT distinct `ltt_bet`.`round_id` , `ltt_round`.`result` , `ltt_round`.`category` FROM `ltt_bet`
                    INNER JOIN `ltt_round` ON `ltt_bet`.`round_id` = `ltt_round`.`round_id`
                    WHERE `ltt_bet`.`status` = 1';

        return $this->_db->query($sql , array());
    }

    public function check_member_round_bet($round, $lottery, $member_id){
        $result = $this->getPosition($round, $lottery);

        $sql = 'select sum(bet_amount) as bet_amount,odds_id from ltt_bet where round_id=? and member_id=?  Group by odds_id';
        $params[] = $result[0]['round_id'];
        $params[] = $member_id;
        $member_bet = $this->_db->query($sql , $params);

        $odds_member_bet = array();
        foreach($member_bet as $item){
            $odds_member_bet[$item['odds_id']] = $item;
        }
        unset($member_bet);
        unset($result);

        return $odds_member_bet;
    }

    /**
     * [getLastLotteryResult 抓取資料庫最後一筆結果以及投注期數]
     *
     * @param integer $lottery 彩種代號
     * @return void
     */
    public function getLastLotteryResult($lottery) {
        $sql = '
        SELECT
            ltt_result.number AS round,
            ltt_result.content AS numbers,
            ltt_round.bet_round,
            ltt_round.stop_time

        FROM ltt_result,
               (
                 SELECT
                   number AS bet_round,
                   stop_time
                 FROM ltt_round
                 WHERE category = ?
                 ORDER BY round_id DESC LIMIT 1
               ) AS ltt_round
        WHERE
            ltt_result.category = ?
        ORDER BY result_id desc limit 1';

        $result = $this->_db->query($sql,array($lottery,$lottery));

        if($result) {
            $result = $result[0];
            $stop_time = strtotime($result[0]['stop_time']);
            $result['bet_stop_time'] = $stop_time;
            if ($lottery === 9) {
                $result['open_term_time'] = $stop_time + 300;
            } elseif ($lottery === 98 || $lottery === 99) {
                $result['open_term_time'] = $stop_time + 30;
            } elseif ($lottery === 29) {
                $result['open_term_time'] = $stop_time + 20;
            } else {
                $result['open_term_time'] = $stop_time + 60;
            }
        }
        return $result;
    }

    /**
     * [getTodayLatestRound] 撈取今天的最後一期
     *
     * @param integer $category 採種
     * @return void
     */
    public function getTodayLatestRound($category) {
        $today = date('Y-m-d') . ' 23:59:59';
        $sql = ' SELECT number FROM ltt_round WHERE category = ? AND stop_time <= ? ORDER BY round_id DESC LIMIT 1';

        $result = $this->_db->query($sql, array($category, $today));

        if(count($result) > 0) {
            return $result[0]['number'];
        }
    }

    /**
     * [getLastLotteryRound] 撈取昨天的最後一期
     *
     * @param integer $category 採種
     * @return void
     */
    public function getLastLotteryRound($category) {
        $yesterday = date('Y-m-d',strtotime('-1 day' ,strtotime(date('Y-m-d'))));
        $yesterday_start = $yesterday . ' 00:00:00';
        $yesterday_stop = $yesterday . ' 23:59:59';

        $sql = ' SELECT number FROM ltt_round WHERE category = ? AND stop_time BETWEEN ? AND ? ORDER BY number DESC LIMIT 1';
        $result = $this->_db->query($sql,array($category, $yesterday_start, $yesterday_stop));

        if(count($result) > 0) {
            return $result[0]['number'];
        }
    }

    /**
     * 假如redis盤口時間已過時，則從DB抓取重新塞回redis裡
     *
     * @return void
     */
    public function updateLttRoundRedis() {

        $cache_handle = new Cache();
        global $g_auto_create_round;
        global $g_ten_lottery;
        global $g_five_lottery;


        foreach($g_auto_create_round as $category => $value) {

            $json = $cache_handle->hGet(CUSTOMER_NAME.'-lottery',$category);
            $json = json_decode($json,true);

            if(!$json) {
                $json = array();
                $sql = ' SELECT number,content FROM ltt_result WHERE category = ? ORDER BY number DESC LIMIT 1';
                $result = $this->_db->query($sql, array($category));

                if(count($result) > 0) {
                    $result =  $result[0];
                    $json['round'] = $result['number'];
                    $json['numbers'] = $result['content'];
                }
            }

            if($json) {

                // 時間到了
                if($json[0]) {
                    if($json[0]['open_term_time'] <= time()) {
                        $round = $json[0]['round'];
                        $numbers = $json[0]['numbers'];
                        array_shift($json);
                        $json[0]['round'] = $round;
                        $json[0]['numbers'] = $numbers;
                        $cache_handle->hSet(CUSTOMER_NAME.'-lottery',$category, json_encode($json));
                    }
                }

                //個數不夠就撈資料
                if(count($json) <= 2) {

                    $sql = ' SELECT number,stop_time FROM ltt_round WHERE category = ? AND stop_time > ? ORDER BY number ASC LIMIT 5';
                    $current_time = date('Y-m-d H:i:s');
                    $result = $this->_db->query($sql,array($category, $current_time));

                    if(count($result) !== 0) {
                        $ltt_round_json = array();

                        foreach($result as $key => $value) {
                            if($key == 0) {
                                if(!$json[0]) {
                                    $ltt_round_json[$key]['round'] = $json['round'];
                                    $ltt_round_json[$key]['numbers'] = $json['numbers'];
                                } else {
                                    $ltt_round_json[$key]['round'] = $json[0]['round'];
                                    $ltt_round_json[$key]['numbers'] = $json[0]['numbers'];
                                }
                            }

                            $open_term_time = strtotime($value['stop_time']);
                            $stop_time = strtotime($value['stop_time']);

                            switch(intval($category)) {
                                case 12: // 北京賽車
                                    $open_term_time = $open_term_time + 30;
                                    break;
                                case 21: // 幸運飛艇
                                    $stop_time = $stop_time - 30;
                                    break;
                                default:
                                    if(in_array($category, $g_ten_lottery)) {
                                        $stop_time = $stop_time - 60;
                                    } else if (in_array($category,$g_five_lottery)) {
                                        $stop_time = $stop_time - 30;
                                    } else {
                                        $stop_time = $stop_time - 9;
                                    }
                                    break;
                            }

                            $ltt_round_json[$key]['bet_stop_time'] = $stop_time;
                            $ltt_round_json[$key]['bet_round'] = $value['number'];
                            $ltt_round_json[$key]['open_term_time'] = $open_term_time;
                        }

                        $ltt_round_json = json_encode($ltt_round_json);
                        $cache_handle->hSet(CUSTOMER_NAME.'-lottery',$category, $ltt_round_json);

                        // print_r($ltt_round_json);
                    }
                }
            }
        }

        unset($cache_handle);
    }
}
