<?php
/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

final class HappyEight extends Lottery
{
    public function __construct($lottery = 0)
    {
        parent::__construct();
        
        $lottery = $lottery > 0 ? $lottery : 18;
        $this->lottery_type = $lottery;
    }
    
    /**
     * 采集结果 号码位数
     *
     */
    protected function getNumberCount()
    {
        return 20;
    }
    
    /**
     * 重写Lottery methods方法
     * @param $bet = array() 注单信息
     * @param $numbers = array(1,3,5,8,9,...) 开奖号码
     */
    public function methods($bet, $numbers)
    {
        //  0输  1赢  2和
        $win = call_user_func_array(array($this, $bet['settle_method']), array($bet, $numbers));

        if (is_array($win)) {
            $this->settleOneBet($bet, $win['win'], $win['win_amount']);
        } else {
            $this->settleOneBet($bet, $win);
        }
    }
    
    protected function methods2($bet, $numbers)
    {
        //  0输  1赢
        $win = call_user_func_array(array($this, $bet['settle_method']), array($bet, $numbers));
        return $win;
    }
    
    protected function betTypeAttached()
    {
        $data = array(
            1   => '10',    // 选一
            2   => '10',    // 选二
            3   => '10',    // 选三
            4   => '10',    // 选四
            5   => '10'     // 选五
        );
        return $data;
    }
    
    protected function betTypeAttachedChild()
    {
        $data = array(
            1 => '1',
            2 => '2',
            3 => '11',    // 选三   (中二 中三)
            4 => '12',    // 选四  (中二  中三  中四)
            5 => '13'     // 选五  (中三  中四  中五)
        );
        return $data;
    }
    
    /**
     * 选一
     *
     */
    protected function fun1($bet, $nums)
    {
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }
    
    /**
     * 选二
     *
     */
    protected function fun2($bet, $nums)
    {
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }
    
    /**
     * 选三
     *
     */
    protected function fun3($bet, $nums)
    {
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }
    
    /**
     * 选四
     *
     */
    protected function fun4($bet, $nums)
    {
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }
    
    /**
     * 选五
     *
     */
    protected function fun5($bet, $nums)
    {
        $data = $this->moreBalls($bet, $nums);
        return $data;
    }
    
    /**
     * 玩法: 总和单双
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun20($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkDouble($sum);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 玩法: 总和小大
     * @param $bet 投注信息
     * @param $nums 开奖号码
     * @return int 0输  1赢  2和
     */
    protected function fun21($bet, $nums)
    {
        $sum = array_sum($nums);
        $val = $this->checkBigTieSmall($sum, 810);
        $win = ($bet['number'] == $val) ? 1 : 0;
        return $win;
    }
    
    /**
     * 上盘  下盘  中盘
     *
     */
    protected function fun22($bet, $nums)
    {
        $win = ($this->checkUpDown($nums) == $bet['number']) ? 1 : 0;
        return $win;
    }
    
    protected function checkUpDown($nums)
    {
        $a   = 0;
        $b   = 0;
        $val = 0;
        foreach ($nums as $value) {
            if ($value > 40) {
                $b++;
            } else {
                $a++;
            }
        }
        if ($a > $b) {
            $val = 1;
        } elseif ($a < $b) {
            $val = 2;
        } elseif ($a == $b) {
            $val = 3;
        }
        return $val;
    }
    
    /**
     * 奇盘 偶盘 和盘
     *
     */
    protected function fun23($bet, $nums)
    {
        $win = ($this->checkDoubleCount($nums) == $bet['number']) ? 1 : 0;
        return $win;
    }
    
    protected function checkDoubleCount($nums)
    {
        $a   = 0;
        $b   = 0;
        $val = 0;
        foreach ($nums as $value) {
            if (1 == $this->checkDouble($value)) {
                $a++;
            } else {
                $b++;
            }
        }
        if ($a > $b) {
            $val = 1;
        } elseif ($a < $b) {
            $val = 2;
        } elseif ($a == $b) {
            $val = 3;
        }
        return $val;
    }
}
