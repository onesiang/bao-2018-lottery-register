<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 会员配置类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class MemberConfig extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function getByMemberId($member_id, $field = '*')
    {
        $sql = 'SELECT ' . $field . ' FROM uc_member_config WHERE member_id=?';
        $result = $this->_db->query($sql, array($member_id));
        if ($result && is_array($result)) {
            $result = $result[0];
        }
        return $result;
    }

    public function allowBetModule($member_id)
    {
        $sql = 'SELECT allow_bet_module FROM uc_member_config WHERE member_id=?';
        $result = $this->_db->query($sql, array($member_id));
        if ($result && is_array($result)) {
            $result = $result[0]['allow_bet_module'];
            $result = $result ? explode(',', $result) : '';
        }
        return $result;
    }

    public function updateAllowBetModule($member_id, $bet_module)
    {
        $sql = 'UPDATE uc_member_config SET allow_bet_module=? WHERE member_id=?';
        $result = $this->_db->query($sql, array($bet_module, $member_id));
        return $result;
    }

    public function updateCasinoPoint($amount, $member_id)
    {
        $sql = 'UPDATE uc_member_config SET casino_amount=casino_amount+? WHERE member_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($amount, $member_id));
        return $result;
    }

    public function getList($parameters)
    {
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $page = isset($parameters['page']) ? floor($parameters['page']) : '';
        $page_size = isset($parameters['page_size']) ? floor($parameters['page_size']) : 20;

        $where = '';
        $params = array();
        if ($account) {
            $where .= ' AND a.account = ?';
            $params[] = $account ;
        }

        $sql = 'SELECT COUNT(member_id) AS total FROM uc_member AS a WHERE 1 ' . $where;
        $result = $this->_db->query($sql, $params);

        $return = array();
        $return['total_page'] = ceil($result[0]['total'] / $page_size);
        $return['total_page'] = $return['total_page'] > 0 ? $return['total_page'] : 1;
        $page = $page > 0 ? $page : 1;
        $page = $page > $return['total_page'] ? $return['total_page'] : $page;
        $return['page'] = $page;
        $limits = ($page - 1) * $page_size . ',' . $page_size;

        $sql = 'SELECT a.account, a.member_id, b.* ';
        //$sql   .= ' b.sport_single_min, b.sport_single_max, b.sport_parlay_min, b.sport_parlay_max, b.sport_match_max, ';
        //$sql   .= ' b.casino_return_type, b.casino_return_rate, b.allow_bet_module, ';
        //$sql   .= ' b.lottery_min, b.lottery_max, b.sport_return_type, b.sport_return_rate, b.lottery_return_type, b.lottery_return_rate ';
        $sql .= ' FROM uc_member AS a LEFT JOIN uc_member_config AS b ON a.member_id=b.member_id WHERE 1 ' . $where;
        $sql .= ' ORDER BY a.member_id';
        $sql .= ' LIMIT ' . $limits;
        $result = $this->_db->query($sql, $params);
        $return['list'] = $result;

        unset($sql, $result);

        return $return;
    }

    /* public function updateCheckBetAmount($member_id, $amount, $add = 0)
    {
    if (1 == $add) {
    $sql = 'UPDATE uc_member_config SET check_bet_amount=check_bet_amount+? WHERE member_id=?';
    } else {
    $sql = 'UPDATE uc_member_config SET check_bet_amount=? WHERE member_id=?';
    }
    $result = $this->_db->query($sql, array($amount, $member_id));
    return $result;
    }*/

    public function updateCheckBetAmount($member_id, $amount, $add = 0)
    {
        if (1 == $add) {
            $sql[] = "UPDATE uc_member_config
                    SET check_bet_amount =
                        CASE WHEN (check_bet_amount + ($amount) ) < 0
                        THEN 0
                        ELSE (check_bet_amount + ($amount) )
                        END
                    WHERE member_id= $member_id";
            //result = $this->_db->query($sql, array($amount, $member_id));
            $result = $this->_db->nativeQuery($sql);
        } elseif (2 == $add) {
            // $sql = 'UPDATE uc_member_config SET check_bet_amount=check_bet_amount-? WHERE member_id=?';
            $sql = 'UPDATE uc_member_config SET check_bet_amount= CASE WHEN (check_bet_amount-?) < 0 THEN 0 ELSE check_bet_amount-? END WHERE member_id = ?';
            $result = $this->_db->query($sql, array($amount, $amount, $member_id));
        } else {
            $sql = 'UPDATE uc_member_config SET check_bet_amount=? WHERE member_id=?';
            $result = $this->_db->query($sql, array($amount, $member_id));
        }
        return $result;
    }

    public function updateMemberConfigByLevel($level, $parameters)
    {
        $sql = 'UPDATE uc_member_config a LEFT JOIN uc_member b ON a.member_id=b.member_id SET ';
        $set = '';
        $params = array();
        if (isset($parameters['sport_return_type'])) {
            $set .= ' a.sport_return_type=?,';
            $params[] = $parameters['sport_return_type'];
        }
        if (isset($parameters['sport_return_rate'])) {
            $set .= ' a.sport_return_rate=?,';
            $params[] = $parameters['sport_return_rate'];
        }
        if (isset($parameters['lottery_return_type'])) {
            $set .= ' a.lottery_return_type=?,';
            $params[] = $parameters['lottery_return_type'];
        }
        if (isset($parameters['lottery_return_rate'])) {
            $set .= ' a.lottery_return_rate=?,';
            $params[] = $parameters['lottery_return_rate'];
        }
        if (isset($parameters['casino_return_type'])) {
            $set .= ' a.casino_return_type=?,';
            $params[] = $parameters['casino_return_type'];
        }
        if (isset($parameters['casino_return_rate'])) {
            $set .= ' a.casino_return_rate=?,';
            $params[] = $parameters['casino_return_rate'];
        }
        if (isset($parameters['electronic_return_type'])) {
            $set .= ' a.electronic_return_type=?,';
            $params[] = $parameters['electronic_return_type'];
        }
        if (isset($parameters['electronic_return_rate'])) {
            $set .= ' a.electronic_return_rate=?,';
            $params[] = $parameters['electronic_return_rate'];
        }
        if ($set) {
            $set = trim($set, ',');
            $sql .= $set . ' WHERE 1 ';
            if ($level >= 0) {
                $sql .= ' AND b.level=? ';
                $params[] = $level;
            }
            $result = $this->_db->query($sql, $params);
            return $result;
        }
        return 0;
    }

    public function edit($member_id, $data)
    {
        $set = ' SET lottery_min=?, lottery_max=?,lottery_return_type=?, lottery_return_rate=?, allow_bet_module=?, check_bet_amount=?, withdrawal_count_max=?';
        $params = array(
            $data['lottery_min'],
            $data['lottery_max'],
            $data['lottery_return_type'],
            $data['lottery_return_rate'],
            $data['allow_bet_module'],
            $data['check_bet_amount'],
            $data['withdrawal_count_max'],
            $member_id,
        );

        $sql = 'SELECT
                    member_id,check_bet_amount,
                    lottery_min, lottery_max,
                    lottery_return_type, lottery_return_rate,
                    allow_bet_module, withdrawal_count_max
                FROM uc_member_config WHERE member_id=?';
        $result = $this->_db->query($sql, array($member_id));
        if ($result) {

            $log_detail = '';
            $argument = array();

            if ($data['check_bet_amount'] != $result[0]['check_bet_amount']) {
                $set .= ', modified_time=NOW() ';
                $log_detail .= '打码量:' . money_from_db($data['check_bet_amount']) . ',';
                $argument['check_bet_amount'] = $data['check_bet_amount'];
            }

            if (intval($data['withdrawal_count_max']) != intval($result[0]['withdrawal_count_max'])) {
                $log_detail .= '当天取款次数上限:' . $data['withdrawal_count_max'] . ',';
                $argument['withdrawal_count_max'] = $data['withdrawal_count_max'];
            }

            if ($result[0]['allow_bet_module']) {
                $allow_bet_module_result = explode(',', $result[0]['allow_bet_module'])[1];
                $allow_bet_module_data = explode(',', $data['allow_bet_module'])[1];

                if ($allow_bet_module_data != $allow_bet_module_result) {
                    $log_detail .= '允许投注:' . ((boolean) $allow_bet_module_data ? '是' : '否') . ',';
                    $argument['allow_bet_module'] = $allow_bet_module_data;
                }
            }

            if ($data['lottery_return_type'] != $result[0]['lottery_return_type']) {
                $log_detail .= '返水类型:' . $data['lottery_return_type'] . ',';
                $argument['lottery_return_type'] = $data['lottery_return_type'];
            }

            if ($data['lottery_return_rate'] != $result[0]['lottery_return_rate']) {
                $log_detail .= '返水比例:' . $data['lottery_return_rate'] . ',';
                $argument['lottery_return_rate'] = $data['lottery_return_rate'];
            }

            if ($data['lottery_min'] != $result[0]['lottery_min']) {
                $log_detail .= '单式最低投注限额:' . $data['lottery_min'] . ',';
                $argument['lottery_min'] = $data['lottery_min'];
            }

            if ($data['lottery_max'] != $result[0]['lottery_max']) {
                $log_detail .= '单式最高投注限额:' . $data['lottery_max'] . ',';
                $argument['lottery_max'] = $data['lottery_max'];
            }

            $sql = 'UPDATE uc_member_config ' . $set . ' WHERE member_id=?';
            $result = $this->_db->query($sql, $params);

            if ($log_detail && $result) {
                $member_handle = new Member();
                $member = $member_handle->getByMemberId($member_id);
                $operate_log_handle = new OperateLog();
                $parameters = array(
                    'manager_account' => $data['manager_account'],
                    'account' => $member['account'],
                    'account_type' => 1,
                    'module' => 'member',
                    'argument' => $argument,
                    'detail' => '编辑会员设定|' . trim($log_detail, ','),
                );
                $operate_log_handle->add($parameters);
                unset($operate_log_handle);
            }

        } else {
            $sql = 'INSERT INTO uc_member_config ' . $set . ', member_id=?';
            $result = $this->_db->query($sql, $params);
        }
        return $result;
    }
}
