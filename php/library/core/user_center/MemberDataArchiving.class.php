<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 数据归档
 *
 * @package Wwin
 * @final
 */
final class MemberDataArchiving extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * 归档
     *
     */
    public function archiving()
    {
        $member_handle = new MemberStatistics();

        for ($i=12; $i >= 3; $i--) {
            $month_data = 'midnight first day of -' . $i . ' month';
            $date_start = date('Y-m-01', strtotime($month_data));
            $date_stop  = date('Y-m-d 23:59:59', strtotime("$date_start +1 month -1 day"));
            $month      = date('Y-m', strtotime($month_data));

            $argument = array(
                'date_start' => $date_start,
                'date_stop'  => $date_stop
            );
            $data = $member_handle->memberReport($argument);
            if ($data) {
                foreach ($data as $key=> $row) {
                    $account = $row['account'];
                    $one_data  = array(
                        'sport_bet_amount'        => $row['sport_bet_amount'],
                        'sport_valid_amount'      => $row['sport_valid_amount'],
                        'sport_win_amount'        => $row['sport_win_amount'],
                        'lottery_bet_amount'      => $row['lottery_bet_amount'],
                        'lottery_valid_amount'    => $row['lottery_valid_amount'],
                        'lottery_win_amount'      => $row['lottery_win_amount'],
                        'casino_bet_amount'       => $row['casino_bet_amount'],
                        'casino_valid_amount'     => $row['casino_valid_amount'],
                        'casino_win_amount'       => $row['casino_win_amount'],
                        'elc_bet_amount'          => $row['elc_bet_amount'],
                        'elc_valid_amount'        => $row['elc_valid_amount'],
                        'elc_win_amount'          => $row['elc_win_amount'],
                        'online_deposit'          => $row['online_deposit'],
                        'company_deposit'         => $row['company_deposit'],
                        'hand_deposit'            => $row['hand_deposit'],
                        'other_adjust'            => $row['other'],
                        'recharge'                => $row['recharge'],
                        'online_withdraw'         => $row['online_withdraw'],
                        'auto_return_water'       => $row['auto_return_water'],
                        'auto_winnings'           => $row['auto_winnings'],
                        'hand_return_water'       => $row['hand_return_water'],
                        'hand_winnings'           => $row['hand_winnings']
                    );
                    $result = $this->add($key, $account, $month, $date_start, $date_stop, $one_data);
                    if ($result) {
                        /*
                        // 体育注单
                        $sql = 'DELETE FROM spr_bet WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));
                        */

                        // 彩票注单
                        $params = array(
                            $key,
                            date('Y-m-d H:i:s', strtotime($date_start) ),
                            date('Y-m-d H:i:s', strtotime($date_stop) )
                        );
                        $sql = 'DELETE  FROM ltt_bet WHERE member_id=? AND bet_time>? AND bet_time<?';
                        $this->_db->query($sql, $params);

                        /*
                        // 真人注单
                        $sql = 'DELETE  FROM csn_bet WHERE account=? AND bet_time>? AND bet_time<?';
                        $this->_db->query($sql, array($account, $date_start, $date_stop));

                        // 电子注单
                        $sql = 'DELETE  FROM electronic_bet WHERE account=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($account, $date_start, $date_stop));
                        */

                        // 取款
                        $sql = 'DELETE  FROM fnn_withdrawal WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));


                        // fnn_record
                        $sql = 'DELETE  FROM fnn_record WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));


                        // 额度添加
                        $sql = 'DELETE  FROM fnn_adjust WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));


                        // 额度转移
                        $sql = 'DELETE  FROM fnn_casino_transfer WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));


                        // 存款
                        $sql = 'DELETE  FROM fnn_deposit WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));

                        //公司入款
                        $sql = 'DELETE  FROM fnn_remittance WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $this->_db->query($sql, array($key, $date_start, $date_stop));
                    }
                }
                break;
            }
        }
    }

    public function cleanup()
    {
        for ($i=12; $i >= 3; $i--) {
            $month_data = 'midnight first day of -' . $i . ' month';
            $date_start = date('Y-m-01', strtotime($month_data));
            $date_stop  = date('Y-m-d 23:59:59', strtotime("$date_start +1 month -1 day"));

            Log::record($date_start . ':' .  $date_stop, 'info','cleanup_data/');

            // delete 其它类型数据
            $sql = 'DELETE FROM mdl_notice WHERE module=1 AND method=1 AND submit_time>? AND submit_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'info','cleanup_data/');

            /*
            $sql = 'DELETE FROM spr_result WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_soccer WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_basketball WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_baseball WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_tennis WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_volleyball WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');

            $sql = 'DELETE FROM spr_live WHERE update_time>? AND update_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'cleanup_data');
            */

            $sql = 'DELETE  FROM mdl_message WHERE send_time>? AND send_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));
            Log::record($sql, 'info','cleanup_data/');
        }
    }

    public function archivingData($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }
        $account = isset($argument['account']) ? $argument['account'] : '';

        $condition = array();
        $parameter = array();
        if ($account) {
            $condition[] = ' AND account=?';
            $parameter[] = $account;
        }

        $return = array();

        $sql = 'SELECT COUNT(id) AS count FROM uc_member_data_archiving WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = array();
        $sql[] = 'SELECT * FROM uc_member_data_archiving WHERE 1';
        $sql[] = implode('', $condition);
        $sql[] = ' ORDER BY id DESC LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query(implode('', $sql), $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;

        return $return;
    }

    /*public function archiving() {
        $member_handle = new MemberStatistics();

        for($i=12; $i >= 3; $i--) {
            $month_data = '-' . $i . ' month';
            $date_start = date('Y-m-01', strtotime($month_data));
            $date_stop  = date('Y-m-d 23:59:59', strtotime("$date_start +1 month -1 day"));

            // delete 其它类型数据
            $sql = 'DELETE FROM mdl_notice WHERE module=1 AND method=1 AND submit_time>? AND submit_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE FROM spr_result WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE FROM spr_soccer WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE FROM spr_basketball WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE FROM spr_tennis WHERE start_time>? AND start_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE FROM spr_live WHERE update_time>? AND update_time<?';
            $this->_db->query($sql, array($date_start, $date_stop));

            $sql = 'DELETE  FROM mdl_message WHERE send_time>? AND send_time<?';
            $result = $this->_db->query($sql, array($date_start, $date_stop));

            $argument = array(
                'date_start' => $date_start,
                'date_stop'  => $date_stop
            );
            $data = $member_handle->memberReport($argument);
            if($data) {
                foreach($data as $key=> $row) {
                    $account = $row['account'];
                    $delete  = 1;
                    $add_result = 1;
                    $this->_db->beginTransaction();
                    // 体育注单
                    $sql = 'SELECT bet_id FROM spr_bet WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                    $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                    if($result) {
                        $sql = 'DELETE FROM spr_bet WHERE member_id=? AND submit_time>? AND submit_time<?';
                        $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                    }
                    // 彩票注单
                    if($delete) {
                        $params = array(
                            $key,
                            date('Y-m-d H:i:s', strtotime($date_start) + 43200),
                            date('Y-m-d H:i:s', strtotime($date_stop) + 43200)
                        );
                        $sql = 'SELECT bet_id  FROM ltt_bet WHERE member_id=? AND bet_time>? AND bet_time<? LIMIT 1';
                        $result = $this->_db->query($sql, $params);
                        if($result) {
                            $sql = 'DELETE  FROM ltt_bet WHERE member_id=? AND bet_time>? AND bet_time<?';
                            $delete = $this->_db->query($sql, $params);
                        }
                    }

                    // 真人注单
                    if($delete) {
                        $sql = 'SELECT bet_id  FROM csn_bet WHERE account=? AND bet_time>? AND bet_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($account, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM csn_bet WHERE account=? AND bet_time>? AND bet_time<?';
                            $delete = $this->_db->query($sql, array($account, $date_start, $date_stop));
                        }
                    }

                    // 电子注单
                    if($delete) {
                        $sql = 'SELECT bet_id  FROM electronic_bet WHERE account=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($account, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM electronic_bet WHERE account=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($account, $date_start, $date_stop));
                        }
                    }

                    // 取款
                    if($delete) {
                        $sql = 'SELECT withdrawal_id FROM fnn_withdrawal WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_withdrawal WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    // fnn_record
                    if($delete) {
                        $sql = 'SELECT record_id FROM fnn_record WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_record WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    // 额度添加
                    if($delete) {
                        $sql = 'SELECT adjust_id FROM fnn_adjust WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_adjust WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    // 额度转移
                    if($delete) {
                        $sql = 'SELECT transfer_id FROM fnn_casino_transfer WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_casino_transfer WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    // 存款
                    if($delete) {
                        $sql = 'SELECT deposit_id FROM fnn_deposit WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_deposit WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    //公司入款
                    if($delete) {
                        $sql = 'SELECT remittance_id FROM fnn_remittance WHERE member_id=? AND submit_time>? AND submit_time<? LIMIT 1';
                        $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        if($result) {
                            $sql = 'DELETE  FROM fnn_remittance WHERE member_id=? AND submit_time>? AND submit_time<?';
                            $delete = $this->_db->query($sql, array($key, $date_start, $date_stop));
                        }
                    }

                    if($delete) {
                        $one_data  = array(
                            'sport_bet_amount'        => $row['sport_bet_amount'],
                            'sport_valid_amount'      => $row['sport_valid_amount'],
                            'sport_win_amount'        => $row['sport_win_amount'],
                            'lottery_bet_amount'      => $row['lottery_bet_amount'],
                            'lottery_valid_amount'    => $row['lottery_valid_amount'],
                            'lottery_win_amount'      => $row['lottery_win_amount'],
                            'casino_bet_amount'       => $row['casino_bet_amount'],
                            'casino_valid_amount'     => $row['casino_valid_amount'],
                            'casino_win_amount'       => $row['casino_win_amount'],
                            'elc_bet_amount'          => $row['elc_bet_amount'],
                            'elc_valid_amount'        => $row['elc_valid_amount'],
                            'elc_win_amount'          => $row['elc_win_amount'],
                            'online_deposit'          => $row['online_deposit'],
                            'company_deposit'         => $row['company_deposit'],
                            'hand_deposit'            => $row['hand_deposit'],
                            'other_adjust'            => $row['other'],
                            'recharge'                => $row['recharge'],
                            'online_withdraw'         => $row['online_withdraw'],
                            'auto_return_water'       => $row['auto_return_water'],
                            'auto_winnings'           => $row['auto_winnings'],
                            'hand_return_water'       => $row['hand_return_water'],
                            'hand_winnings'           => $row['hand_winnings']
                        );
                        $add_result = $this->add($one_data);
                    }
                    if($add_result) {
                        $this->_db->commit();
                    }
                    else {
                        $this->_db->rollBack();
                    }

                }
                break;
            }


        }
    }*/

    private function add($member_id, $account, $month, $date_start, $date_stop, $argument)
    {
        $sql = 'INSERT INTO uc_member_data_archiving SET member_id=?,account=?,number=?,submit_time=?,start_time=?,stop_time=?,';
        $params = array(
            $member_id,
            $account,
            $month,
            date('Y-m-d H:i:s'),
            $date_start,
            $date_stop
        );
        $fields = array();
        foreach ($argument as $key => $value) {
            $fields[] = $key;
            $params[] = $value;
        }
        $sql .= implode('=?,', $fields) . '=? ';
        $result = $this->_db->query($sql, $params);
        return $result;
    }
}
