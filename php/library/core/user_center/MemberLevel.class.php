<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 会员等级类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class MemberLevel extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    public function add($argument)
    {
        $level = $this->getByLevel($argument[0]);
        if ($level) {
            $argument[] = $level['level_id'];
            return $this->edit($argument);
        }
        if ('' == $argument[1]) {
            $argument[1] == $argument[0];
        }

        if ($argument[10] && isset($argument[10])) {
            $manager_account = $argument[10];
            unset($argument[10]);
        }

        $sql = array();
        $sql[] = 'INSERT INTO uc_member_level SET level=?,name=?,deposit_amount_min=?';
        $sql[] = ',deposit_amount_max=?,deposit_count_min=?,deposit_count_max=?';
        $sql[] = ',withdrawal_amount_min=?,withdrawal_amount_max=?';
        $sql[] = ',withdrawal_count_min=?,withdrawal_count_max=?';
        if ($this->_db->query(implode('', $sql), $argument)) {
            if ($manager_account) {
                $operate_log_handle = new OperateLog();
                $parameters = array(
                    'manager_account' => $manager_account,
                    'account' => '',
                    'account_type' => 1,
                    'module' => 'member',
                    'argument' => [],
                    'detail' => '管理员新增会员层级|' . $argument[1]
                );
                $operate_log_handle->add($parameters);
                unset($operate_log_handle);
            }
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * [add_rate_level 新增反水層級]
     *
     * @param [type] $argument
     * @return void
     */
    public function add_rate_level($argument)
    {
        $sql = array();
        $sql[] = 'INSERT INTO uc_member_return SET name=?,bet_valid_amount_min=?';
        $sql[] = ',bet_valid_amount_max=?,rate=?';
        if ($this->_db->query(implode('', $sql), $argument)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function get($level_id)
    {
        $sql = 'SELECT * FROM uc_member_level WHERE level_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($level_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function get_rete($level_id)
    {
        $sql = 'SELECT * FROM uc_member_return WHERE level_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($level_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function edit($argument)
    {
        $level_id = $argument[10];
        $new_level = $argument[0];
        $manager_account = '';

        foreach ($argument as $key => $value) {
            if ($value == '') {
                $argument[$key] = 0;
            }
        }

        if ($argument[11] && isset($argument[11])) {
            $manager_account = $argument[11];
            unset($argument[11]);
        }

        $sql = 'SELECT level_id FROM uc_member_level WHERE level=?';
        $result = $this->_db->query($sql, array($new_level));

        if ($result) {
            if ($result[0]['level_id'] != $level_id) {
                return -1;
            }
        }

        $sql = 'SELECT
                    level,name,deposit_amount_max,deposit_count_max,
                    withdrawal_amount_max,withdrawal_count_max
                FROM
                    uc_member_level
                WHERE level_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($level_id));

        if ($result) {
            $level = $result[0]['level'];

            $log_detail = '';

            if ($result[0]['name'] != $argument[1]) {
                $log_detail .= '层级名称:' . $argument[1] . ',';
            }

            if ($result[0]['deposit_amount_max'] != $argument[3]) {
                $log_detail .= '会员最高存款额度:' . $argument[3] . ',';
            }

            if ($result[0]['deposit_count_max'] != $argument[5]) {
                $log_detail .= '会员最高存款次数:' . $argument[5] . ',';
            }

            if ($result[0]['withdrawal_amount_max'] != $argument[7]) {
                $log_detail .= '会员最高取款额度:' . $argument[7] . ',';
            }

            if ($result[0]['withdrawal_count_max'] != $argument[9]) {
                $log_detail .= '会员最高取款次数:' . $argument[9];
            }

            if ($manager_account) {
                $operate_log_handle = new OperateLog();
                $parameters = array(
                    'manager_account' => $manager_account,
                    'account' => '',
                    'account_type' => 1,
                    'module' => 'member',
                    'argument' => [],
                    'detail' => '管理员编辑会员层级|' . trim($log_detail, ',')
                );
                $operate_log_handle->add($parameters);
                unset($operate_log_handle);
            }
        } else {
            return 0;
        }

        $sql = array();
        $sql[] = 'UPDATE uc_member_level SET level=?,name=?,deposit_amount_min=?';
        $sql[] = ',deposit_amount_max=?,deposit_count_min=?,deposit_count_max=?';
        $sql[] = ',withdrawal_amount_min=?,withdrawal_amount_max=?';
        $sql[] = ',withdrawal_count_min=?,withdrawal_count_max=? WHERE level_id=?';

        if ($this->_db->query(implode('', $sql), $argument)) {
            if ($level != $new_level) {
                $member_handle = new Member();
                $member_handle->updateMemberByLevel($level, array('level' => $new_level));
                unset($member_handle);
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function rate_edit($argument)
    {
        $sql = array();
        $sql[] = 'UPDATE uc_member_return SET name=?,bet_valid_amount_min=?';
        $sql[] = ',bet_valid_amount_max=?,rate=? WHERE level_id=?';

        if ($this->_db->query(implode('', $sql), $argument)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getRateId($valid)
    {
        $sql = 'SELECT rate FROM uc_member_return WHERE ? >= bet_valid_amount_min  AND  ? <= bet_valid_amount_max  LIMIT 1';
        $result = $this->_db->query($sql, array($valid, $valid));
        if (!$result) {
            return 0;
        } else {
            return $result[0]['rate'];
        }
    }

    /**
     * [delete  刪除會員層級]
     *
     * @param int $level_id
     * @param string $manager_account
     * @return void
     */
    public function delete($level_id, $manager_account = '')
    {
        $level_data = $this->get($level_id);
        if ($level_data && $level_data['level'] > 0) {
            $this->_db->delete('uc_member_level', $level_data);
            $sql = 'DELETE FROM uc_member_level WHERE level_id=? LIMIT 1';
            $result = $this->_db->query($sql, array($level_id));
            if ($result) {
                $member_handle = new Member();
                $member_handle->updateMemberByLevel($level_data['level'], array('level' => 0));
                unset($member_handle);

                if ($manager_account) {
                    $operate_log_handle = new OperateLog();
                    $parameters = array(
                        'manager_account' => $manager_account,
                        'account' => '',
                        'account_type' => 1,
                        'module' => 'member',
                        'argument' => [],
                        'detail' => '管理员删除会员层级|' . $level_data['name']
                    );
                    $operate_log_handle->add($parameters);
                    unset($operate_log_handle);
                }

                return 1;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    public function delete_rate($level_id)
    {
        $sql = 'DELETE FROM uc_member_return WHERE level_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($level_id));
        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getByLevel($level)
    {
        $sql = 'SELECT * FROM uc_member_level WHERE level=? LIMIT 1';
        $result = $this->_db->query($sql, array($level));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function getLevels()
    {
        $sql = 'SELECT * FROM uc_member_level ORDER BY level ASC';
        $result = $this->_db->query($sql);
        return $result;
    }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $return = array();

        $sql = 'SELECT level_id FROM uc_member_level WHERE level=0';
        $result = $this->_db->query($sql);
        if (!$result) {
            $sql = 'INSERT INTO uc_member_level SET level=0,name="未分层"';
            $this->_db->query($sql);
        }

        $sql = 'SELECT COUNT(*) AS count FROM uc_member_level WHERE 1';
        $result = $this->_db->query($sql);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        $return['total_page'] = ($return['total_page']) ? $return['total_page'] : 1;
        $return['page_size'] = $argument['page_size'];
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM uc_member_level WHERE 1';
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql);
        $return['list'] = $result;
        $return['argument'] = $argument;
        return $return;
    }

    public function map()
    {
        $sql = 'SELECT level,name FROM uc_member_level ORDER BY level';
        $result = $this->_db->query($sql);
        $map = array();
        if ($result) {
            foreach ($result as $row) {
                $map[$row['level']] = $row['name'];
            }
        }
        return $map;
    }

    public function select($name, $default = 0, $style = '')
    {
        $sql = 'SELECT level,name FROM uc_member_level ORDER BY level';
        $result = $this->_db->query($sql);
        $content = array();
        if ($style) {
            $style = ' class="' . $style . '"';
        }
        $content[] = '<select id="' . $name . '" name="' . $name . '"' . $style . '>';
//        if (-1 == $default) {
        $content[] = '<option value="-1">全部</option>';
//        }
        if ($result) {
            foreach ($result as $row) {
                $content[] = '<option value="' . $row['level'] . '"';
                if ($row['level'] == $default) {
                    $content[] = ' selected';
                }
                $content[] = '>' . $row['name'] . '</option>';
            }
        }
        $content[] = '</select>';
        return implode('', $content);
    }

    /**
     * [get_return_list 返水比例列表]
     *
     * @param [type] $argument
     * @return void
     */
    public function get_return_list($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();
        $return = array();

        $sql = 'SELECT COUNT(*) AS count FROM uc_member_return WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = 'SELECT * FROM uc_member_return WHERE 1';
        $sql .= implode('', $condition);
        $sql .= ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query($sql, $parameter);

        $return['list'] = $result;

        $return['argument'] = $argument;
        return $return;
    }
}
