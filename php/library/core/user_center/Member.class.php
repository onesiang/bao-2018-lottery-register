<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 会员类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Member extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * register
     * 会员注册
     *
     * @access public
     * @param array $argument 注册资料
     * <code>
     * -- agent 代理
     * -- account 账号
     * -- password 密码
     * -- password_repeat 重复密码
     * -- real_name 真实姓名
     * -- birthday 生日
     * -- withdrawal_code 取款密码
     * -- mobile 手机，可选
     * -- qq QQ号，可选
     * -- wx 微信号，可选
     * -- email 邮箱，可选
     * -- register_from 来源
     * </code>
     * @return array 注册结果，0-状态，1-uid
     * <code>
     * -- -12 会员已经存在
     * -- -11 验证码错误
     * -- -10 邮箱错误
     * -- -9 微信号错误
     * -- -8 QQ号错误
     * -- -7 手机号错误
     * -- -6 取款密码错误
     * -- -5 生日错误
     * -- -4 真实姓名错误
     * -- -3 两次密码不同
     * -- -2 密码不符合规范
     * -- -1 账号不符合规范
     * -- 1 注册成功
     * </code>
     */
    public function register($argument)
    {
        $config_handle = new Config();
        $config_fields = array(
            'MEMBER_LOGIN_VALIDATE',
            'DEFAULT_AGENT',
            'LOTTERY_REAL_TIME_RETURN_RATE',
            'LOTTERY_RETURN_TYPE',
            'REGISTER_LIMIT',
            'REGISTER_LEVEL',
        );
        $config = $config_handle->getByName($config_fields);
        unset($config_handle);

        $checkAuth = $this->checkAuthcode($config['MEMBER_LOGIN_VALIDATE'], $argument['auth']);

        if (!$checkAuth) {
            return array(-11, '验证码错误，请重新输入');
        }

        $ip = get_client_ip();
        $argument['register_ip'] = $ip;

        $sql = 'SELECT account FROM uc_member WHERE account=? ';
        $result = $this->_db->query($sql, array($argument['account']));
        if ($result) {
            return array(-12, '亲，账号已经存在，请换过重试');
        }

        $sql = 'SELECT account FROM uc_member_inactive WHERE account=?';
        $result = $this->_db->query($sql, array($argument['account']));
        if ($result) {
            return array(-12, '亲，账号已经存在，请换过重试');
        }

        //验证有效性
        $register_limit = explode(',', $config['REGISTER_LIMIT']);
        $check_limit = $this->check_limit_condition($argument, $register_limit);
        if (is_array($check_limit)) {
            return $check_limit;
        }

        if (isset($argument['agent']) && $argument['agent']) {
            $agent_handle = new Agent();
            $agent_data = $agent_handle->getValidByAgentId($argument['agent'], 'account');
            $agent = ($agent_data) ? $agent_data['account'] : $config['DEFAULT_AGENT'];
        } else {
            $agent = $config['DEFAULT_AGENT'];
        }

        $level = $config['REGISTER_LEVEL']; // 默认等级
        $category = 1; // 默认类型：普通会员
        $status = 1; // 默认状态：启用

        $time = date('Y-m-d H:i:s');
        $uid = uniqid(time());
//        $time = et_to_bj_date($time);

        $member_code = $this->createMemberCode();
        $promote_code = $this->createPromoteCode();

        /**
         * 整合插入数据库的参数（关键数据加密）
         */
        $parameter = array(
            $argument['account'],
            kg_encrypt($argument['password']),
            $uid,
            $argument['real_name'] ? $argument['real_name'] : '',
            $argument['birthday'],
            $argument['withdrawal_code'] ? kg_encrypt($argument['withdrawal_code']) : '',
            $argument['mobile'] ? kg_encrypt($argument['mobile']) : '',
            $argument['qq'] ? kg_encrypt($argument['qq']) : '',
            $argument['wx'] ? kg_encrypt($argument['wx']) : '',
            $argument['email'] ? kg_encrypt($argument['email']) : '',
            $agent,
            $ip,
            $time,
            $ip,
            $time,
            $level,
            $category,
            $status,
            $time,
            $time,
            $argument['register_from'],
            $member_code,
            $promote_code
        );

        $this->_db->beginTransaction();

        $sql = array();
        $sql[] = 'INSERT INTO uc_member (account,password,uid,real_name,birthday';
        $sql[] = ',withdrawal_code,mobile,qq,wx,email,agent';
        $sql[] = ',register_ip,register_time,login_ip,login_time,level,category';
        $sql[] = ',status,active_time,password_time,register_from,member_code,promote_code) VALUES (?,?,?';
        $sql[] = ',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        $member_id = $this->_db->query(implode('', $sql), $parameter);

        if (!$member_id) {
            return array(-17, '会员注册失败，请重新申请！');
        }else{
            $_SESSION[SESSION_NAME . '_user_uid'] = $uid;
            $_SESSION[SESSION_NAME . '_user_id'] = $member_id;
            $_SESSION[SESSION_NAME . '_user_login_time'] = $time;
            $_SESSION[SESSION_NAME . '_user_account'] = $argument['account'];
            $_SESSION[SESSION_NAME . '_user_amount'] = 0;
            $_SESSION[SESSION_NAME . '_user_level'] = $config['REGISTER_LEVEL'];
        }

        $sql = 'INSERT INTO uc_member_config (
            member_id,
            lottery_return_type,
            lottery_return_rate
        ) VALUES (?,?,?)';
        $parameter = array(
            $member_id,
            $config['LOTTERY_RETURN_TYPE'],
            $config['LOTTERY_REAL_TIME_RETURN_RATE'],
        );
        $result = $this->_db->query($sql, $parameter);

        if($argument['promote_code'] != ''){
            $promote_agent = $this->upateAgentMember(intval($member_id),intval($argument['promote_code']));
            if(!$promote_agent){
                $result = false;
            }
        }

        if($result) {
            $this->_db->commit();
        }
        else {
            $this->_db->rollBack();
        }

        return ($result) ? array(1, $uid, $member_code) : array(-18, '会员注册失败，请联系在线客服');
    }

// 測試結束需搬移此批fucntion  結束於355行
    /**
     * updateAgentMember
     * 註冊為會員下線
     *
     * @access private
     * @param $member_id[用戶id]
     * @param $agent_code[用戶上級邀請碼]
     * @return boolean
     */
    private function upateAgentMember($member_id,$agent_code)
    {
        try{
            $agentId = $this->getAgentId($agent_code);
            if(!$agentId){
                throw new Exception('取得上線邀請碼失敗！');
            }

            $findAgent = $this->findAgent($agentId);
            if($findAgent != ''){
                $updateFirstFloor = $this->updateFirstFloor($member_id, $agentId);
            }else{
                $updateFirstFloor = $this->insertFirstFloor($member_id, $agentId);
            }
            if(!$updateFirstFloor){
                throw new Exception('更新下線失敗！！');
            }

            $upperAgentId = $this->findUpperAgent($agentId);
            if($upperAgentId != '') {
                $updateUpperFloor = $this->updateUpperFloor($member_id, $agentId, $upperAgentId);

                if(!$updateUpperFloor){
                    throw new Exception('更新上線的上線失敗！！');
                }
            }
            return true;
        }catch (Exception $e){
            Log::record('updateAgentMember => '.$e, 'error', 'loginErr/');
            return false;
        }
    }

    /**
     * getAgentId
     * 取得此次推廣會員的member_id
     *
     * @param $agent_code
     * @return int
     */
    private function getAgentId($agent_code)
    {
        $sql = 'SELECT member_id FROM uc_member WHERE promote_code = ? LIMIT 1';
        $data = $this->_db->query($sql, array($agent_code));

        $member_id = 0;
        if(!empty($data)){
            $member_id = $data[0]['member_id'];
        }
        return $member_id;
    }

    /**
     * findAgent
     * 查詢上線是下線是否有人
     *
     * @param $agentId
     * @return int
     */
    private function findAgent($agentId)
    {
        $sql = 'SELECT first_floor_id FROM uc_member_relation WHERE self_floor_id = ? GROUP BY first_floor_id';
        $data = $this->_db->query($sql, array($agentId));

        $up_member = '';
        if(!empty($data)){
            $up_member = $data[0]['first_floor'];
        }
        return $up_member;
    }

    /**
     * findUpperAgent
     * 查詢此用戶上線是否還有上線
     *
     * @param $agentId
     * @return int
     */
    private function findUpperAgent($agentId)
    {
        $sql = 'SELECT self_floor_id FROM uc_member_relation WHERE first_floor_id = ? GROUP BY self_floor_id';
        $data = $this->_db->query($sql, array($agentId));

        $upper_member = false;
        if(!empty($data)){
            $upper_member = $data[0]['self_floor_id'];
        }
        return $upper_member;
    }

    /**
     * updateFirstFloor
     * 若此用戶為一線的第一位下線，更新此結果
     *
     * @param $member_id,$agentId
     * @return int
     */
    private function updateFirstFloor($member_id,$agentId)
    {
        $sql = 'UPDATE uc_member_relation SET first_floor_id = ? WHERE self_floor_id = ?';
        return $this->_db->query($sql, array($member_id, $agentId));
    }

    /**
     * insertFirstFloor
     * 若此用戶不為一線的第一位下線，插入此結果
     *
     * @param $member_id,$agentId
     * @return int
     */
    private function insertFirstFloor($member_id,$agentId)
    {
        $sql = 'INSERT INTO uc_member_relation (self_floor_id,first_floor_id) VALUES (?,?)';
        return $this->_db->query($sql, array($agentId, $member_id));
    }

    /**
     * updateUpperFloor
     * 更新上線的上線的資料
     *
     * @param $member_id,$agentId
     * @return int
     */
    private function updateUpperFloor($member_id, $agentId, $upperAgentId)
    {
        $sql = 'UPDATE uc_member_relation SET second_floor_id = ? WHERE first_floor_id = ? AND self_floor_id = ?';
        return $this->_db->query($sql, array($member_id, $agentId, $upperAgentId));
    }
// 測試結束需搬移此批fucntion 開始於197行

    private function checkAuthcode($memberLoginValidate, $authCode)
    {
        if ($memberLoginValidate) {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            $authCode = strtolower($authCode);
            if (!isset($_SESSION['auth_code']) || $authCode !== $_SESSION['auth_code']) {
                return false;
            }
        }
        return 1;
        exit;
    }

    /**
     * check_limit_condition
     * 检测条件限制
     *
     * @access private
     * @param array $argument
     * @param array $register_limit [0,0,0,0] 同名限制,同IP限制,同手机限制,同邮箱限制
     * @return int -12:同账号  -13:同名  -14:同IP -15:同手机 -16:同邮箱(当前没有限制邮箱必填 暂时不判断邮箱)
     */
    private function check_limit_condition($argument, $register_limit)
    {
        if ($register_limit[0] && $argument['real_name']) {
            $real_name = $argument['real_name'];
            $sql = 'SELECT real_name FROM uc_member WHERE real_name=?';
            $result = $this->_db->query($sql, array($real_name));
            if ($result) {
                return array(-13, '亲，该姓名已经被注册咯！');
            }
        }
        if ($register_limit[1] && $argument['register_ip']) {
            $sql = 'SELECT register_ip FROM uc_member WHERE register_ip=?';
            $result = $this->_db->query($sql, array($argument['register_ip']));
            if ($result) {
                return array(-14, '亲，同一IP只能注册一个账号！');
            }
        }
        if ($register_limit[2] && $argument['mobile']) {
            $mobile = kg_encrypt($argument['mobile']);
            $sql = 'SELECT mobile FROM uc_member WHERE mobile=?';
            $result = $this->_db->query($sql, array($mobile));
            if ($result) {
                return array(-15, '亲，同一手机只能注册一个账号！');
            }
        }
        return 1;
    }

    private function check_inactive_login($account, $password)
    {
        $sql = 'SELECT * FROM uc_member_inactive';
        $sql .= ' WHERE binary account=? AND password=? LIMIT 1';
        $result = $this->_db->query($sql, array($account, kg_encrypt($password)));
        if ($result) {
            $inactive_id = $result[0]['inactive_id'];
            $data = explode(';', $result[0]['content']);
            $status = 1;
            $this->_db->beginTransaction();
            foreach ($data as $value) {
                $result = $this->_db->query($value, array());
                if (!$result) {
                    $status = 0;
                    break;
                }
            }
            if (1 == $status) {
                $sql = 'DELETE FROM uc_member_inactive WHERE inactive_id=? LIMIT 1';
                $status = $this->_db->query($sql, array($inactive_id));
            }

            if ($status) {
                $this->_db->commit();
                $sql = 'SELECT member_id,login_time,password_time,amount,status FROM uc_member';
                $sql .= ' WHERE binary account=? AND password=? LIMIT 1';
                $result = $this->_db->query($sql, array($account, kg_encrypt($password)));
            } else {
                $this->_db->rollBack();
            }
        }
        return $result;
    }

    /**
     * login
     * 登陆
     *
     * @access public
     * @param string $account 用户名
     * @param string $password 密码
     * @param string $code 验证码
     * @param string $browser 登录设备
     * @return array
     */
    public function login($account, $password, $code = null, $browser = 'pc')
    {
        $config_handle = new Config();
        $config = $config_handle->getByName(array('MEMBER_LOGIN_VALIDATE', 'CHANGE_PASSWORD_DAY'));
        unset($config_handle);

        //验证码校验
        $checkAuth = $this->checkAuthcode($config['MEMBER_LOGIN_VALIDATE'], $code);
        if (!$checkAuth) {
            return array(-1, 'authcode');
        }

        $sql = 'SELECT * FROM uc_member WHERE account=? AND password=? LIMIT 1';
        $result = $this->_db->query($sql, array($account, kg_encrypt($password)));

        if (!$result) {
            return array(-2, 'mismatch');
            // $result = $this->check_inactive_login($account, $password);
            // if (!$result) {
            //     $return = array(-2, '');
            //     return $return;
            // }
        }
        $member = $result[0];
        $redis_member = $member; //將在線人數資訊寫入redis
        //1 => '启用',2 => '暂停', 3 => '停用',4 => '待定',
        if (2 === $member['status']) {
            return array(-3, 'stop');
        }

        if (3 === $member['status']) {
            return array(-4, 'ban');
        }

        $ip = get_client_ip();
        $time = date('Y-m-d H:i:s');
        $uid = uniqid(time());

        $redis_member['active_time'] = $time;

        $sql = 'UPDATE uc_member
            SET
                uid=?,
                login_ip=?,
                login_time=?,
                active_time=?,
                browser=?,
                online=1,
                total_login_count = total_login_count + 1
            WHERE member_id=?
            LIMIT 1';

        $param = array(
            $uid,
            $ip,
            $time,
            $time,
            $browser,
            $member['member_id'],
        );

        $login = $this->_db->query($sql, $param);

        if (!$login) {
            Log::record("MemberId:{$member['member_id']}, Account:{$member['account']}", 'error', 'loginErr/');
            Log::record(json_encode($param), 'error', 'loginErr/');
            return array(-5, 'unconfirm');
        }

        $_SESSION[SESSION_NAME . '_user_uid'] = $uid;
        $_SESSION[SESSION_NAME . '_user_id'] = $member['member_id'];
        $_SESSION[SESSION_NAME . '_user_login_time'] = $time;
        $_SESSION[SESSION_NAME . '_user_account'] = $account;
        $_SESSION[SESSION_NAME . '_user_amount'] = $member['amount'];
        $_SESSION[SESSION_NAME . '_user_level'] = $member['level'];

        $sql = 'INSERT INTO mdl_syslogin_log SET user_id=?,account=?,login_ip=?,login_time=?,login_url=?,type=?,browser=?';
        $login_host = $_SERVER['HTTP_HOST'];
        $login_host = $login_host ? $login_host : '';
        $params = array(
            $member['member_id'],
            $account,
            $ip,
            $time,
            $login_host,
            0,
            $browser,
        );
        $this->_db->query($sql, $params);

        // 强制修改密码判断
        $change_password = 0;
        if ($config['CHANGE_PASSWORD_DAY']) {
            $datediff = floor((time() - strtotime($member['password_time'])) / 86400);
            if ($datediff >= $config['CHANGE_PASSWORD_DAY']) {
                $change_password = 1;
            }
        }

        $return = array(1, $uid, $change_password, $member['member_code']);

        $this->setOnlineMemberInfoToRedis($redis_member);

        return $return;
    }

    /**
     * logout
     * 登出
     *
     * @access public
     * @param string $uid uid
     * @return bool
     */
    public function logout($uid)
    {
        $time = date('Y-m-d H:i:s');
        $sql = 'UPDATE uc_member SET uid=?, online=0, casino_online=1 WHERE uid=? LIMIT 1';
        return $this->_db->query($sql, array($time, $uid));
    }

    public function setBankCardAtFirstTime($argument)
    {
        $config_handle = new Config();
        $config = $config_handle->get_cache_by_name(array('REGISTER_LIMIT'));
        unset($config_handle);

        $register_limit = explode(',', $config['REGISTER_LIMIT']);
        if ($register_limit[0] || $register_limit[2]) {
            $check_limit = $this->check_limit_condition($register_limit, $argument);
            if (is_array($check_limit)) {
                return $check_limit;
            }
        }

        $memberIdForEditProfile = $this->checkDataisValid($argument);
        if (is_array($memberIdForEditProfile)) {
            return $memberIdForEditProfile;
        }

        $checkIsBankCardExists = $this->isBankCardExists($argument['bank_number']);
        if ($checkIsBankCardExists) {
            return array(-100, ' 亲！！不能输入同样的銀行卡号哦!');
        }

        $sql = 'UPDATE uc_member SET
                    real_name = ?,
                    mobile = ?,
                    withdrawal_code = ?
                WHERE member_id = ?';
        $updateMemberInfo = $this->_db->query($sql, array(
            $argument['real_name'],
            kg_encrypt($argument['mobile']),
            kg_encrypt($argument['withdrawal_code']),
            $memberIdForEditProfile,
        ));

        $reultForAddBankCard = $this->addBankcard(
            array(
                $argument['bank_name'],
                kg_encrypt($argument['bank_account_name']),
                kg_encrypt($argument['bank_number']),
            ),
            $memberIdForEditProfile
        );
        if ($reultForAddBankCard) {
            return array(1, '资料完善成功！');
        }
    }

    /**
     * [isBankCardExists 銀行卡是否重複]
     *
     * @param [type] $bankcode
     * @return boolean
     */
    public function isBankCardExists($bankcode)
    {
        $sql = 'SELECT * FROM uc_member_bankcard where bank_number=?';
        $result = $this->_db->query($sql, array(kg_encrypt($bankcode)));
        return ($result) ? $result : false;
    }

    public function getBankcard($member_id)
    {
        $sql = 'SELECT * FROM uc_member_bankcard WHERE member_id = ?';
        $result = $this->_db->query($sql, array($member_id));
        if (!$result) {
            return array();
        } else {
            return $result;
        }
    }

    public function getBankCardById($memberId)
    {
        $sql = 'SELECT * FROM uc_member_bankcard where member_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($memberId));
        return ($result) ? $result : false;
    }

    public function checkBankCardId($memberid, $bankid)
    {
        $sql = ' SELECT id FROM uc_member_bankcard WHERE member_id = ?  AND id = ?; ';
        $result = $this->_db->query($sql, array($memberid, $bankid));
        return ($result) ? $result[0] : [];
    }

    /**
     * [addBankcard 新增銀行卡]
     *
     * @param array $argument
     * @return void
     */
    public function addBankcard($argument, $member_id)
    {
        $sql = 'INSERT uc_member_bankcard SET
            bank_name = ?,
            bank_account_name = ?,
            bank_number = ?,
            member_id = ?';
        $argument[] = $member_id;
        return $this->_db->query($sql, $argument);
    }

    public function checkDataisValid($argument)
    {
        $member = $this->getByUid($argument['uid']);
        if (!$member) {
            return array(-1, '亲,请先登录！!');
        }

        if ($member['password'] == !kg_encrypt($argument['password_old'])) {
            return array(-2, '亲,登入密码错误！!');
        }

        if (!$argument['bank_name']) {
            return array(-3, '亲,请选择银行!');
        }

        if (!preg_match('/^[0-9]{16,19}$/i', $argument['bank_number'])) {
            return array(-5, '亲,请输入正确的银行卡号!');
        }

        if (!preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $argument['real_name'])) {
            return array(-44, '亲,姓名只能为汉字!');
        }

        if (!preg_match('/^[0-9]{4}$/i', $argument['withdrawal_code'])) {
            return array(-9, '亲,请输入正确的银行密碼!');
        }

        if (!preg_match('/^[0-9]{11}$/i', $argument['mobile'])) {
            return array(-10, '亲,请输入正确的手机号码!');
        }
        return $member['member_id'];
    }

    public function updateActiveTime($account)
    {
        $time = date('Y-m-d H:i:s');
        $sql = 'UPDATE uc_member SET active_time=? WHERE account=? LIMIT 1';
        return $this->_db->query($sql, array($time, $account));
    }

    /**
     * checkLogin
     * 验证会员是否登陆
     *
     * @access public
     * @param string $uid UID
     * @return bool 已经登陆返回用户信息，反之返回FALSE
     */
    public function checkLogin($uid, $fields = '*')
    {
        $member = $this->getByUid($uid, $fields);
        if (!$member || 1 != $member['status']) {
            return false;
        }
        $member['amount_display'] = money_from_db($member['amount']);
        return $member;
    }

    /**
     * getByUid
     * 获取会员资料
     *
     * @access public
     * @param string $uid uid
     * @param string $column 资料列名
     * @return array
     */
    public function getByUid($uid, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM uc_member WHERE uid=? LIMIT 1';
        $result = $this->_db->query($sql, array($uid));
        if (!$result) {
            return array();
        } else {
            return $result[0];
        }
    }

    /**
     * getByAccount
     * 获取会员资料
     *
     * @access public
     * @param string $account 账号
     * @param string $column 资料列名
     * @return array
     */
    public function getByAccount($account, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM uc_member WHERE account=? LIMIT 1';
        $result = $this->_db->query($sql, array($account));
        if (!$result) {
            return array();
        } else {
            return $result[0];
        }
    }

    /**
     * getByMemberId
     * 获取会员资料
     *
     * @access public
     * @param int $member_id 编号
     * @param string $column 资料列名
     * @return array
     */
    public function getByMemberId($member_id, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM uc_member WHERE member_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($member_id));
        if (!$result) {
            return array();
        } else {
            return $result[0];
        }
    }

    /**
     * editPassword
     * 修改密码
     *
     * @access public
     * @param array $argument 参数
     * @return int
     */
    public function editPassword($argument)
    {
        $member = $this->getByUid($argument['uid']);
        if (!$member) {
            return -1; // 会员不存在
        }
        if ($argument['password_old'] == $argument['password_new']) {
            return -2; // 新旧密码相同
        }
        if ($argument['password_new'] != $argument['password_confirm']) {
            return -3; // 两次新密码不同
        }
        if (!preg_match('/^[A-Za-z0-9]{6,20}$/', $argument['password_new'])) {
            return -4; // 新密码不符合规则
        }
        if ($member['password'] != kg_encrypt($argument['password_old'])) {
            return -5; // 旧密码错误
        }
        $parameter = array(
            'password' => kg_encrypt($argument['password_new']),
            'password_time' => date('Y-m-d H:i:s'),
        );
        if ($this->edit($member['member_id'], $parameter)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function editWithdrawalPassword($argument)
    {
        $member = $this->getByUid($argument['uid']);
        if (!$member) {
            return -1; // 会员不存在
        }
        if (!preg_match('/^[0-9]{4}$/', $argument['withdrawal_code'])) {
            return -4;
        }
        if ($member['withdrawal_code'] != kg_encrypt($argument['password_old'])) {
            return -5; // 旧密码错误
        }
        if ($member['withdrawal_code'] == kg_encrypt($argument['withdrawal_code'])) {
            return -2; // 新旧密码相同
        }
        $parameter = array(
            'withdrawal_code' => kg_encrypt($argument['withdrawal_code']),
        );
        if ($this->edit($member['member_id'], $parameter)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function memberBetReportByMember($parameters)
    {
        $lottery_bet = 0;
        $lottery_valid = 0;
        $lottery_win = 0;

        $sql = 'SELECT SUM(valid_amount) AS valid_amount, SUM(bet_amount) AS bet_amount, SUM(win_amount) AS win_amount FROM ltt_bet WHERE member_id=? AND bet_time>? AND bet_time<=? AND status=2 AND parlay=0';
        $result = $this->_db->query($sql, array($member_id, $l_date_start, $l_date_stop));
        if ($result && $result[0]['bet_amount']) {
            $lottery_bet = money_from_db($result[0]['bet_amount']);
            $lottery_valid = money_from_db($result[0]['valid_amount']);
            $lottery_win = money_from_db($result[0]['win_amount']);
        }

        $data = array(
            'lottery_bet' => $lottery_bet,
            'lottery_valid' => $lottery_valid,
            'lottery_win' => $lottery_win,
            'win_total' => ceil($lottery_win),
            'valid_bet_total' => ceil($lottery_valid),
        );
        return $data;
    }

    public function getDml($member_id)
    {
        $limit = array();
        $sql = 'SELECT check_bet_amount FROM uc_member_config WHERE member_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($member_id));
        if ($result) {
            $limit = $result[0];
        }
        return $limit;
    }

    /**
     * edit
     * 更新会员资料
     *
     * @access private
     * @param int $member_id 会员编号
     * @param array $data 数据
     * <code>
     * $data:
     * -- column1 value1
     * -- column2 value2
     * ...
     * </code>
     * @return int
     * <code>
     * -- -1 会员不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    private function edit($member_id, $data)
    {
        $member = $this->getByMemberId($member_id);
        if (!$member) {
            return -1;
        }
        $sql = 'UPDATE uc_member SET ';
        $parameter = array();
        foreach ($data as $key => $value) {
            $sql .= $key . '=?,';
            $parameter[] = $value;
        }
        $sql = trim($sql, ',');
        $sql .= ' WHERE member_id=?';
        $parameter[] = $member_id;
        return $this->_db->query($sql, $parameter);
    }

    // public function checkData($uid, $name, $bankcode)
    // {
    //     $member = $this->getByUid($uid, 'member_id');
    //     if (!$member) {
    //         return -1; // 用户未登陆
    //     }
    //     $sql = 'SELECT id FROM uc_member_bankcard WHERE member_id = ? AND bank_account_name = ? ';
    //     $result = $this->_db->query($sql, array($member['member_id'], $name));
    //
    //     if ($result) {
    //         $sql = ' SELECT id FROM uc_member_bankcard WHERE member_id = ?  AND bank_number = ?; ';
    //         $result = $this->_db->query($sql, array($member['member_id'],  kg_encrypt($bankcode)));
    //         if ($result) {
    //             return -99;
    //         } else {
    //             return 1;
    //         }
    //     } else {
    //         return -100;
    //     }
    // }

    // public function check_same_name($name, $mobile, $limit_name, $limit_mobile)
    // {
    //     $sql = 'SELECT `member_id` FROM `uc_member` WHERE  ';
    //     if ($limit_name) {
    //         $sql .= '`real_name` = ?';
    //         $check_name =  $this->_db->query($sql, array(kg_encrypt($name)));
    //         if ($check_name) {
    //             return -99 ;
    //         }
    //     }
    //
    //     $sql = 'SELECT `member_id` FROM `uc_member` WHERE  ';
    //     if ($limit_mobile) {
    //         $sql .= '`mobile` = ?';
    //         $check_mobile =  $this->_db->query($sql, array(kg_encrypt($mobile)));
    //         if ($check_mobile) {
    //             return -98 ;
    //         }
    //     }
    //     return 1;
    // }

    /**
     * [EditBankcard 編輯會員銀行卡]
     *
     * @param [array] $argument
     * @return void
     */
    // public function EditBankcard($argument)
    // {
    //     /*[uid] => 15160233085a5cae0c95de7
    //     [bank_name] => 南京银行
    //     [bank_number] => 4545454
    //     [mobile] => 54
    //     [bank_account_name] => 5454
    //     [id] => 21*/
    //     $parameter = array(
    //         $argument['bank_account_name'],
    //         $argument['bank_number'],
    //         $argument['mobile'],
    //         $argument['id'],
    //     );
    //
    //     $sql_parameter = array();
    //     $condition = '';
    //
    //     if ($argument['bank_account_name'] && isset($argument['bank_account_name'])) {
    //         $condition .= ' bank_account_name = ? ,';
    //         $sql_parameter[] = $argument['bank_account_name'];
    //     }
    //
    //     if ($argument['bank_number'] && isset($argument['bank_number'])) {
    //         $sql_parameter[] = kg_encrypt($argument['bank_number']);
    //         $condition .= ' bank_number = ? ,';
    //     }
    //
    //     if ($argument['mobile'] && isset($argument['mobile'])) {
    //         $sql_parameter[] = $argument['mobile'];
    //         $condition .= ' mobile = ?  ';
    //     }
    //
    //     $sql_parameter[] = $argument['id'];
    //
    //     $condition = trim($condition, ',');
    //
    //     $sql = 'UPDATE uc_member_bankcard SET ' . $condition . ' WHERE id = ?';
    //
    //     if ($this->_db->query($sql, $sql_parameter)) {
    //         return 1;
    //     }
    //
    //     return 0;
    //
    //     // $sql = 'UPDATE uc_member_bankcard SET bank_account_name = ? , bank_number = ?, mobile=? WHERE id = ?';
    //     // return $this->_db->query($sql, $parameter);
    // }

    /**
     * getMemberAmount
     * 获取会员额度
     *
     * @access public
     * @param int $member_id 会员编号
     * @return array 会员额度
     * <code>
     * -- amount 系统额度
     * </code>
     */
    // public function getMemberAmount($member_id)
    // {
    //     $member_amount = array();
    //     $member = $this->getByMemberId($member_id);
    //     if (!$member) {
    //         return $member_amount;
    //     }
    //     $member_amount = array(
    //         'amount' => money_from_db($member['amount']),
    //     );
    //     return $member_amount;
    // }

    /**
     * checkPasswordisdefault
     * 驗證會員密碼是否為預設的
     *
     * @access public
     * @param string $account 帳號
     * @return bool 如果為預設密碼就是true，反之返回FALSE
     */
    // public function checkPasswordDefaultbyAccount($account)
    // {
    //     $sql = 'SELECT `password` FROM uc_member WHERE  account=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($account));
    //     if (!$result) {
    //         return false;
    //     } else {
    //         if (kg_decrypt($result[0]['password']) == '888888') {
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     }
    //     return false;
    // }

    /**
     * exists
     * 检测会员是否存在
     *
     * @access public
     * @param string $account 账号
     * @return bool 会员存在返回TRUE，不存在返回FALSE
     */
    // public function exists($account)
    // {
    //     $sql = 'SELECT member_id FROM uc_member WHERE account=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($account));
    //     if ($result) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    /**
     * updateLevel
     * 更新用户等级
     *
     * @access public
     * @param string $account 关键字
     * @param int $level 新的等级，默认提升1级
     * @return int
     * <code>
     * -- -1 会员不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    // public function updateLevel($account, $new_level = 0)
    // {
    //     $flag = false;
    //     $member = $this->getByAccount($account);
    //     if (!$member) {
    //         return -1; // 会员不存在
    //     }
    //     $new_level = $new_level ? $new_level : 0;
    //     $sql = 'UPDATE uc_member SET level=? WHERE member_id=? LIMIT 1';
    //     $flag = $this->_db->query($sql, array($new_level, $member['member_id']));
    //     return $flag;
    // }
    //
    // public function updateLevelByMemberId($member_id, $new_level = 0)
    // {
    //     $new_level = $new_level ? $new_level : 0;
    //     $sql = 'UPDATE uc_member SET level=? WHERE member_id=? LIMIT 1';
    //     $flag = $this->_db->query($sql, array($new_level, $member_id));
    //     return $flag;
    // }

    /**
     * 更改会员状态
     *
     */
    // public function updateMemberStatus($status, $member_id)
    // {
    //     $sql = 'UPDATE uc_member SET status=? WHERE member_id=?';
    //     $result = $this->_db->query($sql, array($status, $member_id));
    //     return $result;
    // }

    /**
     * 更改会员类型
     *
     */
    // public function updateMemberCategory($category, $member_id)
    // {
    //     $sql = 'UPDATE uc_member SET category=? WHERE member_id=?';
    //     $result = $this->_db->query($sql, array($category, $member_id));
    //     return $result;
    // }

    /**
     * getBetLimit
     * 获取投注限额
     *
     * @access public
     * @param int $member_id 会员编号
     * @return array
     */
    // public function getBetLimit($member_id)
    // {
    //     $limit = null;
    //     $member = $this->getByMemberId($member_id);
    //     if (!$member) {
    //         return array(); // 会员不存在
    //     }
    //     $sql = 'SELECT * FROM uc_member_config WHERE member_id=? LIMIT 1';
    //     $result = $this->_db->query($sql, array($member['member_id']));
    //     if ($result) {
    //         $limit = $result[0];
    //     }
    //     return $limit;
    // }

    /**
     * updateAmount
     * 更新额度
     *
     * @access public
     * @param int $member_id 会员编号
     * @param int $amount 金额
     * @param bool $force 是否强制更新
     * @return int
     * <code>
     * -- -2 更新后额度为负数
     * -- -1 会员不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    public function updateAmount($member_id, $amount, $force = false)
    {
        $member = $this->getByMemberId($member_id);
        if (!$member) {
            return 0;
        }

        $where = '';
        if (!$force) {
            if ($amount < 0) {
                $diff_amount = abs($amount);
                $where = ' AND amount >= ' . $diff_amount;
            }
            if ($amount < 0 && $member['amount'] + $amount < 0) {
                return 0; // 更新后额度为负数
            }
        }

        $sql = 'UPDATE uc_member SET amount=amount+? WHERE member_id=? ' . $where . ' LIMIT 1';
        if ($this->_db->query($sql, array($amount, $member_id))) {
            $amount_display = money_from_db($member['amount'] + $amount);

            //$url = 'http://' . DATA_HOST . ':' . DATA_PORT . '/?action=money';
            //$url .= '\&account=' . $member['account'] . '\&module=amount\&amount=' . $amount_display;
            //pclose(popen('wget ' . $url . ' -O /dev/null -o /dev/null &', 'r'));
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 更新额度及有效投注
     *
     * 用于需要更新有效投注的操作
     *
     * @access public
     * @param int $member_id 会员编号
     * @param int $amount 额度
     * @param int $valid_amount 有效投注
     * @param bool $force 是否强制更新
     * @return bool
     */
    public function updateValidAmount($member_id, $amount, $valid_amount, $force = false)
    {
        return $this->updateAmount($member_id, $amount, $force);
    }

    /**
     * updateDeposit
     * 更新存款资料
     *
     * @access public
     * @param int $member_id 会员编号
     * @param int $amount 金额
     * @return int
     * <code>
     * -- -1 会员不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    public function updateDeposit($member_id, $amount)
    {
        $member = $this->getByMemberId($member_id);
        if (!$member) {
            return -1; // 会员不存在
        }
        $member['deposit_amount'] += $amount;
        $member['deposit_count']++;
        $parameter = array(
            'deposit_amount' => $member['deposit_amount'],
            'deposit_count' => $member['deposit_count'],
        );
        return $this->edit($member['member_id'], $parameter);
    }

    /**
     * updateWithdrawal
     * 更新取款资料
     *
     * @access public
     * @param int $member_id 会员编号
     * @param int $amount 金额
     * @return int
     * -- -1 会员不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    public function updateWithdrawal($member_id, $amount)
    {
        $member = $this->getByMemberId($member_id);
        if (!$member) {
            return -1; // 会员不存在
        }
        $member['withdrawal_amount'] += $amount;
        $member['withdrawal_count']++;
        $parameter = array(
            'withdrawal_amount' => $member['withdrawal_amount'],
            'withdrawal_count' => $member['withdrawal_count'],
        );
        return $this->edit($member['member_id'], $parameter);
    }

    // public function updateMemberByLevel($level, $argument = array())
    // {
    //     $sql = 'UPDATE uc_member SET ';
    //     $params = array();
    //     $set = array();
    //     foreach ($argument as $key => $value) {
    //         $set = $key . '=?,';
    //         $params[] = $value;
    //     }
    //     $set = trim($set, ',');
    //     $sql .= $set . ' WHERE level=?';
    //     $params[] = $level;
    //     $result = $this->_db->query($sql, $params);
    //     return $result;
    // }

    /**
     * updateLastAction
     * 更新会员最后动作
     *
     * @access public
     * @param string $uid UID
     * @param string $action 动作
     * @return bool
     */
    // public function updateLastAction($uid, $action)
    // {
    //     $member = $this->getByUid($uid);
    //     if (!$member) {
    //         return -1; // 会员不存在
    //     }
    //     $parameter = array(
    //         'action' => $action,
    //         'active_time' => date('Y-m-d H:i:s'),
    //     );
    //     return $this->edit($member['member_id'], $parameter);
    // }

    // public function getLastDepositTime($member_id)
    // {
    //     $sql = "SELECT deposit_confirm_time, remittance_confirm_time, adjust_submit_time , uc_member_config_modified_time FROM (
    //             SELECT confirm_time as deposit_confirm_time , member_id
    //             FROM fnn_deposit
    //             WHERE status=2 AND member_id= ?
    //             ORDER BY confirm_time DESC LIMIT 1
    //         )as a
    //         LEFT JOIN (
    //             SELECT confirm_time as remittance_confirm_time ,member_id
    //             FROM fnn_remittance
    //             WHERE status=2 AND member_id= ?
    //             ORDER BY confirm_time DESC LIMIT 1
    //         )as b ON b.member_id = a.member_id
    //         LEFT JOIN (
    //             SELECT submit_time as adjust_submit_time , member_id
    //             FROM fnn_adjust
    //             WHERE member_id= ? AND type=13 AND check_amount !=0
    //             ORDER BY submit_time DESC LIMIT 1
    //         )as c ON c.member_id = a.member_id
    //         LEFT JOIN (
    //             SELECT modified_time as uc_member_config_modified_time ,member_id
    //             FROM uc_member_config
    //             WHERE member_id=250
    //         ) as d ON d.member_id = a.member_id;";
    //     $result = $this->_db->query($sql, array($member_id,$member_id,$member_id));
    //     $last_depost_time = 0;
    //     if (count($result) > 0) {
    //         $result = $result[0];
    //         $last_depost_time = ($result['deposit_confirm_time'])?strtotime($result['deposit_confirm_time']):0;
    //         $last_remittance_time = ($result['remittance_confirm_time'])?strtotime($result['remittance_confirm_time']):0;
    //         $last_adjust_time = ($result['adjust_submit_time'])?strtotime($result['adjust_submit_time']):0;
    //         $last_uc_member_config_time = ($result['uc_member_config_modified_time'])?strtotime($result['uc_member_config_modified_time']):0;
    //
    //         if ($last_remittance_time > $last_depost_time) {
    //             $last_depost_time = $last_remittance_time;
    //         }
    //         if ($last_adjust_time > $last_depost_time) {
    //             $last_depost_time = $last_adjust_time;
    //         }
    //         if ($last_uc_member_config_time > $last_depost_time) {
    //             $last_depost_time = $last_uc_member_config_time;
    //         }
    //     }
    //     $last_depost_time = date('Y-m-d H:i:s', $last_depost_time);
    //     return $last_depost_time;
    //
    //     print_r($result);
    //     print_r($member_id);
    //     exit;
    //     $last_depost_time = 0;
    //     $sql = 'SELECT confirm_time FROM fnn_deposit WHERE status=2 AND member_id=? ORDER BY confirm_time DESC LIMIT 1';
    //     $result = $this->_db->query($sql, array($member_id));
    //     if ($result) {
    //         $last_depost_time = strtotime($result[0]['confirm_time']);
    //     }
    //
    //     $last_remittance_time = 0;
    //     $sql = 'SELECT confirm_time FROM fnn_remittance WHERE status=2 AND member_id=? ORDER BY confirm_time DESC LIMIT 1';
    //     $result = $this->_db->query($sql, array($member_id));
    //     if ($result) {
    //         $last_remittance_time = strtotime($result[0]['confirm_time']);
    //     }
    //     if ($last_remittance_time > $last_depost_time) {
    //         $last_depost_time = $last_remittance_time;
    //     }
    //
    //     $last_adjust_time = 0;
    //     // fnn_adjust里只有人工入款的类型才需要重算统计打码的时间点
    //     $sql = 'SELECT submit_time FROM fnn_adjust WHERE member_id=? AND type=13 AND check_amount !=0 ORDER BY submit_time DESC LIMIT 1';
    //     $result = $this->_db->query($sql, array($member_id));
    //     if ($result) {
    //         $last_adjust_time = strtotime($result[0]['submit_time']);
    //     }
    //     if ($last_adjust_time > $last_depost_time) {
    //         $last_depost_time = $last_adjust_time;
    //     }
    //
    //     $last_uc_member_config_time = 0;
    //     // uc_member_config裡的check_bet_amount被变更後,modified_time时间会变更
    //     $sql = 'SELECT modified_time FROM uc_member_config WHERE member_id=?';
    //     $result = $this->_db->query($sql, array($member_id));
    //     if ($result) {
    //         $last_uc_member_config_time = strtotime($result[0]['modified_time']);
    //     }
    //     if ($last_uc_member_config_time > $last_depost_time) {
    //         $last_depost_time = $last_uc_member_config_time;
    //     }
    //
    //     $last_depost_time = date('Y-m-d H:i:s', $last_depost_time);
    //     return $last_depost_time;
    // }

    // public function allMember($argument)
    // {
    //     $level = isset($argument['level']) ? $argument['level'] : -1;
    //     $agent = isset($argument['agent']) ? $argument['agent'] : '';
    //     $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
    //     $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
    //
    //     $where = '';
    //     $params = array();
    //     if ($level >= 0) {
    //         $where .= ' AND level=? ';
    //         $params[] = $level;
    //     }
    //     if ($agent) {
    //         $where .= ' AND agent=? ';
    //         $params[] = $agent;
    //     }
    //     if ($date_start) {
    //         $where .= ' AND register_time>? ';
    //         $params[] = $date_start;
    //     }
    //     if ($date_stop) {
    //         $where .= ' AND register_time<? ';
    //         $params[] = $date_stop;
    //     }
    //     $sql = 'SELECT * FROM uc_member WHERE 1 ' . $where;
    //     $result = $this->_db->query($sql, $params);
    //     return $result;
    // }

    /**
     * [memberList 會員列表]
     * @param  [type] $argument [description]
     * @return [array]           [description]
     */
    // public function memberList($argument)
    // {
    //     global $g_page_size;
    //     if (!isset($argument['page_size'])) {
    //         $argument['page_size'] = $g_page_size['general'];
    //     }
    //     // 1.準備搜尋條件
    //     $condition = array();
    //     $parameter = array();
    //     if (isset($argument['agent']) && $argument['agent']) {
    //         $condition[] = ' AND agent=?';
    //         $parameter[] = $argument['agent'];
    //     }
    //     if (isset($argument['status']) && $argument['status']) {
    //         $condition[] = ' AND status=?';
    //         $parameter[] = $argument['status'];
    //     }
    //     if (isset($argument['category']) && $argument['category']) {
    //         $condition[] = ' AND category=?';
    //         $parameter[] = $argument['category'];
    //     }
    //     if (isset($argument['online']) && -1 != $argument['online']) {
    //         $condition[] = ' AND online=?';
    //         if ($argument['online'] == "在线") {
    //             $argument['online'] = 1;
    //             $isFilterOnline = true; // 要顯示在線
    //         }
    //         $parameter[] = $argument['online'];
    //
    //         if ($isFilterOnline) { // 顯示在線人數還要判斷 活動時間
    //             $parameter[] = date('Y-m-d H:i:s', time() - 1200);
    //             $condition[] = ' AND active_time > ?   ';
    //         }
    //     }
    //     if (isset($argument['level']) && $argument['level'] >= 0) {
    //         $condition[] = ' AND level=?';
    //         $parameter[] = $argument['level'];
    //     }
    //
    //     $date_start  = isset($argument['date_start']) ? $argument['date_start'] : '';
    //     $date_stop   = isset($argument['date_stop']) ? $argument['date_stop'] : '';
    //     $time_column = isset($argument['time_column']) ? $argument['time_column'] : '';
    //     $unlogin_day = isset($argument['unlogin_day']) ? intval($argument['unlogin_day']) : 0;
    //     if ($date_start) {
    //         if ('register_time' == $time_column) {
    //             $condition[] = ' AND register_time>? ';
    //             $parameter[] = $date_start;
    //         } elseif ('login_time' == $time_column) {
    //             $condition[] = ' AND login_time>? ';
    //             $parameter[] = $date_start;
    //         }
    //     }
    //     if ($date_stop) {
    //         if ('register_time' == $time_column) {
    //             $condition[] = ' AND register_time<=? ';
    //             $parameter[] = $date_stop;
    //         } elseif ('login_time' == $time_column) {
    //             $condition[] = ' AND login_time<=? ';
    //             $parameter[] = $date_stop;
    //         }
    //     }
    //     if ($unlogin_day) {
    //         $unlogin_date = date('Y-m-d', time() - $unlogin_day * 86400);
    //         $condition[] = ' AND login_time<? ';
    //         $parameter[] = $unlogin_date;
    //     }
    //     if (isset($argument['column']) && $argument['column'] &&
    //         isset($argument['keyword']) && $argument['keyword']) {
    //         switch ($argument['column']) {
    //             case 'account':
    //                 $fuzzy = isset($argument['fuzzy']) ? $argument['fuzzy'] : 0;
    //                 if ($fuzzy) {
    //                     $condition[] = ' AND account LIKE ? ';
    //                     $parameter[] = '%' . $argument['keyword']. '%';
    //                 } else {
    //                     $condition[] = ' AND account =? ';
    //                     $parameter[] = $argument['keyword'];
    //                 }
    //                 break;
    //             case 'real_name':
    //                 $condition[] = ' AND real_name=?';
    //                 // $parameter[] = $this->encryptedColumnMatch('real_name', $argument['keyword']);
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'register_ip':
    //                 $condition[] = ' AND register_ip LIKE ?';
    //                 $parameter[] = $argument['keyword'] . '%';
    //                 break;
    //             case 'qq':
    //                 $condition[] = ' AND qq=?';
    //                 // $parameter[] = $this->encryptedColumnMatch('qq', $argument['keyword']);
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'mobile':
    //                 $condition[] = ' AND mobile=?';
    //                 // $parameter[] = $this->encryptedColumnMatch('mobile', $argument['keyword']);
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'email':
    //                 $condition[] = ' AND email=?';
    //                 // $parameter[] = $this->encryptedColumnMatch('email', $argument['keyword']);
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'bank_number':
    //                 $condition[] = ' AND bank_number=?';
    //                 $parameter[] = $argument['keyword'];
    //                 break;
    //         }
    //     }
    //     if (isset($argument['valuable'])) {
    //         if (0 == $argument['valuable']) {
    //             $condition[] = ' AND deposit_count=?';
    //             $parameter[] = $argument['valuable'];
    //         } elseif (1 == $argument['valuable']) {
    //             $condition[] = ' AND deposit_count>=?';
    //             $parameter[] = $argument['valuable'];
    //         }
    //     }
    //     // 排序
    //     $order_by = isset($argument['order_by']) ? $argument['order_by'] : 0;
    //     $order_by_sql = '';
    //     switch ($order_by) {
    //         case 1:
    //             $order_by_sql = ' ORDER BY register_time DESC ';
    //             break;
    //         case 2:
    //             $order_by_sql = ' ORDER BY register_time ASC ';
    //             break;
    //         case 3:
    //             $order_by_sql = ' ORDER BY amount DESC ';
    //             break;
    //         case 4:
    //             $order_by_sql = ' ORDER BY amount ASC ';
    //             break;
    //         case 5:
    //             $order_by_sql = ' ORDER BY ag_amount DESC ';
    //             break;
    //         case 6:
    //             $order_by_sql = ' ORDER BY ag_amount ASC ';
    //             break;
    //         case 7:
    //             $order_by_sql = ' ORDER BY lebo_amount DESC ';
    //             break;
    //         case 8:
    //             $order_by_sql = ' ORDER BY lebo_amount ASC ';
    //             break;
    //     }
    //
    //     $return = array();
    //     //2. 計算統計資訊
    //     // 2.1計算總數
    //     $sql = 'SELECT COUNT(member_id) AS count FROM uc_member WHERE 1';
    //     $sql .= implode('', $condition);
    //     $result = $this->_db->query($sql, $parameter);
    //     $return['total_record'] = $result[0]['count'];
    //     //3.計算頁數
    //     $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
    //     if ($argument['page'] > $return['total_page']) {
    //         $argument['page'] = $return['total_page'];
    //     }
    //     if ($argument['page'] < 1) {
    //         $argument['page'] = 1;
    //     }
    //     $return['page'] = $argument['page'];
    //
    //     $total_data = array(
    //         'member_total' => 0,
    //         'amount'       => 0
    //     );
    //     // 2.2 計算會員總金額（系統額度）
    //     $sql = 'SELECT COUNT(member_id) AS member_total,SUM(amount) as amount FROM uc_member';
    //     $result = $this->_db->query($sql, array());
    //     if ($result) {
    //         $total_data['member_total'] = $result[0]['member_total'];
    //         $total_data['amount']       = $result[0]['amount'];
    //     }
    //     //4.撈出正式資料
    //     $sql = array();
    //     $sql[] = 'SELECT * FROM uc_member WHERE 1';
    //     $sql[] = implode('', $condition);
    //     $sql[] = $order_by_sql;
    //     $sql[] = ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
    //     $result = $this->_db->query(implode('', $sql), $parameter);
    //     $return['list'] = $result;
    //     $return['argument'] = $argument;
    //     $return['data']     = $total_data;
    //
    //     return $return;
    // }

    // public function memberLogList($argument)
    // {
    //     global $g_page_size;
    //     if (!isset($argument['page_size'])) {
    //         $argument['page_size'] = $g_page_size['general'];
    //     }
    //
    //     $condition = array();
    //     $parameter = array();
    //     if (isset($argument['agent']) && $argument['agent']) {
    //         $condition[] = ' AND agent=?';
    //         $parameter[] = $argument['agent'];
    //     }
    //     if (isset($argument['status']) && $argument['status']) {
    //         $condition[] = ' AND status=?';
    //         $parameter[] = $argument['status'];
    //     }
    //     if (isset($argument['member_id']) && $argument['member_id']) {
    //         $condition[] = ' AND member_id=?';
    //         $parameter[] = $argument['member_id'];
    //     }
    //     if (isset($argument['category']) && $argument['category']) {
    //         $condition[] = ' AND category=?';
    //         $parameter[] = $argument['category'];
    //     }
    //     if (isset($argument['online']) && -1 != $argument['online']) {
    //         $condition[] = ' AND online=?';
    //         $parameter[] = $argument['online'];
    //     }
    //     if (isset($argument['level']) && $argument['level'] > 0) {
    //         $condition[] = ' AND level=?';
    //         $parameter[] = $argument['level'];
    //     }
    //
    //     $date_start = isset($argument['date_start']) ? $argument['date_start'] : '';
    //     $date_stop = isset($argument['date_stop']) ? $argument['date_stop'] : '';
    //     $time_column = isset($argument['time_column']) ? $argument['time_column'] : '';
    //     $unlogin_day = isset($argument['unlogin_day']) ? intval($argument['unlogin_day']) : 0;
    //     if ($date_start) {
    //         if ('register_time' == $time_column) {
    //             $condition[] = ' AND register_time>? ';
    //             $parameter[] = $date_start;
    //         } elseif ('login_time' == $time_column) {
    //             $condition[] = ' AND login_time>? ';
    //             $parameter[] = $date_start;
    //         }
    //     }
    //     if ($date_stop) {
    //         if ('register_time' == $time_column) {
    //             $condition[] = ' AND register_time<=? ';
    //             $parameter[] = $date_stop;
    //         } elseif ('login_time' == $time_column) {
    //             $condition[] = ' AND login_time<=? ';
    //             $parameter[] = $date_stop;
    //         }
    //     }
    //     if ($unlogin_day) {
    //         $unlogin_date = date('Y-m-d', time() - $unlogin_day * 86400);
    //         $condition[] = ' AND login_time<? ';
    //         $parameter[] = $unlogin_date;
    //     }
    //     if (isset($argument['column']) && $argument['column'] &&
    //         isset($argument['keyword']) && $argument['keyword']) {
    //         switch ($argument['column']) {
    //             case 'account':
    //                 $fuzzy = isset($argument['fuzzy']) ? $argument['fuzzy'] : 0;
    //                 if ($fuzzy) {
    //                     $condition[] = ' AND account LIKE ? ';
    //                     $parameter[] = '%' . $argument['keyword'] . '%';
    //                 } else {
    //                     $condition[] = ' AND account =? ';
    //                     $parameter[] = $argument['keyword'];
    //                 }
    //                 break;
    //             case 'real_name':
    //                 $condition[] = ' AND real_name=?';
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'register_ip':
    //                 $condition[] = ' AND register_ip LIKE ?';
    //                 $parameter[] = $argument['keyword'] . '%';
    //                 break;
    //             case 'qq':
    //                 $condition[] = ' AND qq=?';
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'mobile':
    //                 $condition[] = ' AND mobile=?';
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'email':
    //                 $condition[] = ' AND email=?';
    //                 $parameter[] = kg_encrypt($argument['keyword']);
    //                 break;
    //             case 'bank_number':
    //                 $condition[] = ' AND bank_number=?';
    //                 $parameter[] = $argument['keyword'];
    //                 break;
    //         }
    //     }
    //     if (isset($argument['valuable'])) {
    //         if (0 == $argument['valuable']) {
    //             $condition[] = ' AND deposit_count=?';
    //             $parameter[] = $argument['valuable'];
    //         } elseif (1 == $argument['valuable']) {
    //             $condition[] = ' AND deposit_count>=?';
    //             $parameter[] = $argument['valuable'];
    //         }
    //     }
    //
    //     $order_by = isset($argument['order_by']) ? $argument['order_by'] : 0;
    //     $order_by_sql = '';
    //     switch ($order_by) {
    //         case 1:
    //             $order_by_sql = ' ORDER BY register_time DESC ';
    //             break;
    //         case 2:
    //             $order_by_sql = ' ORDER BY register_time ASC ';
    //             break;
    //         case 3:
    //             $order_by_sql = ' ORDER BY amount DESC ';
    //             break;
    //         case 4:
    //             $order_by_sql = ' ORDER BY amount ASC ';
    //             break;
    //         case 5:
    //             $order_by_sql = ' ORDER BY ag_amount DESC ';
    //             break;
    //         case 6:
    //             $order_by_sql = ' ORDER BY ag_amount ASC ';
    //             break;
    //         case 7:
    //             $order_by_sql = ' ORDER BY lebo_amount DESC ';
    //             break;
    //         case 8:
    //             $order_by_sql = ' ORDER BY lebo_amount ASC ';
    //             break;
    //     }
    //
    //     $return = array();
    //
    //     $sql = 'SELECT COUNT(member_id) AS count FROM uc_member_log WHERE 1';
    //     $sql .= implode('', $condition);
    //     $result = $this->_db->query($sql, $parameter);
    //     $return['total_record'] = $result[0]['count'];
    //     $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
    //     if ($argument['page'] > $return['total_page']) {
    //         $argument['page'] = $return['total_page'];
    //     }
    //     if ($argument['page'] < 1) {
    //         $argument['page'] = 1;
    //     }
    //     $return['page'] = $argument['page'];
    //
    //     $total_data = array(
    //         'member_total' => 0,
    //         'amount' => 0,
    //     );
    //     $sql = 'SELECT COUNT(member_id) AS member_total,SUM(amount) as amount FROM uc_member_log';
    //     $result = $this->_db->query($sql, array());
    //     if ($result) {
    //         $total_data['member_total'] = $result[0]['member_total'];
    //         $total_data['amount'] = $result[0]['amount'];
    //     }
    //
    //     $sql = array();
    //     $sql[] = 'SELECT * FROM uc_member_log WHERE 1';
    //     $sql[] = implode('', $condition);
    //     $sql[] = $order_by_sql;
    //     $sql[] = ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
    //     $result = $this->_db->query(implode('', $sql), $parameter);
    //     $return['list'] = $result;
    //     $return['argument'] = $argument;
    //     $return['data'] = $total_data;
    //
    //     return $return;
    // }
    //

    /**
     * getByAccount
     * 获取会员资料 模糊搜尋
     *
     * @access public
     * @param string $account 账号
     * @param string $column 资料列名
     * @return array
     */
    // public function getByAccountFuzzy($account, $column = '*')
    // {
    //     $sql = 'SELECT ' . $column . ' FROM uc_member WHERE binary account LIKE ? ';
    //     $result = $this->_db->query($sql, array('%' . $account . '%'));
    //     return $result;
    // }
    //
    // public function memberListByAgent($argument)
    // {
    //     $sql = 'SELECT * FROM uc_member WHERE agent=? ORDER BY member_id DESC';
    //     $result = $this->_db->query($sql, array($argument['agent']));
    //     return $result;
    // }

    /**
     * 在線人數
     * @param array $argument account,output
     * @return mixed array or integer 在線列表或總數
     */
    // public function onlineList($argument = array())
    // {
    //     if (isset($argument['output']) && $argument['output']=='sum') {
    //         $sql = 'SELECT COUNT(*) as sum FROM uc_member WHERE 1 ';
    //     } else {
    //         $sql = 'SELECT uid,member_id,account,agent,login_ip,login_time,action,active_time';
    //         $sql .= ',level FROM uc_member WHERE 1 ';
    //     }
    //
    //     if (isset($argument['account']) && $argument['account']) {
    //         if (isset($argument['fuzzy']) && $argument['fuzzy']) {
    //             $sql   .= ' AND account Like ? ';
    //             $result = $this->_db->query($sql, array('%'.$argument['account'].'%'));
    //         } else {
    //             $sql   .= ' AND account=? ';
    //             $result = $this->_db->query($sql, array($argument['account']));
    //         }
    //     } else {
    //         $sql   .= ' AND online=1 AND active_time>? ';
    //         $time   = time() - 1200;
    //         $time   = date('Y-m-d H:i:s', $time);
    //         $result = $this->_db->query($sql, array($time));
    //     }
    //     if (isset($argument['output']) && $argument['output']=='sum') {
    //         return $result[0]['sum'];
    //     } else {
    //         return $result;
    //     }
    // }

    /**
     * [managerEdit 管理員編輯會員資訊]
     *
     * @param int $member_id
     * @param array $argument
     * @return void
     */
    // public function managerEdit($member_id, $argument)
    // {
    //     $sql = array();
    //     $parameter = array();
    //     if (isset($argument['level'])) {
    //         $sql[] = ',level=?';
    //         $parameter[] = $argument['level'];
    //     }
    //     if (isset($argument['real_name']) && $argument['real_name']) {
    //         $sql[] = ',real_name=?';
    //         $parameter[] = $argument['real_name'];
    //     }
    //     if (isset($argument['mobile'])) {
    //         $sql[] = ',mobile=?';
    //         $parameter[] = kg_encrypt($argument['mobile']);
    //     }
    //     if (isset($argument['qq'])) {
    //         $sql[] = ',qq=?';
    //         $parameter[] = kg_encrypt($argument['qq']);
    //     }
    //     if (isset($argument['wx'])) {
    //         $sql[] = ',wx=?';
    //         $parameter[] = kg_encrypt($argument['wx']);
    //     }
    //     if (isset($argument['email'])) {
    //         $sql[] = ',email=?';
    //         $parameter[] = kg_encrypt($argument['email']);
    //     }
    //     // if (isset($argument['bank_name'])) {
    //     //     $sql[] = ',bank_name=?';
    //     //     $parameter[] = $argument['bank_name'];
    //     // }
    //     // if (isset($argument['bank_address'])) {
    //     //     $sql[] = ',bank_address=?';
    //     //     $bankcard_parameter[] = $argument['bank_address'];
    //     // }
    //     // if (isset($argument['bank_number'])) {
    //     //     $sql[] = ',bank_number=?';
    //     //     $parameter[] = kg_encrypt($argument['bank_number']);
    //     // }
    //     if (isset($argument['note'])) {
    //         $sql[] = ',note=?';
    //         $parameter[] = $argument['note'];
    //     }
    //     if (isset($argument['agent']) && $argument['agent']) {
    //         $sql[] = ',agent=?';
    //         $parameter[] = $argument['agent'];
    //     }
    //     if (isset($argument['password']) && $argument['password']) {
    //         $sql[] = ',password=?';
    //         $parameter[] = kg_encrypt($argument['password']);
    //     }
    //     if (isset($argument['withdrawal_code']) && $argument['withdrawal_code']) {
    //         $sql[] = ',withdrawal_code=?';
    //         $parameter[] = kg_encrypt($argument['withdrawal_code']);
    //     }
    //     $sql[] = ' WHERE member_id=? LIMIT 1';
    //     $sql = implode('', $sql);
    //     $sql = 'UPDATE uc_member SET ' . trim($sql, ',');
    //     $parameter[] = $member_id;
    //     if ($this->_db->query($sql, $parameter)) {
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    /**
     *  獲取使用者金額
     * ** */
    // public function getAmount()
    // {
    //     $uid = get_session_uid();
    //
    //     if (!empty($uid)) {
    //         $sql = 'SELECT amount FROM `uc_member` WHERE uid = ? ';
    //         $parameter[] = $uid;
    //         return $this->_db->query($sql, $parameter)[0]['amount'];
    //     }
    //     return 0;
    // }

    // public function BankCardDelete($bid)
    // {
    //     // bank_name   bank_account_name   bank_address    bank_number member_id   mobile
    //     $sql = 'DELETE FROM uc_member_bankcard WHERE id = ?';
    //     if ($this->_db->query($sql, array($bid))) {
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }
    //
    // /**
    //  *  根據日期撈出註冊人數
    //  * * */
    // public function getTotalRegisterByDate($date_start)
    // {
    //     $date_end = $date_start;
    //     $date_start .= '  00:00:00';
    //     $date_end .= ' 23:59:59';
    //
    //     $sql = 'SELECT COUNT(*) as total_registered FROM uc_member WHERE register_time >= ?  AND register_time <= ?';
    //     $result = $this->_db->query($sql, array($date_start, $date_end));
    //
    //     return $result[0]['total_registered'] ? $result[0]['total_registered'] : 0;
    // }
    // public function updateMemberStatus($status, $member_id)
    // {
    //     $sql = 'UPDATE uc_member SET status=? WHERE member_id=?';
    //     $result = $this->_db->query($sql, array($status, $member_id));
    //     return $result;
    // }

    /**
     * 离线 socket调用 离线是casino_online可以不离线
     *
     */
    // public function offline($account)
    // {
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE uc_member SET uid=?, online=0 WHERE account=? LIMIT 1';
    //     return $this->_db->query($sql, array($time, $account));
    // }

    /**
     * 踢人 后台调用 强制online和casino_line都离线
     *
     */
    // public function kickout($account)
    // {
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE uc_member SET uid=?, online=0, casino_online=1 WHERE account=? LIMIT 1';
    //     return $this->_db->query($sql, array($time, $account));
    // }

    // public function online($account)
    // {
    //     $time = date('Y-m-d H:i:s');
    //     $sql = 'UPDATE uc_member SET online=1, casino_online=2, active_time=? WHERE account=? LIMIT 1';
    //     return $this->_db->query($sql, array($time, $account));
    // }

    // public function updateAgAmount($account, $amount)
    // {
    //     $amount = money_to_db($amount);
    //     $sql = 'UPDATE uc_member SET ag_amount=? WHERE account=? LIMIT 1';
    //     return $this->_db->query($sql, array($amount, $account));
    // }
    //
    // public function updateLeboAmount($member_id, $amount)
    // {
    //     $sql = 'UPDATE uc_member SET lebo_amount=lebo_amount+? WHERE member_id=? LIMIT 1';
    //     if ($this->_db->query($sql, array($amount, $member_id))) {
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    /**
     * [setMemberInfoToRedis 將會員資訊存入redis以便計算在線人數]
     *
     * @param Member $member
     * @return void
     */
    public function setOnlineMemberInfoToRedis($member)
    {
        if ($member) {
            $redis_hash = CUSTOMER_NAME . '_online_user';
            $cache_handle = new Cache();
            $cache_result = $cache_handle->get(CUSTOMER_NAME.'-'.$redis_hash);
            $cache_result = json_decode($cache_result, true);

            $account = $member['account'];
            $cache_result[$account]['agent'] = $member['agent'];
            $cache_result[$account]['level'] = $member['level'];
            $cache_result[$account]['login_ip'] = $member['login_ip'];
            $cache_result[$account]['login_time'] = $member['login_time'];
            $cache_result[$account]['account'] = $member['account'];
            $cache_result[$account]['active_time'] = date('Y-m-d H:i:s');
            $cache_result[$account]['uid'] = $member['uid'];
            $cache_result[$account]['member_id'] = $member['member_id'];

            $cache_result = json_encode($cache_result);
            $cache_handle->set(CUSTOMER_NAME.'-'.$redis_hash, $cache_result);
            $cache_handle->expire(CUSTOMER_NAME.'-'.$redis_hash, 1200);

            unset($cache_handle);
        }
    }

    /**
     * [delOnlineMemberInfoInRedis 刪除存在於redis中的在線人數資訊]
     *
     * @param string $account 會員帳號
     * @return void
     */
    public function delOnlineMemberInfoInRedis($account)
    {
        if ($account) {
            $cache_handle = new Cache();
            $redis_hash = CUSTOMER_NAME . '_online_user';
            $cache_result = $cache_handle->get(CUSTOMER_NAME.'-'.$redis_hash);
            $cache_result = json_decode($cache_result, true);
            unset($cache_result[$account]);
            $cache_handle->set(CUSTOMER_NAME.'-'.$redis_hash, json_encode($cache_result));
        }
    }

    public function createMemberCode()
    {
        while(true){
            $code = [];
            for($i=1;$i<=5;$i++){
                $code[] = mt_rand(5,9).substr( base_convert(uniqid(), 16, 10) ,-5);
            }

            $checks = $this->checkMemberCodeUnique($code);
            foreach($checks as $check){
                $fund = array_search($check,$code,false);
                if ($fund === false) {
                    continue;
                }
                unset($code[$fund]);
            }

            if(!empty($code)){
                $code = array_values($code);
                return $code[0];
            }
        }
    }

    private function checkMemberCodeUnique($code){
        $sql = 'SELECT member_code FROM uc_member WHERE member_code in(?,?,?,?,?);';
        $result = $this->_db->query($sql, $code);
        return array_column($result, 'member_code');
    }

    private function createPromoteCode()
    {
        while(true){

            $cod=null;
            for($i=0;$i<=5;$i++){
                $cod .= mt_rand(1,9);
            }
            $code[] = $cod;

            $checks = $this->checkPromoteCodeUnique($code);
            foreach($checks as $check){
                $fund = array_search($check,$code,false);
                if ($fund === false) {
                    continue;
                }
                unset($code[$fund]);
            }

            if(!empty($code)){
                $code = array_values($code);
                return $code[0];
            }
        }
    }

    private function checkPromoteCodeUnique($code){
        $sql = 'SELECT promote_code FROM uc_member WHERE promote_code in(?,?,?,?,?,?);';
        $result = $this->_db->query($sql, $code);
        return array_column($result, 'promote_code');
    }
}
