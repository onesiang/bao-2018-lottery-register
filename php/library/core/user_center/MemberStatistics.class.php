<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 会员数据统计
 *
 * @final
 */
final class MemberStatistics extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {
    }

    private function subtypesValue()
    {
        $subtypes = array(
            1 => 'online_deposit', // 线上存款
            2 => 'company_deposit', // 公司入款
            3 => 'online_withdraw', // 在线取款
            4 => 'change_into', // 转入
            5 => 'change_out', // 转出
            6 => 'bet', // 投注
            7 => 'pay_out', // 派彩
            8 => 're_settle', // 重新结算
            9 => 'bet_cancel', // 投注取消
            10 => 'score_reset', // 比分注单重置
            11 => 'auto_return_water', // 自动返水
            12 => 'auto_winnings', // 自动彩金
            13 => 'hand_deposit', // 人工入款
            14 => 'hand_return_water', // 人工返水
            15 => 'hand_winnings', // 人工彩金
            16 => 'other', // 其它
            17 => 'node', // 归档
            18 => 'charge', // 手续费
            20 => 'recharge', // 兑换码/充值
            21 => 'luckdraw', // 活动彩金
        );
        return $subtypes;
    }

    public function mr($parameters)
    {
        $time_zone   = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $date_start  = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop   = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $account     = isset($parameters['account']) ? $parameters['account'] : '';
        $category    = isset($parameters['category']) ? $parameters['category'] : '';
        $page        = isset($parameters['page']) ? intval($parameters['page']) : 1;
        $page_size   = isset($parameters['page_size']) ? $parameters['page_size'] : 20;
        $fuzzy       = isset($parameters['fuzzy']) ? $parameters['fuzzy'] : 0;

        if ($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        $data   = array();
        $one = array(
            'online_deposit'     => 0,
            'company_deposit'    => 0,
            'online_withdraw'    => 0,
            'hand_deposit'      => 0,
            'change_into'        => 0,
            'change_out'         => 0,
            'sport_bet_amount'   => 0,
            'sport_valid_amount' => 0,
            'sport_win_amount'   => 0,
            'lottery_bet_amount' => 0,
            'lottery_valid_amount' => 0,
            'lottery_win_amount'   => 0,
            'casino_valid_amount'  => 0,
            'casino_bet_amount'    => 0,
            'casino_valid_amount'  => 0,
            'casino_win_amount'    => 0,
            'elc_bet_amount'       => 0,
            'elc_valid_amount'     => 0,
            'elc_win_amount'       => 0,
            'win_amount'           => 0,
            'auto_return_water'    => 0,
            'auto_winnings'        => 0,
            'hand_return_water'    => 0,
            'hand_winnings'        => 0,
            'other'                => 0,
            'node'                 => 0,
            'charge'               => 0,
            'recharge'             => 0,
            'luckdraw'			   => 0,
            'uc_member_amount'     => 0
        );

        $where  = '';
        $category_where = '';
        $category_params = [];
        $params = array();
        if ($category > 0) {
            $where   .= ' AND b.category=?';
            $category_where .= ' AND a.category = ? ';
            $params[] = $category;
            $category_params[] = $category;
        }

        if ($account) {
            if ($fuzzy) {
                $where   .= ' AND b.account Like ? ';
                $params[] = '%'.$account.'%';
            } else {
                $where   .= ' AND b.account=?';
                $params[] = $account;
            }
        } else {
            $member_params = array();
            $member_params[] = $date_start;
            $member_params[] = $date_stop;
            $member_params[] = $date_start;
            $member_params[] = $date_stop;
            $member_params[] = $date_start;
            $member_params[] = $date_stop;

            $sql  = 'select count(member_id) as cnt FROM uc_member AS a where ';
            $sql .= '(exists(select 1 from fnn_withdrawal b where a.member_id=b.member_id and b.submit_time>? AND b.submit_time<? AND b.status = 2) or ';
            $sql .= 'exists(select 1 from fnn_record c where a.member_id=c.member_id and c.submit_time>? AND c.submit_time<? AND c.subtype IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21) ) or ';
            $sql .= 'exists(select 1 from ltt_bet d force index(bet_time) where a.member_id=d.member_id and d.bet_time>? AND d.bet_time<?)) ';
            $sql .= $category_where;
            $result = $this->_db->query($sql, array_merge($member_params, $category_params));

            $data['total_page'] = ceil($result[0]['cnt']/$page_size);
            $data['page'] = ($page-1) < 0 ? 1:$page;
            if ($data['page'] > $data['total_page']) {
                $data['page'] = $data['total_page'];
            }
            $limit = (($data['page']-1)*$page_size).' , '.$page_size;

            $sql  = 'select member_id FROM uc_member AS a where ';
            $sql .= '(exists(select 1 from fnn_withdrawal b where a.member_id=b.member_id and b.submit_time>? AND b.submit_time<? AND b.status = 2) or ';
            $sql .= 'exists(select 1 from fnn_record c where a.member_id=c.member_id and c.submit_time>? AND c.submit_time<? AND c.subtype  IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21) ) or ';
            $sql .= 'exists(select 1 from ltt_bet d force index(bet_time) where a.member_id=d.member_id and d.bet_time>? AND d.bet_time<?)) ';
            $sql .= $category_where;
            $sql .= 'LIMIT '.$limit;
            $result = $this->_db->query($sql, array_merge($member_params, $category_params));

            if(empty($result))
                return false;

            $where1 = '';
            foreach ($result as $key =>$res) {
                $where1 .=$res['member_id'];
                if ((count($result)-1)!=$key) {
                    $where1 .=',';
                }
            }
            $where .= ' AND a.member_id in('.$where1.') ';
            unset($member_params);
        }

        $params[] = $date_start;
        $params[] = $date_stop;

        $sql  = 'SELECT SUM(a.win_amount) AS win_amount, SUM(a.valid_amount) AS valid_amount, SUM(a.bet_amount) AS bet_amount, a.member_id, b.account, b.amount as uc_member_amount FROM ltt_bet AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ';
        $sql .= $where . ' AND a.bet_time>? AND a.bet_time<? AND a.status=2 AND a.parlay=0 GROUP BY a.member_id';
        $result = $this->_db->query($sql, $params);

        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
                $data['list'][$key]['uc_member_amount'] = $row['uc_member_amount'];
            }

            $data['list'][$key]['lottery_bet_amount'] += $row['bet_amount'];
            $data['list'][$key]['lottery_valid_amount'] += $row['valid_amount'];
            $data['list'][$key]['lottery_win_amount'] += $row['win_amount'];
        }

        $sql  = 'SELECT SUM(a.amount) AS amount, a.member_id, a.account FROM fnn_withdrawal AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ';
        $sql .= $where . ' AND a.submit_time>? AND a.submit_time<? AND a.status=2 GROUP BY a.member_id';
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
            }
            $data['list'][$key]['online_withdraw'] += $row['amount'];
        }
        $sql  = 'SELECT a.amount,a.subtype, b.member_id, b.account FROM fnn_record AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ' . $where;
        $sql .= ' AND a.submit_time>? AND a.submit_time<? AND a.subtype IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21)';
        $result = $this->_db->query($sql, $params);
        $subtypes = $this->subtypesValue();
        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
            }
            $data['list'][$key][$subtypes[$row['subtype']]] += $row['amount'];
        }

        foreach($data['list'] as $key => $item){
            if(!isset($item['lottery_valid_amount'])){
                $data['list'][$key]['lottery_valid_amount'] = 0;
            }
        }

        return $data;
    }

    /**
     * [memberReport 會員報表]
     *
     * @param array $parameters
     * @return void
     */
    public function memberReport($parameters)
    {
        $time_zone   = isset($parameters['time_zone']) ? $parameters['time_zone'] : 0;
        $date_start  = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop   = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $account     = isset($parameters['account']) ? $parameters['account'] : '';
        $category    = isset($parameters['category']) ? $parameters['category'] : '';
        $page        = isset($parameters['page']) ? intval($parameters['page']) : 1;
        $page_size   = isset($parameters['page_size']) ? $parameters['page_size'] : 20;
        $fuzzy       = isset($parameters['fuzzy']) ? $parameters['fuzzy'] : 0;

        if ($date_stop) {
            $day_stop  = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        $data   = array();
        $one = array(
            'online_deposit'     => 0,
            'company_deposit'    => 0,
            'online_withdraw'    => 0,
            'hand_deposit'      => 0,
            'change_into'        => 0,
            'change_out'         => 0,
            'sport_bet_amount'   => 0,
            'sport_valid_amount' => 0,
            'sport_win_amount'   => 0,
            'lottery_bet_amount' => 0,
            'lottery_valid_amount' => 0,
            'lottery_win_amount'   => 0,
            'casino_valid_amount'  => 0,
            'casino_bet_amount'    => 0,
            'casino_valid_amount'  => 0,
            'casino_win_amount'    => 0,
            'elc_bet_amount'       => 0,
            'elc_valid_amount'     => 0,
            'elc_win_amount'       => 0,
            'win_amount'           => 0,
            'auto_return_water'    => 0,
            'auto_winnings'        => 0,
            'hand_return_water'    => 0,
            'hand_winnings'        => 0,
            'other'                => 0,
            'node'                 => 0,
            'charge'               => 0,
            'recharge'             => 0,
            'luckdraw'			   => 0,
            'uc_member_amount'     => 0
        );

        // 体
        $where  = '';
        $category_where = '';
        $category_params = [];
        $params = array();
        if ($category > 0) {
            $where   .= ' AND b.category=?';
            $category_where .= ' AND a.category = ? ';
            $params[] = $category;
            $category_params[] = $category;
        }

        if ($account) {
            if ($fuzzy) {
                $where   .= ' AND b.account Like ? ';
                $params[] = '%'.$account.'%';
            } else {
                $where   .= ' AND b.account=?';
                $params[] = $account;
            }
        } else {
            $member_params = array();
            $member_params[] = $date_start;
            $member_params[] = $date_stop;
            $member_params[] = $date_start;
            $member_params[] = $date_stop;
            $member_params[] = $date_start;
            $member_params[] = $date_stop;

            $sql = 'select count(member_id) as cnt FROM uc_member AS a where ';
            $sql .= '(exists(select 1 from fnn_withdrawal b force index(submit_time) where a.member_id=b.member_id and b.submit_time>? AND b.submit_time<? AND b.status = 2) or ';
            $sql .= 'exists(select 1 from fnn_record c force index(submit_time,subtype) where a.member_id=c.member_id and c.submit_time>? AND c.submit_time<? AND c.subtype IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21) ) or ';
            $sql .= 'exists(select 1 from ltt_bet d force index(bet_time) where a.member_id=d.member_id and d.bet_time>? AND d.bet_time<?)) ';
            $sql .= $category_where;
            $result = $this->_db->query($sql, array_merge($member_params, $category_params));

            $data['total_page'] = ceil($result[0]['cnt'] / $page_size);
            $data['page'] = ($page - 1) < 0 ? 1 : $page;
            if ($data['page'] > $data['total_page']) {
                $data['page'] = $data['total_page'];
            }

            $start_page = ($data['page'] - 1) * $page_size < 0 ? 0 : ($data['page'] - 1) * $page_size;

            $limit = $start_page . ' , ' . $page_size;

            $sql = 'select member_id FROM uc_member AS a where ';
            $sql .= '(exists(select 1 from fnn_withdrawal b force index(submit_time) where a.member_id=b.member_id and b.submit_time>? AND b.submit_time<? AND b.status = 2) or ';
            $sql .= 'exists(select 1 from fnn_record c force force index(submit_time,subtype) where a.member_id=c.member_id and c.submit_time>? AND c.submit_time<? AND c.subtype  IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21) ) or ';
            $sql .= 'exists(select 1 from ltt_bet d force index(bet_time) where a.member_id=d.member_id and d.bet_time>? AND d.bet_time<?)) ';
            $sql .= $category_where;
            $sql .= 'LIMIT ' . $limit;
            $result = $this->_db->query($sql, array_merge($member_params, $category_params));

            $where1 = '';
            foreach ($result as $key =>$res) {
                $where1 .=$res['member_id'];
                if ((count($result)-1)!=$key) {
                    $where1 .=',';
                }
            }
            $where .= ' AND a.member_id in('.$where1.') ';
            unset($member_params);
        }

        $where  = '';
        $params = array();
        if ($member_id) {
            $where   .= ' AND a.member_id=?';
            $params[] = $member_id;
        }
        if ($category > 0) {
            $where   .= ' AND b.category=?';
            $params[] = $category;
        }
        $params[] = date('Y-m-d H:i:s', strtotime($date_start));
        $params[] = date('Y-m-d H:i:s', strtotime($date_stop));
        $sql  = 'SELECT SUM(a.valid_amount) AS valid_amount, SUM(a.bet_amount) AS bet_amount, SUM(a.win_amount) AS win_amount, b.member_id, b.account FROM ltt_bet AS a
        LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 '. $where . ' AND a.bet_time >? AND '.'a.bet_time< ? AND a.status=2 AND a.parlay=0 GROUP BY a.member_id ';

        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
            }
            $data['list'][$key]['lottery_bet_amount'] += $row['bet_amount'];
            $data['list'][$key]['lottery_valid_amount'] += $row['valid_amount'];
            $data['list'][$key]['lottery_win_amount'] += $row['win_amount'];
        }
        unset($result);

        $sql  = 'SELECT SUM(a.amount) AS amount, a.member_id, a.account FROM fnn_withdrawal AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ';
        $sql .= $where . ' AND a.submit_time>? AND a.submit_time<? AND a.status=2 GROUP BY a.member_id';
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
            }
            $data['list'][$key]['online_withdraw'] += $row['amount'];
        }

        $sql  = 'SELECT a.amount,a.subtype, b.member_id, b.account FROM fnn_record AS a LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ' . $where;
        $sql .= ' AND a.submit_time>? AND a.submit_time<? AND a.subtype IN (1,2,4,5,11,12,13,14,15,16,17,18,20,21)';
        $result = $this->_db->query($sql, $params);
        $subtypes = $this->subtypesValue();
        foreach ($result as $row) {
            $key = $row['member_id'];
            if (!isset($data['list'][$key])) {
                $data['list'][$key] = $one;
                $data['list'][$key]['account'] = $row['account'];
            }
            $data['list'][$key][$subtypes[$row['subtype']]] += $row['amount'];
        }

        return $data;
    }

    public function totalReport($parameters)
    {
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        if ($date_stop) {
            $day_stop = $date_stop . ' 23:59:59';
            $date_stop = strtotime($day_stop) ? $day_stop : $date_stop;
        }
        // 体
        // $sql    = 'SELECT SUM(win_amount) AS win_amount FROM spr_bet WHERE submit_time>? AND submit_time<? AND status=2 AND display=1 AND settle=1';
        // $result = $this->_db->query($sql, array($date_start, $date_stop));
        // $sport_win_amount = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        // $sport_win_amount = money_from_db($sport_win_amount);

        // 彩票
        $l_date_start = date('Y-m-d H:i:s', strtotime($date_start));
        $l_date_stop = date('Y-m-d H:i:s', strtotime($date_stop));
        $sql = 'SELECT SUM(win_amount) AS win_amount FROM ltt_bet WHERE bet_time>? AND bet_time<? AND status=2 AND parlay=0';
        $result = $this->_db->query($sql, array($l_date_start, $l_date_stop));
        $lottery_win_amount = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        $lottery_win_amount = money_from_db($lottery_win_amount);

        // 真人
        // $sql    = 'SELECT SUM(win_amount) AS win_amount FROM csn_bet WHERE bet_time>=? AND bet_time<=? ';
        //$result = $this->_db->query($sql, array($date_start, $date_stop));
        //$casino_win_amount = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        // $casino_win_amount = 0;
        // $casino = array(
        //     'ag'      => 0,
        //     'ag_cost' => 0,
        //     'agq'     => 0,
        //     'lebo'    => 0,
        //     'ds'      => 0,
        //     'ab'      => 0,
        //     'bbin'    => 0
        // );
        // foreach ($casino as $key => $value) {
        //     $sql = 'SELECT SUM(win_amount) AS win_amount FROM csn_bet WHERE platform=? AND bet_time>=? AND bet_time<=? ';
        //     $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
        //     $casino[$key] = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        //     $casino_win_amount += $casino[$key];
        // }

        // 小费
        // $sql    = 'SELECT SUM(transfer_amount) AS transfer_amount FROM csn_trs WHERE platform="ag" AND bet_time>=? AND bet_time<=? ';
        // $result = $this->_db->query($sql, array($date_start, $date_stop));
        // $ag_cost_amount = ($result && $result[0]['transfer_amount']) ? $result[0]['transfer_amount'] : 0;
        // $casino['ag_cost'] = floor($ag_cost_amount);

        // 电子
        //$sql    = 'SELECT SUM(win_amount) AS win_amount FROM electronic_bet WHERE submit_time>=? AND submit_time<=? ';
        //$result = $this->_db->query($sql, array($date_start, $date_stop));
        //$electronic_win_amount = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        // $electronic_win_amount = 0;
        // $electronic = array(
        //     'mg'    => 0,
        //     'bbin'  => 0
        // );
        // foreach ($electronic as $key => $value) {
        //     $sql = 'SELECT SUM(win_amount) AS win_amount FROM electronic_bet WHERE platform=? AND submit_time>=? AND submit_time<=? ';
        //     $result = $this->_db->query($sql, array($key, $date_start, $date_stop));
        //     $electronic[$key] = ($result && $result[0]['win_amount']) ? $result[0]['win_amount'] : 0;
        //     $electronic_win_amount += $electronic[$key];
        // }

        // 在线取款
        $sql = 'SELECT SUM(amount) AS amount FROM fnn_withdrawal WHERE submit_time>? AND submit_time<? AND status=2';
        $result = $this->_db->query($sql, array($date_start, $date_stop));
        $withdrawal_amount = ($result && $result[0]['amount']) ? $result[0]['amount'] : 0;
        $withdrawal_amount = money_from_db($withdrawal_amount);

        // 存款
        $sql = 'SELECT SUM(amount) AS amount FROM fnn_record WHERE submit_time>? AND submit_time<? AND subtype IN (1,2,13)';
        $result = $this->_db->query($sql, array($date_start, $date_stop));
        $deposit_amount = ($result && $result[0]['amount']) ? $result[0]['amount'] : 0;
        $deposit_amount = money_from_db($deposit_amount);

        // 剩余点数
        $sql = 'SELECT * FROM mdl_agent_config';
        $result = $this->_db->query($sql, array());
        $point_data = array();
        foreach ($result as $row) {
            $point_data[$row['module_name']] = $row['amount'];
        }

        $data = array(
            'sport' => $sport_win_amount,
            'lottery' => $lottery_win_amount,
            'casino' => $casino_win_amount,
            'casino_data' => $casino,
            'electronic' => $electronic_win_amount,
            'electronic_data' => $electronic,
            'total' => ($sport_win_amount + $lottery_win_amount + $casino_win_amount + $electronic_win_amount),
            'withdrawal' => $withdrawal_amount,
            'deposit' => $deposit_amount,
            'point_data' => $point_data,
        );

        return $data;
    }

    public function sendReport()
    {
        $begin_time = time(); // 程序开始时间

        //报表发送到DG 3天前的自然月的第一天至最后一天
        $firstday = date('Y-m-01', time() - (86400 * 3));
        $now_month_day = date('Y-m-01', time());
        if ($firstday != $now_month_day) {
            $rand_num = rand(1, 10);
            if ($rand_num > 5) {
                $firstday = $now_month_day;
            }
        }
        $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day")); // totalReport里有转换成23:59:59
        //$lastday  = date('Y-m-d 23:59:59', strtotime("$firstday +1 month -1 day"));
        //$st_handle = new MemberStatistics();
        $params = array(
            'date_start' => $firstday,
            'date_stop' => $lastday,
        );
        $data = $this->totalReport($params);
        //unset($st_handle);

        /*
        // 如果执行耗时小于10秒 则分散一下发送时间
        if ((time() - $begin_time) < 10) {
        $sleep_num = rand(1, 20);
        sleep($sleep_num);
        }

        $site_handle = new Site();
        $site = $site_handle->get();
        unset($site_handle);

        $time   = time();
        $config_handle = new Config();
        $config        = $config_handle->getByName(array('APIKEY'));
        $sign = md5(kg_decrypt($config['APIKEY']) . '^' . $time . '^' . $site['casino_agent']);
        $post_data = array(
        'agent'      => $site['casino_agent'],
        'time'       => $time,
        'date_start' => $firstday . ' 00:00:00',
        'date_stop'  => $lastday .  ' 23:59:59',
        'sign'       => $sign,
        'data'       => php_escape(json_encode($data))
        );
        $curl_option = array(
        'port' => 443,
        'ssl'  => 1
        );
        $curl_handle = new Curl($curl_option);
        //$url = 'https://dg.goodgame.com/api/report.php';
        $url = 'https://www.l9088.com/api/report.php';

        Log::record('AA:' . $url . ':' . print_r($post_data), 'send_report');
        $data = $curl_handle->post($url, $post_data);

        // 不成功就重发一次
        if (1 != $data || '1' != $data) {
        $data = $curl_handle->post($url, $post_data);
        }
        unset($curl_handle);
        Log::record('BB---', 'send_report');
         */

        return $data;
    }
}
