<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 基类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 */
class Wwin
{
    /**
     * _db
     * 数据库句柄
     *
     * @access protected
     * @var KgMysql
     */
    protected $_db = null;

    /**
     * _cache
     * 缓存句柄
     *
     * @access protected
     * @var Cache
     */
    protected $_cache = null;

    /**
     * __construct
     * 构造函数
     *
     * @access public
     * @return self
     */
    public function __construct()
    {
        $this->_db = new KgMysql();
        $this->_cache = new Cache();
    }
}
