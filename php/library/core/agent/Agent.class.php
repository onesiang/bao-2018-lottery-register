<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 代理类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * <code>
 * -- status 0审核中  1启用  2禁用  3禁止存款
 * </code>
 * @final
 */
final class Agent extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {}

    /**
     * register
     * 代理注册
     *
     * @access public
     * @param array $argument 注册资料
     * <code>
     * -- account 账号
     * -- password 密码
     * -- password_repeat 重复密码
     * -- real_name 真实姓名
     * -- mobile 手机，可选
     * -- qq QQ号，可选
     * -- wx 微信号，可选
     * -- email 邮箱，可选
     * -- withdrawal_code 取款密码
     * </code>
     * @return string 注册状态，返回1为注册成功，其它为失败
     * <code>
     * -- -7 代理已经存在
     * -- -6 QQ号错误
     * -- -5 手机号错误
     * -- -4 真实姓名错误
     * -- -3 两次密码不同
     * -- -2 密码不符合规范
     * -- -1 账号不符合规范
     * </code>
     */
    public function register($argument)
    {
        $return = array();
        /**
         * 验证注册资料合法性
         */
        if (!isset($argument['add_type']) || 1 != $argument['add_type']) {
            // 验证码校验
            // TODO
        }
        if (!preg_match('/^[A-Za-z0-9]{6,20}$/', $argument['account'])) {
            $return = -1;
            return $return;
        }
        if (!preg_match('/^[A-Za-z0-9]{6,20}$/', $argument['password'])) {
            $return = -2;
            return $return;
        }
        if ($argument['password'] !== $argument['password_confirm']) {
            $return = -3;
            return $return;
        }
        if (!preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $argument['real_name'])) {
            $return = -4;
            return $return;
        }
        if (!preg_match('/^[0-9]{4}$/', $argument['withdrawal_code'])) {
            $return = -6;
            return $return;
        }
        if (!isset($argument['mobile']) || empty($argument['mobile'])) {
            $argument['mobile'] = '';
        } elseif (!preg_match('/^1[0-9]{10}$/', $argument['mobile'])) {
            $return = -5;
            return $return;
        }
        if (!isset($argument['qq']) || '' === $argument['qq']) {
            $argument['qq'] = '';
        } elseif (!preg_match('/^[1-9][0-9]{4,11}$/', $argument['qq'])) {
            $return = -7;
            return $return;
        }
        if (!isset($argument['wx']) || '' === $argument['wx']) {
            $argument['wx'] = '';
        } elseif (!preg_match('/^[0-9a-zA-Z_\-]{6,20}$/', $argument['wx'])) {
            $return = -8;
            return $return;
        }
        if (!isset($argument['email']) || '' === $argument['email']) {
            $argument['email'] = '';
        } elseif (!filter_var($argument['email'], FILTER_VALIDATE_EMAIL)) {
            $return = -9;
            return $return;
        }
        if (!isset($argument['url']) || !$argument['url']) {
            $argument['url'] = '';
        }
        /*if (count($return) > 0) {
        return implode(',', $return);
        }*/

        /**
         * 验证有效性
         */
        if ($this->exists($argument['account'])) {
            $return = -10;
            return $return;
        }

        $config_handle = new Config();
        $config = $config_handle->getByName(array('AGENT_AUTO_ACTIVE'));
        unset($config_handle);

        $status = 0;
        if ($config['AGENT_AUTO_ACTIVE']) {
            $status = 1;
        }

        $ip = get_client_ip();
        $time = date('Y-m-d H:i:s');

        /**
         * 整合插入数据库的参数（关键数据加密）
         */
        $parameter = array(
            $argument['account'],
            kg_md5($argument['password']),
            $argument['real_name'],
            $argument['url'],
            kg_encrypt($argument['mobile']),
            kg_encrypt($argument['qq']),
            kg_encrypt($argument['wx']),
            kg_encrypt($argument['email']),
            kg_encrypt($argument['withdrawal_code']),
            $ip,
            $time,
            $status,
        );
        $sql = array();
        $sql[] = 'INSERT INTO gnt_agent (account,password,real_name,url,mobile,qq';
        $sql[] = ',wx,email,withdrawal_code,register_ip,register_time,status)';
        $sql[] = ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
        if ($this->_db->query(implode('', $sql), $parameter)) {
            return 1;
        } else {
            return -11;
        }
    }

    /**
     * getConfig
     * 获取某一代理其他設定资料
     *
     * @access public
     * @param int $agent_id 编号
     * @return array
     */
    public function getConfig($agent_id)
    {
        $sql = 'SELECT DEPOSIT_POUNDAGE_RATE, WITHDRAWAL_POUNDAGE_RATE, VALID_MEMBER_AMOUNT FROM gnt_agent WHERE agent_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($agent_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function getConfigByAccount($acount)
    {
        $sql = 'SELECT DEPOSIT_POUNDAGE_RATE, WITHDRAWAL_POUNDAGE_RATE, VALID_MEMBER_AMOUNT FROM gnt_agent WHERE account=? LIMIT 1';
        $result = $this->_db->query($sql, array($acount));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    /**
     * getConfig
     * 获取所有代理其他設定资料
     *
     * @access public
     * @return array
     */
    public function getAllConfig()
    {
        $sql = 'SELECT agent_id, account, DEPOSIT_POUNDAGE_RATE, WITHDRAWAL_POUNDAGE_RATE, VALID_MEMBER_AMOUNT FROM gnt_agent';
        $result = $this->_db->query($sql);

        return $result;
    }

    /**
     * login
     * 登陆
     *
     * @access public
     * @param string $account 用户名
     * @param string $password 密码
     * @param string $code 验证码
     * @return array
     * <code>
     * -- 0 代码 [-1 - 账号或密码错; 1 - 成功; 2 - 强制修改密码]
     * -- 1 数据 代码值为1、2时返回uid
     * </code>
     */
    public function login($account, $password, $code = null)
    {
        $return = array(0, '');

        $config_handle = new Config();
        $config = $config_handle->getByName(array('AGENT_LOGIN_VALIDATE', 'CHANGE_PASSWORD_DAY'));
        unset($config_handle);

        /**
         * 验证码校验
         */
        if ($config['AGENT_LOGIN_VALIDATE']) {
            session_start();
            $code = strtolower($code);
            if (!isset($_SESSION['auth_code']) || $code !== $_SESSION['auth_code']) {
                $return = array(-1, '');
                return $return;
            }
        }

        $sql = 'SELECT agent_id,password_time,account FROM gnt_agent WHERE binary account=? AND password=? LIMIT 1';
        $result = $this->_db->query($sql, array($account, kg_md5($password)));
        if (!$result) {
            $return = array(-2, '');
            return $return;
        }
        $agent = $result[0];
        $ip = get_client_ip();
        $time = date('Y-m-d H:i:s');
        $uid = uniqid(time());
        $sql = 'UPDATE gnt_agent SET uid=?,login_ip=?,login_time=? WHERE agent_id=? LIMIT 1';
        $parameter = array($uid, $ip, $time, $agent['agent_id']);
        $this->_db->query($sql, $parameter);

        $sql = 'INSERT INTO mdl_syslogin_log SET user_id=?,account=?,login_ip=?,login_time=?,login_url=?,type=?,browser=?';
        $login_host = $_SERVER['HTTP_HOST'];
        $login_host = $login_host ? $login_host : '';
        $params = array(
            $agent['agent_id'],
            $agent['account'],
            $ip,
            $time,
            $login_host,
            2,
            '',
        );
        $this->_db->query($sql, $params);

        // 强制修改密码判断
        $change_password = false;
        if ($config['CHANGE_PASSWORD_DAY']) {
            $datediff = floor((time() - strtotime($agent['password_time'])) / 86400);
            if ($datediff >= intval($config['CHANGE_PASSWORD_DAY'])) {
                $change_password = true;
            }
        }

        if ($change_password) {
            $return = array(2, $uid);
        } else {
            $return = array(1, $uid);
        }
        return $return;
    }

    /**
     * logout
     * 登出
     *
     * @access public
     * @param string $uid uid
     * @return boolean
     */
    public function logout($uid)
    {
        $time = date('Y-m-d H:i:s');
        $sql = 'UPDATE gnt_agent SET uid=? WHERE uid=? LIMIT 1';
        return $this->_db->query($sql, array($time, $uid));
    }

    /**
     * checkLogin
     * 验证代理是否登陆
     *
     * @access public
     * @param string $uid UID
     * @return bool 已经登陆返回TRUE，反之返回FALSE
     */
    public function checkLogin($uid)
    {
        $agent = $this->getByUid($uid);
        if (!$agent) {
            return false;
        }
        $ip = get_client_ip();
        if ($agent['login_ip'] != $ip) {
            return false;
        }
        return $agent;
    }

    /**
     * exists
     * 检测代理是否存在
     *
     * @access public
     * @param string $account 账号
     * @return bool 代理存在返回TRUE，不存在返回FALSE
     */
    public function exists($account)
    {
        $sql = 'SELECT agent_id FROM gnt_agent WHERE account=? LIMIT 1';
        $result = $this->_db->query($sql, array($account));
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * getByUid
     * 获取代理资料
     *
     * @access public
     * @param string $uid uid
     * @param string $column 资料列名
     * @return array
     */
    public function getByUid($uid, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM gnt_agent WHERE uid=? LIMIT 1';
        $result = $this->_db->query($sql, array($uid));
        if (!$result) {
            return array();
        } else {
            return $result[0];
        }
    }

    /**
     * getByAccount
     * 获取代理资料
     *
     * @access public
     * @param string $account 账号
     * @param string $column 资料列名
     * @return array
     */
    public function getByAccount($account, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM gnt_agent WHERE account=? LIMIT 1';
        $result = $this->_db->query($sql, array($account));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    /**
     * getByAgentId
     * 获取代理资料
     *
     * @access public
     * @param int $agent_id 编号
     * @param string $column 资料列名
     * @return array
     */
    public function getByAgentId($agent_id, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM gnt_agent WHERE agent_id=? LIMIT 1';
        $result = $this->_db->query($sql, array($agent_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function getValidByAgentId($agent_id, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM gnt_agent WHERE agent_id=? AND status=1 LIMIT 1';
        $result = $this->_db->query($sql, array($agent_id));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }
    
    public function getValidByAgentAccount($agent_account, $column = '*')
    {
        $sql = 'SELECT ' . $column . ' FROM gnt_agent WHERE account = ? AND status = 1 LIMIT 1';
        $result = $this->_db->query($sql, array($agent_account));
        if ($result) {
            $result = $result[0];
        }
        return $result;
    }

    public function getByUrl($url)
    {
        $url_field = '%' . $url . '%';
        $sql = 'SELECT agent_id,url FROM gnt_agent WHERE status=1 AND url LIKE ? LIMIT 1';
        $result = $this->_db->query($sql, array($url_field));
        $agent_id = 0;
        if ($result) {
            $url_data = explode(',', $result[0]['url']);
            foreach ($url_data as $row) {
                if ($row == $url) {
                    $agent_id = $result[0]['agent_id'];
                    break;
                }
            }
        }
        return $agent_id;
    }

    /**
     * edit
     * 更新代理资料
     *
     * @access private
     * @param string $uid uid
     * @param array $data 数据
     * <code>
     * $data:
     * -- column1 value1
     * -- column2 value2
     * ...
     * </code>
     * @return int
     * <code>
     * -- -1 代理不存在
     * -- 0 更新失败
     * -- 1 更新成功
     * </code>
     */
    private function edit($uid, $data)
    {
        $agent = $this->getByUid($uid);
        if (!$agent) {
            return -1;
        }
        $sql = 'UPDATE gnt_agent SET ';
        $parameter = array();
        foreach ($data as $key => $value) {
            $sql .= $key . '=?,';
            $parameter[] = $value;
        }
        $sql = trim($sql, ',');
        $sql .= ' WHERE agent_id=?';
        $parameter[] = $agent['agent_id'];
        return $this->_db->query($sql, $parameter);
    }

    public function editByAgentId($agent_id, $data)
    {
        $sql = 'UPDATE gnt_agent SET ';
        $parameter = array();
        foreach ($data as $key => $value) {
            $sql .= $key . '=?,';
            $parameter[] = $value;
        }
        $sql = trim($sql, ',');
        $sql .= ' WHERE agent_id=?';
        $parameter[] = $agent_id;
        return $this->_db->query($sql, $parameter);
    }

    /**
     * editPassword
     * 修改密码
     *
     * @access public
     * @param string $uid uid
     * @param string $password 密码
     * @return int
     * <code>
     * -- 0 修改失败
     * -- 1 修改成功
     * </code>
     */
    public function editPassword($uid, $argument)
    {
        $return = array();
        if (!preg_match('/^[A-Za-z0-9]{6,20}$/', $argument['password_old'])) {
            $return[] = -1;
            return $return;
        }
        if (!preg_match('/^[A-Za-z0-9]{6,20}$/', $argument['password_new'])) {
            $return[] = -2;
            return $return;
        }
        if ($argument['password_new'] !== $argument['password_confirm']) {
            $return[] = -3;
            return $return;
        }
        $agent = $this->getByUid($uid);
        if (!$agent) {
            return -4; // 代理不存在
        }
        if (kg_md5($argument['password_old']) !== $agent['password']) {
            $return[] = -5; // 旧密码错误
            return $return;
        }
        $password_new = kg_md5($argument['password_new']);
        if ($agent['password'] === $password_new) {
            return -6; // 新旧密码相同
        }

        $time = date('Y-m-d H:i:s');

        $sql = 'UPDATE gnt_agent SET uid=?,password=?,password_time=? WHERE agent_id=? LIMIT 1';
        $parameter = array(
            $time,
            $password_new,
            $time,
            $agent['agent_id'],
        );
        $status = $this->_db->query($sql, $parameter);
        if ($status) {
            $return[] = 1;
        } else {
            $return[] = 0;
        }
        return $return;
    }

    public function getList($argument)
    {
        global $g_page_size;
        if (!isset($argument['page_size'])) {
            $argument['page_size'] = $g_page_size['general'];
        }

        $condition = array();
        $parameter = array();
        if (isset($argument['status']) && '' != $argument['status'] && -1 != $argument['status']) {
            $condition[] = ' AND status=?';
            $parameter[] = $argument['status'];
        }
        if (isset($argument['account']) && $argument['account']) {
            if (isset($argument['fuzzy']) && $argument['fuzzy']) {
                $condition[] = ' AND account Like ? ';
                $parameter[] = '%' . $argument['account'] . '%';
            } else {
                $condition[] = ' AND account=?';
                $parameter[] = $argument['account'];
            }
        }

        $return = array();

        $sql = 'SELECT COUNT(agent_id) AS count FROM gnt_agent WHERE 1';
        $sql .= implode('', $condition);
        $result = $this->_db->query($sql, $parameter);
        $return['total_record'] = $result[0]['count'];
        $return['total_page'] = ceil($return['total_record'] / $argument['page_size']);
        if ($argument['page'] > $return['total_page']) {
            $argument['page'] = $return['total_page'];
        }
        if ($argument['page'] < 1) {
            $argument['page'] = 1;
        }
        $return['page'] = $argument['page'];

        $sql = array();
        $sql[] = 'SELECT * FROM gnt_agent WHERE 1';
        $sql[] = implode('', $condition);
        if (isset($argument['order_by'])) {
            switch ($argument['order_by']) {
                case 1:
                    $sql[] = ' ORDER BY account';
                    break;
                case 2:
                    $sql[] = ' ORDER BY account DESC';
                    break;
                case 3:
                    $sql[] = ' ORDER BY register_time';
                    break;
                case 4:
                    $sql[] = ' ORDER BY register_time DESC';
                    break;
            }
        }
        $sql[] = ' LIMIT ' . ($argument['page'] - 1) * $argument['page_size'] . ',' . $argument['page_size'];
        $result = $this->_db->query(implode('', $sql), $parameter);
        $return['list'] = $result;
        $return['argument'] = $argument;

        return $return;
    }

    public function map()
    {
        $sql = 'SELECT account FROM gnt_agent';
        $result = $this->_db->query($sql);
        $map = array();
        if ($result) {
            foreach ($result as $row) {
                $map[$row['account']] = $row['account'];
            }
        }
        return $map;
    }

    public function select($name, $default = -1, $style = '')
    {
        $sql = 'SELECT account FROM gnt_agent WHERE 1 ORDER BY account';
        $result = $this->_db->query($sql);
        if ($result) {
            $content = array();
            if ($style) {
                $style = ' class="' . $style . '"';
            }
            $content[] = '<select id="' . $name . '" name="' . $name . '"' . $style . '>';
            $content[] = '<option value="">全部</option>';
            foreach ($result as $row) {
                $content[] = '<option value="' . $row['account'] . '"';
                if ($row['account'] == $default) {
                    $content[] = ' selected';
                }
                $content[] = '>' . $row['account'] . '</option>';
            }
            $content[] = '</select>';
            return implode('', $content);
        } else {
            return '';
        }
    }

}
