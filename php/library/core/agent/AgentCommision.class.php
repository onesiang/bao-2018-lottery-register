<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 代理佣金结算
 *
 */
final class AgentCommision extends Wwin
{

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone()
    {}

    /**
     * [getList 獲取退佣金清單]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */

    public function getList($parameters)
    {
        $number = isset($parameters['number']) ? $parameters['number'] : '';
        $account = isset($parameters['account']) ? $parameters['account'] : '';
        $type = isset($parameters['type']) ? $parameters['type'] : '';

        $where = '';
        $params = array();

        $where .= ' AND month=?';
        $params[] = $number;
        if ($account) {
            if (isset($parameters['fuzzy']) && $parameters['fuzzy']) {
                $where .= ' AND agent Like ?';
                $params[] = '%' . $account . '%';
            } else {
                $where .= ' AND agent=?';
                $params[] = $account;
            }
        }
        if ('agent' == $type) {
            $where .= ' AND status > 0';
        }

        $sql = 'SELECT * FROM gnt_agent_commision WHERE 1 ' . $where . ' ORDER BY member_count DESC';
        $result = $this->_db->query($sql, $params);
        return $result;
    }

    /**
     * [agentMemberReport 會員佣金報告]
     * @param  [type] $parameters [description]
     * @return [array]             [description]
     */
    public function agentMemberReport($parameters)
    {
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $agent = isset($parameters['agent']) ? $parameters['agent'] : '';
        $category = isset($parameters['category']) ? $parameters['category'] : '';
        $unique_member_id = isset($parameters['unique_member_id']) ? $parameters['unique_member_id'] : '';
        $page        = isset($parameters['page']) ? intval($parameters['page']) : 1;
        $page_size   = isset($parameters['page_size']) ? $parameters['page_size'] : 20;


        $data = array();
        if ($unique_member_id && is_array($unique_member_id)) {
            $unique_member_id = join(',', $unique_member_id);
        }

        if (isset($_SESSION['unique_member_id'])) {
            unset($_SESSION['unique_member_id']);
        }

        $where = '';
        $params = array();
        if ($agent) {
            $where = ' AND agent=? ';
            $params[] = $agent;
        }
        if ($category > 0) {
            $where .= ' AND category=? ';
            $params[] = $category;
        }

        if ($unique_member_id) {
            $where .= " AND member_id IN ($unique_member_id)";
        } else {
            //總頁數計算
            $sql      = 'SELECT count(*) as count FROM uc_member WHERE 1 '.$where;
            $result   = $this->_db->query($sql,$params);
            $res_data['total_page'] = ceil($result[0]['count'] / $page_size);

            $limit = (($page - 1) * $page_size) . ' , ' . $page_size;
            $where .= 'LIMIT ' . $limit;
        }

        $sql = 'SELECT member_id, account, agent, amount,register_time,login_time FROM uc_member WHERE 1 ' . $where;
        $result = $this->_db->query($sql, $params);

        $fields = array(
            'amount' => 0,
            'lottery_valid_bet' => 0,
            'total_valid_bet' => 0,
            'lottery_win' => 0,
            'lottery_water' => 0,
            'human_water' => 0,
            'total_water' => 0,
            'deposit' => 0,
            'human_deposit' => 0,
            'other_deposit' => 0,
            'withdrawal' => 0,
            'award_poundage' => 0,
            'other_charge' => 0,
        );

        $members = '';
        $accounts = '';
        foreach ($result as $row) {
            $members .= $row['member_id'] . ',';
            $accounts .= '"' . $row['account'] . '",';
            $data[$row['member_id']] = $fields;
            $data[$row['member_id']]['account'] = $row['account'];
            $data[$row['member_id']]['amount'] = $row['amount'];
            $data[$row['member_id']]['agent'] = $row['agent'];
            $data[$row['member_id']]['amount'] = $row['amount'];
            $data[$row['member_id']]['register_time'] = $row['register_time'];
            $data[$row['member_id']]['login_time'] = $row['login_time'];
        }
        $members = trim($members, ',');
        $accounts = trim($accounts, ',');
        $member_data = array();
        $member_to_id = array();
        $member_params = array();
        foreach ($result as $row) {
            $member_data[$row['member_id']] = $row['agent'];
            $member_to_id[$row['account']] = $row['member_id'];
        }
        // 存取款 1 线上存款  2 公司入款 3 在线取款 11 自动返水 14 人工返水  12自动彩金 15人工彩金
        $where = '';
        $params = array();
        if ($date_start) {
            $where .= ' AND submit_time >= ? ';
            $params[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND submit_time <= ? ';
            $params[] = $date_stop;
        }
        if ($members) {
            $where .= ' AND member_id IN(' . $members . ') ';
        }
        $sql = 'SELECT member_id, type, subtype, amount FROM fnn_record WHERE 1 ' . $where . ' AND subtype IN(1,2,11,12,13,14,15,16)';
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $member_id = $row['member_id'];
            if (isset($data[$member_id])) {
                // 取款不从fnn_record里统计 单独另算
                switch ($row['subtype']) {
                    case 1:
                    case 2:
                        $data[$member_id]['deposit'] += $row['amount'];
                        break;
                    //case 3:
                    //$data[$member_id]['withdrawal'] += $row['amount'];
                    //break;
                    case 11: // 自动返水

                        if (2 == $row['type']) {
                            $data[$member_id]['lottery_water'] += $row['amount'];
                        }
                        $data[$member_id]['total_water'] += $row['amount'];
                        break;
                    case 14:
                        // 人工返水
                        $data[$member_id]['human_water'] += $row['amount'];
                        break;
                    case 12: // 自动彩金
                    case 15: // 人工彩金
                        $data[$member_id]['award_poundage'] += $row['amount'];
                        break;
                    case 13:
                        // 人工入款
                        $data[$member_id]['human_deposit'] += $row['amount'];
                        break;
                    case 16:
                        //其它额度添加
                        $data[$member_id]['other_deposit'] += $row['amount'];
                        break;
                }
            }
        }

        // 彩票
        $where = '';
        $params = array();
        if ($date_start) {
            $where .= ' AND bet_time >= ? ';
            $params[] = date('Y-m-d H:i:s', strtotime($date_start));
        }
        if ($date_stop) {
            $where .= ' AND bet_time <= ? ';
            $params[] = date('Y-m-d H:i:s', strtotime($date_stop));
        }
        if ($members) {
            $where .= ' AND member_id IN(' . $members . ')';
        }
        $sql = 'SELECT member_id, SUM(win_amount) AS win_amount, SUM(valid_amount) AS valid_amount FROM ltt_bet WHERE 1 ';
        $sql .= $where . ' AND status=2 GROUP BY member_id';
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $member_id = $row['member_id'];
            if (isset($data[$member_id])) {
                $data[$member_id]['lottery_valid_bet'] += $row['valid_amount'];
                $data[$member_id]['lottery_win'] += $row['win_amount'];
            }
        }

        // 在线取款
        $where = '';
        $params = array();
        if ($date_start) {
            $where .= ' AND submit_time >= ? ';
            $params[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND submit_time <= ? ';
            $params[] = $date_stop;
        }
        if ($members) {
            $where .= ' AND member_id IN(' . $members . ') ';
        }
        $sql = 'SELECT member_id, amount FROM fnn_withdrawal WHERE status=2 ' . $where;
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            if (isset($data[$row['member_id']])) {
                $data[$row['member_id']]['withdrawal'] -= $row['amount']; // fnn_withdrawal里取款金额是正数 这里把它变成负数
            }
        }

        $res_data['list'] = $data;
        $res_data['page'] = $page;
        return $res_data;
    }

    public function agentMemberReportCount($parameters)
    {
        $agent = isset($parameters['agent']) ? $parameters['agent'] : '';
        $category    = isset($parameters['category']) ? $parameters['category'] : '';

        $where  = '';
        $params = array();
        if ($agent) {
            $where     = ' AND agent=? ';
            $params[]  = $agent;
        }

        if ($category > 0) {
            $where   .= ' AND category=? ';
            $params[] = $category;
        }

        $sql      = 'SELECT count(*) as count FROM uc_member WHERE 1 ' . $where;
        $result   = $this->_db->query($sql, $params);

        return $result[0]['count'];
    }

    /**
     * [agentReport 代理报表总计]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
    public function agentReport($parameters)
    {
        $date_start = isset($parameters['date_start']) ? $parameters['date_start'] : '';
        $date_stop = isset($parameters['date_stop']) ? $parameters['date_stop'] : '';
        $category = isset($parameters['category']) ? $parameters['category'] : '';
        $agent = isset($parameters['agent']) ? $parameters['agent'] : '';
        $agent_data = array();

        $params = array();
        $sql = 'SELECT account,real_name FROM gnt_agent WHERE 1 ';
        if ($agent) {
            $sql .= ' AND account=?';
            $params[] = $agent;
        }
        $result = $this->_db->query($sql, $params);

        // total到外面去计算吧 可以省掉许多+-运算的消耗
        foreach ($result as $row) {
            $agent_data[$row['account']] = array(
                'account' => $row['account'],
                'real_name' => $row['real_name'],
                'child' => array(),
                'lottery_valid_bet' => 0,
                'ag_valid_bet' => 0,
                'agq_valid_bet' => 0,
                'lebo_valid_bet' => 0,
                'ds_valid_bet' => 0,
                'ab_valid_bet' => 0,
                'bbin_valid_bet' => 0,
                'elc_mg_valid_bet' => 0,
                'elc_bbin_valid_bet' => 0,
                'lottery_win' => 0,
                'ag_win' => 0,
                'agq_win' => 0,
                'lebo_win' => 0,
                'ds_win' => 0,
                'ab_win' => 0,
                'bbin_win' => 0,
                'elc_mg_win' => 0,
                'elc_bbin_win' => 0,
                'lottery_water' => 0,
                'human_water' => 0,
                'deposit' => 0,
                'online_deposit' => 0,
                'human_deposit' => 0,
                'other_deposit' => 0,
                'withdrawal' => 0,
                'award_poundage' => 0,
                'other_charge' => 0,
            );
        }
        unset($result);

        /*$sql = 'SELECT member_id, account, agent FROM uc_member WHERE 1 ';
        $params = array();
        if($category > 0) {
        $sql .= ' AND category=? ';
        $params[] = $category;
        }
        $result   = $this->_db->query($sql, $params);
        $members  = '';
        $accounts = '';
        foreach($result as $row) {
        $members  .= '"' . $row['member_id'] . '",';
        $accounts .= '"' . $row['account'] . '",';
        if(isset($agent_data[$row['agent']]) && !isset($agent_data[$row['agent']]['child'][$row['member_id']])) {
        $agent_data[$row['agent']]['child'][$row['member_id']] = array('valid_bet' => 0);
        }
        }
        $members  = trim($members, ',');
        $accounts = trim($accounts, ',');

        $member_data = array();
        $member_to_id = array();
        foreach($result as $row) {
        $member_data[$row['member_id']] = $row['agent'];
        $member_to_id[$row['account']]  = $row['member_id'];
        }
        unset($result);*/
        // 返水 存取款 1 线上存款  2 公司入款 3 在线取款 11 自动返水 14 人工返水
        $where = '';
        $params = array();
        if ($date_start) {
            $where .= ' AND a.submit_time >= ? ';
            $params[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND a.submit_time <= ? ';
            $params[] = $date_stop;
        }
        if ($agent) {
            $where .= ' AND b.agent=? ';
            $params[] = $agent;
        }
        /*if($members) {
        $where .= ' AND member_id IN(' . $members . ') ';
        }*/
        $sql = 'SELECT a.member_id,a.type,a.subtype,a.amount,b.account,b.agent FROM fnn_record AS a ';
        $sql .= ' LEFT JOIN uc_member AS b ON a.member_id=b.member_id ';
        $sql .= ' WHERE 1 ' . $where . 'AND a.subtype IN(1,2,11,12,13,14,15,16)';
        $result = $this->_db->query($sql, $params);

        $uniq_member = array();
        $total_count = 0;

        foreach ($result as $row) {
            //$agent_account = isset($member_data[$row['member_id']]) ? $member_data[$row['member_id']] : '';
            $agent_account = $row['agent'];
            if ($agent_account && isset($agent_data[$agent_account])) {
                switch ($row['subtype']) {
                    case 1:
                        $agent_data[$agent_account]['online_deposit'] += $row['amount'];
                        break;
                    case 2:
                        $agent_data[$agent_account]['deposit'] += $row['amount'];
                        break;
                    //case 3:
                    //$agent_data[$agent_account]['withdrawal'] += abs($row['amount']);
                    //break;
                    case 11: // 自动返水
                        if (2 == $row['type']) {
                            $agent_data[$agent_account]['lottery_water'] += $row['amount'];
                        }
                        break;
                    case 14:
                        // 人工返水
                        $agent_data[$agent_account]['human_water'] += $row['amount'];
                        break;
                    case 12: // 自动彩金
                    case 15: // 人工彩金
                        $agent_data[$agent_account]['award_poundage'] += $row['amount'];
                        break;
                    case 13:
                        // 人工入款
                        $agent_data[$agent_account]['human_deposit'] += $row['amount'];
                        break;
                    case 16:
                        //其它额度添加
                        $agent_data[$agent_account]['other_deposit'] += $row['amount'];
                        break;
                }

                if (!in_array($row['member_id'], $uniq_member)) {
                    $total_count += 1;
                    $uniq_member[] = $row['member_id'];
                }
            }
        }

        // 彩票
        $where  = '';
        $params = array();
        if($date_start) {
            $where     .= ' AND a.bet_time >= ? ';
            $params[]   = date('Y-m-d H:i:s', strtotime($date_start) + 43200);
        }
        if($date_stop) {
            $where     .= ' AND a.bet_time <= ? ';
            $params[]   = date('Y-m-d H:i:s', strtotime($date_stop) + 43200);
        }
        if($agent) {
            $where    .= ' AND b.agent=? ';
            $params[]  = $agent;
        }
        /*if($members) {
            $where .= ' AND member_id IN(' . $members . ') ';
        }*/
        if($agent) {
            $where .= ' AND b.agent=? ';
            $params[] = $agent;
        }
        $sql  = 'SELECT a.member_id, SUM(a.win_amount) AS win_amount, SUM(a.valid_amount) AS valid_amount,b.account,b.agent FROM ltt_bet AS a ';
        $sql .= ' LEFT JOIN uc_member AS b ON a.member_id=b.member_id WHERE 1 ';
        $sql .= $where . ' AND a.status=2 GROUP BY a.member_id';
        $result = $this->_db->query($sql, $params);
        foreach($result as $row) {
            //$agent_account = isset($member_data[$row['member_id']]) ? $member_data[$row['member_id']] : '';
            $agent_account = $row['agent'];
            if($agent_account && isset($agent_data[$agent_account])) {
                if(!isset($agent_data[$agent_account]['child'][$row['member_id']]['valid_bet'])) {
                    $agent_data[$agent_account]['child'][$row['member_id']]['valid_bet'] = 0;
                }
                $agent_data[$agent_account]['child'][$row['member_id']]['valid_bet'] += $row['valid_amount'];
                
                $agent_data[$agent_account]['lottery_valid_bet'] += $row['valid_amount'];
                $agent_data[$agent_account]['lottery_win'] += $row['win_amount'];
            }
        }
        unset($result);

        // 在线取款
        $where = '';
        $params = array();
        if ($date_start) {
            $where .= ' AND a.submit_time >= ? ';
            $params[] = $date_start;
        }
        if ($date_stop) {
            $where .= ' AND a.submit_time <= ? ';
            $params[] = $date_stop;
        }
        if ($agent) {
            $where .= ' AND b.agent=? ';
            $params[] = $agent;
        }
        $sql = 'SELECT a.amount,b.agent, b.member_id FROM fnn_withdrawal AS a LEFT JOIN uc_member AS b on a.member_id=b.member_id WHERE a.status=2 ' . $where;
        $result = $this->_db->query($sql, $params);
        foreach ($result as $row) {
            $agent_data[$row['agent']]['withdrawal'] -= $row['amount'];

            if (!in_array($row['member_id'], $uniq_member)) {
                $total_count += 1;
                $uniq_member[] = $row['member_id'];
            }
        }
        unset($result);
        //unset($result, $members, $accounts, $member_data, $member_to_id);
        $agent_data[$agent]['total_count'] = $total_count;
        $_SESSION['unique_member_id'] = $uniq_member;
        return $agent_data;
    }

    /**
     * [getAgentCommisionConfig 依照盈利金額與會員數獲取退佣比例值]
     * @param  [array] $data         [gnt_commsion_config list]
     * @param  [integer] $win_amount   [盈利金額]
     * @param  [integer] $member_count [會員數]
     * @return [array]               [description]
     */
    public function getAgentCommisionConfig($data, $win_amount, $member_count)
    {
        if(empty($data))
            return false;

        $vals = array();
        foreach ($data as $key => $row) {
            $vals[$key] = $row['win_min'];
        }
        array_multisort($vals, SORT_ASC, $data);
        $index = -1;
        $win_amount = abs($win_amount);
        foreach ($data as $key => $row) {
            if ($win_amount >= $row['win_min']) {
                if ($member_count >= $row['member_count']) {
                    $index = $key;
                } else {
                    break;
                }
            }
        }
        if ($index >= 0) {
            return $data[$index];
        }
        return false;
    }

    /**
     * [getCommisionConfigList description]
     * @param  integer $config_id [description]
     * @return [type]             [description]
     */
    public function getCommisionConfigList($config_id = 0)
    {
        $sql  = 'SELECT config_id,win_min, win_max, member_count, lottery_commision_rate, ';
        $sql .= 'lottery_return_rate, sport_commision_rate, casino_commision_rate, electronic_commision_rate ';
        $sql .= 'sport_return_rate, ';
        $sql .= 'FROM gnt_commision_config ';

        if ($config_id) {
            $sql .= ' WHERE config_id=?';
            $result = $this->_db->query($sql, array($config_id));
            $result = isset($result[0]) ? $result[0] : false;
        } else {
            $sql .= ' ORDER BY win_min ASC ';
            $result = $this->_db->query($sql, array());
        }
        
        return $result;
    }

    /**
     * [addCommisionConfig description]
     * @param [type] $parameters [description]
     * @param [type] $config_id  [description]
     */
    public function addCommisionConfig($parameters, $config_id)
    {
        $where = '';
        $params = array(
            isset($parameters['win_min']) ? $parameters['win_min'] : 0,
            isset($parameters['win_max']) ? $parameters['win_max'] : 0,
            isset($parameters['member_count']) ? $parameters['member_count'] : 0,
            isset($parameters['lottery_commision_rate']) ? $parameters['lottery_commision_rate'] : 0,
            isset($parameters['lottery_return_rate']) ? $parameters['lottery_return_rate'] : 0,
        );
        if ($config_id > 0) {
            $sql = 'UPDATE gnt_commision_config ';
            $params[] = $config_id;
            $where = ' WHERE config_id=?';
        } else {
            $sql = 'INSERT INTO gnt_commision_config ';
        }
        $sql .= ' SET win_min=?, win_max=?, member_count=?, lottery_commision_rate=?,';

        $sql .= ' lottery_return_rate=? ' . $where;

        $result = $this->_db->query($sql, $params);
        return $result;
    }

    /**
     * [delCommisionConfig description]
     * @param  [type] $config_id [description]
     * @return [type]            [description]
     */
    public function delCommisionConfig($config_id)
    {
        $sql = 'DELETE FROM gnt_commision_config WHERE config_id=?';
        $result = $this->_db->query($sql, array($config_id));
        return $result;
    }

    /**
     * [addNewNumber 新建一期佣金結算]
     * @param [type] $number [description]
     * @return [array]       [description]
     */
    public function addNewNumber($number)
    {
        $format_num = explode('-', $number);
        $number = intval(str_replace('-', '', $number));
        $now_month = intval(date('Ym'));
        $error = '';
        if ($number) {
            // 確認有無未結佣金
            $sql = 'SELECT commision_id FROM gnt_agent_commision WHERE status=0';
            $result = $this->_db->query($sql, array());
            if ($result) {
                $error = 'have undone record';
                return array('error' => $error);
            }
            // 上個月才可執行計算
            if ($number < $now_month) {
                // 已經結算過確認
                $sql = 'SELECT month FROM gnt_agent_commision WHERE month=? LIMIT 1';
                $result = $this->_db->query($sql, array($number));
                if ($result) {
                    $error = 'commision number already have';
                } else {
                    $start = $format_num[0] . '-' . $format_num[1] . '-01';
                    $stop = date('Y-m-d', strtotime($start . ' +1 month -1 day'));
                    $params = array(
                        'date_start' => $start,
                        'date_stop' => $stop,
                    );
                    $result = $this->agentReport($params);

                    $commision_config_list = $this->getCommisionConfigList();

                    $config_handle = new Config();
                    $config = $config_handle->getByName(array('DEPOSIT_POUNDAGE_RATE', 'WITHDRAWAL_POUNDAGE_RATE', 'VALID_MEMBER_AMOUNT'));
                    $deposit_rate = $config['DEPOSIT_POUNDAGE_RATE'] * 0.01; // 存款手续费比例
                    $withdrawal_rate = $config['WITHDRAWAL_POUNDAGE_RATE'] * 0.01; // 取款手续费比例
                    if ($result) {

                        // 上一期未达门槛 累积
                        $last_month = intval(substr($number, -2)) - 1;
                        if (0 == $last_month) {
                            $last_year = intval(substr($number, 0, 4)) - 1;
                            $last_month = $last_year . '12';
                        } else {
                            $last_month = $number - 1;
                        }
                        $sql = 'SELECT * FROM gnt_agent_commision WHERE month=? AND status=2';
                        $last_data = $this->_db->query($sql, array($last_month));
                        $last_agent_commision = array();
                        foreach ($last_data as $row) {
                            $last_agent_commision[$row['agent']] = $row;
                        }
                        unset($last_data);

                        $now_date = date('Y-m-d H:i:s');
                        foreach ($result as $row) {
                            // 獲取手續費比例
                            $agent_config_handle = new Agent();
                            $config = $agent_config_handle->getConfigByAccount($row['account']);
                            $deposit_rate = $config['DEPOSIT_POUNDAGE_RATE'] * 0.01; // 存款手续费比例
                            $withdrawal_rate = $config['WITHDRAWAL_POUNDAGE_RATE'] * 0.01; // 取款手续费比例
                            unset($agent_config_handle);
                            // 計算退佣金比例
                            $member_count = count($row['child']);
                            $commision_config = $this->getAgentCommisionConfig($commision_config_list, $row['lottery_win'], $member_count);
                            $status = 0;
                            Log::record('計算退佣金比例 |' . $row['account'] . ' | lottery_win:' . $row['lottery_win'] . ' | member_count:' . $member_count . ' | 結果:' . (isset($commision_config['lottery_commision_rate']) ? $commision_config['lottery_commision_rate'] : 0) . '%', 'info','lottery/agent_commision_');

                            if ($commision_config) {
                                $lottery_commision_rate = $commision_config['lottery_commision_rate'] * 0.01;
                                $lottery_valid_rate = $commision_config['lottery_return_rate'] * 0.01;
                            } else {
                                //$status                    = 2; // 未达门槛
                                $lottery_commision_rate = 0;
                                $lottery_valid_rate = 0;
                            }
                            if (isset($last_agent_commision[$row['account']])) {
                                $one_agent_commision = $last_agent_commision[$row['account']];
                                $unsettle_lottery_bet_amount = $one_agent_commision['unsettle_lottery_bet_amount'] + $one_agent_commision['lottery_bet_amount'];
                                $unsettle_lottery_win_amount = $one_agent_commision['unsettle_lottery_win_amount'] + $one_agent_commision['lottery_win_amount'];
                                $unsettle_charge = $one_agent_commision['unsettle_charge'];
                                $unsettle_charge += $one_agent_commision['deposit_poundage'] + $one_agent_commision['withdrawal_poundage'];
                                $unsettle_charge += $one_agent_commision['award_poundage'];
                                $unsettle_charge += $one_agent_commision['lottery_return_amount'];
                                unset($one_agent_commision);
                            } else {
                                $unsettle_lottery_bet_amount = 0;
                                $unsettle_lottery_win_amount = 0;
                                $unsettle_charge = 0;
                            }

                            $sql = 'INSERT INTO gnt_agent_commision SET agent=?, real_name=?, member_count=?, month=?, ';
                            $sql .= ' unsettle_lottery_bet_amount=?,';
                            $sql .= ' lottery_bet_amount=? , ';
                            $sql .= ' unsettle_lottery_win_amount=?,';
                            $sql .= ' lottery_win_amount=? , ';
                            $sql .= ' lottery_return_amount=?,';
                            $sql .= ' commision_rate=?, valid_bet_rate=?,';
                            $sql .= ' deposit_amount=?, withdrawal_amount=?, deposit_poundage=?, withdrawal_poundage=?, award_poundage=?,';
                            $sql .= ' unsettle_charge=?, commision_amount=?, submit_time=?, status=?';

                            $deposit_poundage = $row['deposit'] * $deposit_rate;
                            $withdrawal_poundage = $row['withdrawal'] * $withdrawal_rate;
                            $total_poundage = $deposit_poundage + $withdrawal_poundage + $row['total_water'] + $row['award_poundage'];
                            // $commision_amount    = -($row['sport_win'] * $sport_commision_rate + $row['casino_win'] * $casino_commision_rate + $row['electronic_win'] * $electronic_commision_rate);
                            $commision_amount += $row['lottery_valid_bet'] * $lottery_valid_rate;
                            $parameters = array(
                                $row['account'],
                                $row['real_name'],
                                $member_count,
                                $number,
                                $unsettle_lottery_bet_amount,
                                $row['lottery_valid_bet'],
                                $unsettle_lottery_win_amount,
                                $row['lottery_win'],
                                $row['lottery_water'],
                                ($lottery_commision_rate * 100),
                                ($lottery_valid_rate * 100 . '%,'),
                                $row['deposit'],
                                $row['withdrawal'],
                                $deposit_poundage,
                                $withdrawal_poundage,
                                $row['award_poundage'],
                                $unsettle_charge,
                                $commision_amount,
                                $now_date,
                                $status,
                            );
                            $this->_db->query($sql, $parameters);
                        }
                    }
                }
            } else {
                $error = 'commision is not in time';
            }
        } else {
            $error = 'commision number error';
        }

        return array('error' => $error);
    }

    public function updateCommisionStatus($status, $commision_id)
    {
        $commision_id = explode(',', $commision_id);
        $id = '';
        foreach ($commision_id as $val) {
            $id .= intval($val) . ',';
        }
        $id = trim($id, ',');
        $sql = 'UPDATE gnt_agent_commision SET status=? WHERE commision_id IN(' . $id . ') AND status=0';
        $result = $this->_db->query($sql, array($status));
        return $result;
    }

    public function updateBankInfo($commision_id, $final_commision, $bank_info, $note, $operater)
    {
        $sql = 'UPDATE gnt_agent_commision SET final_commision=?, bank_info=?, note=?, operator=?, status=4 WHERE commision_id=?';
        $result = $this->_db->query($sql, array($final_commision, $bank_info, $note, $operater, $commision_id));
        return $result;
    }

    public function chargeUpdate($id, $poundage, $other_charge)
    {
        $sql = 'UPDATE gnt_agent_commision SET poundage=?, other_charge=? WHERE commision=? AND status=0';
        $result = $this->_db->query($sql, array($poundage, $other_charge, $id));
        return $result;
    }

    public function lockUpdate($id)
    {
        $sql = 'UPDATE gnt_agent_commision SET status=1 WHERE commision=? AND status=0';
        $result = $this->_db->query($sql, array($id));
        return $result;
    }

    public function unLockUpdate($id)
    {
        $sql = 'UPDATE gnt_agent_commision SET status=0 WHERE commision=? AND status=1';
        $result = $this->_db->query($sql, array($id));
        return $result;
    }

}
