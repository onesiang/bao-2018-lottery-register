<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 商户名,不得与其他用户相同,必须为一码。
 */
define('CUSTOMER_NAME', 'wwin_lottery');

/**
 * 系统默认美国东部时间，该时间比北京时间晚12小时。
 */
//ini_set('date.timezone', 'America/Grenada');
ini_set('date.timezone', 'Asia/Taipei');

//ini_set('session.cookie_httponly', 1);

/**
 * 定义数据库配置
 */
define('DB_HOST', '172.17.0.1');
define('DB_USER', 'wwin');
define('DB_PASS', '1qaz@WSX');
//define('DB_PASS', 'root@abc.com');
define('DB_NAME', 'wwin');

define('LotteryKJPort', 1234);
define('LotteryKjIp', 'localhost');

/**
 *  session 配置
 * ** */
define('SESSION_TIMEOUT', 1500); //1hour
define('SESSION_NAME', CUSTOMER_NAME); //SESSION命名

/**
 * PC無法進入彩票頁面
 */
define('PC_BLOCK', false);

/**
 * 定义缓存配置
 */
define('CACHE_HOST', 'redis_cluster');
define('CACHE_PORT', '7000');
define('CACHE_DB', 1);
define('CACHE_DB_CLUSTER', 1);


/**
 * azure storage資訊,上傳圖片
 */
define('AZURE_ACCOUNT_NAME', 'baoqatdiag');
define('AZURE_ACCOUNT_KEY', 'RK5GYRfJaacoXam++WWyaC4GFjqOptroeE57a3UJ4MsPItwKHoAndBTmhp1fQEOcyVx+rQTbdQXr9oPrnQFYag==');
define('AZURE_ACCOUNT_URL', 'https://'.AZURE_ACCOUNT_NAME.'.blob.core.windows.net');
define('AZURE_ACCOUNT_CONTARINER', 'lottery-upload');
// azure storage: true 使用, false 不使用
define('AZURE_UPLOAD', true);

/**
 * 调试模式开关
 */
define('DEBUG', false);

/**
 * 数据更新通知请求端口
 */
define('DATA_HOST', '127.0.0.1');
define('DATA_PORT', 8005);
/**
 * xhprof开关
 */
$xhprof_switch = DEBUG ? 1 : mt_rand(1, 1000);

/**
 * 设定xhprof记录时间，单位：毫秒。
 * 当脚本运行超过这个时间时记录
 */
$xhprof_record_time = 3000;

/**
 * 内存消耗记录开关
 */
$memory_usage_switch = DEBUG ? 1 : mt_rand(1, 1000);

/**
 * 设定内存消耗记录值，单位：MB
 * 当脚本运行超过这个值时记录
 */
$memory_record_cost = 100;

header('content-type: text/html; charset=utf-8');

/**
 * 設定sync 連線domain
 */
define('SYNC_HOST', '172.17.0.1');
define('SYNC_PORT', '6680');

/**
 * 要寫入檔案LOG等級(該數字以下的log(log等級))
 * EMERGENCE(0)<CRITICAL(1)<ALERT(2)<ERROR(3)<WARNING(4)<NOTICE(5)<INFO(6)<DEBUG(7)<CUSTOM(8)<SPECIAL(9)
 */
define('WRITE_LOG_LEVEL', 3);

/**
 * graylog 配置
 */
define('GRAYLOG_HOST', "127.0.0.1");
define('GRAYLOG_PORT', 12201);
define('GRAYLOG_TYPE', 'TCP');
