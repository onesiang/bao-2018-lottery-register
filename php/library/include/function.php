<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

spl_autoload_register(function ($class_name) {
    global $class_list;
    if (!class_exists($class_name)) {
        require ROOT . DS . 'library' . DS . $class_list[$class_name];
    }
});

function kg_error_handler($code, $string, $file, $line)
{
    $message = $string . ': (file:' . $file . ', line:' . $line . ') code: ' . $code;
    Log::record($message, 'error', 'global/');
}

function kg_debug_backtrace()
{
    $debug_info = debug_backtrace();
    $return = array();
    foreach ($debug_info as $item) {
        if (isset($item['file']) && isset($item['line'])) {
            $return[] = array(
                'file' => $item['file'],
                'line' => $item['line'],
            );
        }
    }
    return $return;
}

function isMobileDevice()
{
    $aMobileUA = array(
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile',
    );

    foreach ($aMobileUA as $sMobileKey => $sMobileOS) {
        if (preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT'])) {
            return 0;
        }
    }
    return 1;
}

/**
 * 记录调试信息
 *
 * @param void
 * @return void
 */
function debug_log()
{
    if (!defined('NO_XHPROF')) {
        save_xhprof_data();
    }
    memory_usage_log();
}

/**
 * 记录xhprof日志
 *
 * @return void
 * @return void
 */
function save_xhprof_data()
{
    return true;
    // php7不支援，先跳過
    global $xhprof_switch, $xhprof_record_time;
    if (1 === $xhprof_switch) {
        $xhprof_data = xhprof_disable();
        $cost_time = round($xhprof_data['main()']['wt'] / 1000);
        if ($cost_time > $xhprof_record_time) {
            $script_name = str_replace('/', '-', $_SERVER['SCRIPT_NAME']);
            $file_name = ROOT . DS . 'log' . DS . sprintf('%05s', $cost_time) . '-' . date('YmdHis') . $script_name . '.xhprof';
            $handle = fopen($file_name, 'wb');
            if (function_exists('igbinary_serialize')) {
                fwrite($handle, igbinary_serialize($xhprof_data));
            } else {
                fwrite($handle, serialize($xhprof_data));
            }
            fclose($handle);
        }
    }
}

/**
 * 记录内存消耗日志
 *
 * @return void
 * @return void
 */
function memory_usage_log()
{
    global $memory_usage_switch, $memory_record_cost;
    $memory = byte_format(memory_get_usage(), 'MB', 2, false);
    if ($memory > $memory_record_cost) {
        $script_name = str_replace('/', '-', $_SERVER['SCRIPT_NAME']);
        $file_name = ROOT . DS . 'log' . DS . 'memory_usage_' . date('Y-m-d') . '.log';
        $content = $script_name . ' at ' . date('Y-m-d H:i:s') . ' : ' . $memory . ' MB';
        $handle = fopen($file, 'ab');
        fwrite($handle, $content);
        fclose($handle);
    }
}

/**
 * 字节格式化为MB、GB等方便识别的格式
 *
 * @param int $byte 字节数
 * @param string $unit 单位，可选值有“B、KB、MB、GB、TB、PB、EB、ZB、YB”，默认值“MB”。
 * @param int $decimal 小数点位数，默认2……
 * @param bool $suffix 是否添加单位，默认添加。
 * @return string 格式化后的值
 */
function byte_format($byte, $unit = 'MB', $decimal = 2, $suffix = true)
{
    $unit_list = array(
        'B' => 0,
        'KB' => 1,
        'MB' => 2,
        'GB' => 3,
        'TB' => 4,
        'PB' => 5,
        'EB' => 6,
        'ZB' => 7,
        'YB' => 8,
    );

    $value = 0;
    if ($byte > 0) {
        if (!array_key_exists($unit, $unit_list)) {
            $pow = floor(log($byte) / log(1024));
            $unit = array_search($pow, $unit_list);
        }

        $value = ($byte / pow(1024, floor($unit_list[$unit])));
    }

    if (!is_numeric($decimal) || $decimal < 0) {
        $decimal = 2;
    }

    $value = round($value, $decimal);
    if (true === $suffix) {
        $value .= $unit;
    }

    return $value;
}

function exceptionReturn($value, $msg)
{
    return "{$value}#{$msg}";
}

/**
 * 重新封装json_encode函数，使中文保持原状
 *
 * @param mixed $value 待编码的value
 * @return string 编码后的字符串
 */
function kg_json_encode($value = false)
{
    if (is_null($value)) {
        return '"NULL"';
    } elseif ($value === false) {
        return '"FALSE"';
    } elseif ($value === true) {
        return '"TRUE"';
    }
    if (is_scalar($value)) {
        if (is_float($value)) {
            return floatval(str_replace(",", ".", strval($value)));
        }

        if (is_string($value)) {
            $json_replaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
            $value = str_replace($json_replaces[0], $json_replaces[1], $value);
            /**
             * 0开头的数字在javascript下解析会导致无法解析，
             * 解析方案是碰到这样的数字转为字符串。
             */
            if (is_numeric($value)) {
                if ('0' == substr($value, 0, 1) && '.' !== substr($value, 1, 1)) {
                    return '"' . $value . '"';
                } else {
                    return $value;
                }
            } else {
                return '"' . $value . '"';
            }
        } else {
            return $value;
        }
    }

    $is_list = true;
    for ($i = 0, reset($value); $i < count($value); $i++, next($value)) {
        if (key($value) !== $i) {
            $is_list = false;
            break;
        }
    }

    $result = array();
    if ($is_list) {
        foreach ($value as $v) {
            $result[] = kg_json_encode($v);
        }
        return '[' . join(',', $result) . ']';
    } else {
        foreach ($value as $k => $v) {
            $result[] = '"' . $k . '":' . kg_json_encode($v);
        }
        return '{' . join(',', $result) . '}';
    }
}

function kg_get($key, $default = '')
{
    $value = isset($_GET[$key]) ? block_xss($_GET[$key]) : $default;
    return $value;
}

function kg_post($key, $default = '')
{
    $value = isset($_POST[$key]) ? block_xss($_POST[$key]) : $default;
    return $value;
}

function kg_request($key, $default = '')
{
    $value = isset($_REQUEST[$key]) ? block_xss($_REQUEST[$key]) : $default;
    return $value;
}

function kg_echo($string)
{
    //echo htmlentities($string, ENT_NOQUOTES, 'UTF-8');
    echo $string;
}

function block_xss($string)
{
    $pattern = array(
        '/\\s+/',
        '/<(\\/?)(script|i?frame|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU',
        '/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU',
    );
    $str = preg_replace($pattern, '', $string);
    return addslashes($str);
}

function replace_htmlchars($str, $rec = '')
{
    $str = htmlspecialchars_decode($str);
    return preg_replace('/<(.*?)>/', $rec, $str);
}

function is_HTTPS()
{
    if (!isset($_SERVER['HTTPS'])) {
        return false;
    }
    if ($_SERVER['HTTPS'] === 1) { //Apache
        return true;
    } elseif ($_SERVER['HTTPS'] === 'on') { //IIS
        return true;
    } elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
        return true;
    }
    return false;
}

/**
 * 获取客户端IP
 *
 * @param void
 * @return string IP地址
 */
function get_client_ip()
{
    if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
        $ip = getenv('HTTP_CLIENT_IP');
    } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] && strcasecmp($_SERVER['HTTP_X_FORWARDED_FOR'], 'unknown')) {
        $ip = trim(array_shift(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])));
    } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
        $ip = getenv('REMOTE_ADDR');
    } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
        $ip = $_SERVER['REMOTE_ADDR'];
    } else {
        $ip = '0.0.0.0';
    }
    if ($ip) {
        $ip_data = explode(',', $ip);
        if (isset($ip_data[0])) {
            $ip = $ip_data[0];
        }
    }
    return $ip;
}

function get_domain()
{
    return isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
}

/**
 * 获取IP归属地信息
 *
 * @param string $ip ip
 * @return array 归属地信息
 * <code>
 * 0 - 国家、地区
 * 1 - 详细地址
 * </code>
 */
function ip_location($ip)
{
    $file_name = ROOT . DS . 'library' . DS . 'include' . DS . 'qqwry.dat';
    if (false === ($handle = fopen($file_name, 'rb'))) {
        return array('Invalid IP data file', '');
    }
    if (false === ($data_begin = fread($handle, 4)) ||
        false === ($data_end = fread($handle, 4))) {
        return array('Invalid IP data file', '');
    }
    $ip_begin = implode('', unpack('L', $data_begin));
    if ($ip_begin < 0) {
        $ip_begin += pow(2, 32);
    }
    $ip_end = implode('', unpack('L', $data_end));
    if ($ip_end < 0) {
        $ip_end += pow(2, 32);
    }
    $begin_number = 0;
    $end_number = ($ip_end - $ip_begin) / 7 + 1;

    $ip_long = ip2long($ip);

    $ip_number_1 = 0;
    $ip_number_2 = 0;
    $ip_address_1 = '';
    $ip_address_2 = '';

    while ($ip_number_1 > $ip_long || $ip_number_2 < $ip_long) {
        $middle = intval(($end_number + $begin_number) / 2);

        fseek($handle, $ip_begin + 7 * $middle);
        $ip_data_1 = fread($handle, 4);
        if (strlen($ip_data_1) < 4) {
            fclose($handle);
            return array('System Error 1', '');
        }
        $ip_number_1 = implode('', unpack('L', $ip_data_1));
        if ($ip_number_1 < 0) {
            $ip_number_1 += pow(2, 32);
        }
        if ($ip_number_1 > $ip_long) {
            $end_number = $middle;
            continue;
        }

        $data_seek = fread($handle, 3);
        if (strlen($data_seek) < 3) {
            fclose($handle);
            return array('System Error 2', '');
        }
        $data_seek = implode('', unpack('L', $data_seek . chr(0)));
        fseek($handle, $data_seek);
        $ip_data_2 = fread($handle, 4);
        if (strlen($ip_data_2) < 4) {
            fclose($handle);
            return array('System Error 3', '');
        }
        $ip_number_2 = implode('', unpack('L', $ip_data_2));
        if ($ip_number_2 < 0) {
            $ip_number_2 += pow(2, 32);
        }

        if ($ip_number_2 < $ip_long) {
            if ($middle == $begin_number) {
                fclose($handle);
                return array('Unknown', '');
            }
            $begin_number = $middle;
        }
    }

    $ip_flag = fread($handle, 1);
    if ($ip_flag == chr(1)) {
        $ipSeek = fread($handle, 3);
        if (strlen($ipSeek) < 3) {
            fclose($handle);
            return array('System Error 4', '');
        }
        $ipSeek = implode('', unpack('L', $ipSeek . chr(0)));
        fseek($handle, $ipSeek);
        $ip_flag = fread($handle, 1);
    }

    if ($ip_flag == chr(2)) {
        $address_seek_1 = fread($handle, 3);
        if (strlen($address_seek_1) < 3) {
            fclose($handle);
            return array('System Error 5', '');
        }
        $ip_flag = fread($handle, 1);
        if ($ip_flag == chr(2)) {
            $address_seek_2 = fread($handle, 3);
            if (strlen($address_seek_2) < 3) {
                fclose($handle);
                return array('System Error 6', '');
            }
            $address_seek_2 = implode('', unpack('L', $address_seek_2 . chr(0)));
            fseek($handle, $address_seek_2);
        } else {
            fseek($handle, -1, SEEK_CUR);
        }

        while (($char = fread($handle, 1)) != chr(0)) {
            $ip_address_2 .= $char;
        }

        $address_seek_1 = implode('', unpack('L', $address_seek_1 . chr(0)));
        fseek($handle, $address_seek_1);

        while (($char = fread($handle, 1)) != chr(0)) {
            $ip_address_1 .= $char;
        }
    } else {
        fseek($handle, -1, SEEK_CUR);
        while (($char = fread($handle, 1)) != chr(0)) {
            $ip_address_1 .= $char;
        }

        $ip_flag = fread($handle, 1);
        if ($ip_flag == chr(2)) {
            $address_seek_2 = fread($handle, 3);
            if (strlen($address_seek_2) < 3) {
                fclose($handle);
                return array('System Error 7', '');
            }
            $address_seek_2 = implode('', unpack('L', $address_seek_2 . chr(0)));
            fseek($handle, $address_seek_2);
        } else {
            fseek($handle, -1, SEEK_CUR);
        }
        while (($char = fread($handle, 1)) != chr(0)) {
            $ip_address_2 .= $char;
        }
    }
    fclose($handle);

    if (preg_match('/http/i', $ip_address_2)) {
        $ip_address_2 = '';
    }
    $search = array(
        '/CZ88\.NET/is',
        '/^\s*/is',
        '/\s*$/is',
    );
    $ip_address_1 = preg_replace($search, '', $ip_address_1);
    $ip_address_1 = iconv('GB2312', 'UTF-8//TRANSLIT//IGNORE', $ip_address_1);
    $ip_address_2 = preg_replace($search, '', $ip_address_2);
    $ip_address_2 = iconv('GB2312', 'UTF-8//TRANSLIT//IGNORE', $ip_address_2);

    return array($ip_address_1, $ip_address_2);
}

/**
 * 货币转为数据库存储格式，单位为分
 *
 * @param int $amount 待转换的数值
 * @return int 转换后的数值
 */
function money_to_db($amount)
{
    return $amount * 100;
}

/**
 * 数据库存储的货币转为通用格式，单位为元
 *
 * @param int $amount 待转换的数值
 * @return int 转换后的数值
 */
function money_from_db($amount)
{
    return round($amount / 100, 2);
}

/**
 * 获取北京时间
 *
 * @param int $time 美东时间
 * @return int 北京时间
 */
function get_bj_time($time = '')
{
    if (!$time) {
        $time = time();
    }
    // $time += 43200;
    $time += 0;
    return $time;
}

function get_bj_date($time = 0)
{
    $time = get_bj_time($time);
    return date('Y-m-d H:i:s', $time);
}

function get_bj_day($time = 0)
{
    $time = get_bj_time($time);
    return date('Y-m-d', $time);
}

/**
 * 获取北京时间戳
 *
 * @param string $str 文本格式时间
 * @return int 时间戳
 */
function bj_strtotime($str)
{
    $time = strtotime($str);
    $time -= 0;
    // $time -= 43200;
    return $time;
}

/**
 * 美东->北京时间
 *
 */
function et_to_bj_date($str)
{
    $time = strtotime($str);
    // $time +=43200;
    $time += 0;
    return date('Y-m-d H:i:s', $time);
}

/**
 * 北京时间->美东
 *
 */
function bj_to_et_date($str)
{
    $time = strtotime($str);
    // $time -=43200;
    $time -= 0;
    return date('Y-m-d H:i:s', $time);
}

/**
 * 获取限制的时间起始日期
 *
 */
function get_limit_start_date($date_start, $date_stop = '', $limit_day = 14)
{
    $time = $date_stop ? strtotime($date_stop) : time();
    $limit_date = $date_start;
    $diff_time = $time - strtotime($date_start);
    $time_long = 86400 * $limit_day; // 最大范围
    if ($diff_time > $time_long) {
        $limit_date = date('Y-m-d', $time - $time_long);
    }
    return $limit_date;
}

/**
 * 更换索引
 * 将数组的索引改为指定键名的值
 *
 * @param array $array 待更换索引的数组
 * @param string $index 指定的键名
 * @return array 更新索引后的数组
 */
function change_array_index($array, $index)
{
    $return = array();
    if (is_array($array)) {
        foreach ($array as $row) {
            $return[$row[$index]] = $row;
        }
    }
    return $return;
}

/**
 * 验证日期
 *
 * @param string $date 待验证日期，可验证如“2000-01-01”和“2000/01/01”这样的格式。
 * @return bool 是正确的日期返回TRUE，反之返回FALSE
 */
function check_date($date)
{
    $flag = '';
    if (strpos($date, '-')) {
        $flag = '-';
    } elseif (strpos($date, '/')) {
        $flag = '/';
    }
    $date_array = explode($flag, $date);
    return checkdate($date_array[1], $date_array[2], $date_array[0]);
}

/**
 * 获取uid
 *
 * @param void
 * @return string uid
 */
function get_uid()
{
    $uid = isset($_GET['uid']) ? $_GET['uid'] : '';
    //$uid = (isset($_SESSION[SESSION_NAME.'_user_uid']))? $_SESSION[SESSION_NAME.'_user_uid'] : '';
    if (!preg_match('/^[0-9a-z]+$/i', $uid)) {
        $uid = '';
    }
    return $uid;
}

/**
 *  從 session 獲取 uid
 * ** */
function get_session_uid()
{
    $uid = (isset($_SESSION[SESSION_NAME . '_user_uid'])) ? $_SESSION[SESSION_NAME . '_user_uid'] : '';
    if (!preg_match('/^[0-9a-z]+$/i', $uid)) {
        $uid = '';
    }
    return $uid;
}

function get_cdn()
{
    $cdn = isset($_GET['cdn']) ? $_GET['cdn'] : '';
    if (!$cdn) {
        $cdn = (isset($_COOKIE['cdn']) && $_COOKIE['cdn']) ? $_COOKIE['cdn'] : '';
    }
    return $cdn;
}

/**
 * 汉字转unicode

 *@param string $word 待转字符串
 *@return string 转换后字符串
 */
function uni_encode($word)
{
    $word = json_encode($word);
    $word = preg_replace_callback('/\\\\u(\w{4})/', create_function('$hex', 'return \'&#\'.hexdec($hex[1]).\';\';'), substr($word, 1, strlen($word) - 2));
    return $word;
}

/**
 * unicode转汉字
 *
 * @param string $unicode 待转字符串
 * @return string 转换后字符串
 */
function uni_decode($unicode)
{
    if (preg_match('/(&#\d{5};)/', $unicode)) {
        $unicode = json_encode($unicode);
        $word = json_decode(preg_replace_callback('/&#(\d{5});/', create_function('$dec', 'return \'\\u\'.dechex($dec[1]);'), $unicode));
    } else {
        $word = $unicode;
    }
    return $word;
}

/**
 * 通信传递过来的字符串参数是否可等值与int型
 * 如字符串值'123' 其值可等于 int型 123 返回true  '0123' 不同于 123 返回false;
 * @param string
 * @return boolean
 */
function is_stringint($value)
{
    $check = false;
    if (is_numeric($value)) {
        $new = floor($value)+'';
        $value += '';
        if ($new == $value) {
            $check = true;
        }
    }
    return $check;
}

/**
 * 匹配数字 字母 横杠 下划线
 *
 */
function checkLegal($val)
{
    return preg_match("/^[0-9a-zA-Z-_]+$/u", $val);
}

/**
 * 解析浏览器传过来的被escapte编码后的字符串
 *
 * @param string $str 待转字符串
 * @return string 转换后的字符串
 */
function js_unescape($str)
{
    $ret = '';
    $len = strlen($str);

    for ($i = 0; $i < $len; $i++) {
        if ($str[$i] == '%' && $str[$i + 1] == 'u') {
            $val = hexdec(substr($str, $i + 2, 4));

            if ($val < 0x7f) {
                $ret .= chr($val);
            } elseif ($val < 0x800) {
                $ret .= chr(0xc0 | ($val >> 6)) . chr(0x80 | ($val & 0x3f));
            } else {
                $ret .= chr(0xe0 | ($val >> 12)) . chr(0x80 | (($val >> 6) & 0x3f)) . chr(0x80 | ($val & 0x3f));
            }

            $i += 5;
        } elseif ($str[$i] == '%') {
            $ret .= urldecode(substr($str, $i, 3));
            $i += 2;
        } else {
            $ret .= $str[$i];
        }
    }
    return $ret;
}

function php_escape($str)
{
    preg_match_all("/[\x80-\xff].|[\x01-\x7f]+/", $str, $r);
    $ar = $r[0];
    foreach ($ar as $k => $v) {
        if (ord($v[0]) < 128) {
            $ar[$k] = rawurlencode($v);
        } else {
            $ar[$k] = "%u" . bin2hex(iconv("GB2312", "UCS-2", $v));
        }
    }
    return join("", $ar);
}

function order_id()
{
    $mt = round(microtime(true) * 1000) . mt_rand(100, 999);
    //$mt = round(microtime(TRUE) * 1000);
    //$mt = substr($mt, -10) . mt_rand(10, 99) . mt_rand(100, 999);
    return $mt;
}

function str_cut($str, $length, $extra = '...')
{
    $s = '';
    $i = 0;
    $l = 0;
    $str_length = strlen($str);
    $s = $str;
    $f = true;

    while ($i < $str_length) {
        if (ord($str[$i]) < 0x80) {
            $l++;
            $i++;
        } elseif (ord($str[$i]) < 0xe0) {
            $l++;
            $i += 2;
        } elseif (ord($str[$i]) < 0xf0) {
            $l += 2;
            $i += 3;
        } elseif (ord($str[$i]) < 0xf8) {
            $l += 1;
            $i += 4;
        } elseif (ord($str[$i]) < 0xfc) {
            $l += 1;
            $i += 5;
        } elseif (ord($str[$i]) < 0xfe) {
            $l += 1;
            $i += 6;
        }

        if (($l >= $length - 1) && $f) {
            $s = substr($str, 0, $i);
            $f = false;
        }

        if (($l >= $length) && ($i <= $str_length)) {
            $s = $s . $extra;
            break;
        }
    }

    return $s;
}

function time_diff($time1, $time2)
{
    return strtotime($time2) - strtotime($time1);
}

function isset_echo($array, $key, $default = '')
{
    $value = isset($array[$key]) ? $array[$key] : $default;
    return $value;
}

function hex_str($hex)
{
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    }
    return $string;
}

function str_hex($string)
{
    $hex = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $hex .= dechex(ord($string[$i]));
    }
    $hex = strtoupper($hex);
    return $hex;
}

/**
 * 自定义提示页
 *
 * @param string $title 标题
 * @param string $content 内容
 * @param string $url 链接
 * @param int $type 类型[1 - 警告; 2 - 成功; 3 - 出错]
 * @param boolean $auto_jump 是否自动跳转
 * @param string $target 目标框体[window - 本页; parent - 上级; top - 顶级]
 *
 */
function kg_dialog($title, $content, $url, $type = 2, $auto_jump = true, $target = 'window', $die_exit = true)
{
    $html = array();
    $html[] = '<!DOCTYPE html>';
    $html[] = '<html>';
    $html[] = '<head>';
    $html[] = '	<meta charset="utf-8" />';
    $html[] = '	<title></title>';
    $html[] = '	<style>';
    $html[] = '		body { margin: 0; padding: 0; width: 100%; height: 100%;}';
    $html[] = '		.tip { position: absolute; left: 50%; top: 50%; margin-left: -294px; margin-top: -154px; width: 588px; height: 308px; background: url(/img/public/tip_bg.png);}';
    $html[] = '		.tip_title { position: absolute; top: 36px; left: 45px; width: 500px; text-align: center; font-size: 24px; font-weight: bold; }';
    $html[] = '		.tip_content { position: absolute; top: 130px; left: 158px; width: 460px; text-left: center; font-size: 14px; font-weight: bold; }';
    $html[] = '		.tip_url { position: absolute; top: 180px; left: 128px; width: 460px; text-align: center; font-size: 14px; font-weight: bold; }';
    $html[] = '		.tip_url a { color: #800;}';
    $html[] = '		.tip_icon_1,';
    $html[] = '		.tip_icon_2,';
    $html[] = '		.tip_icon_3 { position: absolute; top: 106px; left: 36px; width: 128px; height: 128px; }';
    $html[] = '		.tip_icon_1 { background: url(/img/public/tip_icon_1.png);}';
    $html[] = '		.tip_icon_2 { background: url(/img/public/tip_icon_2.png);}';
    $html[] = '		.tip_icon_3 { background: url(/img/public/tip_icon_3.png);}';
    $html[] = '	</style>';
    $html[] = '</head>';
    $html[] = '<body>';
    $html[] = '<div class="tip">';
    $html[] = '	<div class="tip_title">' . $title . '</div>';
    $html[] = '	<div class="tip_icon_' . $type . '"></div>';
    $html[] = '	<div class="tip_content">' . $content . '</div>';
    if ($url) {
        switch ($target) {
            case 'window':
                $_target = '_self';
                break;
            case 'parent':
                $_target = '_parent';
                break;
            case 'top':
                $_target = '_top';
                break;
        }
        $html[] = '	<div class="tip_url"><a href="' . $url . '" target="' . $_target . '">返回</a></div>';
    }
    $html[] = '</div>';
    if ($url && $auto_jump) {
        $html[] = '<script>';
        $html[] = 'setTimeout(function() {';
        $html[] = $target . '.location.href = "' . $url . '";';
        $html[] = '}, 3000);';
        $html[] = '</script>';
    }
    $html[] = '</body>';
    $html[] = '</html>';
    echo implode('', $html);
    if ($die_exit) {
        exit();
    }
}

function kg_stop_pc($title, $content, $url, $type = 2, $auto_jump = true, $target = 'window', $die_exit = true)
{
    header("Location:block.php");
}

/**
 * 保证单进程
 *
 * @param string $process_ame 进程名
 * @param string $pid_file 进程文件路径
 * @return boolean 是否继续执行当前进程
 */
function single_process($process_name, $pid_file)
{
    if (file_exists($pid_file) && $fp = @fopen($pid_file, "rb")) {
        flock($fp, LOCK_SH);
        if (filesize($pid_file)) {
            $last_pid = fread($fp, filesize($pid_file));
        }
        fclose($fp);
        if (!empty($last_pid)) {
            // $command = exec("/bin/ps -p $last_pid -o command=");
            $command = exec("/bin/ps -eaf | grep {$last_pid}");
            if ($command == $process_name) {
                return false;
            }
        }
    }
    $cur_pid = posix_getpid();

    if ($fp = @fopen($pid_file, "wb")) {
        fputs($fp, $cur_pid);
        ftruncate($fp, strlen($cur_pid));
        fclose($fp);
        return true;
    } else {
        return false;
    }
}

/**
 * 获取当前进程对应的Command
 *
 * @return string 命令及其参数
 */
function get_current_command()
{
    $pid = posix_getpid();
    $command = exec("/bin/ps -eaf | grep {$pid}");
    return $command;
}

function get_yesterday($date = '')
{
    if (!$date) {
        $yesterday = date('Y-m-d', strtotime('-1 day'));
    } else {
        $yesterday = date('Y-m-d', strtotime($date) - 86400);
    }
    return $yesterday;
}

function is_number($var)
{
    if ($var == (string) (float) $var) {
        return (bool) is_numeric($var);
    }
    if ($var >= 0 && is_string($var) && !is_float($var)) {
        return (bool) ctype_digit($var);
    }
    return (bool) is_numeric($var);
}

function random_string($length)
{
    $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $chars_length = strlen($chars);
    $string = array();
    for ($i = 0; $i < $length; $i++) {
        $string[] = $chars[mt_rand(0, $chars_length - 1)];
    }
    return implode('', $string);
}

function convertUrlQuery($query)
{
    $queryParts = explode('&', $query);
    $params = array();
    foreach ($queryParts as $param) {
        $item = explode('=', $param);
        $params[$item[0]] = $item[1];
    }
    return $params;
}

function kg_md5($var)
{
    return md5($var);
}

function curl_kj($url, $is_post)
{
    $url = str_replace(" ", '%20', $url);
    try {
        $ch = curl_init($url);
        if ($is_post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array());
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array('Accept: application/json', 'Content-Type: application/json')
            );
            curl_setopt(
                $ch,
                CURLOPT_USERAGENT,
                "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1"
            );

            header('Content-Type: application/json');
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);

        if (!$data) {
            Log::record('error => ' . curl_error($ch), 'error', 'settle/');
        }

        Log::record('data => ' . $data, 'info', 'settle/');
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        Log::record('http code => ' . $httpcode, 'info', 'settle/');
        curl_close($ch);
        return $data;
    } catch (Exception $ex) {
        Log::record($ex->getMessage(), 'error', 'settle/');
    }
}

// $key = 'bRuD5WYQBqE70nAuhU=w5wd0rdHR9yLlM6wt2vteuini';

function kg_decrypt($data)
{
    $encryption_key = base64_decode('bRuD5WYQBqE70nAuhU=w5wd0rdHR9yLlM6wt2vteuini');
    // $iv = (openssl_cipher_iv_length('aes-256-cbc'));
    // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    if (strlen($iv) % 16) {
        $iv = str_pad($iv, ((strlen($iv) + 16) - strlen($iv) % 16), "\0");
    }
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}

function kg_encrypt($data)
{
    $encryption_key = base64_decode('bRuD5WYQBqE70nAuhU=w5wd0rdHR9yLlM6wt2vteuini');
    // Generate an initialization vector
    $iv = (openssl_cipher_iv_length('aes-256-cbc'));
    // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
    return base64_encode($encrypted . '::' . $iv);
}

function json_validate($string)
{
    // decode the JSON data
    $result = json_decode($string);

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
            break;
        default:
            $error = 'Unknown JSON error occured.';
            break;
    }

    if ($error !== '') {
        // throw the Exception or exit // or whatever :)
        Log::record('error => ' . $error, 'error', 'json_error/');
        return false;
    }

    // everything is OK
    return $result;
}
