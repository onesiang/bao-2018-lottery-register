<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 定义系统根目录
 */
define('ROOT', dirname(dirname(dirname(__FILE__))));

/**
 * 定义系统分隔符
 */
define('DS', DIRECTORY_SEPARATOR);
/**
 * 定義彩票
 */
define('LOTTERY_SIX_MARK', 9);

require ROOT . DS . 'library' . DS . 'include' . DS . 'config.php';
require ROOT . DS . 'library' . DS . 'include' . DS . 'class_map.php';
require ROOT . DS . 'library' . DS . 'include' . DS . 'function.php';
require ROOT . DS . 'vendor' . DS . 'autoload.php';



if (!isset($_SESSION[SESSION_NAME.'_noticeid'])) {
    $_SESSION[SESSION_NAME.'_noticeid'] = 0 ;
}

/**
 * xhprof开关
 */
if (function_exists('xhprof_enable') && 1 === $xhprof_switch) {
    if (!defined('NO_XHPROF')) {
        xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
    }
}

set_error_handler('kg_error_handler', E_ALL);

register_shutdown_function('debug_log');

$g_db_handle = null;

use sentry\sentry;

// Raven_Autoloader::register();
// $client = new Raven_Client('https://0cb6a6e6026f4a66bba8f2da94dd9518:e6dcca40f5984a46a1ae1f9bbcacc2e5@sentry.io/265213');
// $error_handler = new Raven_ErrorHandler($client);
// $error_handler->registerExceptionHandler();
// $error_handler->registerErrorHandler(true, E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_USER_DEPRECATED);
// $error_handler->registerShutdownFunction();
