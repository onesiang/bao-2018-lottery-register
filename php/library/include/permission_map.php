<?php
$g_permission = array(
    'manager_list' => array(1, '查看管理员列表'),
    'manager_add' => array(2, '添加管理员'),
    'manager_edit' => array(3, '编辑管理员'),
    'manager_delete' => array(4, '删除管理员'),
    'manager_config' => array(5, '修改其他管理员的管理权限'),
    'manager_log_list' => array(6, '查看操作日志'),
    'manager_password' => array(7, '修改其他管理员密码'),
    'manager_self_password' => array(8, '修改自己的密码'),
    'manager_self_config' => array(9, '修改自己的管理权限'), //n
    'manager_report' => array(19, '查看管理员报表'),

    'agent_list' => array(10, '查看代理列表'),
    'agent_add' => array(11, '添加代理'),
    'agent_edit' => array(12, '编辑代理'),
    'agent_config_edit' => array(13, '修改设定'),
    'agent_config_list' => array(14, '查看设定'),
    'agent_commision_list' => array(14, '代理佣金查询'), //n
    'agent_commision_settle_list' => array(15, '查看佣金结算'),
    'agent_commision_create' => array(16, '佣金新建期数'),
    'agent_commision_opt' => array(17, '佣金结算操作'),
    'agent_commision_report' => array(18, '查看佣金报表'), //n

    'member_list' => array(20, '查看会员列表'),
    'member_add' => array(21, '添加会员'),
    'member_edit' => array(22, '编辑会员其它数据/如：同步额度'),
    'member_delete' => array(23, '删除会员'), //n
    'member_online' => array(24, '查看会员在线列表'),
    'member_kickoff' => array(25, '踢人'),
    'member_finance_list' => array(26, '查看会员对账列表'), //n
    'member_finance_edit' => array(27, '编辑会员对账'),
    'member_level_list' => array(28, '查看会员等级列表'),
    'member_level_add' => array(29, '添加会员等级'),
    'member_config_list' => array(30, '查看会员配置 限额/返水'),
    'member_config_edit' => array(31, '编辑会员配置 限额/返水'),
    'member_level_edit' => array(32, '编辑会员等级内容'),
    'member_level_delete' => array(33, '删除会员等级'),
    'member_status' => array(34, '编辑会员状态'),
    'member_edit_info' => array(35, '编辑姓名/密码/联系方式'),
    'member_edit_bank_info' => array(36, '编辑取款资料'),
    'member_edit_note' => array(37, '修改备注'),
    'member_view' => array(38, '查看会员资料'),
    'member_bet' => array(39, '查看会员注单'),
    'member_amount' => array(40, '查看会员金流'),
    'member_query' => array(41, '查询会员体系'), //n
    'member_export' => array(42, '导出会员数据'), //n
    'change_agent' => array(43, '更换代理'), //n

    'sport_match_list' => array(50, '查看体育赛事列表'),
    'sport_match_add' => array(51, '添加体育赛事'), //n
    'sport_match_edit' => array(52, '编辑体育赛事'), //n
    'sport_match_delete' => array(53, '删除体育赛事'), //n
    'sport_score_list' => array(54, '查看比分列表'),
    'sport_score_edit' => array(55, '编辑比分'), //n
    'sport_score_delete' => array(56, '删除比分'), //n
    'sport_bet_list' => array(57, '查看体育注单列表'),
    'sport_bet_edit' => array(58, '编辑体育注单'), //n
    'sport_bet_delete' => array(59, '删除体育注单'), //n
    'sport_report_list' => array(60, '查看体育报表'),
    'sport_bet_settle' => array(61, '体育注单结算'), //n
    'sport_member_list' => array(62, '查看体育会员投注统计列表'),
    'sport_edit_limit' => array(63, '设置赛事限额'), //n

    'video_bet_list' => array(70, '查看真人视讯注单列表'),
    'video_bet_edit' => array(71, '编辑真人视讯注单'),
    'video_bet_delete' => array(72, '删除真人视讯注单'),
    'video_amount_list' => array(73, '视讯额度列表'),
    'video_amount_add' => array(74, '添加真人视讯额度'),
    'video_amount_edit' => array(75, '编辑真人视讯额度'),
    'video_report_list' => array(76, '查看真人视讯报表'),
    'video_transfer_list' => array(77, '查看额度转移'),

    'lottery_position_list' => array(80, '查看彩票盘口'),
    'lottery_position_add' => array(81, '添加彩票盘口'),
    'lottery_position_edit' => array(82, '编辑彩票盘口'),
    'lottery_position_cancel' => array(83, '盘口取消'),
    'lottery_bet_list' => array(84, '查看彩票注单列表'),
    'lottery_bet_settle' => array(85, '彩票注单结算'),
    'lottery_odds_list' => array(86, '查看彩票赔率'),
    'lottery_odds_edit' => array(87, '编辑彩票赔率'),
    'lottery_member_list' => array(88, '查看彩票会员投注统计列表'),
    'lottery_bet_report' => array(89, '查看彩票报表'),
    'lottery_bet_cancel' => array(90, '彩票注单取消'),

    'notice_list' => array(100, '查看公告列表'),
    'notice_add' => array(101, '添加公告'),
    'notice_edit' => array(102, '编辑公告'),
    'notice_delete' => array(103, '删除公告'),

    'msg_list' => array(110, '查看信箱列表'),
    'msg_add' => array(111, '添加信件'),
    'msg_edit' => array(112, '回复信件'),
    'msg_close' => array(113, '关闭信件'),

    'picture_list' => array(120, '查看广告图片列表'),
    'picture_add' => array(121, '添加广告图片'),
    'picture_edit' => array(122, '编辑广告图片'),
    'picture_delete' => array(123, '删除广告图片'),
    'activity_list' => array(124, '查看优惠活动列表'),
    'activity_add' => array(125, '添加优惠活动'),
    'activity_edit' => array(126, '编辑优惠活动'),
    'activity_delete' => array(127, '删除优惠活动'),
    'article_category_add' => array(128, '添加文章分类'), //n
    'article_category_edit' => array(129, '编辑文章分类'), //n
    'article_category_delete' => array(130, '删除文章分类'), //n
    'article_category_list' => array(131, '文章分类列表'), //n
    'article_add' => array(132, '添加文章'), //n
    'article_edit' => array(133, '编辑文章'), //n
    'article_delete' => array(134, '删除文章'), //n
    'article_list' => array(135, '文章列表'), //n

    'config_list' => array(140, '查看系统配置列表'),
    'config_edit' => array(141, '修改系统参数'),

    'remittance_list' => array(160, '查看公司入款列表'),
    'remittance_confirm' => array(161, '公司入款确认/取消'),

    'bankcard_list' => array(170, '查看入款帐号列表'),
    'bankcard_add' => array(171, '添加入款帐号'),
    'bankcard_edit' => array(172, '编辑入款帐号'),
    'bankcard_delete' => array(173, '删除入款帐号'),

    'deposit_list' => array(175, '查看在线存款列表'),
    'deposit_confirm' => array(176, '在线存款确认/取消'),

    'payment_list' => array(185, '查看支付接口列表'),
    'payment_edit' => array(186, '编辑支付接口'),
    'payment_delete' => array(187, '删除支付接口'),
    'payment_add' => array(188, '添加支付接口'),

    'adjust_list' => array(190, '查看额度调整列表'),
    'adjust_add' => array(191, '添加额度调整'),

    'withdrawal_list' => array(195, '查看在线取款列表'),
    'withdrawal_confirm' => array(196, '在线取款确认/取消'),

    'statements_list' => array(200, '查看财务报表'),
    'lottery_analysis' => array(201, '生成投注结果'), //n
    'header_stastics' => array(202, '页首统计资讯'),
    'income_view_min' => array(203, '入款可见最小金额'),
    'income_view_max' => array(204, '入款可见最大金额'),
    'withdraw_view_min' => array(205, '出款可见最小金额'),
    'withdraw_view_max' => array(206, '出款可见最大金额'),
    'register_limit_config' => array(208, '注册设置'),
    'qrcode_manage' => array(209, '二维码管理'),
    'manager_login_list' => array(210, '查看登入日志'),
);
