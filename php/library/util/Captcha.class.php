<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 验证码类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Captcha {
    
    private $_width = 64; // 图片宽
    private $_height = 24; // 图片高
    private $_length = 4; // 验证码长度
    private $_bg_color = '#ffffff'; // 图片背景色
    private $_font = ''; // 字体
    private $_font_size_range = array(14, 14); // 字体大小范围
    private $_font_angle_range = array(-5, 5); // 旋转角度范围
    private $_font_color_range = array('#f36161', '#6bc146', '#5368bd', '#000000'); // 字体颜色，红绿蓝黑
    
    //private $_character = '23456789ABCDEFGHKLMNPQRSTUVWXYabcdefhjkmnprstwxy'; // 验证码范围，剔除容易混淆的数字和字母
	private $_character = '0123456789'; // 验证码范围，剔除容易混淆的数字和字母
    
    private $_padding = 1; // 字符间距
    private $_x_base = 1; // x轴两边距离
    private $_y_base = 1; // y轴两边距离
    private $_line = TRUE; // 是否画干扰线
    private $_auto_size = TRUE; // 是否自动调整大小
    
    private $_font_size = ''; // 字体大小
    private $_font_angle = ''; // 字体倾角
    private $_font_color = ''; // 字体颜色
    private $_code = array(); // 验证码
    private $_image = ''; // 图形句柄

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}
    
    /**
     * 构造函数
     * 初始化字体文件路径
     * 
     * @access public
     * @param array $option 选项
     * @return void
     * <code>
     * -- length 长度
     * -- line 是否画干扰线
     * </code>
     */
    public function __construct($option = array()) {
        $this->_font = ROOT . DS . 'library' . DS . 'include' . DS . 'font.ttf';
        if (isset($option['length'])) {
            $this->_length = $option['length'];
        }
        if (isset($option['line'])) {
            $this->_line = $option['line'];
        }
        if (isset($option['width'])) {
            $this->_width = $option['width'];
        }
        if (isset($option['height'])) {
            $this->_height = $option['height'];
        }
    }
    
    /**
     * getCode
     * 获取验证码
     * 
     * @access public
     * @param void
     * @return string 验证码
     */
    public function getCode() {
        $this->_font_angle = range($this->_font_angle_range[0], $this->_font_angle_range[1]);
        $this->_font_size = range($this->_font_size_range[0], $this->_font_size_range[1]);
        
        $character_length = strlen($this->_character);
        $angle_length = count($this->_font_angle);
        $size_length = count($this->_font_size);
        $color_length = count($this->_font_color_range);
        
        $code = array();
        for ($i = 0; $i < $this->_length; $i ++) {
            $character = $this->_character{mt_rand(0, $character_length - 1)};
            $angle = $this->_font_angle[mt_rand(0, $angle_length - 1)];
            $size = $this->_font_size[mt_rand(0, $size_length - 1)];
            $color = $this->_font_color_range[mt_rand(0, $color_length - 1)];
            $bound = $this->calculateTextBox($size, $angle, $character);
            $this->_code[] = array($size, $angle, $color, $character, $bound);
            $code[] = $character;
        }
        $code_content = implode('', $code);
        $code_content = strtolower($code_content);
        return $code_content;
    }

    /**
     * create
     * 生成验证码图片
     * 
     * @access public
     * @param void
     * @return void
     */
    public function create() {
        if (empty($this->_code)) {
            $this->create();
        }
        if ($this->_auto_size) {
            list($this->_width, $this->_height) = $this->getImageSize($this->_code);
        }
        $this->_image = imagecreate($this->_width, $this->_height);
        $background = $this->getBackground($this->getColor($this->_bg_color));
        imagefilledrectangle($this->_image, 0, 0, $this->_width, $this->_height, $background);
        $line_color = array();
        foreach ($this->_code as $code) {
            $bound = $code[4];
            $color = $this->getBackground($this->getColor($code[2]));
            $line_color[] = $color;
            imagettftext($this->_image, $code[0], $code[1], $this->_x_base, $bound['height'], $color, $this->_font, $code[3]);
            $this->_x_base += $bound['width'] * $this->_padding - $bound['left'];
        }
        if (TRUE === $this->_line) {
            $color = $line_color[mt_rand(0, $this->_length - 1)];
            $this->writeLine($color, $this->_x_base);
        }
        header('content-type: image/png');
        imagepng($this->_image);
        imagedestroy($this->_image);
    }

    /**
     * calculateTextBox
     * 通过字体角度得到字体矩形宽度
     *
     * @access public
     * @param int $font_size 字体尺寸
     * @param float $font_angle 旋转角度
     * @param string $font_file 字体文件路径
     * @param string $text 写入字符
     * @return array 返回长宽位置
     */
    private function calculateTextBox($font_size, $font_angle, $text) {
        $box = imagettfbbox($font_size, $font_angle, $this->_font, $text);

        $min_x = min(array($box[0], $box[2], $box[4], $box[6]));
        $max_x = max(array($box[0], $box[2], $box[4], $box[6]));
        $min_y = min(array($box[1], $box[3], $box[5], $box[7]));
        $max_y = max(array($box[1], $box[3], $box[5], $box[7]));

        return array(
        'left' => ($min_x >= -1) ? -abs($min_x + 1) : abs($min_x + 2),
        'top' => abs($min_y),
        'width' => $max_x - $min_x,
        'height' => $max_y - $min_y,
        'box' => $box
        );
    }

    /**
     * getColor
     * 得到颜色值
     * 
     * @access private
     * @param string $color 颜色的16进制码
     * @return array 颜色的RGB码
     */
    private function getColor($color) {
        return array(
            hexdec($color[1] . $color[2]), 
            hexdec($color[3] . $color[4]), 
            hexdec($color[5] . $color[6])
        );
    }

    /**
     * getBackground
     * 分配颜色
     * 
     * @access private
     * @param array $color 颜色
     * @return resource
     */
    private function getBackground($color) {
        return imagecolorallocate($this->_image, $color[0], $color[1], $color[2]);
    }

    /**
     * getImageSize
     * 获取图片信息
     * 
     * @access private
     * @param array $data 字符信息
     * @return array 图片宽高
     */
    private function getImageSize($data) {
        $width = $this->_x_base;
        $height = 0;
        foreach ($data as $value)
        {
            $width = $width + $value[4]['width'] * $this->_padding - $value[4]['left'];
            $height = $height > $value[4]['height'] ? $height : $value[4]['height'];
        }
        return array(max($width, $this->_width), max($height, $this->_height));
    }

    /**
     * writeLine
     * 画干扰线
     * 
     * @access private
     * @param resource $color 颜色
     * @param int $width 宽度
     * @return void
     */
    private function writeLine($color, $width) {
        $h1 = mt_rand(-5, 5);
        $h2 = mt_rand(-1, 1);
        $h3 = mt_rand(4, 6);
        $w2 = mt_rand(10 ,15);

        for($i = -$width / 2; $i < $width / 2;$i = $i + 0.1) {
            $y = $this->_height / $h3 * sin($i / $w2) + $this->_height / 2 + $h1;
            imagesetpixel($this->_image, $i + $width / 2, $y, $color);
            if (0 !== $h2) {
                imagesetpixel($this->_image, $i + $width / 2,$y + $h2, $color);
            }
        }
    }
}