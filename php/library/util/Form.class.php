<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || TRUE !== KGS) {
    exit('Invalid Access');
}

/**
 * 表单组件类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Form {

    /**
     * __clone
     * 防止克隆
     *
     * @access private
     * @return void
     */
    private function __clone(){}

    public static function create($data, $name, $default, $style, $extra = '', $extra_value = '') {
        $html = array();
        $id   = $name ? ('id="' . $name . '"') : '';
        $html[] = '<select ' . $id . ' name="' . $name . '" class="' . $style . '">';
        if ($extra) {
            $html[] = '<option value="' . $extra_value . '">' . $extra . '</option>';
        }
        foreach ($data as $key => $value) {
            $html[] = '<option value="' . $key . '"';
            if ($key == $default) {
                $html[] = ' selected';
            }
            $html[] = '>' . $value . '</option>';
        }
        $html[] = '</select>';
        return implode('', $html);
    }

    public static function createNoIndex($data, $name, $default, $style, $extra = '', $extra_value = '') {
        $html = array();
        $html[] = '<select id="' . $name . '" name="' . $name . '" class="' . $style . '">';
        if ($extra) {
            $html[] = '<option value="' . $extra_value . '">' . $extra . '</option>';
        }
        foreach ($data as $value) {
            $html[] = '<option value="' . $value . '"';
            if ($value == $default) {
                $html[] = ' selected';
            }
            $html[] = '>' . $value . '</option>';
        }
        $html[] = '</select>';
        return implode('', $html);
    }

    public static function latestDate($name, $default = '', $style = '') {
        $count = 200; // 显示最近1周日期
        $time = time();
        $data = array();
        for ($i = 0; $i < $count; $i ++) {
            $data[] = date('Y-m-d', $time - 86400 * $i);
        }
        return self::createNoIndex($data, $name, $default, $style, '');
    }

    public static function status($name, $default = 0, $style = '', $extra = '', $extra_value = '') {
        global $g_status;
        return self::create($g_status, $name, $default, $style, $extra, $extra_value);
    }

    public static function betOrderBy($name, $default = '', $style = '') {
        global $g_bet_order_by;
        return self::create($g_bet_order_by, $name, $default, $style, '');
    }

    public static function userBetOrderBy($name, $default = '', $style = '', $title = '') {
        global $g_user_bet_order_by;
        return self::create($g_user_bet_order_by, $name, $default, $style, $title);
    }

    public static function rollOutInOrderBy($name, $default = '', $style = '') {
        global $g_out_in_order_by;
        return self::create($g_out_in_order_by, $name, $default, $style, '');
    }

    public static function rollOutInSubtype($name, $default = '', $style = '', $title = '') {
        global $roll_in_out_subtype;
        return self::create($roll_in_out_subtype, $name, $default, $style, $title);
    }

    public static function senderCategoryBy($name, $default = '', $style = '') {
        global $g_sender_category;
        return self::create($g_sender_category, $name, $default, $style, '');
    }

    public static function sportType($name, $default = '', $style = '', $title = '') {
        global $g_sport_type;
        return self::create($g_sport_type, $name, $default, $style, $title);
    }



	public static function lotteryType($name, $default = '', $style = '', $title = '') {
        global $g_lottery_name;
        return self::create($g_lottery_name, $name, $default, $style, $title);
    }



    public static function betConfirm($name, $default = '', $style = '') {
        global $g_bet_confirm;
        return self::create($g_bet_confirm, $name, $default, $style, '状态', -1);
    }

    public static function betStatus($name, $default = '', $style = '', $title = '结果') {
        global $g_bet_status;
        return self::create($g_bet_status, $name, $default, $style, $title);
    }

    public static function winLose($name, $default = '', $style = '', $title = '输赢') {
        global $g_win_lose;
        return self::create($g_win_lose, $name, $default, $style, $title);
    }

    public static function autoFresh($name, $default = '', $style = '', $title = '') {
        global $g_auto_fresh;
        return self::create($g_auto_fresh, $name, $default, $style, $title);
    }

    public static function timeZone($name, $default = '', $style = '', $title = '') {
        global $g_time_zone;
        return self::create($g_time_zone, $name, $default, $style, $title);
    }

    public static function timeType($name, $default = '', $style = '', $title = '') {
        global $g_time_type;
        return self::create($g_time_type, $name, $default, $style, $title);
    }

    public static function depositStatus($name, $default = '', $style = '') {
        global $g_deposit_status;
        return self::create($g_deposit_status, $name, $default, $style, '状态');
    }

    public static function withdrawalStatus($name, $default = '', $style = '') {
        global $g_withdrawal_status;
        return self::create($g_withdrawal_status, $name, $default, $style, '状态');
    }

    public static function bank($name, $default = '', $style = '') {
        global $g_bank;
        return self::createNoIndex($g_bank, $name, $default, $style, '选择银行');
    }

	public static function bank_deposit($name, $default = '', $style = '') {
        global $g_bank_deposit;
        return self::createNoIndex($g_bank_deposit, $name, $default, $style, '选择银行');
    }

    public static function betCancel($name, $default = '', $style = '') {
        global $g_bet_cancel;
        return self::create($g_bet_cancel, $name, $default, $style, '取消类型');
    }

    public static function returnType($name, $default = '', $style = '') {
        global $g_return_type;
        return self::create($g_return_type, $name, $default, $style);
    }

    public static function casinoReturnType($name, $default = '', $style = '') {
        global $g_return_type;
        $return_type = $g_return_type;
        unset($return_type[1]);
        return self::create($return_type, $name, $default, $style);
    }


    public static function module($name, $default = '', $style = '', $extra = '', $extra_value = '') {
        global $g_module;
        return self::create($g_module, $name, $default, $style, $extra, $extra_value);
    }

    public static function casinoModule($name, $default = '', $style = '', $extra = '', $extra_value = '') {
        global $g_casino_module;
        return self::create($g_casino_module, $name, $default, $style, $extra, $extra_value);
    }

    public static function casinoName($name, $default = '', $style = '') {
        global $g_casino_module_name;
        return self::create($g_casino_module_name, $name, $default, $style, '平台', '');
    }

    public static function electronicName($name, $default = '', $style = '') {
        global $g_electronic_module_name;
        return self::create($g_electronic_module_name, $name, $default, $style, '平台', '');
    }

    public static function managerType($name, $default = '', $style = '') {
        global $g_manager_type;
        $manager_type = array(
            1 => $g_manager_type[1],
            2 => $g_manager_type[2]
        );

        return self::create($manager_type, $name, $default, $style, '类型', 0);
    }

    public static function managerTypeBylevel($name, $default = '', $style = '' , $level)
    {
        global $g_manager_type;
        $manager_type = array();
        for ($i = 1; $i < $level ; $i++) {
            if($i > 3) continue;
            if ($g_manager_type[$i]) {
                $manager_type[$i] = $g_manager_type[$i];
            }
        }
        return self::create($manager_type, $name, $default, $style, '类型', 0);
    }

    public static function userType($name, $default = '', $style = '') {
        global $g_user_type;
        return self::create($g_user_type, $name, $default, $style);
    }

    public static function memberStatus($name, $default = '', $style = '') {
        global $g_member_status;
        return self::create($g_member_status, $name, $default, $style, '会员状态');
    }

    public static function memberCategory($name, $default = '', $style = '') {
        global $g_member_category;
        return self::create($g_member_category, $name, $default, $style, '账户类型');
    }

    public static function agentStatus($name, $default = '', $style = '', $title = '') {
        global $g_status;
        return self::create($g_status, $name, $default, $style, $title);
    }

    public static function memberReportStatus($name, $default = '', $style = '', $title = '') {
        global $g_member_report_status;
        return self::create($g_member_report_status, $name, $default, $style, $title);
    }

    public static function positionType($name, $default = '', $style = '') {
        global $g_position_type;
        return self::create($g_position_type, $name, $default, $style);
    }

    public static function betPageStyle($name, $default = '', $style = '') {
        global $g_bet_page_style;
        return self::create($g_bet_page_style, $name, $default, $style);
    }

    public static function withdrawlDepositType($name, $default = '', $style = '', $title='类型') {
        global $g_finance_subtype;
        $types = array(
            1 => $g_finance_subtype[1],
            2 => $g_finance_subtype[2],
            3 => $g_finance_subtype[3],
            13 => $g_finance_subtype[13],
            14 => $g_finance_subtype[14],
            15 => $g_finance_subtype[15],
            21 => $g_finance_subtype[21]
        );
        return self::create($types, $name, $default, $style, $title);
    }

    public static function paymentType($name, $default, $style, $extra = '', $extra_value = '') {
        global $g_payment_type;
        return self::create($g_payment_type, $name, $default, $style, $extra, $extra_value);
    }

    public static function adjustType($name, $default = '', $style = '') {
        global $g_adjust_type;
        return self::create($g_adjust_type, $name, $default, $style, '类型');
    }

    public static function addAdjustType($name, $default = '', $style = '') {
        global $g_adjust_type;
        $adjust_type = $g_adjust_type;
        unset($adjust_type[11]);
        unset($adjust_type[20]);
        return self::create($adjust_type, $name, $default, $style, '类型');
    }

    public static function domainCategory($name, $default = '', $style = '') {
        global $g_domain_category;
        return self::create($g_domain_category, $name, $default, $style, '类型');
    }

    public static function fastRound($name, $default = '', $style = '', $title = '') {
        global $g_fast_round;
        return self::create($g_fast_round, $name, $default, $style, $title);
    }

    public static function pageBar($total_page, $page, $extra_parameters = '') {
        $html = array();
        if ($total_page > 1) {
            $parameters = '?';
            if (is_array($extra_parameters)) {
                if (isset($extra_parameters['page'])) {
                    unset($extra_parameters['page']);
                }
                foreach ($extra_parameters as $key => $value) {
                    $parameters .= $key . '=' . $value . '&';
                }
            }
            else {
                $parameters = $extra_parameters ? '?' . $extra_parameters . '&' : '?';
            }
            $html[] = '<form method="post" action="' . $parameters . '">';
            $html[] = '<div class="page">';
            $gap  = 4;
            $line = ceil($page / $gap) - 1;
            $pre  = ($line - 1) * $gap + 1;
            $fre  = ceil($page / $gap) * $gap + 1;

            if ($page > 1) {
                $html[] = '<a href="' . $parameters . 'page=1">首页</a>';
                $html[] = '<a href="' . $parameters . 'page=' . ($page - 1) . '">上页</a>';
                if($pre > 0) {
                    $html[] = '<a href="' . $parameters . 'page=' . $pre . '"> << </a>';
                }
            }
            else {
                $html[] = '<span class="un">首页</span>';
                $html[] = '<span class="un">上页</span>';
            }

            if($total_page >= $page) {
                $a = $line * $gap + 1;
                $b = $a + ($gap - 1);
                $b = $b < $total_page ? $b : $total_page;

                for($i = $a; $i <= $b; $i++) {
                    if($i == $page) {
                        $html[] = '<span class="on">' . $page . '</span>';
                    }
                    else {
                        $html[] = '<a href="' . $parameters . 'page=' . $i . '">'.$i.'</a>';
                    }
                }
            }

            if ($page < $total_page) {
                if($fre < $total_page) {
                    $html[] = '<a href="' . $parameters . 'page=' . $fre . '"> >> </a>';
                }
                $html[] = '<a href="' . $parameters . 'page=' . ($page + 1) . '">下页</a>';
                $html[] = '<a href="' . $parameters . 'page=' . $total_page . '">尾页</a>';
            }
            else {
                $html[] = '<span class="un">下页</span>';
                $html[] = '<span class="un">尾页</span>';
            }
            $html[] = '<input name="page" type="text" class="input_s" />';
            $html[] = '<input class="on" type="submit"  value="跳转" />';
            $html[] = '<span>共' . $total_page . '页</span>';
            $html[] = '</div>';
            $hitm[] = '</form>';
        }
        return implode('', $html);
    }
}
