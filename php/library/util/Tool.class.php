<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 工具类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
final class Tool
{
    public static function dateRange()
    {
        $today = date('Y-m-d');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $yesterday = mktime(0, 0, 0, $month, $day - 1, $year);
        $seven       = date('Y-m-d', strtotime('-7 day'));
        $month_start = mktime(0, 0, 0, $month, 1, $year);
        $month_stop = mktime(0, 0, 0, $month + 1, 0, $year);
        $last_month_start = mktime(0, 0, 0, $month - 1, 1, $year);
        $last_month_stop = mktime(0, 0, 0, $month, 0, $year);
        $range = array(
            'today' => array($today, $today),
            'yesterday' => array(date('Y-m-d', $yesterday), date('Y-m-d', $yesterday)),
            'seven' => array($seven, $today),
            'month' => array(date('Y-m-d', $month_start), date('Y-m-d', $month_stop)),
            'last_month' => array(date('Y-m-d', $last_month_start), date('Y-m-d', $last_month_stop))
        );
        return $range;
    }

    public static function protectName($name)
    {
        $first_name = str_cut($name, 3, '');
        return $first_name . '**';
    }
    
    public static function protectBankcard($card)
    {
        $head = substr($card, 0, 4);
        $tail = substr($card, -3);
        return $head . '***' . $tail;
    }
    
    public static function protectData($data)
    {
        $length = strlen($data);
        if ($length < 3) {
            return $data;
        } elseif ($length < 6) {
            $head = substr($data, 0, 1);
            $tail = substr($data, -1);
            return $head . '*****' . $tail;
        } else {
            $head = substr($data, 0, 3);
            $tail = substr($data, -3);
            return $head . '*****' . $tail;
        }
    }
    
    public static function _mobiles()
    {
        $mobile = '';
        $agent  = isset($_SERVER['HTTP_USER_AGENT']) ? strtolower($_SERVER['HTTP_USER_AGENT']) : '';
        if ($agent) {
            $mobile = (strpos($agent, 'windows nt')) ? 'pc' : '';
            if (!$mobile) {
                $mobile = (strpos($agent, 'mac os')) ? 'mac' : '';
            }
            if (!$mobile) {
                $mobile = (strpos($agent, 'iphone')) ? 'iphone' : '';
            }
            if (!$mobile) {
                $mobile = (strpos($agent, 'android')) ? 'android' : '';
            }
            if (!$mobile) {
                $mobile = (strpos($agent, 'ipad')) ? 'ipad' : '';
            }
        }
        return $mobile;
    }
    
    public static function mobiles()
    {
        $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
        $mobile_browser = 0;
        $mobile_name = 'mobile';
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_name = strtolower($_SERVER['HTTP_USER_AGENT']);
        }
        $mobile_browser++;
        if ((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') !== false)) {
            $mobile_browser++;
        }
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            $mobile_browser++;
        }
        if (isset($_SERVER['HTTP_PROFILE'])) {
            $mobile_browser++;
        }
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
        'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
        'wapr','webc','winw','winw','xda','xda-'
        );
        if (in_array($mobile_ua, $mobile_agents)) {
            $mobile_name = $mobile_ua;
        }
        $mobile_browser++;
        if (strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false) {
            $mobile_browser++;
        }
        // Pre-final check to reset everything if the user is on Windows
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false) {
            $mobile_browser=0;
        }
        // But WP7 is also Windows, with a slightly different characteristic
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false) {
            $mobile_name = 'win phone';
        }
        $mobile_browser++;
        if (!$mobile_browser) {
            $mobile_name = 'pc';
        }
        return $mobile_name;
    }

    /**
     * 判断浏览器裝置
     * @return string
     */
    public static function use_device() {
        $device_name = 'mobile';
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        if(stripos($_SERVER['HTTP_USER_AGENT'],"Android") && stripos($_SERVER['HTTP_USER_AGENT'],"mobile")){
            $Android = true;
        }else if(stripos($_SERVER['HTTP_USER_AGENT'],"Android")){
            $Android = false;
            $AndroidTablet = true;
        }else{
            $Android = false;
            $AndroidTablet = false;
        }

        $webOS = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $RimTablet= stripos($_SERVER['HTTP_USER_AGENT'],"RIM Tablet");

        if( $iPod || $iPhone ){
            //Default
        }else if($iPad){
            $device_name = 'talbe';
        }else if($Android){
            //Default
        }else if($AndroidTablet){
            $device_name = 'talbe';
        }else if($webOS){
            //Default
        }else if($BlackBerry){
            //Default
        }else if($RimTablet){
            $device_name = 'talbe';
        }else if(stristr($_SERVER['HTTP_VIA'],"wap")){// 先检查是否为wap代理，准确度高
            //Default
        }else if(strpos(strtoupper($_SERVER['HTTP_ACCEPT']),"VND.WAP.WML") > 0){// 检查浏览器是否接受 WML.
            //Default
        }else if(preg_match('/(blackberry|configuration\/cldc|hp |hp-|htc |htc_|htc-|iemobile|kindle|midp|mmp|motorola|mobile|nokia|opera mini|opera |Googlebot-Mobile|YahooSeeker\/M1A1-R2D2|android|iphone|ipod|mobi|palm|palmos|pocket|portalmmm|ppc;|smartphone|sonyericsson|sqh|spv|symbian|treo|up.browser|up.link|vodafone|windows ce|xda |xda_)/i', $_SERVER['HTTP_USER_AGENT'])){
            //Default
        }else{
            $device_name = 'pc';
        }

        return $device_name;
    }
}
