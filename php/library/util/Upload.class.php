<?php

/**
 * 访问限制，每个非web直接访问的页面都要有这个判断。
 */
if (!defined('KGS') || true !== KGS) {
    exit('Invalid Access');
}

/**
 * 上传组件类
 *
 * @package Wwin
 * @author iwin <iwin.org@gmail.com>
 * @final
 */
class Upload
{
    private $_allow_type;
    private $_upload_folder;

    public function __construct()
    {
        $this->_allow_type = array(
            IMAGETYPE_GIF,
            IMAGETYPE_JPEG,
            IMAGETYPE_PNG,
        );
        $this->_upload_folder = 'lottery-upload';
    }

    public function run($name, $from = '')
    {
        $file_name = $_FILES[$name]['name'];
        
        if ($file_name) {
            $file_name = substr($file_name, -3);
            $file_name = $name . date('Ymdhis') . '.' . $file_name;
            $file_temp = $_FILES[$name]['tmp_name'];
            $type = exif_imagetype($file_temp);
            $source = $file_temp;
            $target = ROOT . DS . 'fenghuang' . DS . $this->_upload_folder ;//. DS . $file_name;
    
            if (!file_exists($target)) { // create directory if not exists
                mkdir($target, 0777);
            }
            $target .= DS . $file_name;
    
            $path = $this->_upload_folder . '/' . $file_name;
            if (file_exists($target)) {
                return array(-1, '');
            }
            if (!$this->checkType($type)) {
                return array(-2, '');
            }
            if (!$this->create($type, $source, $target)) {
                return array(-3, '');
            }
    
            if ($from == 'picture') {
                $imageresize = new ImageResize();
                $imageresize
                    ->load($_FILES[$name]['tmp_name'])
                    ->fixed_given_size(true) //生成的图片是否以给定的宽度和高度为准
                    ->keep_ratio(false) //是否保持原图片的原比例
                    ->quality(50) //设置生成图片的质量 0-100，如果生成的图片格式为png格式，数字越大，压缩越大，如果是其他格式，如jpg，gif，数组越小，压缩越大
                    ->size(640, 280) //设置生成图片的宽度和高度
                    ->save($_FILES[$name]['tmp_name']); //保存生成图片的路径
            } else if ($from == 'activity_small_image') {
                $imageresize = new ImageResize();
                $imageresize
                    ->load($_FILES[$name]['tmp_name'])
                    ->fixed_given_size(true) //生成的图片是否以给定的宽度和高度为准
                    ->keep_ratio(false) //是否保持原图片的原比例
                    ->quality(50) //设置生成图片的质量 0-100，如果生成的图片格式为png格式，数字越大，压缩越大，如果是其他格式，如jpg，gif，数组越小，压缩越大
                    ->size(750, 260) //设置生成图片的宽度和高度
                    ->save($_FILES[$name]['tmp_name']); //保存生成图片的路径
            } else if ($from == 'activity_big_image') {
                $imageresize = new ImageResize();
                $imageresize
                    ->load($_FILES[$name]['tmp_name'])
                    ->width(840) //设置生成图片的宽度，高度将按照宽度等比例缩放
                    ->fixed_given_size(true) //生成的图片是否以给定的宽度和高度为准
                    ->keep_ratio(false) //是否保持原图片的原比例
                    ->quality(50) //设置生成图片的质量 0-100，如果生成的图片格式为png格式，数字越大，压缩越大，如果是其他格式，如jpg，gif，数组越小，压缩越大
                    ->save($_FILES[$name]['tmp_name']); //保存生成图片的路径
            }
    
            if (defined('AZURE_UPLOAD') && AZURE_UPLOAD == true) {
                $azurestorage_handle = new AzureStorage();
                $status = $azurestorage_handle->upload_blob($file_name, fopen($_FILES[$name]['tmp_name'], "r"));
                if (1 !== $status) {
                    kg_echo($azurestorage_handle->_error);
                    @unlink($target);
                    return array(-4, '');
                }
            }

            return array(1, $path, $file_temp);
        }
        return array(1,'','');
    }

    private function checkType($type)
    {
        if (in_array($type, $this->_allow_type)) {
            return true;
        } else {
            return false;
        }
    }

    private function create($type, $source, $target)
    {
        $im = null;
        try {
            switch ($type) {
                case IMAGETYPE_GIF:
                    $im = imagecreatefromgif($source);
                    imagegif($im, $target);
                    break;
                case IMAGETYPE_JPEG:
                    $im = imagecreatefromjpeg($source);
                    imagejpeg($im, $target, 90);
                    break;
                case IMAGETYPE_PNG:
                    $im = imagecreatefrompng($source);
                    imagesavealpha($im, true);
                    imagepng($im, $target);
                    break;
            }
            imagedestroy($im);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
