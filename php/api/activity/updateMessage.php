<?php
define('KGS',true);
require_once '../../library/include/global.php';

$uid = get_session_uid();
$messageid = kg_post('messageid',0);

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

if(!$member){
    exit(0);
}
unset($member_handle);

$message_handle = new MailBox();
$message_handle->updateReadStatus($messageid,$member['member_id']);

unset($message_handle);
