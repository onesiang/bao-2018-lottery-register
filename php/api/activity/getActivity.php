<?php
define('KGS', true);
require '../../library/include/global.php';

$device = 'M';

$activity_handle = new Activity();
$activity_list = $activity_handle->get($device);
unset($activity_handle);
$azure_handle = new AzureStorage();
foreach ($activity_list as $row) {
    $row['small_image'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($row['small_image']) : 'images/' . $row['small_image'];
    $row['big_image'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($row['big_image']) : 'images/' . $row['big_image'];
    $row['date_start'] = explode(' ',$row['date_start'])[0];
    $row['date_stop'] = explode(' ',$row['date_stop'])[0];
    $new_activity_list[] = $row;
}
unset($azure_handle);

kg_echo(json_encode($new_activity_list));
exit();
