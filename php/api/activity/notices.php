<?php
define('KGS', true);
require_once '../../library/include/global.php';

$notice_handle = new Notice();
$notice_get = $notice_handle->get_web_notice();

$notice = array() ;

if ($notice_get) {
    foreach ($notice_get as $key => $value) {
        // $notice .= '<a id='. $value['notice_id'] .'_'. $value['module'] .' href="#">【'.$value['title'] ."】&nbsp;&nbsp;".$value['content'] . '</a>' . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $notice[] = array(
                id =>  $value['notice_id'],
                module => $value['module'],
                title => $value['title'],
                content => $value['content']
        );
    }
}

kg_echo(json_encode($notice));
