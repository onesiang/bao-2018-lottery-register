<?php
define('KGS', true);
require_once '../../library/include/global.php';

$reply_handle = new MailBox();

$uid = get_session_uid();
$messageid = kg_post('message_Id',0);
$content = kg_post('content',0);

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

$reply_message = $reply_handle->replyMessage($member['member_id'], $messageid, $content);


if ($reply_message) {
    foreach ($reply_message as $key => $value) {

        $reply[] = $value;
    }
    array_push($reply,$member['account']);
}

kg_echo(json_encode($reply));

exit();
