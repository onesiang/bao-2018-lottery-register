<?php
    define('KGS', true);
    require_once '../../library/include/global.php';
    require_once '../../library/include/variable.php';

    $page       = kg_request('pagenum', 1);
    $page_size  = kg_request('pagesize', 15);
    $uid = get_session_uid();

    $member_handle = new Member();
    $member = $member_handle->checkLogin($uid);
    if (!$member) {
        kg_echo(0);
        exit();
    }

    $parameters = array(
        'uid'        => $uid,
        'member_id'  => $member['member_id'],
        'page'       => $page,
        'page_size'  => $page_size
    );


    $handle = new MailBox();
    $data   = $handle->getList($parameters);
    unset($handle);

    kg_echo(json_encode($data));
    exit();
