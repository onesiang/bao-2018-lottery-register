<?php
define('KGS', true);
require_once '../../library/include/global.php';

$notice_handle = new Notice();

$uid = get_session_uid();

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);
unset($member_handle);

$total_unread = $notice_handle->getUnReadTotalNotice($member);

unset($notice_handle);

kg_echo(
    $total_unread
);

exit();
