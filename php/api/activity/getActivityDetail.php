<?php
define('KGS', true);
require '../../library/include/global.php';

if (isset($_GET['activity_id'])) {
    $activity_id = $_GET['activity_id'];
}

$device = 'M';

$activity_handle = new Activity();
$activity_list = $activity_handle->getDetail($activity_id);
unset($activity_handle);
if ($activity_list) {
    $azure_handle = new AzureStorage();
    $activity_list['small_image'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($activity_list['small_image']) : 'images/' . $activity_list['small_image'];
    $activity_list['big_image'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($activity_list['big_image']) : 'images/' . $activity_list['big_image'];
    $activity_list['date_start'] = explode(' ',$activity_list['date_start'])[0];
    $activity_list['date_stop'] = explode(' ',$activity_list['date_stop'])[0];
    unset($azure_handle);
}

kg_echo(json_encode($activity_list));
exit();

?>
