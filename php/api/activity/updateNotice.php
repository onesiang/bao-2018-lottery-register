<?php
define('KGS',true);
require_once '../../library/include/global.php';

$uid = get_session_uid();
$noticeid = kg_post('noticeid',0);

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

if(!$member){
    exit(0);
}
unset($member_handle);

$notice_handle = new Notice();
$notice_handle->updateNoticeRead($noticeid,$member['member_id']);

unset($notice_handle);
