<?php
define('KGS', true);
require_once '../../library/include/global.php';

$message_handle = new MailBox();

$uid = get_session_uid();
$messageid = kg_post('messageId',0);

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

$total_message = $message_handle->getReplyList($member['member_id'],$messageid);

$total = [];

if ($total_message) {
    foreach ($total_message as $key => $value) {
         $value['member_id'] = $member['member_id'];
         $value['account'] = $member['account'];
         $total[] = $value;

    }
}

kg_echo(json_encode($total));

exit();
