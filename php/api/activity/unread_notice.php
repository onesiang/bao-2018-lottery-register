<?php
define('KGS', true);
require_once '../../library/include/global.php';

$notice_handle = new Notice();

$uid = get_session_uid();

$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

$total_unread = $notice_handle->NoticeRead($member);

$total = [];

if ($total_unread) {
    foreach ($total_unread as $key => $value) {
        // $notice .= '<a id='. $value['notice_id'] .'_'. $value['module'] .' href="#">【'.$value['title'] ."】&nbsp;&nbsp;".$value['content'] . '</a>' . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $total[] = $value;
    }
}

kg_echo(json_encode($total));

exit();
