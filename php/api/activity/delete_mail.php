<?php

define('KGS', true);

require_once '../../library/include/global.php';
require_once '../../library/include/variable.php';

$messageid = kg_post('messageid');
$uid = get_session_uid();
$member_handle = new Member();
$member = $member_handle->checkLogin($uid);

if ($messageid && $member) {
    $mailbox_handle = new MailBox();
    $result = $mailbox_handle->delMessage($member['member_id'], $messageid);
    if ($result) {
        kg_echo(true);
    }
    kg_echo(false);
}
kg_echo(false);
exit();
