<?php
    define('KGS', true);
    require '../library/include/global.php';

    $config_handle = new Config();
    $config = $config_handle->getByName(array('VERSION', 'NAME', 'DC_DOMAIN', 'MCDN', 'CS_URL', 'MEMBER_LOGIN_VALIDATE','PAY_URL'));
    $maintaince_data = $config_handle->getMaintaince();
    unset($config_handle);

    // include_once('front_header.php');
?>

<!DOCTYPE html>
<!-- saved from url=(0047)http://shishizhong7.com/?a=kefu&rand=1514633379 -->
<html>
	<head>
		 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
		<meta name="format-detection" content="telephone=no">
		<title>在线客服</title>
		 <script src="<?php kg_echo($cdn); ?>/php/js/jquery.js?v=<?php kg_echo($config['VERSION']); ?>" type="text/javascript"></script>
	</head>

	<body>
		<div class="site_wrapper">

			<!--<div class="header_wrapper common_header">

				<header>
					<div class="t-center header_content_block">
						<a href="main.php" class="go_back_link">
							<img src="images/navigation_bar_back.png" alt="" class="back_icon">
						</a>
						<div class="d_ib header_name">
							<a href="#" class="">
								<span class="">在线客服</span>
							</a>
						</div>
						<div class="clear"></div>
					</div>
				</header>

			</div>-->

			<!-- <header class="top2" style="display:none">
				<div class="inner">
					<span class="icon-left fa fa-chevron-left" aria-hidden="true" onclick="window.history.go(-1)"></span>
						<p class="title" id="title" style="float: left;margin-left: 4rem;">
							在线客服
						</p>
				</div>
			</header> -->

			<div style="width: 100%;height: 100%;">
				<iframe id="iframepage" name="browserFrame" width="100%" scrolling="no" style="height: 97vh; border-style:hidden;" src="<?=$config['CS_URL'];?>" ></iframe>
			</div>
		</div>
		<script>
		    $(function () {
				document.getElementById('iframepage').style.height = window.innerHeight + "px";

				// 修復 iphone header固定 bug
				var iframe = document.getElementById('iframepage');
				var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
				var inputbox = innerDoc.getElementById('inputbox');

				$(inputbox).on('focus',function(e){
						$('.common_header').css('position','absolute');
				}).on('blur',function(e){
						$('.common_header').css('position','fixed');
				})

		    });
		</script>
	</body>
</html>
