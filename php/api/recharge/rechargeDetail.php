<?php

define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

try {
    $paymentType = kg_post('type');
    $payment_handle = new Payment();
    $level = $_SESSION[SESSION_NAME . '_user_level'];
    $gateway = array();
    $thirdPartyGateway = array();
    if ($paymentType === 'bank_transfer' || $paymentType === 'weixin' || $paymentType === 'alipay') {
        $key = 'name as realName,
                bank_address,
                limit_amount,
                img_path,number,
                bankcard_id as id,
                bank_name as name,
                notice as description';
        $gateway = $payment_handle->getBankCard($key, $level, $paymentType);
    }

    if ($paymentType !== 'bank_transfer') {
        $detect_handle = new Mobile_Detect;
        $is_mobile = $detect_handle->isMobile();
        $device = ($is_mobile)? 'h5' : 'web';
        $like = '';
        switch ($paymentType) {
            case 'weixin':
                $like = "b.name like '%微信%'";
                break;
            case 'alipay':
                $like = "b.name  like '%支付宝%'";
                break;
            case 'qq':
                $like = "b.name like '%QQ%'";
                break;
            case  'jing_dong':
                $like = "b.name like '%京东%'";
                break;
            case'yin_lian':
                $like = "b.name like '%银联%'";
                break;
            case'meituan':
                $like = "b.name like '%美团%'";
                break;
            case'baidu':
                $like = "b.name like '%百度%'";
                break;
            case'ctf':
                $like = "b.name like '%财付通%'";
                break;
            case 'online_bank':
                $like = "b.category='b2c'";
                break;
        }
        $thirdPartyGateway = $payment_handle->getThirdPartyGateway($level, $like, $device);
    }
    unset($payment_handle);
    if (empty($thirdPartyGateway) && empty($gateway)) {
        throw new Exception('亲，该渠道没有可用的充值方式，请重新选择！', -1);
    }
    echo json_encode(
        array_merge(
            $gateway,
            $thirdPartyGateway
        )
    );
} catch (\Exception $e) {
    echo json_encode(
        array(
            $e->getCode(),
            $e->getMessage()
        )
    );
}
exit;
