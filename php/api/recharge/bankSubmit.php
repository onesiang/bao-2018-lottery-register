<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

try {
    $uid = $_SESSION[SESSION_NAME . '_user_uid'];
    $member_handler = new Member();
    $member = $member_handler->getByUid($uid);

    $action = kg_post('action');

    if ($uid && count($member) > 0) {
        switch ($action) {
            case 'step1':
                $argument = array(
                    'bankcard_id' => kg_post('bankcard_id'),
                    'bank_name' => kg_post('bank_name'),
                    'amount' => kg_post('amount'),
                    'deposit_time' => date('Y-m-d H:i:s'),
                    'order_id' => order_id(),
                );
                break;

            case 'step2':
                $argument = array(
                    'bankcard_id' => kg_post('id'),
                    'bank_name' => kg_post('bank_name'),
                    'amount' => kg_post('amount'),
                    'name' => kg_post('real_name'),
                    'deposit_time' => date('Y-m-d H:i:s'),
                    'order_id' => order_id(),
                    'rem_id' => kg_post('rem_id'),
                );
                break;
        }

        foreach ($argument as $value) {
            if (!$value) {
                throw new \Exception('亲，请填写完整的资料', -2);
            }
        }
        $config_handle = new Config();
        $config = $config_handle->getByName(array('OFFLINE_DEPOSIT_MIN'));
        unset($config_handle);
        if ($argument['amount'] < $config['OFFLINE_DEPOSIT_MIN']) {
            $depositMin = $config['OFFLINE_DEPOSIT_MIN'];
            throw new \Exception("亲，您的储值不得小于最低存款{$depositMin}元", -3);
        }

        $bankcard_handle = new Payment();
        $bankcard = $bankcard_handle->getBankCardById($argument['bankcard_id']);
        unset($bankcard_handle);
        if (!$bankcard) {
            throw new \Exception('亲，您选择的银行卡无效请重新选择', -4);
        }

        if ($argument['amount'] > money_from_db($bankcard['limit_amount'])) {
            $limit_amount = money_from_db($bankcard['limit_amount']);
            throw new \Exception("亲，您的充值金额不得大于最大{$limit_amount}元", 6);
        }

        $remittance_handle = new Remittance();

        switch ($action) {
            case 'step1':
                $bankcard_summary = '[' . $bankcard['bank_name'] . ']' . $bankcard['name'];
                $result = $remittance_handle->addRemittance(
                    array(
                        $member['member_id'],
                        $member['account'],
                        $argument['bankcard_id'],
                        money_to_db($argument['amount']),
                        $argument['bank_name'],
                        $argument['deposit_time'],
                        $argument['deposit_time'],
                        $argument['order_id'],
                        0,
                        $bankcard_summary,
                    )
                );
                $result = array($result, $argument['order_id']);
                echo json_encode(($result) ? $result : -1);
                break;
            case 'step2':
                // if ($bankcard['name'] !== $argument['name']) {
                //     throw new \Exception('存款姓名非法 请重新选择', -5);
                // }
                $result = $remittance_handle->updateRemittance(
                    array(
                        $argument['name'],
                        $argument['deposit_time'],
                        $argument['deposit_time'],
                        1,
                        0,
                        $argument['rem_id'],
                    )
                );
                if ($result) {
                    echo json_encode(
                        array(
                            $result,
                            '申请提交成功！',
                        )
                    );
                } else {
                    throw new \Exception('亲，请重新提交', $result);
                }
                break;
        }
    } else {
        echo -401;
        exit;
    }
} catch (\Exception $e) {
    echo json_encode(
        array(
            $e->getCode(),
            $e->getMessage(),
        )
    );
}
exit;
