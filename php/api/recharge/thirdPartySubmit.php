<?php
/**
 * Created by PhpStorm.
 * User: kang
 * Date: 2018/8/1
 * Time: 10:22 PM
 */
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

try {
    $uid = $_SESSION[SESSION_NAME . '_user_uid'];
    $amount = kg_post('amount');
    $id = kg_post('id');

    if (!$id) {
        throw new Exception('亲，请选择正确的支付渠道！', -1);
    }
    // MOAmount: 99
    // posturl:
    // id: 1709
    // fnn_payment_id: 137
    // fnn_payment_type: 65
    // banktype:

    if (!$amount) {
        throw new Exception('亲，请填写正确的金额！', -1);
    }

    $returnUrl = $_SERVER['HTTP_HOST'];

    $detect_handle = new Mobile_Detect;
    $is_mobile = $detect_handle->isMobile();
    $http = 'http://';
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
        if (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https') {
            $http = 'https://';
        }
    }
    $paymeny_handle = new Payment();
    $paymentGateway = $paymeny_handle->checkThirdPaymentById($id);

    if (empty($paymentGateway)) {
        throw new Exception('亲，请重新选择支付渠道选！', -1);
    }

    if ($amount > $paymentGateway['limit_amount']) {
        $amount = $paymentGateway['limit_amount'];
        throw new Exception("亲，您储值的金额不能大于{$amount}", -1);
    }

    if ($amount < $paymentGateway['lower_limit_amount']) {
        $amount = $paymentGateway['lower_limit_amount'];
        throw new Exception("亲，您储值的金额不能小于{$amount}", -1);
    }

    $formServer = $paymentGateway['domain'];
    $content = array();
    $content[] = '<form id="pay-form" method="POST" action="' . $http . $formServer . '" target="my_iframe">';
    $content[] = '<input type="hidden" name="fnn_payment_id" value="' . $paymentGateway['fnn_payment_id'] . '" />';
    $content[] = '<input type="hidden" name="fnn_payment_type" value="' . $paymentGateway['payment_type'] . '" />';
    $content[] = '<input type="hidden" name="uid" value="' . $uid . '" />';
    $content[] = '<input type="hidden" name="id" value="' . $id . '" />';
    $content[] = '<input type="hidden" name="MOAmount" value="' . $amount . '" />';
    $content[] = '<input type="hidden" name="domain" value="' . $returnUrl . '" />';
    $content[] = '<input type="hidden" name="bank_list" value="' . $banktype . '" />';
    $content[] = '<input type="hidden" name="is_mobile" value="' . $is_mobile . '" />';
    $content[] = '</form>';
    $content[] = '<iframe name="my_iframe" id="my_iframe" target="_top"  style="display:none"></iframe>';
    echo json_encode(array(1, implode('', $content)));
} catch (\Exception $e) {
    echo json_encode(array($e->getCode(), $e->getMessage()));
}
