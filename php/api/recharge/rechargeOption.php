<?php

define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

$payment_handle = new Payment();
$bankcard = $payment_handle->getBankCard('bank_name', $_SESSION[SESSION_NAME . '_user_level']);//這邊有先寫死不限會員等級
$payOptionListData = array(
    'weixin' => [
        'name' => '微信支付',
        'img_src' => '/img/recharge/weixin.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/weixin'
    ],
    'ali' => [
        'name' => '支付宝支付',
        'img_src' => '/img/recharge/alipay.png',
        'description' => '小额支付，方便更快捷',
        'url' => '/recharge/alipay'
    ],
    'qq' => [
        'name' => 'QQ钱包',
        'img_src' => '/img/recharge/qq.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/qq'
    ],
    'jd' => [
        'name' => '京东钱包',
        'img_src' => '/img/recharge/jing_dong.png',
        'description' => '入款新方式，支付更随心',
        'url' => '/recharge/jing_dong'
    ],
    'yl' => [
        'name' => '银联',
        'img_src' => '/img/recharge/yin_lian.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/yin_lian'
    ],
    'bank' => [
        'name' => '在线网银',
        'img_src' => '/img/recharge/yin_lian.png',
        'description' => '支持32家银行，转帐更便捷',
        'url' => '/recharge/online_bank'
    ],
    'bank_card' => [
        'name' => '银行转帐',
        'img_src' => '/img/recharge/bank_transfer.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/bank_transfer'
    ],
    'meituan' => [
        'name' => '美团钱包',
        'img_src' => '/img/recharge/bank_transfer.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/bank_transfer'
    ],
    'baidu' => [
        'name' => '百度钱包',
        'img_src' => '/img/recharge/bank_transfer.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/bank_transfer'
    ],
    'ctf' => [
        'name' => '财付通支付',
        'img_src' => '/img/recharge/bank_transfer.png',
        'description' => '简易、快捷、到帐不操心',
        'url' => '/recharge/bank_transfer'
    ]
);

$payOptionList = array(
    'weixin' => false,
    'ali' => false,
    'qq' => false,
    'jd' => false,
    'yl' => false,
    'bank' => false,
    'baidu' => false,
    'cft' => false,
    'bank_card' => false,
    'meituan' => false
);

foreach ($bankcard as $key => $value) {
    if ($value['bank_name'] == '支付宝转账到银行卡' || $value['bank_name'] == '支付宝扫码') {
        $payOptionList['ali'] = true;
    } elseif ($value['bank_name'] == '微信转账到银行卡' || $value['bank_name'] == '微信扫码') {
        $payOptionList['weixin'] = true;
    } else {
        $payOptionList['bank_card'] = true;
    }
}
//第三方支付的部分
$payment_list = array();
$payment = $payment_handle->countGateway($_SESSION[SESSION_NAME . '_user_level']);//這邊有先寫死不限會員等級 $member['level']
unset($payment_handle);

$result = [];
if (count($payment) > 0) {
    foreach ($payment as $key => $value) {
        switch ($key) {
            case 'wx':
                if ($value > 0) {
                    $payOptionList['weixin'] = true;
                }
                break;
            case 'ali':
                if ($value > 0) {
                    $payOptionList['ali'] = true;
                }
                break;
            case 'qq':
                if ($value > 0) {
                    $payOptionList['qq'] = true;
                }
                break;
            case 'jd':
                if ($value > 0) {
                    $payOptionList['jd'] = true;
                }
                break;
            case 'yl':
                if ($value > 0) {
                    $payOptionList['yl'] = true;
                }
                break;
            case 'b2c':
                if ($value > 0) {
                    $payOptionList['bank'] = true;
                }
                break;
            case 'meituan':
                if ($value > 0) {
                    $payOptionList['meituan'] = true;
                }
                break;
            case 'baidu':
                if ($value > 0) {
                    $payOptionList['baidu'] = true;
                }
                break;
            case 'cft':
                if ($value > 0) {
                    $payOptionList['cft'] = true;
                }
                break;
        }
    }
    foreach ($payOptionList as $key => $value) {
        if ($value) {
            if ($payOptionListData[$key]) {
                $result[] = $payOptionListData[$key];
            }
        }
    }
}
echo json_encode($result);
exit;
