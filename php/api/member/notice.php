<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
$notice_handle = new Notice();

if (0 == $module) {
    $notice_list = $notice_handle->homeList(0);
} else {
    $notice_list = $notice_handle->simpleList(5, $date);
}
unset($notice_handle);

if (count($notice_list) > 0) {
    foreach ($notice_list as $k => $v) {
        $notice_list[$k]['content'] = preg_replace("/(\r\n|\n|\r){2,}/", "<br>", $v['content']);
    }
}

$Member_handle = new Member();
$member = $Member_handle->getByUid($uid);

if (count($member) > 0) {
    $page = kg_post('pagenum', 1);
    $page_size = kg_post('pagesize', 15);
    $parameters = array(
        'uid' => $uid,
        'member_id' => $member['member_id'],
        'page' => $page,
        'page_size' => $page_size
    );

    $handle = new MailBox();
    $message_data = $handle->getList($parameters);
    $message_list = $message_data['list'];
    unset($handle);
    $plode = sizeof($notice_list);  
    for ($i=0; $i < $plode; $i++) {
        $notice_list[$i]['submit_time'] = explode(' ',$notice_list[$i]['submit_time'])[0];
    }

    $data = array(
        'announcementList' => $notice_list,
        'messageList' => $message_list
    );

    kg_echo(json_encode($data));
    exit();
} else {
    echo -401;
    exit;
}
