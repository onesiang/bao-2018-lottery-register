<?php
define('KGS', true);
require '../../library/include/global.php';


use Firebase\JWT\JWT;
try {
    $register_from = $_SERVER['HTTP_HOST'];
    $argument = array(
        'account' => kg_post('account'),
        'password' => kg_post('password'),
        'password_confirm' => kg_post('password_confirm'),
        'auth' => kg_post('auth'),
        'register_from' => $register_from
    );

    if (!preg_match('/^(?!.*[^a-zA-Z0-9])(?=.*\d)(?=.*[a-zA-Z]).{6,20}$/', kg_post('account'))) {
        throw new \Exception("账号为8-20位数字和英文字符组合！", -1);
    }

    if (!preg_match('/^(?!.*[^a-zA-Z0-9])(?=.*\d)(?=.*[a-zA-Z]).{6,20}$/', kg_post('password'))) {
        throw new \Exception("密码为8-20位数字和英文字符组合！", -2);
    }

    if (kg_post('password') !== kg_post('password_confirm')) {
        throw new \Exception("两次密码输入不同！", -3);
    }
    if (kg_post('auth') == null) {
        throw new \Exception("验证码不能为空!", -4);
    }

    if (isset($_POST['cellphone'])) { //$argument['mobile']
        if (!preg_match('/^1[34578][0-9]{9}$/', kg_post('cellphone'))) {
            throw new \Exception("手机号码格式错误！", -7);
        } else {
            $argument['cellphone'] = kg_post('cellphone');
        }
    }

    if (isset($_POST['qq'])) { //$argument['mobile']
        if (!preg_match('/^[1-9][0-9]{4,11}$/', kg_post('qq'))) {
            throw new \Exception("QQ号错误! ", -8);
        } else {
            $argument['qq'] = kg_post('qq');
        }
    }

    if (isset($_POST['wx'])) { //$argument['mobile']
        if (!preg_match('/^[0-9a-zA-Z_\-]{6,20}$/', kg_post('wx'))) {
            throw new \Exception("微信号错误! ", -9);
        } else {
            $argument['wx'] = kg_post('wx');
        }
    }

    if (isset($_POST['email'])) { //$argument['mobile']
        if (!filter_var(kg_post('email'), FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("邮箱错误！", -10);
        } else {
            $argument['email'] = kg_post('email');
        }
    }

    $config_handle = new Config();
    $config = $config_handle->getByName(array('MEMBER_LOGIN_VALIDATE'));
    unset($config_handle);
    if ($config['MEMBER_LOGIN_VALIDATE']) {
        if (!isset($_POST['auth'])) {
            throw new \Exception("验证码不能为空！！", 0);
        }
        $argument['auth'] = kg_post('auth');
    }

    $member_handle = new Member();
    $result = $member_handle->register($argument);
    if ($result[0] === 1) {

        $key = "example_key";
        $token = array(
            "iss" => $_SERVER['HTTP_HOST'],//jwt签发者
            'sub' => $argument['account'],//sub: jwt所面向的用户
            "aud" => "http://example.com",// 接收jwt的一方
            "iat" => strtotime(date('Y-m-d H:i:s')),//jwt的签发时间
            'jti' => $result[1]  //jwt的唯一身份标识
        );
        $result[1] = JWT::encode($token, $key);
    }
    echo json_encode($result);

} catch (\Exception $e) {

    echo json_encode(
        array(
            $e->getCode(),
            $e->getMessage(),
            $config
        )
    );
}


exit;
