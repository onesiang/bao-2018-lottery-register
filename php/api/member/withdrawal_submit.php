<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

// bankid: 48
// amount: 1000
// withdrawal_code: 1234
// ajax: 1

try {
    $uid = $_SESSION[SESSION_NAME . '_user_uid'];
    $memberId = $_SESSION[SESSION_NAME . '_user_id'];

    if (!$uid) {
        echo -401;
        exit;
    }

    $member_handle = new Member();
    $member = $member_handle->getByUid($uid);

    if (!$member || $memberId !== $member['member_id']) {
        echo -401;
        exit;
    }

    $amount = kg_post('amount');
    $withdrawal_code = kg_post('withdrawal_code');
    $bankid = kg_post('bankid');

    $checkBankid = $member_handle->checkBankCardId($memberId, $bankid);
    unset($member_handle);

    if (!$checkBankid) {
        throw new \Exception('亲，银行卡无效请重新选择银行卡！', -1);
    }

    if (!is_numeric($amount)) {
        throw new \Exception('请填写正确的取款金额！', -2);
    }

    $config_handle = new Config();
    $config = $config_handle->getByName(array(
        'DEPOSIT_LEVEL',
        'WITHDRAWAL_MIN',
        'WITHDRAWAL_MAX',
        'CHECK_WITHDRAWAL_CODE',
        'WITHDRAWAL_DIBS',
        'WITHDRAWAL_MODE',
        'WITHDRAWAL_POUNDAGE',
        'WITHDRAWAL_ADMIN',
    ));
    unset($config_handle);

    if ($amount % $config['WITHDRAWAL_DIBS'] !== 0) {
        throw new \Exception("亲，取款金额为{$config['WITHDRAWAL_DIBS']}的倍数！", -3);
    }

    if (money_to_db($amount) > $member['amount']) {
        throw new \Exception("亲，取款金额大于可用额度！", -4);
    }

    if ($amount < $config['WITHDRAWAL_MIN']) {
        throw new \Exception("取款最小金额为{$config['WITHDRAWAL_MIN']}", -5);
    }

    if ($amount > $config['WITHDRAWAL_MAX']) {
        throw new \Exception("取款最大金额为{$config['WITHDRAWAL_MIN']}", -6);
    }

    if ($config['DEPOSIT_LEVEL']) {
        $level_handle = new MemberLevel();
        $member_level = $level_handle->getByLevel($member['level']);
        unset($level_handle);
        if ($member_level) {
            if ($member_level['withdrawal_amount_max'] && $amount > $member_level['withdrawal_amount_max']) {
                throw new \Exception('亲，取款额度超出您所处会员等级额度上限。', -7);
            }

            if ($member_level['withdrawal_count_max'] && $member['withdrawal_count'] >= $member_level['withdrawal_count_max']) {
                throw new \Exception('亲，您所处会员等级的取款次数已达上限。', -8);
            }
        }
    }

    if ($config['CHECK_WITHDRAWAL_CODE']) {
        $withdrawal_code = $withdrawal_code ? kg_encrypt($withdrawal_code) : '';
        if ($withdrawal_code !== $member['withdrawal_code']) {
            throw new \Exception('亲，您的取款密码错误请重新输入！', -9);
        }
    }

    $withdrawal_handle = new Withdrawal();
    $result = $withdrawal_handle->getWithdrawalByMemberId($memberId, 'submit_time');
    if ($result) {
        $last_time = strtotime($result['submit_time']);
        if (time() - $last_time < 120) {
            throw new \Exception('亲，取款间隔时间为2分钟。坐下来喝杯茶歇一会。', -10);
        }
    }

    $check_data = $withdrawal_handle->showCheckData($memberId, $uid);

    $withdrawal_count_max = $check_data['withdrawal_count_max'];
    $withdrawal_count = $check_data['withdrawal_count'];
    if ($withdrawal_count_max && $withdrawal_count >= $withdrawal_count_max) {
        throw new \Exception('亲，今日取款次数已经超过上限。', -10);
    }

    $check_amount = $check_data['total_check_amount'];
    $valid_amount = $check_data['valid_amount'];

    $actual_amount = $amount;
    if (!$config['WITHDRAWAL_MODE']) {
        if ($valid_amount < $check_amount) {
            throw new \Exception("尚未达到打码需求，无法办理取款！", -11);
        }
    } else {
        $administrative = ($valid_amount < $check_amount) ? ceil($amount * $config['WITHDRAWAL_ADMIN'] * 0.01) : 0;
        $poundage_amount = floor($config['WITHDRAWAL_POUNDAGE'] * $amount * 0.01);
        $actual_amount = $amount - $administrative - $poundage_amount;
        $actual_amount = $actual_amount > 0 ? $actual_amount : 0;
    }

    $argument = array(
        $memberId,
        $member['account'],
        $amount,
        $actual_amount,
        date('Y-m-d H:i:s'),
        order_id(),
        $checkBankid['id'],
    );

    if ($withdrawal_handle->add($argument)) {
        echo json_encode(array(1, '取款申请提交成功！'));
    } else {
        throw new \Exception("取款申请提交失败，请重新提交！", -12);
    }
} catch (\Exception $e) {
    echo json_encode(array($e->getCode(), $e->getMessage()));
}
