<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $Member_handle = new Member();
    $member = $Member_handle->getByUid($uid, 'member_id, amount, withdrawal_code');
    if (count($member) > 0) {
        if ($member['withdrawal_code']) {
            $final_data = [];
            $bank_list = $Member_handle->getBankcard($member['member_id']);
            for ($i = 0, $i_count = count($bank_list); $i < $i_count; $i++) {
                $temp = array(
                    'bankcard_id' => $bank_list[$i]['id'],
                    'bank_account_name' => kg_decrypt($bank_list[$i]['bank_account_name']),
                    'bank_address' => $bank_list[$i]['bank_address'],
                    'bank_name' => $bank_list[$i]['bank_name'],
                    'bank_number' => kg_decrypt($bank_list[$i]['bank_number']),
                    'mobile' => kg_decrypt($bank_list[$i]['mobile']),
                );
                array_push($final_data, $temp);
            }
            $member_config = $Member_handle->getDml($member['member_id']);
            $bet_amount = $member_config['check_bet_amount'];
            $check_bet_amount = $bet_amount /100;
            echo json_encode(
                array(
                    'data' => $final_data,
                    'amount' => $member['amount'] / 100,
                    'dml' => ($check_bet_amount) ? $check_bet_amount : 0
                )
            );
        } else {
            echo "false";
        }
        exit;
    }
}
