<?php
//存款紀錄 包含 線上存款 公司入款 人工入款三種
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    $member = $_Member->getByUid($uid);
    if (count($member) > 0) {
        // $page       = kg_post('page', 1);
        $date_start = kg_post('date_start', '2017-01-01');
        $date_stop = kg_post('date_stop', date('Y-m-d'));
        $time_zone = kg_post('time_zone', 0);
        // $date_start = get_limit_start_date($date_start);

        $final_data = array(
            'uid' => $uid,
            'member_id' => $member['member_id'],
            'account' => $member['account'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'caijin_data' => array(),
        );

        //人工入款 mix 人工返水 mix 人工彩金
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'member_id' => $member['member_id'],
            'type' => [14, 15, 16],
            'time_zone' => $time_zone,
            'countTotal' => true,
            // 'page'       => $page
        );
        $handle = new Adjust();
        $data = $handle->getList($parameters);
        unset($handle);

        if ($data['list']) {
            for ($i = 0, $i_count = count($data['list']); $i < $i_count; $i++) {
                $way = '';
                switch ($data['list'][$i]['type']) {
                    case 14:
                        $way = '人工返水';
                        break;
                    case 15:
                        $way = '人工彩金';
                        break;
                    case 16:
                        $way = '人工入款';
                        break;
                }
                $temp = array(
                    'type' => 1,
                    'way' => $way,
                    'submit_time' => $data['list'][$i]['submit_time'],
                    'amount' => money_from_db($data['list'][$i]['amount']),
                    'summary' => $data['list'][$i]['note'],
                );
                array_push($final_data['caijin_data'], $temp);
            }
        }

        //自動返水 mix 自動派採
        // 11 => 'auto_return_water', // 自动返水
        // 12 => 'auto_winnings', // 自动彩金
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'member_id' => $member['member_id'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'subtype' => [11, 12],
            'time_zone' => $time_zone,
            'countTotal' => true,
            // 'page'       => $page
        );
        $handle = new Finance();
        $data = $handle->getList_new($parameters);
        unset($handle);

        if ($data['list']) {
            for ($i = 0, $i_count = count($data['list']); $i < $i_count; $i++) {
                $way = '';
                switch ($data['list'][$i]['type']) {
                    case 11:
                        $way = '自动返水';
                        break;
                    case 12:
                        $way = '自动彩金';
                        break;
                }
                $temp = array(
                    'type' => 2,
                    'way' => $way,
                    'submit_time' => $data['list'][$i]['submit_time'],
                    'amount' => money_from_db($data['list'][$i]['amount']),
                    'summary' => $data['list'][$i]['note'],
                );
                array_push($final_data['caijin_data'], $temp);
            }
        }
        //依時間做排序
        $_temp_sort = $final_data['deposit_data'];
        usort($_temp_sort, function ($a, $b) {
            return $b['submit_time'] <=> $a['submit_time'];
        });
        $final_data['deposit_data'] = $_temp_sort;

        unset($final_data['uid']);
        unset($final_data['member_id']);
        // print_r($final_data['withdrawal_data']);
        kg_echo(json_encode($final_data));
        exit();
    }
} else {
    echo -401;
    exit();
}
