<?php

//存款紀錄 包含 線上存款 公司入款 人工入款三種
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    if (filter_input(INPUT_POST, 'data', FILTER_SANITIZE_SPECIAL_CHARS)) {
        $member = $_Member->getByUid($uid, 'amount');
        unset($_Member);
        if (count($member) > 0) {
            $member['amount'] = money_from_db($member['amount']);
            echo json_encode($member);
            exit;
        }
    }
}
echo -401;
exit;
