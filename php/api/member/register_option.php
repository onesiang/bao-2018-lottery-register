<?php
define('KGS', true);
require '../../library/include/global.php';

$config_handle = new Config();
$config = $config_handle->getByName(array('REGISTER_OPTIONS', 'MEMBER_LOGIN_VALIDATE'));
unset($config_handle);

$option = array();

$REGISTER_OPTIONS = explode(',', $config['REGISTER_OPTIONS']);

if ($REGISTER_OPTIONS[0]) {
    $option['cellphone'] = $REGISTER_OPTIONS[0];
}

if ($REGISTER_OPTIONS[1]) {
    $option['email'] = $REGISTER_OPTIONS[1];
}

if ($REGISTER_OPTIONS[2]) {
    $option['qq'] = $REGISTER_OPTIONS[2];
}

if ($REGISTER_OPTIONS[3]) {
    $option['wx'] = $REGISTER_OPTIONS[3];
}

if ($REGISTER_OPTIONS[4]) {
    $option['agent'] = $REGISTER_OPTIONS[4];
}
$option['auth_code'] = $config['MEMBER_LOGIN_VALIDATE'];
echo json_encode($option);
exit();
