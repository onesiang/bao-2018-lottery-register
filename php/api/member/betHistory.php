<?php

define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    $member = $_Member->getByUid($uid);
    if (count($member) > 0) {
        $data = array();
        $subtype = kg_post('subtype', 0);
        $status = kg_post('status', 0);
        $date_start = kg_post('date_start', '');
        $date_stop = kg_post('date_stop', '');
        $page_num = kg_post('page_num');
        $page = kg_post('page', $page_num);
        $page = intval($page);
        $page = $page ? $page : 1;
        $is_settle = kg_post('is_settle', 0);
        $countTotal = 0;
        $winTotal = 0;

        $argument = array(
            'account' => $member['account'],
            'page' => $page,
        );
        if ($status > 0) {
            $argument['bet_status'] = $status;
        }
        if ($date_start) {
            $argument['date_start'] = $date_start;
        }

        if ($date_stop) {
            $argument['date_stop'] = $date_stop;
        }

        $argument['time_zone'] = 0;
        if ($subtype) {
            $argument['lottery'] = $subtype;
        }
        $argument['is_settle'] = $is_settle;
        $handle = new Lottery();
        $data = $handle->getBetListFront($argument);
        $list = array();
        foreach ($data['list'] as $value) {
            switch ($value["status"]) {
                case 1:
                    $value['status_summary'] = "未结算";
                    break;
                case 2:
                    $winTotal += money_from_db($value['win_amount']);
                    $value['status_summary'] = "已结算";
                    break;
                case 3:
                    $value['status_summary'] = "已取消";
                    break;
                case 4:
                    $value['status_summary'] = "受理中";
                    break;
            }
            $value['bet_time'] = explode(' ', $value['bet_time'])[1];
            $value['name'] = $g_lottery_name[$value['category']];
            $value['win_amount'] = money_from_db($value['win_amount']);
            $value['bet_amount'] = money_from_db($value['bet_amount']);
            $countTotal += $value['bet_amount'];
            $list[] = $value;
        }
        $data['list'] = $list;
        $out_put = array(
            'subtype' => array(
                'name' => $g_lottery_name[$subtype],
                'tag' => $subtype,
            ),
            'data' => $data,
            'argument' => $argument,
            'total' => array(
                'win' => $winTotal,
                'betTotal' => $countTotal,
            ),
        );
        kg_echo(json_encode($out_put));
        exit();
    }
} else {
    echo -401;
    exit;
}
