<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

// bank_name: 民生银行
// bank_account_name: 建设银行
// bank_number: 6786868768687678678
// ajax: 1

try {
    if (!kg_post('bank_name')) {
        throw new \Exception("请选择取款银行卡银行！！", -3);
    }
    if (!kg_post('bank_number')) {
        throw new \Exception("请输入银行卡号码！！", -1);
    }

    $uid = $_SESSION[SESSION_NAME . '_user_uid'];
    $member_handle = new Member();
    $member = $member_handle->getByUid($uid);

    if (!$member) {
        echo -401;
        exit;
    }

    $bankNumber = replace_htmlchars(kg_post('bank_number'));
    $bankAccountName = replace_htmlchars(kg_post('bank_account_name'));
    $bankName = replace_htmlchars(kg_post('bank_name'));

    $checkCardExists = $member_handle->isBankCardExists($bankNumber);
    if ($checkCardExists) {
        throw new \Exception("银行卡号不能为一致!!", -99);
    }

    $getCardData = $member_handle->getBankCardById($member['member_id']);
    $getCardData = $getCardData[0];

    if (replace_htmlchars(kg_post('bank_account_name')) !== kg_decrypt($getCardData['bank_account_name'])) {
        throw new \Exception("为保证账户安全,新添加的银行卡必须和已添加银行卡的持卡人一致 !!", -100);
    }

    $addBankCardResult = $member_handle->addBankcard(
        array(
            $bankName,
            kg_encrypt($bankAccountName),
            kg_encrypt($bankNumber),
        ),
        $member['member_id']
    );
    if ($addBankCardResult) {
        echo json_encode(array(1,'恭喜您，成功添加新的提款信息',$addBankCardResult));
        exit;
    }
    echo json_encode(array(-4,'新增银行卡失败，请联系在线客服！！'));
} catch (\Exception $e) {
    echo json_encode(
        array(
            $e->getCode(),
            $e->getMessage()
        )
    );
}
exit;
