<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
$member_handle = new Member();
$member_handle->delOnlineMemberInfoInRedis($_SESSION[SESSION_NAME . '_user_account']);
$result = $member_handle->logout($uid);
unset($member_handle);
session_destroy();
session_regenerate_id();
exit;
