<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
$action = kg_post('action');

if ($uid) {
    $member_handle = new Member();
    $member = $member_handle->getByUid($uid);
    if (count($member) > 0) {
        $result = 0;
        if ('withdrawal' == $action) {
            $argument = array(
                'uid' => $uid,
                'password_old' => kg_post('password_old'),
                'withdrawal_code' => kg_post('withdrawal_code')
            );
            $result = $member_handle->editWithdrawalPassword($argument);
            unset($member_handle);
            $log_detail = '会员修改取款密码|' . kg_encrypt(kg_post('withdrawal_code'));
        } else {
            $argument = array(
                'uid' => $uid,
                'password_old' => kg_post('password_old'),
                'password_new' => kg_post('password_new'),
                'password_confirm' => kg_post('password_confirm')
            );

            $result = $member_handle->editPassword($argument);
            unset($member_handle);
            $log_detail = '会员修改登录密码|' . kg_md5(kg_post('password_new'));
        }
        if ($result) {
            $operate_log_handle = new OperateLog();
            $parameters = array(
                'manager_account' => $member['account'],
                'account' => $member['account'],
                'account_type' => 1,
                'module' => 'member',
                'argument' => '',
                'detail' => $log_detail
            );
            $operate_log_handle->add($parameters);
            unset($operate_log_handle);
        }
        kg_echo(json_encode($result));
        exit();
    }
}
echo -401;
exit;
