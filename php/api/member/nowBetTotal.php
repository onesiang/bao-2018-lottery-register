<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    $member = $_Member->getByUid($uid);

    $date_start = date('Y-m-d') . ' 00:00:00';
    $date_stop = date('Y-m-d') . ' 23:59:59';

    if (count($member) > 0) {
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'member_id' => $member['member_id'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
        );
        $handle = new Lottery();
        $data = $handle->getRigtNowBetTotal($parameters);

        if($data[0]['bet_amount'] == '' && $data[0]['bet_amount'] == '') {
            $data[0]['bet_amount'] = '0.00';
            $data[0]['win_amount'] = '0.00';
        }
        kg_echo(json_encode($data[0]));
        exit();
    }
} else {
    echo -401;
    exit();
}
