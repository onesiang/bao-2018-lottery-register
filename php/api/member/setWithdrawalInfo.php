<?php
define('KGS', true);

require '../../library/include/global.php';
require '../../inc/checkLogin.php';

// password_old: 123456
// bank_name: 农业银行
// bank_number: 6786876786786778678
// withdrawal_code: 7866
// bank_account_name: 建设银行
// mobile: 78678678678
// ajax: 1

try {
    if (!$_POST['bank_name']) {
        throw new \Exception("请选择取款银行卡银行", -3);
    }
    if (!$_POST['bank_number']) {
        throw new \Exception("请填写取款银行卡账号！", -5);
    }
    if (!$_POST['bank_account_name']) {
        throw new \Exception("请填写持卡人姓名", -7);
    }

    $uid = $_SESSION[SESSION_NAME . '_user_uid'];
    $argument = array(
        'uid' => $uid,
        'password_old' => replace_htmlchars(kg_post('password_old')),
        'withdrawal_code' => replace_htmlchars(kg_post('withdrawal_code')),
        'mobile' => replace_htmlchars(kg_post('mobile')),
        'real_name' => replace_htmlchars(kg_post('bank_account_name')),
        'bank_name' => replace_htmlchars(kg_post('bank_name')),
        'bank_number' => replace_htmlchars(kg_post('bank_number')),
        'bank_account_name' => replace_htmlchars(kg_post('bank_account_name'))
    );

    $member_handle = new Member();
    $check_option = $member_handle->setBankCardAtFirstTime($argument, $register_limit);
    echo json_encode($check_option);
} catch (\Exception $e) {
    echo json_encode(
        array(
            $e->getCode(),
            $e->getMessage()
        )
    );
}
exit;
