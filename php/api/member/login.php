<?php
define('KGS', true);
require '../../library/include/global.php';
use \Firebase\JWT\JWT;

$account = kg_post('account');
$password = kg_post('password');
$authCode = kg_post('authCode');

if (!$account && !$password) {
    echo json_encode([0,'not empty']);
    exit;
}

$member_handle = new Member();
$result = $member_handle->login($account, $password, $authCode);
if ($result[0] == 1) {
    $key = "example_key";
    $token = array(
        "iss" => $_SERVER['HTTP_HOST'],//jwt签发者
        'sub' => $account,//sub: jwt所面向的用户
        "aud" => "http://example.com",// 接收jwt的一方
        "iat" => strtotime(date('Y-m-d H:i:s')),//jwt的签发时间
        'jti' => $result[1]  //jwt的唯一身份标识
    );
    $jwt = JWT::encode($token, $key);
    $result[1] = $jwt;
}
echo json_encode($result);
exit;
