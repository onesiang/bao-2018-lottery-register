<?php

//存款紀錄 包含 線上存款 公司入款 人工入款三種
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    $member = $_Member->getByUid($uid);
    unset($_Member);
    if (count($member) > 0) {
        // $page       = kg_post('page', 1);
        $date_start = kg_post('date_start', '2017-01-01');
        $date_stop = kg_post('date_stop', date('Y-m-d'));
        $time_zone = kg_post('time_zone', 0);
        // $date_start = get_limit_start_date($date_start);

        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'member_id' => $member['member_id'],
            'time_zone' => $time_zone,
            'countTotal' => true
        );

        $Deposit_data = array();
        $Remittance_data = array();
        $Adjust_data = array();


        $final_data = array(
            'uid' => $uid,
            'member_id' => $member['member_id'],
            'account' => $member['account'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'deposit_data' => array(),
            'withdrawal_data' => array()
        );

        //---------线上存款
        $Deposit_handle = new Deposit();
        $Deposit_data = $Deposit_handle->getList($parameters);
        unset($Deposit_handle);


        //---------公司入款
        $Remittance_handle = new Remittance();
        $Remittance_data = $Remittance_handle->getList($parameters);
        $free_data = $Remittance_handle->get_free($parameters);
        $count = 0;
        foreach ($Remittance_data['list'] as $row) {
            (int)$key = array_search($row['remittance_id'], array_column($free_data, 'table_id'));

            if ($key || $key > -1) {
                $Remittance_data['list'][$count]['free'] = money_from_db($free_data[$key]['amount']);
            }

            $count++;
        }
        unset($Remittance_handle);

        //---------人工入款
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'member_id' => $member['member_id'],
            'type' => 13,
            'time_zone' => $time_zone,
            'page' => $page
        );
        $Adjust_handle = new Adjust();
        $Adjust_data = $Adjust_handle->getList($parameters);
        unset($Adjust_handle);

        //---------處理回來的資料

        if ($Deposit_data['list']) {
            for ($i = 0, $i_count = count($Deposit_data['list']); $i < $i_count; $i++) {
                $status = '';
                switch ($Deposit_data['list'][$i]['status']) {
                    case 1:
                        $status = '未确认';
                        break;
                    case 2:
                        $status = '已确认';
                        break;
                    case 3:
                        $status = '已取消';
                        break;
                    default:
                        $status = '处理中';
                        break;
                }
                $temp = array(
                    'type' => 0,
                    'way' => '[线上存款]',
                    'submit_time' => $Deposit_data['list'][$i]['submit_time'],
                    'amount' => $Deposit_data['list'][$i]['amount'],
                    'summary' => explode(']',$Deposit_data['list'][$i]['payment_summary'])[1],
                    'status' => $status,
                    'order_id' => $Deposit_data['list'][$i]['order_id'],
                    // 'payment_id' => $Deposit_data['list'][$i]['payment_id'],
                    // 'payment_summary' => $Deposit_data['list'][$i]['payment_summary'],
                    // 'confirm_time' => $Deposit_data['list'][$i]['confirm_time'],
                    // 'operator' => $Deposit_data['list'][$i]['operator']
                );
                array_push($final_data['deposit_data'], $temp);
            }
        }

        if ($Remittance_data['list']) {
            for ($i = 0, $i_count = count($Remittance_data['list']); $i < $i_count; $i++) {
                $status = '';
                switch ($Remittance_data['list'][$i]['status']) {
                    case 1:
                        $status = '未确认';
                        break;
                    case 2:
                        $status = '已确认';
                        break;
                    case 3:
                        $status = '已取消';
                        break;
                    default:
                        $status = '处理中';
                        break;
                }
                $temp = array(
                    'type' => 1,
                    'way' => '[公司入款]',
                    'submit_time' => $Remittance_data['list'][$i]['submit_time'],
                    'amount' => money_from_db($Remittance_data['list'][$i]['amount']),
                    'summary' => $Remittance_data['list'][$i]['bankcard_summary'],
                    'status' => $status,
                    'order_id' => $Remittance_data['list'][$i]['order_id'],

                    // 'bankcard_id' => $Remittance_data['list'][$i]['bankcard_id'],
                    // 'bank_name' => $Remittance_data['list'][$i]['bank_name'],
                    // 'name' => $Remittance_data['list'][$i]['name'],
                    // 'deposit_time' => $Remittance_data['list'][$i]['deposit_time'],
                    // 'method' => $Remittance_data['list'][$i]['method'],
                    // 'confirm_time' => $Remittance_data['list'][$i]['confirm_time'],
                    // 'status' => $Remittance_data['list'][$i]['status'],
                    // 'operator' => $Remittance_data['list'][$i]['operator'],
                    // 'manager_note' => $Remittance_data['list'][$i]['manager_note']
                );
                array_push($final_data['deposit_data'], $temp);
            }
        }

        if ($Adjust_data['list']) {
            for ($i = 0, $i_count = count($Adjust_data['list']); $i < $i_count; $i++) {
                $temp = array(
                    'type' => 2,
                    'way' => '[人工入款]',
                    'submit_time' => $Adjust_data['list'][$i]['submit_time'],
                    'amount' => money_from_db($Adjust_data['list'][$i]['amount']),
                    'summary' => explode(']',$Adjust_data['list'][$i]['note'])[1],
                    'status' => '已确认',
                    'order_id' => '',

                    // 'award' => $Adjust_data['list'][$i]['award'],
                    // 'check_amount' => $Adjust_data['list'][$i]['check_amount'],
                    // 'type' => $Adjust_data['list'][$i]['type'],
                    // 'manager' => $Adjust_data['list'][$i]['manager'],
                    // 'description' => $Adjust_data['list'][$i]['description']
                );
                array_push($final_data['deposit_data'], $temp);
            }
        }

        //依時間做排序
        $_temp_sort = $final_data['deposit_data'];
        usort($_temp_sort, function ($a, $b) {
            return $b['submit_time'] <=> $a['submit_time'];
        });
        $final_data['deposit_data'] = $_temp_sort;

        //取款的部分
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'date_start' => $date_start . ' 00:00:00',
            'date_stop' => $date_stop,
            'member_id' => $member['member_id'],
            'time_zone' => $time_zone,
            'countTotal' => true
        );

        $Withdrawal_data = array();
        //---------线上取款
        $Withdrawal_handle = new Withdrawal();
        $Withdrawal_data = $Withdrawal_handle->getList($parameters);
        unset($Withdrawal_handle);

        if ($Withdrawal_data['list']) {
            for ($i = 0, $i_count = count($Withdrawal_data['list']); $i < $i_count; $i++) {
                $status = '';
                switch ($Withdrawal_data['list'][$i]['status']) {
                    case 1:
                        $status = '未确认';
                        break;
                    case 2:
                        $status = '已确认';
                        break;
                    case 3:
                        $status = '已取消';
                        break;
                    default:
                        $status = '处理中';
                        break;
                }
                $temp = array(
                    'type' => -1,
                    'way' => '[线上取款]',
                    'submit_time' => $Withdrawal_data['list'][$i]['submit_time'],
                    'amount' => money_from_db($Withdrawal_data['list'][$i]['amount']),
                    'summary' => explode(']',$Withdrawal_data['list'][$i]['note'])[1],
                    'status' => $status,
                    'order_id' => '',
                    'description' => $Withdrawal_data['list'][$i]['deal_note'],
                    'real_name' => $Withdrawal_data['list'][$i]['real_name'],
                    'bank_name' => $Withdrawal_data['list'][$i]['bank_name']
                );
                array_push($final_data['withdrawal_data'], $temp);
            }
        }
        unset($final_data['uid']);
        unset($final_data['member_id']);
        kg_echo(json_encode($final_data));
        exit();
    }
}
echo -401;
exit;
