<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$request_body = file_get_contents('php://input');
$json = json_decode($request_body, true);

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $_Member = new Member();
    $member = $_Member->getByUid($uid);

    $date_start = date('Y-m-d') . ' 00:00:00';
    $date_stop = date('Y-m-d') . ' 23:59:59';

    if (count($member) > 0) {
        $parameters = array(
            'uid' => $uid,
            'account' => $member['account'],
            'member_id' => $member['member_id'],
            'date_start' => $date_start,
            'date_stop' => $date_stop,
            'status' => $json['status'], //1未结 2已结
        );
        $handle = new Lottery();
        $data = $handle->getRigtNowBetDetail($parameters);

        $result = array();
        foreach ($g_lottery_name as $key => $value) {
            $result['list'][$key] = array('tag' => $key, 'name' => $g_lottery_name[$key], 'bet_amount' => 0, 'count' => 0, 'win_amount' => 0);
        }

        foreach ($data as $value) {
            $result['list'][$value['tag']]['count'] = $value['count'];
            $result['list'][$value['tag']]['bet_amount'] = $value['bet_amount'];
            $result['list'][$value['tag']]['win_amount'] = $value['win_amount'];
            $total_win_amount += $value['win_amount'];
            $total_bet_amount += $value['bet_amount'];
        }
        $result['total'] = array('win_total' => $total_win_amount, 'bet_total' => $total_bet_amount);
        kg_echo(json_encode($result));
        exit();
    }
} else {
    echo -401;
    exit();
}
