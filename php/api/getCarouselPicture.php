<?php
define('KGS', true);
require '../library/include/global.php';


$picture_handle = new Picture();
$picture = $picture_handle->showList();
unset($picture_handle);


$azure_handle = new AzureStorage();
foreach ($picture as $row) {
    $row['path'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($row['path']) : 'images/' . $row['path'];
    $row['path_pc'] = defined('AZURE_ACCOUNT_URL') ? $azure_handle->get_blob_url($row['path_pc']) : 'images/' . $row['path_pc'];
    $new_picture_list[] = $row;
}
unset($azure_handle);

kg_echo(json_encode($new_picture_list));
exit();
