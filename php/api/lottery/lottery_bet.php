<?php
ini_set('display_errors','1');
 error_reporting(E_ALL);
define('KGS', true);
require_once '../../library/include/global.php';
require_once '../../library/include/variable.php';
require '../../inc/checkLogin.php';

$uid = $_SESSION[SESSION_NAME . '_user_uid'];
if ($uid) {
    $member_handle = new Member();
    $member = $member_handle->getByUid($uid);
}

$request_body = file_get_contents('php://input');
$json = json_decode($request_body, true);

$cdn = get_cdn();

$member_id = 0;
$account = '';
$choose_position = 0;
$bet_limit_min = 0;
$bet_limit_max = 0;

if ($member) {
    $member_id = $member['member_id'];
    $account = $member['account'];
}

if ($member_id > 0) {
    $handle = new MemberConfig();
    $member_config = $handle->getByMemberId($member_id);
    if ($member_config) {
        $bet_limit_min = $member_config['lottery_min'];
        $bet_limit_max = $member_config['lottery_max'];
    }
}
$bet_limit_min = $bet_limit_min > 0 ? $bet_limit_min : $config['LOTTERY_BET_MIN'];
$bet_limit_max = $bet_limit_max > 0 ? $bet_limit_max : $config['LOTTERY_BET_MAX'];

$bet_limit_handle = new LotteryBetLimit();
$bet_limit = $bet_limit_handle->getBetLimitByCategory($json['lottery']);
if ($bet_limit && $bet_limit['min']) {
    $bet_limit_min = $bet_limit['min'];
}
if ($bet_limit && $bet_limit['max']) {
    $bet_limit_max = $bet_limit['max'];
}
unset($bet_limit_handle, $bet_limit);

$error = '';
$data = array();

if ($member && 1 != $member['status']) {
    $error = 'member freeze';
} else {
    $member_id = isset($member['member_id']) ? $member['member_id'] : 0;
}

if (!$error) {
    if ($lottery_maintain) {
        $error = 'this lottery maintain';
    }
}

if (!$error) {
    $member_config = new MemberConfig();
    $allow_bet_module = $member_config->allowBetModule($member_id);
    if ($allow_bet_module && !$allow_bet_module[1]) {
        $error = 'not allow bet';
    }
}

if (!$error) {
    //$bet_round_id  = isset($_POST['bet_round']) ? $_POST['bet_round_id'] : 0;
    // $bet_round = isset($_POST['bet_round']) ? $_POST['bet_round'] : 0;
    // $bets = isset($_POST['bets']) && $_POST['bets'] ? js_unescape($_POST['bets']) : '';
    // $bets2 = isset($_POST['bets2']) && $_POST['bets2'] ? js_unescape($_POST['bets2']) : '';
    // $bet_type = isset($_POST['bet_type']) && $_POST['bet_type'] ? js_unescape($_POST['bet_type']) : '';
    // $position_type = isset($_POST['position_type']) && $_POST['position_type'] ? js_unescape($_POST['position_type']) : '';

    $lottery = isset($json['lottery']) ? $json['lottery'] : '';
    $bet_round = isset($json['bet_round']) ? $json['bet_round'] : 0;
    $bet_data = isset($json['bet_data']) ? $json['bet_data'] : '';
    $bet_amount = isset($json['bet_amount']) ? $json['bet_amount'] : '';
    $bet_type = isset($json['bet_type']) ? $json['bet_type'] : 1;
    $position_type = kg_request('position_type', 0);
    $lottery_type = kg_request('lottery_type', 0);

    if ($bet_type == 2) {
        $bets2 = json_encode($bet_data);
    } else {
        //為什麼要這樣寫呢 就是要先符合舊的那樣先讓它動起來啦抱歉歉
        $bet_data_temp = [];
        foreach ($bet_data as $key => $value) {
            // $bet_data_temp[$value['odds_id']] = $bet_amount;
            $bet_data_temp[$value['odds_id']] = $bet_amount ? $bet_amount : $value['betAmount'];
        }
        $bets = json_encode($bet_data_temp);
    }

    if (is_stringint($bet_round)) {
        $bet_round = floor($bet_round);
        $handle = null;
        switch ($lottery) {
            case 9:
                $handle = new SixMark();
                break;
            case 10:
                $handle = new Cqssc();
                break;
            case 11:
                $handle = new Gdklsf();
                break;
            case 12:
            case 29:
                $handle = new Bjpk($lottery);
                break;
            case 13:
                $handle = new Gdklsf($lottery);
                break;
            case 14:
                $handle = new Cqssc($lottery);
                break;
            case 15:
                $handle = new ElevenSsc($lottery);
                break;
            case 16:
                $handle = new ElevenSsc($lottery);
                break;
            case 17:
                $handle = new ElevenSsc($lottery);
                break;
            case 18:
                $handle = new HappyEight($lottery);
                break;
            case 19:
                $handle = new ThreeBall($lottery);
                break;
            case 20:
                $handle = new ThreeBall($lottery);
                break;
            case 21: //幸运飞艇
                $handle = new Bjpk($lottery);
                break;
            case 22: //幸运28
                $handle = new Bj28($lottery);
                break;
            case 23: //福利三分彩
                $handle = new Bjpk($lottery);
                break;
            case 24: //跑马
                $handle = new Bjpk($lottery);
                break;
            case 25: //福利五分彩
                $handle = new Cqssc($lottery);
                break;
            case 28: //北京28
                $handle = new Bj28($lottery);
                break;
            case 64: //安徽快3
                $handle = new K3($lottery);
                break;
            case 70: //湖北快3
                $handle = new K3($lottery);
                break;
            case 98: //
                $handle = new HappyEight($lottery);
                break;
            case 99: //
                $handle = new Bj28($lottery);
                break;
            case 30: //超级赛车
                $handle = new Bjpk($lottery);
                break;
            case 31: //超级快三
                $handle = new K3($lottery);
                break;
            case 32: //江苏快三
                $handle = new K3($lottery);
                break;
            case 33: //广西快三
                $handle = new K3($lottery);
                break;
            case 34: //超级二八
                $handle = new Bj28($lottery);
                break;
            case 35: //美国二八
                $handle = new Bj28($lottery);
                break;
            case 36: //极速六合彩
                $handle = new SixMark($lottery);
                break;
            case 37: //福利时时彩
                $handle = new Cqssc($lottery);
                break;
            case 38: //新疆时时彩
                $handle = new Cqssc($lottery);
                break;
            case 39: //河北快三
                $handle = new K3($lottery);
                break;
        }
        if ($handle) {
            if ($bets) {
                $return = $handle->bet($member_id, $bet_round, $bets, $position_type, $lottery_type);
            } else {
                $return = $handle->bet2($member_id, $bet_round, $bets2, $position_type, $lottery_type);
            }
            $error = $return['error'];
            $data = $return['data'];
            unset($handle);
        } else {
            $error = 'lottery error!';
        }
    } else {
        $error = 'param error!';
    }
}

$output = array(
    'error' => $error,
    'data' => $data,
);

if (!$error) {
    $member_handle->setOnlineMemberInfoToRedis($member);
    unset($member_handle);
}

kg_echo(json_encode($output));

exit();
