<?php
define('KGS', true);
require_once '../../library/include/global.php';
ini_set('display_errors', '1');
error_reporting(E_ALL);
$uid = get_uid();
$data = array();
$lottery = kg_get('lottery', 0);
$handle = new Cache();
switch ($lottery) {
    case 0:
        $result = $handle->hGetAll(CUSTOMER_NAME . '-lottery');
        break;
    default:
        $result = $handle->hGet(CUSTOMER_NAME . '-lottery', $lottery);
        break;
}
//kg_echo(json_encode($result));
//exit();
//
$bjpk10 = 0;

unset($handle);
if ($result) {
    $now_time = 0;
    if (is_array($result)) {
        foreach ($result as $key => $value) {
            $info = json_decode($result[$key], true)[0];
            if ($info) {
                $bet_remaining_time = 0;

                // $bjpk10 = ((int)$key == 12)? 10 : 0 ;//先注解 之後會用得到

                if (isset($info['bet_stop_time'])) {
                    $bet_remaining_time = $info['bet_stop_time'] - $now_time + $bjpk10;
                    $bet_remaining_time = $bet_remaining_time > 0 ? $bet_remaining_time : 0;
                }

                $open_remaining_time = 0;
                if (isset($info['open_term_time'])) {
                    $open_remaining_time = $info['open_term_time'] - $now_time + $bjpk10;
                    $open_remaining_time = $open_remaining_time > 0 ? $open_remaining_time : 0;
                }

                $data[$key] = array(
                    'numbers' => isset($info['numbers']) ? $info['numbers'] : '',
                    'round' => isset($info['round']) ? $info['round'] : 0,
                    'bet_round' => isset($info['bet_round']) ? $info['bet_round'] : 0,
                    'bet_remaining_time' => $bet_remaining_time,
                    'open_remaining_time' => $open_remaining_time,
                    'bet_remaining_time_tt' => date('Y-m-d H:i:s', $info['open_term_time']),
                    'open_remaining_time_' => date('Y-m-d H:i:s', $info['bet_stop_time']),
                    'now' => date('Y-m-d H:i:s', $now_time),
                );
            }
        }
    } else {
       $result = json_decode($result, true);

       $bet_remaining_time = $result['bet_stop_time'] - $now_time;
       $bet_remaining_time = $bet_remaining_time > 0 ? $bet_remaining_time : 0;

       $open_remaining_time = $result['open_term_time'] - $now_time;
       $open_remaining_time = $open_remaining_time > 0 ? $open_remaining_time : 0;

       $data[$lottery] = array(
           'numbers' => $result['numbers'],
           'round' => $result['round'],
           'bet_round' => $result['bet_round'],
           'bet_remaining_time' => $bet_remaining_time,
           'open_remaining_time' => $open_remaining_time,
           //'server_time'         => $result['server_time']
       );
    }

    // $member_handle = new Member();
    // $amount = $member_handle->getAmount();
    // $data['amount'] = $amount;
}
//else {
//     $lottery_handle = new Lottery();
//     $result = $lottery_handle->getLastLotteryResult($lottery);
//
//     $now_time = time();
//     $bet_remaining_time = strtotime($result['bet_stop_time']) - $now_time;
//     $bet_remaining_time = $bet_remaining_time > 0 ? $bet_remaining_time : 0;
//
//     $open_remaining_time = strtotime($result['open_term_time']) - $now_time;
//     $open_remaining_time = $open_remaining_time > 0 ? $open_remaining_time : 0;
//
//     $data[$lottery] = array(
//         'numbers' => $result['numbers'],
//         'round' => $result['round'],
//         'bet_round' => $result['bet_round'],
//         'bet_remaining_time' => $bet_remaining_time,
//         'open_remaining_time' => $open_remaining_time,
//     );
//
//     unset($lottery_handle);
// }

kg_echo(json_encode($data));

exit();
