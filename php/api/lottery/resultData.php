<?php
define('KGS', true);
require_once '../../library/include/global.php';
require_once '../../library/include/variable.php';


$lottery = kg_get('lottery', '');
$pagesize = kg_get('pagesize', $g_page_size['general']);
$pagenum = kg_get('pagenum', 1);
$startindex = ($pagenum - 1) * $pagesize;
$result_id = kg_get('result_id', 0);
$selected_date = kg_get('selected_date', date("Y-m-d"));

$handle = new Lottery();
$data = $handle->getRecordList2($lottery, $startindex, $pagesize, $result_id, $selected_date);
unset($handle);

foreach ($data as $key => $value) {
    $data[$key]['content'] = explode(',', $value['content']);
}

kg_echo(json_encode($data));

exit();
