<?php
define('KGS', true);
require '../../library/include/global.php';
require '../../library/include/variable.php';

$lottery = 12;
$odds_handle = new LotteryOdds();
$odds = $odds_handle->getOddsList($lottery);
unset($odds_handle);

echo json_encode($odds);
