<?php
define('KGS', true);
require '../../../library/include/global.php';
// require '../../../library/include/variable.php';


$lottery = 32;
$odds_handle = new LotteryOdds();
$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-jiangsuk3');

if ($result) {
    echo $result;
} else {
    $odds = $odds_handle->getOddsList($lottery);

    $firstBetList = [];
    $secondBetList = [];
    $thirdBetList = [];
    $fourthBetList = [];
    $fifthBetList = [];

    //三軍
    for ($index = 1; $index <= 6; $index++) {
        $oddsData = $odds[5][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $firstBetList[0]['name'] = $title;
        $firstBetList[0]['detail'][$index -1] = $oddsData;
        $firstBetList[0]['detail'][$index -1]['name'] = (int)$detailName;
        $firstBetList[0]['detail'][$index -1]['odds'] = $oddsData['odds_a'];
        $firstBetList[0]['detail'][$index -1]['odds_id'] = $oddsData['odds_id'];
    }

    //總和 大小 單雙
    $firstBetList[1]['name'] = '总和';
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $odds[2][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $detailName = $tempString[1];
        $firstBetList[1]['detail'][$index %2] = $oddsData;
        $firstBetList[1]['detail'][$index %2]['name'] = $detailName;
        $firstBetList[1]['detail'][$index %2]['odds'] = $oddsData['odds_a'];
        $firstBetList[1]['detail'][$index %2]['odds_id'] = $oddsData['odds_id'];
    }
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $odds[3][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $detailName = $tempString[1];
        $firstBetList[1]['detail'][$index +1] = $oddsData;
        $firstBetList[1]['detail'][$index +1]['name'] = $detailName;
        $firstBetList[1]['detail'][$index +1]['odds'] = $oddsData['odds_a'];
        $firstBetList[1]['detail'][$index +1]['odds_id'] = $oddsData['odds_id'];
    }

    //围骰、全骰
    $secondBetList[0]['name'] = '围骰、全骰';
    for ($index = 1; $index <= 7; $index++) {
        $oddsData = $odds[4][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $detailName = $tempString[1];
        $secondBetList[0]['detail'][$index -1] = $oddsData;
        if ($index == 7){
            $secondBetList[0]['detail'][$index -1]['name'] = $detailName;
        }
        else{
            $secondBetList[0]['detail'][$index -1]['name'] = $detailName . ',' . $detailName . ',' . $detailName;
        }
        $secondBetList[0]['detail'][$index -1]['odds'] = $oddsData['odds_a'];
        $secondBetList[0]['detail'][$index -1]['odds_id'] = $oddsData['odds_id'];
    }

    //點數
    for ($index = 4; $index <= 17; $index++) {
        $oddsData = $odds[1][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $thirdBetList[0]['name'] = $title;
        $thirdBetList[0]['detail'][$index -1] = $oddsData;
        $thirdBetList[0]['detail'][$index -1]['name'] = $detailName . '点';
        $thirdBetList[0]['detail'][$index -1]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index -1]['odds_id'] = $oddsData['odds_id'];
    }

    //长牌
    $positionData =  $odds[7][1];
    $index = 0;
    $fourthBetList[0]['name'] = '长牌';
    for ($front = 1; $front <= 5; $front++) {
        for ($behind = $front+1; $behind <= 6; $behind++) {
            $oddsData = $positionData[$front . ',' . $behind];
            $tempString = explode('|', $oddsData['name']);
            $detailName = $tempString[1];
            $fourthBetList[0]['detail'][$index] = $oddsData;
            $fourthBetList[0]['detail'][$index]['name'] = $detailName;
            $fourthBetList[0]['detail'][$index]['odds'] = $oddsData['odds_a'];
            $fourthBetList[0]['detail'][$index]['odds_id'] = $oddsData['odds_id'];
            $index ++;
        }
    }

    //短牌
    for ($index = 1; $index <= 6; $index++) {
        $oddsData = $odds[6][1][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $fifthBetList[0]['name'] = $title;
        $fifthBetList[0]['detail'][$index -1] = $oddsData;
        $fifthBetList[0]['detail'][$index -1]['name'] = $detailName . ',' . $detailName;
        $fifthBetList[0]['detail'][$index -1]['odds'] = $oddsData['odds_a'];
        $fifthBetList[0]['detail'][$index -1]['odds_id'] = $oddsData['odds_id'];
    }

    $result['firstBetList'] = $firstBetList;
    $result['secondBetList'] = $secondBetList;
    $result['thirdBetList'] = $thirdBetList;
    $result['fourthBetList'] = $fourthBetList;
    $result['fifthBetList'] = $fifthBetList;

    $odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-jiangsuk3', json_encode($result));
    echo json_encode($result);
}

unset($odds_handle);

exit();
