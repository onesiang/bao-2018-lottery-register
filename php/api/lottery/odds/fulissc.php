<?php
define('KGS', true);
require '../../../library/include/global.php';
// require '../../../library/include/variable.php';

$lottery = 37;
$odds_handle = new LotteryOdds();
$odds = $odds_handle->getOddsList($lottery);

$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-fulissc');

if ($result) {
    echo $result;
} else {

    $odds = $odds_handle->getOddsList($lottery);

    $firstBetList = [];
    $secondBetList = [];
    $thirdBetList = [];

// // 名次1~5 [1][1][1]冠軍 一號
    $gameData = $odds[1];
    for ($positionIndex = 1; $positionIndex <= 5; $positionIndex++) {
        $positionData = $gameData[$positionIndex];
        for ($index = 0; $index <= 9; $index++) {
            $oddsData = $positionData[$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $firstBetList[$positionIndex - 1]['name'] = $title;
            $firstBetList[$positionIndex - 1]['detail'][$index] = $oddsData;
            $firstBetList[$positionIndex - 1]['detail'][$index]['name'] = $detailName;
            $firstBetList[$positionIndex - 1]['detail'][$index]['odds'] = $oddsData['odds_a'];
            $firstBetList[$positionIndex - 1]['detail'][$index]['odds_id'] = $oddsData['odds_id'];
        }
    }

// // //玩法7 前中後 顺子 对子 半顺 杂六
    $gameData = $odds[7];
    for ($positionIndex = 1; $positionIndex <= 3; $positionIndex++) {
        $positionData = $gameData[$positionIndex];
        for ($index = 1; $index <= 5; $index++) {
            $oddsData = $positionData[$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $secondBetList[$positionIndex - 1]['name'] = $title;
            $secondBetList[$positionIndex - 1]['detail'][$index - 1] = $oddsData;
            $secondBetList[$positionIndex - 1]['detail'][$index - 1]['name'] = $detailName;
            $secondBetList[$positionIndex - 1]['detail'][$index - 1]['odds'] = $oddsData['odds_a'];
            $secondBetList[$positionIndex - 1]['detail'][$index - 1]['odds_id'] = $oddsData['odds_id'];
        }
    }

    $thirdBetList[0]['name'] = '总和/龙虎和';
// //玩法5 总和|龙虎  [5][0][1]总和|龙虎|大  [5][0][2]总和|龙虎|小
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $odds[5][0][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $thirdBetList[0]['detail'][$index % 2] = $oddsData;
        $thirdBetList[0]['detail'][$index % 2]['name'] = $detailName;
        $thirdBetList[0]['detail'][$index % 2]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index % 2]['odds_id'] = $oddsData['odds_id'];
    }

// //玩法4 总和|龙虎  [5][0][1]总和|龙虎|單  [5][0][2]总和|龙虎|雙
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $odds[4][0][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $thirdBetList[0]['detail'][$index + 1] = $oddsData;
        $thirdBetList[0]['detail'][$index + 1]['name'] = $detailName;
        $thirdBetList[0]['detail'][$index + 1]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index + 1]['odds_id'] = $oddsData['odds_id'];
    }

// //玩法6 总和|龙虎  [6][0][1]总和|龙虎|  [6][0][2]总和|龙虎| [6][0][3]
    for ($index = 1; $index <= 3; $index++) {
        $oddsData = $odds[6][0][$index];
        $thirdBetList[0]['detail'][$index + 3] = $oddsData;
        $thirdBetList[0]['detail'][$index + 3]['name'] = $oddsData['name'];
        $thirdBetList[0]['detail'][$index + 3]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index + 3]['odds_id'] = $oddsData['odds_id'];
    }

// //雙面 冠軍 龍[7][1][1] 虎[7][1][2] 大[3][1][2] 小[3][1][1] 單[2][1][1] 雙[2][1][2]
    // //雙面 第二名 龍[7][2]][1] 虎[7][2][2] 大[3][2][2] 小[3][2][1] 單[2][2][1] 雙[2][2][2]
    for ($positionIndex = 1; $positionIndex <= 5; $positionIndex++) {
        switch ($positionIndex) {
            case 1:
                $title = '万位';
                break;
            case 2:
                $title = '千位';
                break;
            case 3:
                $title = '百位';
                break;
            case 4:
                $title = '十位';
                break;
            case 5:
                $title = '个位';
                break;
        }
        $thirdBetList[$positionIndex]['name'] = $title;
    }

    for ($positionIndex = 1; $positionIndex <= 5; $positionIndex++) {
        for ($index = 1; $index <= 2; $index++) {
            $oddsData = $odds[3][$positionIndex][$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $thirdBetList[$positionIndex]['detail'][$index % 2] = $oddsData;
            $thirdBetList[$positionIndex]['detail'][$index % 2]['name'] = $detailName;
            $thirdBetList[$positionIndex]['detail'][$index % 2]['odds'] = $oddsData['odds_a'];
            $thirdBetList[$positionIndex]['detail'][$index % 2]['odds_id'] = $oddsData['odds_id'];
        }

        for ($index = 1; $index <= 2; $index++) {
            $oddsData = $odds[2][$positionIndex][$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $thirdBetList[$positionIndex]['detail'][$index + 1] = $oddsData;
            $thirdBetList[$positionIndex]['detail'][$index + 1]['name'] = $detailName;
            $thirdBetList[$positionIndex]['detail'][$index + 1]['odds'] = $oddsData['odds_a'];
            $thirdBetList[$positionIndex]['detail'][$index + 1]['odds_id'] = $oddsData['odds_id'];
        }

    }

    $result['firstBetList'] = $firstBetList;
    $result['secondBetList'] = $secondBetList;
    $result['thirdBetList'] = $thirdBetList;

    $odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-fulissc', json_encode($result));
    echo json_encode($result);
}

unset($odds_handle);

exit();
