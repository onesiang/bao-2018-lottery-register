<?php
define('KGS', true);
require '../../../library/include/global.php';
// require '../../../library/include/variable.php';

$lottery = 12;
$odds_handle = new LotteryOdds();
$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-pk10');
if ($result) {
    echo $result;
} else {
    $odds = $odds_handle->getOddsList($lottery);

    $firstBetList = [];
    $secondBetList = [];
    $thirdBetList = [];
    $fourthBetList = [];

    // 名次1~5 [1][1][1]冠軍 一號
    $gameData = $odds[1];
    for ($positionIndex = 1; $positionIndex <= 5; $positionIndex++) {
        $positionData = $gameData[$positionIndex];
        for ($index = 1; $index <= 10; $index++) {
            $oddsData = $positionData[$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $firstBetList[$positionIndex - 1]['name'] = $title;
            $firstBetList[$positionIndex - 1]['detail'][$index - 1] = $oddsData;
            $firstBetList[$positionIndex - 1]['detail'][$index - 1]['name'] = $detailName;
            $firstBetList[$positionIndex - 1]['detail'][$index - 1]['odds'] = $oddsData['odds_a'];
            $firstBetList[$positionIndex - 1]['detail'][$index - 1]['odds_id'] = $oddsData['odds_id'];
        }
    }

    //玩法1 名次6~10 [1][6][1]第六位 一號
    for ($positionIndex = 6; $positionIndex <= 10; $positionIndex++) {
        $positionData = $gameData[$positionIndex];
        for ($index = 1; $index <= 10; $index++) {
            $oddsData = $positionData[$index];
            $tempString = explode('|', $oddsData['name']);
            $title = $tempString[0];
            $detailName = $tempString[1];
            $secondBetList[$positionIndex - 6]['name'] = $title;
            $secondBetList[$positionIndex - 6]['detail'][$index - 1] = $oddsData;
            $secondBetList[$positionIndex - 6]['detail'][$index - 1]['name'] = $detailName;
            $secondBetList[$positionIndex - 6]['detail'][$index - 1]['odds'] = $oddsData['odds_a'];
            $secondBetList[$positionIndex - 6]['detail'][$index - 1]['odds_id'] = $oddsData['odds_id'];
        }
    }

    //玩法6 冠亚和  [6][0][1]冠亚和|小  [6][0][2]冠亚和|大
    $oddsData = $odds[6][0][2]; //冠亚和|大
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $thirdBetList[0]['name'] = $title;
    $thirdBetList[0]['detail'][0] = $oddsData;
    $thirdBetList[0]['detail'][0]['name'] = $detailName; //冠亚和|大
    $thirdBetList[0]['detail'][0]['odds'] = $oddsData['odds_a']; //冠亚和|大
    $thirdBetList[0]['detail'][0]['odds_id'] = $oddsData['odds_id']; //冠亚和|大

    $oddsData = $odds[6][0][1]; //冠亚和|小
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $thirdBetList[0]['name'] = $title;
    $thirdBetList[0]['detail'][1] = $oddsData;
    $thirdBetList[0]['detail'][1]['name'] = $detailName; //冠亚和|小
    $thirdBetList[0]['detail'][1]['odds'] = $oddsData['odds_a']; //冠亚和|小
    $thirdBetList[0]['detail'][1]['odds_id'] = $oddsData['odds_id']; //冠亚和|小

//玩法5 冠亚和  [5][0][1]冠亚和|單  [5][0][2]冠亚和|雙
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $odds[5][0][$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $thirdBetList[0]['name'] = $title;
        $thirdBetList[0]['detail'][$index + 1] = $oddsData;
        $thirdBetList[0]['detail'][$index + 1]['name'] = $detailName;
        $thirdBetList[0]['detail'][$index + 1]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index + 1]['odds_id'] = $oddsData['odds_id'];
    }

    //玩法4  3~19 [4][0][3]為3 [4][0][19]為19
    $gameData = $odds[4];
    $positionData = $gameData[0];
    for ($index = 3; $index <= 19; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $thirdBetList[0]['name'] = $title;
        $thirdBetList[0]['detail'][$index + 1] = $oddsData;
        $thirdBetList[0]['detail'][$index + 1]['name'] = $detailName;
        $thirdBetList[0]['detail'][$index + 1]['odds'] = $oddsData['odds_a'];
        $thirdBetList[0]['detail'][$index + 1]['odds_id'] = $oddsData['odds_id'];
    }

    //雙面 冠軍 龍[7][1][1] 虎[7][1][2] 大[3][1][2] 小[3][1][1] 單[2][1][1] 雙[2][1][2]
    //雙面 第二名 龍[7][2]][1] 虎[7][2][2] 大[3][2][2] 小[3][2][1] 單[2][2][1] 雙[2][2][2]
    $nameArray = ['', '冠军', '亚军', '季军', '第四名', '第五名', '第六名', '第七名', '第八名', '第九名', '第十名'];
    for ($positionIndex = 1; $positionIndex <= 10; $positionIndex++) {
        $title = $nameArray[$positionIndex];

        $fourthBetList[$positionIndex - 1]['name'] = $title;

        //龍虎
        if ($positionIndex < 6) {
            $positionData = $odds[7][$positionIndex];
            for ($index = 1; $index <= 2; $index++) {
                $oddsData = $positionData[$index];
                // $tempString = explode('|',$oddsData['name']);
                // $detailName = $tempString[1];
                $fourthBetList[$positionIndex - 1]['detail'][$index - 1] = $oddsData;
                // $fourthBetList[$positionIndex-1]['detail'][$index-1]['name'] = $detailName;
                $fourthBetList[$positionIndex - 1]['detail'][$index - 1]['odds'] = $oddsData['odds_a'];
                $fourthBetList[$positionIndex - 1]['detail'][$index - 1]['odds_id'] = $oddsData['odds_id'];
            }
        }

        //大
        $oddsData = $odds[3][$positionIndex][2];
        $tempString = explode('|', $oddsData['name']);
        $detailName = $tempString[1];
        $fourthBetList[$positionIndex - 1]['detail'][2] = $oddsData;
        $fourthBetList[$positionIndex - 1]['detail'][2]['name'] = $detailName;
        $fourthBetList[$positionIndex - 1]['detail'][2]['odds'] = $oddsData['odds_a'];
        $fourthBetList[$positionIndex - 1]['detail'][2]['odds_id'] = $oddsData['odds_id'];

        //小
        $oddsData = $odds[3][$positionIndex][1];
        $tempString = explode('|', $oddsData['name']);
        $detailName = $tempString[1];
        $fourthBetList[$positionIndex - 1]['detail'][3] = $oddsData;
        $fourthBetList[$positionIndex - 1]['detail'][3]['name'] = $detailName;
        $fourthBetList[$positionIndex - 1]['detail'][3]['odds'] = $oddsData['odds_a'];
        $fourthBetList[$positionIndex - 1]['detail'][3]['odds_id'] = $oddsData['odds_id'];

        //單雙
        $positionData = $odds[2][$positionIndex];
        for ($index = 1; $index <= 2; $index++) {
            $oddsData = $positionData[$index];
            $tempString = explode('|', $oddsData['name']);
            $detailName = $tempString[1];
            $fourthBetList[$positionIndex - 1]['detail'][$index + 3] = $oddsData;
            $fourthBetList[$positionIndex - 1]['detail'][$index + 3]['name'] = $detailName;
            $fourthBetList[$positionIndex - 1]['detail'][$index + 3]['odds'] = $oddsData['odds_a'];
            $fourthBetList[$positionIndex - 1]['detail'][$index + 3]['odds_id'] = $oddsData['odds_id'];
        }
    }

    $result['firstBetList'] = $firstBetList;
    $result['secondBetList'] = $secondBetList;
    $result['thirdBetList'] = $thirdBetList;
    $result['fourthBetList'] = $fourthBetList;

    $odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-pk10', json_encode($result));
    echo json_encode($result);
}

unset($odds_handle);

exit();
