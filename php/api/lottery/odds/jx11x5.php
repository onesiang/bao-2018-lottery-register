<?php

define('KGS', true);
require '../../../library/include/global.php';

$lottery = 17;
$odds_handle = new LotteryOdds();
$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-jx11x5');

if ($result) {
    echo $result;
} else {
    $odds = $odds_handle->getOddsListForSixMark($lottery);

    $maintype = []; //玩法1～7
    $danshuang = []; //單雙·
    $daxiao = []; // 大小
    $zongdanshuang = []; //總單雙
    $zongdaxiao = []; //總大小
    $longhu = []; // 龍虎
    $qianzhonghou = []; //前三，中三，後三

    if (count($odds) > 0) {
        foreach ($odds as $key => $value) {

            if ($value['type'] != 6) {
                $value['name'] = explode('|', $value['name'])[1];
            }

            if (is_numeric($value['name'])) {
                $value['digital'] = true;
            } else {
                $value['digital'] = false;
            }

            $value['odds'] = $value['odds_a'];
            unset($value['odds_a'], $value['odds_b']);
            switch ($value['type']) {
                case 1:
                    $maintype[$value['position']][$value['number']] = $value;
                    break;
                case 2:
                    $danshuang[$value['position']][$value['number']] = $value;
                    break;
                case 3:
                    $daxiao[$value['position']][$value['number']] = $value;
                    break;
                case 4:
                    $zongdanshuang[$value['number']] = $value;
                    break;
                case 5:
                    if ($value['number'] == 3) {
                        continue;
                    }
                    $zongdaxiao[$value['number']] = $value;
                    break;
                case 6:
                    $longhu[$value['number']] = $value;
                    break;
                case 7:
                    $qianzhonghou[$value['position']][$value['number']] = $value;
                    break;

            }
        }
    }
    $empty = array(
        'name' => '版面配置用',
        'odds' => 0,
        'useless' => true,
    );
    $maintype[1][12] = $empty;
    $maintype[2][12] = $empty;
    $maintype[3][12] = $empty;
    $maintype[4][12] = $empty;
    $maintype[5][12] = $empty;
    // $maintype[1][] = $empty;
    $OddsList11x5 = array(
        'firstBetList' => array(
            array(
                'name' => '第一球',
                'detail' => array_merge(
                    $maintype[1], $danshuang[1], $daxiao[1]
                ),
            ),
        ),
        'secondBetList' => array(
            array(
                'name' => '第二球',
                'detail' => array_merge(
                    $maintype[2], $danshuang[2], $daxiao[2]
                ),
            ),
        ),
        'thirdBetList' => array(
            array(
                'name' => '第三球',
                'detail' => array_merge(
                    $maintype[3], $danshuang[3], $daxiao[3]
                ),
            ),
        ),
        'fourthBetList' => array(
            array(
                'name' => '第四球',
                'detail' => array_merge(
                    $maintype[4], $danshuang[4], $daxiao[4]
                ),
            ),
        ),
        'fifthBetList' => array(
            array(
                'name' => '第五球',
                'detail' => array_merge(
                    $maintype[5], $danshuang[5], $daxiao[5]
                ),
            ),
        ),
        'sixthBetList' => array(
            array(
                'name' => '总和',
                'detail' => array_merge(
                    $zongdanshuang, $zongdaxiao
                ),
            ),
            array(
                'name' => '龙虎',
                'detail' => $longhu,
            ),
        ),
        'seventhBetList' => array(
            array(
                'name' => '前三',
                'detail' => $qianzhonghou[1],
            ),
            array(
                'name' => '中三',
                'detail' => $qianzhonghou[2],

            ),
            array(
                'name' => '后三',
                'detail' => $qianzhonghou[3],

            ),
        ),
    );
    $odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-jx11x5', json_encode($OddsList11x5));
    unset($odds_handle);
    echo json_encode($OddsList11x5);
    exit();
}
