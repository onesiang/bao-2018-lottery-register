<?php
/**
 * Created by PhpStorm.
 * User: kang
 * Date: 2018/8/3
 * Time: 9:06 PM
 */
define('KGS', true);
require '../../../library/include/global.php';

$lottery = 9;
$odds_handle = new LotteryOdds();
$result = $odds_handle->getRedisData(CUSTOMER_NAME . '-odds-liuhecai');

if ($result) {
    echo $result;
    exit;
} else {
    $odds = $odds_handle->getOddsListForSixMark($lottery);
    $danMa = []; //单码
    $mixed = array(
        'xiaoda' => [], //小大
        'danshuang' => [], //单双
        'hedahexiao' => [], //和大和小
        'hedanheshuang' => [], //和单和双
        'weidaweixiao' => [], //尾大尾小
        'dadanxiaodan' => [], //大单大双
        'dashuangxiaoshuang' => [], //大双小双
    ); //混合

    $sebo = []; //特马色波
    $banbo = []; //特马半波
    $banbanbo = []; //特马斑斑波
    $zhengma = []; //正码

    $zhengTe['one'] = []; //正1特
    $zhengTe['two'] = [];
    $zhengTe['three'] = [];
    $zhengTe['four'] = [];
    $zhengTe['five'] = [];
    $zhengTe['six'] = [];

    $zhengMa['one'] = []; //正1吗
    $zhengMa['two'] = [];
    $zhengMa['three'] = [];
    $zhengMa['four'] = [];
    $zhengMa['five'] = [];
    $zhengMa['six'] = [];

    $lianxiao[2] = []; //连销
    $lianxiao[3] = [];
    $lianxiao[4] = [];
    $lianxiao[5] = [];

    $liamma = []; //连码
    $quanbuzhong = []; //全部中

    $teYiWeiXiao = []; //特肖 一肖 尾肖

    if (count($odds) > 0) {
        foreach ($odds as $key => $value) {
            if ($value['type'] !== 100 && $value['type'] !== 110) {
                $value['name'] = explode('|', $value['name'])[1];
            }

            $value['odds'] = $value['odds_a'];
            unset($value['odds_a'], $value['odds_b']);
            switch ($value['type']) {
                case 1:
                    $danMa[] = $value;
                    break;
                case 2:
                    $mixed['danshuang'][] = $value;
                    break;
                case 3:
                    $xiaoDa['xiaoda'][] = $value;
                    break;
                case 5:
                    $mixed['weidaweixiao'][] = $value;
                    break;
                case 6:
                    $mixed['hedanheshuang'][] = $value;
                    break;
                case 7:
                    $mixed['hedahexiao'][] = $value;
                    break;
                case 8:
                    $sebo[] = $value;
                    break;
                case 10: //半小单
                    break;
                case 11: //半小双
                    break;
                case 12: //半大单
                    break;
                case 13: //半大双
                    break;
                case 14: //特马家禽
                    break;
                case 15: //特马野兽
                    break;
                case 18: //234567891911肖
                    break;
                case 20: //鼠牛虎兔
                    break;
                case 22: //半波红蛋
                case 23: //半波|红双
                case 24: //半波|红小
                case 25: //半波|红大
                case 26: //半波|绿单
                case 27: //半波|绿双
                case 28: //半波|绿小
                case 29: //半波|绿大
                case 30: //半波|蓝单
                case 31: //半波|蓝双
                case 32: //半波|蓝小
                case 33: //半波|蓝大
                    $banbo[] = $value;
                    break;
                case 34: //半波|红合单
                case 35: //半波|红合双
                case 36: //半波|绿合单
                case 37: //半波|绿合双
                case 38: //半波|蓝合单
                case 39: //半波|蓝合双
                    $banbanbo[] = $value;
                    break;
                case 50: //正码
                    $zhengma[$value['name']] = $value;
                    break;
                case 60: //总和|单 双
                    //$zhengma[$value['name']] = $value;
                    break;
                case 61: //总和|小 大
                    //$zhengma[$value['name']] = $value;
                    break;
                case 70: //正特
                    switch ($value['position']) {
                        case 1:
                            $zhengTe['one'][$value['name']] = $value;
                            break;
                        case 2:
                            $zhengTe['two'][$value['name']] = $value;
                            break;
                        case 3:
                            $zhengTe['three'][$value['name']] = $value;
                            break;
                        case 4:
                            $zhengTe['four'][$value['name']] = $value;
                            break;
                        case 5:
                            $zhengTe['five'][$value['name']] = $value;
                            break;
                        case 6:
                            $zhengTe['six'][$value['name']] = $value;
                            break;
                    }
                    break;
                case 71: //正码x|单 双
                case 72: //正码x|xx
                case 73: //正码x|xx
                case 74: //正码x|xx
                case 75: //正码x|xx
                case 76: //正码x|xx
                    switch ($value['position']) {
                        case 1:
                            $zhengMa['one'][$value['type']][$value['position']][] = $value;
                            break;
                        case 2:
                            $zhengMa['two'][$value['type']][$value['position']][] = $value;
                            break;
                        case 3:
                            $zhengMa['three'][$value['type']][$value['position']][] = $value;
                            break;
                        case 4:
                            $zhengMa['four'][$value['type']][$value['position']][] = $value;
                            break;
                        case 5:
                            $zhengMa['five'][$value['type']][$value['position']][] = $value;
                            break;
                        case 6:
                            $zhengMa['six'][$value['type']][$value['position']][] = $value;
                            break;
                    }
                    break;
                case 9: //特码|x
                case 90: //一肖|x
                case 91: //尾数|0
                    $teYiWeiXiao[$value['type']]['odds'] = $value['odds'];
                    $teYiWeiXiao[$value['type']]['detail'][] = $value;
                    break;
                case 100: //2345肖连
                    $lianxiao['detail'][$value['position']][$value['number']] = $value;
                    break;
                case 101: //2345肖连
                    $lianxiao[$value['position']]['odds'] = $value['odds'];
                    $lianxiao[$value['position']]['odds_id'] = $value['odds_id'];
                    break;
                case 105: // 三中二
                case 106: // 三全中
                case 107: // 二全中
                case 108: // 二中特
                case 109: // 特串
                case 111: // 中二,中三的賠率
                case 112: // 中二, 中特的賠率
                    if ($value['type'] != 111 && $value['type'] != 112) {
                        $liamma[$value['type']]['odds'] = $value['odds'];
                        $liamma[$value['type']]['odds_id'] = $value['odds_id'];
                    } else if ($value['type'] == 111) {
                        $liamma['105']['odds'] .= $value['odds'] . '/';
                    } else if ($value['type'] == 112) {
                        $liamma['108']['odds'] .= $value['odds'] . '/';
                    }
                    break;
                case 110:
                    $quanbuzhong[$value['position']]['detail'][] = $value;
                    break;
                case 113:
                    $quanbuzhong[$value['position']]['odds'] = $value['odds'];
                    $quanbuzhong[$value['position']]['odds_id'] = $value['odds_id'];
                    break;
            }
        }
    }
    $sixMarkOddsList = array(
        'temaBetList' => array(
            array(
                'name' => '单码',
                'detail' => $danMa,
            ),
            array(
                'name' => '混合',
                'detail' => array_merge(
                    $mixed['xiaoda'],
                    $mixed['danshuang'],
                    $mixed['hedahexiao'],
                    $mixed['hedanheshuang'],
                    $mixed['weidaweixiao']
                ),
            ),
        ),
        'seboBetList' => array(
            array(
                'name' => '特码色波',
                'detail' => $sebo,
            ),
            array(
                'name' => '特码半波',
                'detail' => $banbo,
            ),
            array(
                'name' => '特码半半波',
                'detail' => $banbanbo,
            ),
        ),
        'zhengmaBetList' => array(
            array(
                'name' => '正码',
                'detail' => $zhengma,
            ),
        ),
        'lianxiaoBetList' => array(
            array(
                'name' => '二肖连',
                'odds' => $lianxiao[2]['odds'],
                'odds_id' => $lianxiao[2]['odds_id'],
                'detail' => $lianxiao['detail'][2],
            ),
            array(
                'name' => '三肖连',
                'odds' => $lianxiao[3]['odds'],
                'odds_id' => $lianxiao[3]['odds_id'],
                'detail' => $lianxiao['detail'][3],
            ),
            array(
                'name' => '四肖连',
                'odds' => $lianxiao[4]['odds'],
                'odds_id' => $lianxiao[4]['odds_id'],
                'detail' => $lianxiao['detail'][4],
            ),
            array(
                'name' => '五肖连',
                'odds' => $lianxiao[5]['odds'],
                'odds_id' => $lianxiao[5]['odds_id'],
                'detail' => $lianxiao['detail'][5],
            ),
        ),
        'zhengtemaBetList' => array(
            array(
                'name' => '正一特',
                'detail' => $zhengTe['one'],
            ),
            array(
                'name' => '正二特',
                'detail' => $zhengTe['two'],
            ),
            array(
                'name' => '正三特',
                'detail' => $zhengTe['three'],
            ),
            array(
                'name' => '正四特',
                'detail' => $zhengTe['four'],
            ),
            array(
                'name' => '正五特',
                'detail' => $zhengTe['five'],
            ),
            array(
                'name' => '正六特',
                'detail' => $zhengTe['six'],
            ),
        ),
        'zhengmanumberBetList' => array(
            array(
                'name' => '正码1',
                'detail' => array_merge(
                    $zhengMa['one'][71][1],
                    $zhengMa['one'][72][1],
                    $zhengMa['one'][73][1],
                    $zhengMa['one'][74][1],
                    $zhengMa['one'][75][1],
                    $zhengMa['one'][76][1]
                ),
            ),
            array(
                'name' => '正码2',
                'detail' => array_merge(
                    $zhengMa['two'][71][2],
                    $zhengMa['two'][72][2],
                    $zhengMa['two'][73][2],
                    $zhengMa['two'][74][2],
                    $zhengMa['two'][75][2],
                    $zhengMa['two'][76][2]
                ),
            ),
            array(
                'name' => '正码3',
                'detail' => array_merge(
                    $zhengMa['three'][71][3],
                    $zhengMa['three'][72][3],
                    $zhengMa['three'][73][3],
                    $zhengMa['three'][74][3],
                    $zhengMa['three'][75][3],
                    $zhengMa['three'][76][3]
                ),
            ),
            array(
                'name' => '正码4',
                'detail' => array_merge(
                    $zhengMa['four'][71][4],
                    $zhengMa['four'][72][4],
                    $zhengMa['four'][73][4],
                    $zhengMa['four'][74][4],
                    $zhengMa['four'][75][4],
                    $zhengMa['four'][76][4]
                ),
            ),
            array(
                'name' => '正码5',
                'detail' => array_merge(
                    $zhengMa['five'][71][5],
                    $zhengMa['five'][72][5],
                    $zhengMa['five'][73][5],
                    $zhengMa['five'][74][5],
                    $zhengMa['five'][75][5],
                    $zhengMa['five'][76][5]
                ),
            ),
            array(
                'name' => '正码6',
                'detail' => array_merge(
                    $zhengMa['six'][71][6],
                    $zhengMa['six'][72][6],
                    $zhengMa['six'][73][6],
                    $zhengMa['six'][74][6],
                    $zhengMa['six'][75][6],
                    $zhengMa['six'][76][6]
                ),
            ),
        ),
        'lianmaBetList' => array(
            array(
                'name' => '三全中',
                'odds' => $liamma[106]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $liamma[106]['odds_id'],
            ),
            array(
                'name' => '三中二',
                'odds' => trim($liamma[105]['odds'], '/'),
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $liamma[105]['odds_id'],
            ),
            array(
                'name' => '二中全',
                'odds' => $liamma[107]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $liamma[107]['odds_id'],
            ),
            array(
                'name' => '二中特',
                'odds' => trim($liamma[108]['odds'], '/'),
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $liamma[108]['odds_id'],
            ),
            array(
                'name' => '特串',
                'odds' => $liamma[109]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $liamma[109]['odds_id'],
            ),
        ),
        'quanbuzhongBetList' => array(
            array(
                'name' => '五不中',
                'odds' => $quanbuzhong[5]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[5]['odds_id'],
            ),
            array(
                'name' => '六不中',
                'odds' => $quanbuzhong[6]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[6]['odds_id'],
            ),
            array(
                'name' => '七不中',
                'odds' => $quanbuzhong[7]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[7]['odds_id'],
            ),
            array(
                'name' => '八不中',
                'odds' => $quanbuzhong[8]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[8]['odds_id'],
            ),
            array(
                'name' => '九不中',
                'odds' => $quanbuzhong[9]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[9]['odds_id'],
            ),
            array(
                'name' => '十不中',
                'odds' => $quanbuzhong[10]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[10]['odds_id'],
            ),
            array(
                'name' => '十一不中',
                'odds' => $quanbuzhong[11]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[11]['odds_id'],
            ),
            array(
                'name' => '十二不中',
                'odds' => $quanbuzhong[12]['odds'],
                'detail' => $quanbuzhong[0]['detail'],
                'odds_id' => $quanbuzhong[12]['odds_id'],
            ),
        ),
        'shengxiaoBetList' => array(
            array(
                'name' => '特肖',
                'odds' => $teYiWeiXiao[9]['odds'],
                'detail' => $teYiWeiXiao[9]['detail'],
            ),
            array(
                'name' => '一肖',
                'odds' => $teYiWeiXiao[90]['odds'],
                'detail' => $teYiWeiXiao[90]['detail'],
            ),
            array(
                'name' => '尾数',
                'odds' => $teYiWeiXiao[91]['odds'],
                'detail' => $teYiWeiXiao[91]['detail'],
            ),
        ),
    );
    $odds_handle->dbOddsToRedis(CUSTOMER_NAME . '-odds-liuhecai', json_encode($sixMarkOddsList));
    unset($odds_handle);
    echo json_encode($sixMarkOddsList);
    exit();
}
