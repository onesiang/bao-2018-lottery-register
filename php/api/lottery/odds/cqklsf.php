<?php
define('KGS', true);
require '../../../library/include/global.php';
// require '../../../library/include/variable.php';

$lottery = 13;
$odds_handle = new LotteryOdds();

$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-cqklsf');

if ($result){
    echo $result;
}else{

$odds = $odds_handle->getOddsList($lottery);

$BetList = [];
$ninthBetList = [];

for ($positionIndex = 1; $positionIndex <= 8; $positionIndex++) {
    //玩法單純的球球
    $gameData = $odds[1];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 20; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][$index - 1] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][$index - 1]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][$index - 1]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][$index - 1]['odds_id'] = $oddsData['odds_id'];
    }


    //玩法單雙
    $gameData = $odds[2];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][$index + 19] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][$index + 19]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][$index + 19]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][$index + 19]['odds_id'] = $oddsData['odds_id'];
    }

    //玩法大小
    $gameData = $odds[3];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][($index%2) + 22] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][($index%2) + 22]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][($index%2) + 22]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][($index%2) + 22]['odds_id'] = $oddsData['odds_id'];
    }

    //玩法尾小 尾大
    $gameData = $odds[4];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 2; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][$index + 23] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][$index + 23]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][$index + 23]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][$index + 23]['odds_id'] = $oddsData['odds_id'];
    }

    //玩法中 發 白
    $gameData = $odds[5];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 3; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][$index + 25] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][$index + 25]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][$index + 25]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][$index + 25]['odds_id'] = $oddsData['odds_id'];
    }

    //玩法西 南 北
    $gameData = $odds[6];
    $positionData = $gameData[$positionIndex];
    for ($index = 1; $index <= 4; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $BetList[$positionIndex - 1][0]['name'] = $title;
        $BetList[$positionIndex - 1][0]['detail'][$index + 28] = $oddsData;
        $BetList[$positionIndex - 1][0]['detail'][$index + 28]['name'] = $detailName;
        $BetList[$positionIndex - 1][0]['detail'][$index + 28]['odds'] = $oddsData['odds_a'];
        $BetList[$positionIndex - 1][0]['detail'][$index + 28]['odds_id'] = $oddsData['odds_id'];
    }

}




// // 總和單雙
for ($index = 1; $index <= 2; $index++) {
    $oddsData = $odds[7][0][$index];
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $ninthBetList[0]['name'] = $title;
    $ninthBetList[0]['detail'][$index -1] = $oddsData;
    $ninthBetList[0]['detail'][$index -1]['name'] = $detailName;
    $ninthBetList[0]['detail'][$index -1]['odds'] = $oddsData['odds_a'];
    $ninthBetList[0]['detail'][$index -1]['odds_id'] = $oddsData['odds_id'];
}

// // 總和小大
for ($index = 1; $index <= 2; $index++) {
    $oddsData = $odds[8][0][$index];
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $ninthBetList[0]['name'] = $title;
    $ninthBetList[0]['detail'][$index +1] = $oddsData;
    $ninthBetList[0]['detail'][$index +1]['name'] = $detailName;
    $ninthBetList[0]['detail'][$index +1]['odds'] = $oddsData['odds_a'];
    $ninthBetList[0]['detail'][$index +1]['odds_id'] = $oddsData['odds_id'];
}

// // 總和尾小大
for ($index = 1; $index <= 2; $index++) {
    $oddsData = $odds[9][0][$index];
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $ninthBetList[0]['name'] = $title;
    $ninthBetList[0]['detail'][$index +3] = $oddsData;
    $ninthBetList[0]['detail'][$index +3]['name'] = $detailName;
    $ninthBetList[0]['detail'][$index +3]['odds'] = $oddsData['odds_a'];
    $ninthBetList[0]['detail'][$index +3]['odds_id'] = $oddsData['odds_id'];
}

// // 總和龍虎
for ($index = 1; $index <= 2; $index++) {
    $oddsData = $odds[10][0][$index];
    $ninthBetList[1]['name'] = '龙虎';
    $ninthBetList[1]['detail'][$index +5] = $oddsData;
    $ninthBetList[1]['detail'][$index +5]['name'] = $oddsData['name'];
    $ninthBetList[1]['detail'][$index +5]['odds'] = $oddsData['odds_a'];
    $ninthBetList[1]['detail'][$index +5]['odds_id'] = $oddsData['odds_id'];
}


$result['firstBetList'] = $BetList[0];
$result['secondBetList'] = $BetList[1];
$result['thirdBetList'] = $BetList[2];
$result['fourthBetList'] = $BetList[3];
$result['fifthBetList'] = $BetList[4];
$result['sixthBetList'] = $BetList[5];
$result['seventhBetList'] = $BetList[6];
$result['eighthBetList'] = $BetList[7];
$result['ninthBetList'] = $ninthBetList;

$odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-cqklsf', json_encode($result));
echo json_encode($result);
}

unset($odds_handle);

exit();
