<?php
define('KGS', true);
require '../../../library/include/global.php';
// require '../../../library/include/variable.php';

$lottery = 28;
$odds_handle = new LotteryOdds();
$odds = $odds_handle->getOddsList($lottery);


$result = $odds_handle->getRedisData(CUSTOMER_NAME.'-odds-bj28');

if ($result){
    echo $result;
}else{

$odds = $odds_handle->getOddsList($lottery);

$firstBetList = [];
$secondBetList = [];
$thirdBetList = [];

// 第一到三位
$gameData = $odds[1];
for ($positionIndex = 1; $positionIndex <= 3; $positionIndex++) {
    $positionData = $gameData[$positionIndex];
    for ($index = 0; $index <= 9; $index++) {
        $oddsData = $positionData[$index];
        $tempString = explode('|', $oddsData['name']);
        $title = $tempString[0];
        $detailName = $tempString[1];
        $firstBetList[$positionIndex - 1]['name'] = $title;
        $firstBetList[$positionIndex - 1]['detail'][$index] = $oddsData;
        $firstBetList[$positionIndex - 1]['detail'][$index]['name'] = $detailName;
        $firstBetList[$positionIndex - 1]['detail'][$index]['odds'] = $oddsData['odds_a'];
        $firstBetList[$positionIndex - 1]['detail'][$index]['odds_id'] = $oddsData['odds_id'];
    }
}

// 總和
for ($index = 0; $index <= 27; $index++) {
    $oddsData = $odds[2][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $title = $tempString[0];
    $detailName = $tempString[1];
    $secondBetList[0]['name'] = $title;
    $secondBetList[0]['detail'][$index] = $oddsData;
    $secondBetList[0]['detail'][$index]['name'] = $detailName;
    $secondBetList[0]['detail'][$index]['odds'] = $oddsData['odds_a'];
    $secondBetList[0]['detail'][$index]['odds_id'] = $oddsData['odds_id'];
}


// 大小
$count = 0;
$thirdBetList[0]['name'] = '混合';
for ($index = 2; $index >= 1; $index--) {
    $oddsData = $odds[3][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $detailName = $tempString[1];
    $thirdBetList[0]['detail'][$count] = $oddsData;
    $thirdBetList[0]['detail'][$count]['name'] = $detailName;
    $thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
    $thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];
    $count ++;
}
//單雙
for ($index = 1; $index <= 2; $index++) {
    $oddsData = $odds[4][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $detailName = $tempString[1];
    $thirdBetList[0]['detail'][$count] = $oddsData;
    $thirdBetList[0]['detail'][$count]['name'] = $detailName;
    $thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
    $thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];
    $count ++;
}
//大單 小單
for ($index = 2; $index >= 1; $index--) {
    $oddsData = $odds[5][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $detailName = $tempString[1];
    $thirdBetList[0]['detail'][$count] = $oddsData;
    $thirdBetList[0]['detail'][$count]['name'] = $detailName;
    $thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
    $thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];
    $count ++;
}
//大雙 小雙
for ($index = 2; $index >= 1; $index--) {
    $oddsData = $odds[6][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $detailName = $tempString[1];
    $thirdBetList[0]['detail'][$count] = $oddsData;
    $thirdBetList[0]['detail'][$count]['name'] = $detailName;
    $thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
    $thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];
    $count ++;
}
//極大 極小
for ($index = 2; $index >= 1; $index--) {
    $oddsData = $odds[7][1][$index];
    $tempString = explode('|', $oddsData['name']);
    $detailName = $tempString[1];
    $thirdBetList[0]['detail'][$count] = $oddsData;
    $thirdBetList[0]['detail'][$count]['name'] = $detailName;
    $thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
    $thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];
    $count ++;
}
//豹子
$oddsData = $odds[8][1][1];
$tempString = explode('|', $oddsData['name']);
$detailName = $tempString[1];
$thirdBetList[0]['detail'][$count] = $oddsData;
$thirdBetList[0]['detail'][$count]['name'] = $detailName;
$thirdBetList[0]['detail'][$count]['odds'] = $oddsData['odds_a'];
$thirdBetList[0]['detail'][$count]['odds_id'] = $oddsData['odds_id'];


$result['firstBetList'] = $firstBetList;
$result['secondBetList'] = $secondBetList;
$result['thirdBetList'] = $thirdBetList;

$odds_handle->dbOddsToRedis(CUSTOMER_NAME.'-odds-bj28', json_encode($result));
echo json_encode($result);
}

unset($odds_handle);

exit();
