<?php
define('KGS', true);
require '../library/include/global.php';
require '../library/include/variable.php';

ini_set('display_errors', '1');
error_reporting(E_ALL);

$picture_handle = new Picture();
$data = $picture_handle->qrcode_picture();
unset($picture_handle);

$azure_handle = new AzureStorage();
$img_url = $azure_handle->get_blob_url($data['list'][0]['path']);


echo(json_encode($img_url));

exit();
