<?php
define('KGS', true);
require 'library/include/global.php';

$promotion_code = '520520';

$config_handle = new Config();
$config = $config_handle->getByName(array('VERSION', 'NAME', 'DC_DOMAIN', 'MCDN', 'CS_URL', 'SHOW_AGENT', 'MEMBER_LOGIN_VALIDATE', 'REGISTER_OPTIONS'));
$register_options = $config_handle->registerOptions($config['REGISTER_OPTIONS']);
unset($config_handle);


//$cdn = isset($config['MCDN']) ? $config['MCDN'] : '';
//if ($cdn) {
//    $cdn = explode(',', $cdn);
//    $cdn_rand = rand(0, count($cdn) - 1);
//    $cdn = $cdn[$cdn_rand];
//}
//
//$domain = $_SERVER['HTTP_HOST'];
//$domain_data = explode('.', $domain);
//if (2 == count($domain_data)) {
//    $domain = 'www.' . $domain;
//}
//
//$agent = '';
//$agent_handle = new Agent();
//$agent_id = $agent_handle->getByUrl($domain);
//if ($agent_id) {
//    $agent = $agent_id;
//} else {
//    $agent = kg_get('ag', '');
//    if ('' != $agent) {
//        if (!$agent_handle->getValidByAgentId($agent)) {
//            $agent = '';
//        }
//    }
//}
//unset($agent_handle);



?>

<head>
    <title></title>
    <meta charset="utf-8">
    <noscript><iframe src=*.html></iframe></noscript>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <META NAME="" CONTENT="">

    <link href="/asset/component/bootstrap.min.css" rel="stylesheet">
    <link href="/asset/css/main.css" rel="stylesheet">
</head>

<body style="background-color:#efeff4;" >
<div class="row top-bar ">
    <h5 class="col-5 text-style"  >注册</h5>
</div>
<div class="container content">
    <table>
        <tr>
            <div class="input-wrapper">
                <input id="agent" name="agent" type="text"  class="text form-control" value="<?php kg_echo($promotion_code); ?>" readonly="readonly" required
                       placeholder="请输入介绍人邀请码"/>
            </div>

            <div class="input-wrapper">
                <input id="account" name="account" type="text" class="text form-control"
                       required placeholder="请输入帐号"/>
            </div>

            <div class="input-wrapper">
                <input id="password" name="password" type="password" class="text form-control"
                       required placeholder="请输入密码"/>
            </div>

            <div class="input-wrapper">
                <input id="password_confirm" name="password_confirm" type="password" class="text form-control"
                       required
                       placeholder="请再次输入密码"/>
            </div>

            <?php
            if ($config['MEMBER_LOGIN_VALIDATE']) {
            ?>
            <div class="input-wrapper">
                <input id="auth" name="auth" maxlength="4" type="text" class="text form-control" value=""  placeholder="请输入图形验证码">
                <img id="auth_code"  style="display:none;" border="0"   title="点击刷新图片" class="veriimg "/>
            </div>
            <?php
                }  ?>

        </tr>


        <tr>

            <span class="input_wrong_notice" id="register_account_tip1" style="display: none;"></span>
            <span class="input_wrong_notice" id="register_password_tip" style="display: none;"></span>
            <span class="input_wrong_notice" id="register_password_confirm_tip" style="display: none;"></span>


            <div class="input_wrong_notice">

            </div>
        </tr>


    </table>

    <table>
        <tr>
            <div >
                <button id="register_submit" variant="success"  class="text form-control">注册</button>

            </div>
            <h5>
            </h5>

            <div class="div-text-center" >

                <span class="gray-text">注册即表示你已同意</span> 《时时彩票网服务条款》

            </div>
            <h5>
            </h5>

            <div class="div-text-center">
                    <span>
                            <span class="gray-text">已有帐号？</span>
                    <span @click="" class="login-right-away">马上登录</span>
                    </span>
            </div>
        </tr>

    </table>



</div>

</body>
<footer>


</footer>

<script src="../asset/component/jquery.js"></script>
<script src="../asset/component/jquery.easing.min.js"></script>
<script src="../asset/component/jquery.min.js"></script>
<script src="../php/vendor/js/layer.js"></script>
<script src="../js/msg.js"></script>
<script src="../js/function.js"></script>
<?php //include('script.php');?>
<script type="text/javascript" src="../js/reg.js?v=<?php kg_echo($config['VERSION']);?>"></script>

<script>

    $(document).ready(function() {
        $('#register_submit').click(function() {

            var promote_code = $('#agent').val(),
                account = $('#account').val(),
                password = $('#password').val(),
                password_confirm = $('#password_confirm').val(),
                auth = $('#auth').val();


            $.ajax({
                type : "POST",
                url : '/api/member/register.php',
                data :
                    {
                        promote_code: promote_code,
                        account: account,
                        password: password,
                        password_confirm: password_confirm,
                        auth: auth,

                    },


                success: function(res) {
                    res = JSON.parse(res);

                    if(res[0] == 1){

                        msg_warning('注册成功', function (index) {
                            layer.close(index)
                        });

                    }else {

                        msg_warning(res[1]);

                    }

                },
                error : function(err) {

                }
            });
        });
    });

</script>




</html>


