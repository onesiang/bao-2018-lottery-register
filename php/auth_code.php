<?php

define('KGS', true);

require 'library/include/global.php';
session_start();
$captcha_handle = new Captcha();
$code = $captcha_handle->getCode();
$cache_id = isset($_GET['id']) ? $_GET['id'] : 0;
if ($cache_id) {
    $cache_handle = new Cache();
    $cache_handle->set($cache_id, $code, 1800); // 同SESSION一致
    unset($cache_handle);
} else {
    if (!isset($_SESSION)) {
        session_start();
    }
}
$_SESSION['auth_code'] = $code;

$captcha_handle->create();
unset($captcha_handle);
exit();
