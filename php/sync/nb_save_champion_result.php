<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/champion_result.pid')) {
    $key = isset($argv[1]) ? $argv[1] : '';
    if (!$key) {
        exit();
    }
    $cache = new Cache();
    $content = $cache->get($key);

    if ($content) {
        $sr_handle = new SportResult();
        $sr_handle->saveChampionResult($content);
        unset($sr_handle);
    }

    $cache->del($key);
    unset($cache);
}
else {
    kg_echo('This script file has aleady been running...');
}

exit();