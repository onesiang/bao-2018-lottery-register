<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/sss.pid')) {
    $sss_handle = new Sss();
    $sss_handle->getList();
    unset($sss_handle);
}
else {
    // kg_echo('This script file (sss) has aleady been running...');
}
exit();