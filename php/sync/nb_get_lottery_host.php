<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

$data = array();
$error = '';

$config_handle = new Config();
$config = $config_handle->getByName(array('PROXY_LOTTERY_HOST','LOTTERY_AUTO_CLOSE'));
if($config) {
    if($config['LOTTERY_AUTO_CLOSE']) {
        $error = 'lottery_auto_close';
    }
    else {
        $proxy_data = explode(':', $config['PROXY_LOTTERY_HOST']);
        $data['lottery_host'] = trim($proxy_data[0]);
        $data['lottery_port'] = trim($proxy_data[1]);
    }
}
unset($config_handle);

echo json_encode($data);

exit();
?>