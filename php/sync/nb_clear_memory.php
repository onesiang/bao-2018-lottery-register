<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/clear_memory.pid')) {
    $match_handle = new Match();
    $match_handle->clearMemory();
    unset($match_handle);
}
else {
    // kg_echo('This script file (clear_memory) has aleady been running...');
}
exit();