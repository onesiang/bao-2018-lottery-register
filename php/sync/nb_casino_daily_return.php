<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/casino_daily_return.pid')) {
    $casino_handle = new Casino();
    $casino_handle->dailyReturn();
    unset($casino_handle);
}
else {
    // kg_echo('This script file (casino_daily_return) has aleady been running...');
}
exit();