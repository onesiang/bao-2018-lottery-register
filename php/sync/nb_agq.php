<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/agq.pid')) {
    $agq_handle = new Agq();
    $agq_handle->getList();
    //$agq_handle->sync(); 这里是同步转账额度的 没必要
    unset($agq_handle);
}
else {
    // kg_echo('This script file (ag) has aleady been running...');
}
exit();