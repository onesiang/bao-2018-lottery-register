#!/bin/sh

PHP=/home/wewin/apt/php/bin/php
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

cd $CURRENT_PATH
pm2 delete lottery_client
sleep 1
pm2 start $CURRENT_PATH/nb_lottery_client.js
