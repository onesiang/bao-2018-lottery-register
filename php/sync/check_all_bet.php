<?php
define('KGS', true);
define('NO_XHPROF', true);

require '../library/include/global.php';

$lottery_process_handle = new Lottery();
$get_round = $lottery_process_handle->check_bet_on_cap();

if(count($get_round)>0){
    foreach ($get_round as $key => $value) {
        if($value['result']){
            $numbers = str_replace(',', ' ', $value['result']);
            $play_type = get_type($value['category']);
            $type = $value['category'] ;
            $rid = $value['round_id'];
            $url = 'http://' . LotteryKjIp . ':' . LotteryKJPort .'/token/kj/'. $play_type .'/'.$rid.'/'.$numbers.'/'.$type.'/1?demo=0';
            $result = curl_kj($url, false);
            print_r($result);
            $result = json_decode($result, true);
            $token = $result['token'];
            Log::record('get play_type: ' . $play_type . ' | kj token url: ' . $url, 'error', 'settle/');

            if ($token) {
                $url = 'http://' . LotteryKjIp .':' . LotteryKJPort .'/'.'kj/' . $play_type . '/'.$rid.'/'. $numbers.'/'. $type.'/1?demo=0&token='.$token  ;
                Log::record('kj url => ' . $url, 'error', 'settle/');
                curl_kj($url, true);
            }
        }
    }
}
unset($lottery_process_handle);

function get_type($type){
    switch ($type) {
        case 9:
            $play_type = 'lhc';
            break;
        case 12:
        case 21:
        case 29:
            $play_type = 'pk10';
            break;
        case 10:
            $play_type = 'ssc';
            break;
        case 11:
        case 13:
            $play_type ='klsf';
            break;
        case 18:
        case 98:
            $play_type = 'bjkl8';
            break;
        case 28:
        case 99:
            $play_type = 'pcdd';
            break;
        case 15:
        case 17:
            $play_type = '11x5';
            break;
        case 64:
        case 70:
            $play_type = 'k3';
            break;
        default:
            $play_type ='';
            break;
    }
    return $play_type ;
}
exit();

?>
