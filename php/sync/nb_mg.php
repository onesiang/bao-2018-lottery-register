<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/mg.pid')) {
    $mg_handle = new Mg();
    $mg_handle->getList();
    unset($mg_handle);
}
else {
    // kg_echo('This script file (mg) has aleady been running...');
}
exit();