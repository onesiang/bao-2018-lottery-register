<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/bbin.pid')) {
    $bbin_handle = new Bbin();
    $bbin_handle->getList();
    unset($bbin_handle);
}
else {
    // kg_echo('This script file (bbin) has aleady been running...');
}
exit();