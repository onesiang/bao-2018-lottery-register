<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/m2d.pid')) {
    $match_handle = new Match();
    $match_handle->memoryToDb();
    unset($match_handle);
}
else {
    // kg_echo('This script file (m2d) has aleady been running...');
}
exit();