<?php

define('KGS', true);
define('NO_XHPROF', true);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/private_lottery.pid')) {

    if (isset($argv[1])) {
        $lottery = intval($argv[1]);
        $lottery_process_handle = new LotteryProcess($lottery);

        $result = $lottery_process_handle->getPrivateLotteryRound($lottery);
        $lottery_process_handle->addNewPrivateLotteryRound($result, $lottery);

        unset($lottery_process_handle);
    }
}
exit();
