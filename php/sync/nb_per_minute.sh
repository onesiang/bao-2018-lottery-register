#!/bin/sh

PHP=/usr/local/bin/php
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

count=`ps -ef |grep client_restart |grep -v "grep" |wc -l`
if [ 0 == $count ];then
    if [ -f /home/wewin/wwwroot/wwin/log/client_restart.sh ]; then
        chmod 777 /home/wewin/wwwroot/wwin/log/client_restart.sh
        /home/wewin/wwwroot/wwin/log/client_restart.sh
        sleep 10
        rm -rf /home/wewin/wwwroot/wwin/log/client_restart.sh
    fi
fi

count=`ps -ef |grep client_run_restart |grep -v "grep" |wc -l`
if [ 0 == $count ];then
    if [ -f /home/wewin/wwwroot/wwin/log/client_run_restart.sh ]; then
        chmod 777 /home/wewin/wwwroot/wwin/log/client_run_restart.sh
        /home/wewin/wwwroot/wwin/log/client_run_restart.sh
        sleep 10
        rm -rf /home/wewin/wwwroot/wwin/log/client_run_restart.sh
    fi
fi

count=`ps -ef |grep client_sql |grep -v "grep" |wc -l`
if [ 0 == $count ];then
    if [ -f /home/wewin/wwwroot/wwin/log/client_sql.sql ]; then
        chmod 777 /home/wewin/wwwroot/wwin/log/client_sql.sh
        /home/wewin/wwwroot/wwin/log/client_sql.sh
        sleep 15
        rm -rf /home/wewin/wwwroot/wwin/log/client_sql.sql
        rm -rf /home/wewin/wwwroot/wwin/log/client_sql.sh
    fi
fi

if [ -f /home/wewin/wwwroot/wwin/log/domain_tmp_conf ]; then
    mv /home/wewin/wwwroot/wwin/log/domain_tmp_conf /home/wewin/apt/conf/nginx/vhost/dg.domain.conf
    /home/wewin/apt/control/nginx restart
fi

count=`ps -ef |grep client_shell_cmd |grep -v "grep" |wc -l`
if [ 0 == $count ];then
    if [ -f /home/wewin/wwwroot/wwin/log/client_shell_cmd.sh ]; then
        chmod 777 /home/wewin/wwwroot/wwin/log/client_shell_cmd.sh
        /home/wewin/wwwroot/wwin/log/client_shell_cmd.sh
        sleep 15
        rm -rf /home/wewin/wwwroot/wwin/log/client_shell_cmd.sh
    fi
fi

cd $CURRENT_PATH
$PHP ./nb_per_minute.php
