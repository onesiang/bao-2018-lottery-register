<?php
define('KGS', true);
define('NO_XHPROF', true);
require_once '../library/common/KgRedis.class.php';
require_once '../library/common/Cache.class.php';
# require_once '../library/core/lottery/Lottery.class.php';
require '../library/include/global.php';

$act = isset($_GET['act']) ? $_GET['act'] : '';
$lottery = isset($_GET['lottery']) ? $_GET['lottery'] : 0;

$cache_handle = new Cache();
$handle = null;
$error = '';
$data = array();
switch ($lottery) {
    case 9:
        $handle = new SixMark();
        break;
    case 10:
        $handle = new Cqssc();
        break;
    case 11:
        $handle = new Gdklsf();
        break;
    case 12:
        $handle = new Bjpk();
        break;
    case 13:
        $handle = new Gdklsf($lottery);
        break;
    case 14:
        $handle = new Cqssc($lottery);
        break;
    case 15:
        $handle = new ElevenSsc($lottery);
        break;
    case 16:
        $handle = new ElevenSsc($lottery);
        break;
    case 17:
        $handle = new ElevenSsc($lottery);
        break;
    case 18:
        $handle = new HappyEight($lottery);
        break;
    case 98: //加拿大菲斯快乐8
        $handle = new HappyEight($lottery);
        break;
    case 19:
        $handle = new ThreeBall($lottery);
        break;
    case 20:
        $handle = new ThreeBall($lottery);
        break;
    case 21: //幸运飞艇
        $handle = new Bjpk($lottery);
        break;
    case 22: //幸运28
        $handle = new HappyEight($lottery);
        break;
    case 23: //福利三分彩
        $handle = new Bjpk($lottery);
        break;
    case 24: //跑马
        $handle = new Bjpk($lottery);
        break;
    case 25: //福利五分彩
        $handle = new Cqssc($lottery);
        break;
    case 28: //北京28
        $handle = new Bj28($lottery);
        break;
    case 99: //加拿大28
        $handle = new Bj28($lottery);
        break;
    case 64: //安徽快3
    case 32: //江蘇快3
    case 33: // 廣西快3
    case 39: // 河北快3
        $handle = new K3($lottery);
        break;
    case 70: //湖北快3
        $handle = new K3($lottery);
        break;
    case 38: // 新疆時時彩
        $handle = new Cqssc($lottery);
        break;
}
if ($handle) {
    if ($act == 'settle') {
        $round = isset($_GET['round']) ? $_GET['round'] : 0;
        $numbers = isset($_GET['numbers']) ? $_GET['numbers'] : 0;
        if ($round && $numbers) {
            $return = $handle->setCollectedResult($round, $numbers);

            /* if (!$return['error']) {
        $result_settle = array(
        'round' => isset($_GET['round']) ? $_GET['round'] : 0,
        'numbers' =>isset($_GET['numbers']) ? $_GET['numbers'] : 0,
        );
        $cache_handle = $this->hSet('lottery', $lottery, json_encode($result_settle));
        }*/
        }
    } elseif ($act == 'next') {
        $bet_round = isset($_GET['bet_round']) ? $_GET['bet_round'] : 0;
        $bet_start_time = isset($_GET['bet_start_time']) ? $_GET['bet_start_time'] : 0;
        $bet_stop_time = isset($_GET['bet_stop_time']) ? $_GET['bet_stop_time'] : 0;

        if ($bet_round && $bet_start_time && $bet_stop_time) {
            $return = $handle->createNewRound($bet_round, $bet_start_time, $bet_stop_time);

            /* if (!$return['error']) {
        $result_next = array(
        'bet_round' => isset($_GET['bet_round']) ? $_GET['bet_round'] : 0 ,
        'bet_start_time' => isset($_GET['bet_start_time']) ? $_GET['bet_start_time'] : 0,
        'bet_stop_time' => isset($_GET['bet_stop_time']) ? $_GET['bet_stop_time'] : 0,
        'open_term_time' => isset($_GET['bet_stop_time']) ? (int)$_GET['bet_stop_time']+60 : 0,
        );
        $cache_handle = $this->hSet('lottery', $lottery, json_encode($result_next));
        }*/
        }
    }

    if ($return) {
        $error = $return['error'];
        $data = $return['data'];
    }

    unset($handle);
}

$return = array(
    'error' => $error,
    'data' => $data,
);

echo json_encode($return);

exit();
