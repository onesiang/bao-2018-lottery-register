<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/result.pid')) {
    $key = isset($argv[1]) ? $argv[1] : '';
    if (!$key) {
        exit();
    }
    $cache = new Cache();
    $content = $cache->get($key);

    if ($content) {
        $sr_handle = new SportResult();
        $sr_handle->saveBatch($content);
        unset($sr_handle);
    }

    $cache->del($key);
    unset($cache);
}
else {
    kg_echo('This script file has aleady been running...');
}

exit();