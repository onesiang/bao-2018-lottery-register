#!/bin/sh

CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
cd $CURRENT_PATH

start() {
    pm2 start $CURRENT_PATH/nb_chat_server.js --name chat_server
}

stop() {
    pm2 stop chat_server
}

restart() {
    stop
    start
}

if [ "$1" = "start" ]; then
    start
elif [ "$1" = "stop" ]; then
    stop
elif [ "$1" = "restart" ]; then
    restart
else
    printf "Usage: ./nb_chat_server.sh {start|stop|restart}\n"
fi