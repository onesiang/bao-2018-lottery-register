<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

$act     = kg_request('act');
$account = kg_request('account');
$utype   = kg_request('utype');

$error = '';
$data  = array();
if('offline' == $act) {
    if('member' == $utype) {
        $handle = new Member();
        $handle->offline($account);
        unset($handle);
    }
    else if('guest' == $utype) {
        $handle = new Guest();
        $handle->leaveByName($account);
        unset($handle);
    }
}
else if('online' == $act) {
    $handle = new Member();
    $handle->online($account);
    unset($handle);
}

$return = array(
    'error' => $error,
    'data'  => $data
);

echo json_encode($return);
exit();
?>