<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';
require '../library/include/variable.php';

if (single_process(get_current_command(), '../log/transferqos.pid')) {
    $handle = new CasinoTransfer();
    $handle->checkDisconnectTransfer();
    unset($handle);
}
else {
    // kg_echo('This script file (transferqos) has aleady been running...');
}
exit();
?>