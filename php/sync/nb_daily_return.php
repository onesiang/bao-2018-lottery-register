<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/daily_return.pid')) {
    $config_handle = new Config();
    $return_amount_time_result = $config_handle->getConfig('ReturnAmountTime')[0];

    $current_time = date('H');
    
    $current_unix_timestamp = time();
    $return_amount_datetime = $return_amount_time_result['update_time'];
    $return_timestamp = strtotime($return_amount_datetime);

    $return_amount_tomorrow_timestamp = strtotime('+1 days',strtotime(date_format(new DateTime($return_amount_datetime),'Y-m-d')));
    
    $return_amount_time_array = explode(',',$return_amount_time_result['value']);
    $new_return_amount_value = $return_amount_time_array[0];
    $old_return_amount_value = $return_amount_time_array[1];
    $effective_return_time_value = $old_return_amount_value;
    
   
    //超過一天取新的,否則取舊的時間
    if(($current_unix_timestamp - $return_amount_tomorrow_timestamp)>= 0) {
        $effective_return_time_value = $new_return_amount_value;
        
        if($return_amount_time_array[0] != $old_return_amount_value)  {
            $parameter = array('ReturnAmountTime' => $new_return_amount_value .',' . $new_return_amount_value);    
            $config_handle->updateByName($parameter);
        }
    }

    Log::record('[nb_daily_return][effective_return_time] => ' . $effective_return_time_value .
                ' |current_time => ' . $current_time,'debug','daily_return/');

    if ($effective_return_time_value == $current_time) {         
        $lottery_handle = new Lottery();
        $lottery_handle->dailyReturn();
    }

    unset($config_handle);
    unset($lottery_handle);
}
else {
    kg_echo('This script file (daily_return) has aleady been running...');
}
exit();
