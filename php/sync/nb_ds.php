<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/ds.pid')) {
    $ds_handle = new Ds();
    $ds_handle->syncBets();
    unset($ds_handle);
}
else {
    // kg_echo('This script file (ds) has aleady been running...');
}
exit();
?>