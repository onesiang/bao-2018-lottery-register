<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/lebo.pid')) {
    $lebo_handle = new Lebo();
    $lebo_handle->getList();
    unset($lebo_handle);
}
else {
    // kg_echo('This script file (lebo) has aleady been running...');
}
exit();