<?php
define('KGS', true);

if ($_GET['token'] != 'l4kjcPFS*Ar4323rE' && $argv[1] != 'l4kjcPFS*Ar4323rE') {
    return false;
}
require_once '../library/include/global.php';
require_once '../library/include/variable.php';
require_once '../library/common/Log.class.php';

$domain = SYNC_HOST;
$port = ":" . SYNC_PORT;

date_default_timezone_set("Asia/Taipei");
function getUrlContent($url)
{
    //  echo 'curl go<br />';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);

    $data = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (!$data) {
        Log::record('error => ' . curl_error($ch) . '| url => ' . $url . ' |httpcode => ' . $httpcode, 'debug', 'api_error/');
        Log::record('url => ' . $url, 'debug', 'api_error/');
    }

    curl_close($ch);
    // print_r($data);
    return ($httpcode >= 200 && $httpcode < 300) ? $data : false;
}
/*
cron job呼叫抓取開彩api結果
處理條目後轉發給nb_lottery_sync.php
主要可分兩個分類
1. 現有盤口開獎結果open -> settle(nb_lottery_sync.php)
2. 即將開新盤資料next -> next(nb_lottery_sync.php)
 */
// 彩種代號與db id對應表
$codeMap = array(
    'bjpk10' => 12, // 5min 北京賽車
    'cqssc' => 10, // 10min 重慶時時彩
    'gdklsf' => 11, // 10 廣東快樂十分
    // 'cqklsf' => 13, // 10 重慶快樂十分
    // 'gd11x5' => 15, // 10  廣東11選5
    // 'jx11x5' => 17, // 10  江西11選5
    'ahk3' => 64, //10       安徽快3
    'hubk3' => 70, // 10   湖北快3
    'mlaft' => 21, // 5    幸運飛艇
    'bjkl8' => 18, //5       北京快樂8
    'bj28' => 28, //5 自己產生的代碼  幸運28
    // 'cakeno' => 98, //加拿大卑斯
    // 'jnd28' => 99, //加拿大28
    'xjssc' => 38, //新疆時時彩
    'jsk3' => 32, // 江蘇快3
    'gxk3' => 33, // 廣西快3
    'hebk3' => 39, //河北快3
);

//開獎期數需要做處理的
$cutLength = array('ahk3', 'hubk3', 'mlaft', 'ahk3', 'cqklsf', 'cqssc', 'gdklsf', 'xjssc', 'jsk3', 'gxk3', 'hebk3');

// 開盤時間5分鐘
$gap5 = array(
    'bjpk10', 'mlaft', 'bjkl8', 'bj28',
);

//開盤時間3分半
$gap3_5 = array(
    //'cakeno', 'jnd28',
);

//開盤時間10分鐘
$gap10 = array(
    'cqssc', 'gdklsf', 'cqklsf', 'hubk3', 'ahk3', 'xjssc', 'jsk3', 'gxk3', 'hebk3',
);

$roundMaxLength = 10;

$src = 'http://a.apiplus.net/newly.do?token=T9A7793582F66A374K&rows=3&format=json&extend=true';

/*
send get to nb_lottery_sync.php
 * param:
 * act= next
 * lottery = id, ex: $codeMap[$json->data[$i]->code]
 * bet_round = string 期數: 810240
 * bet_start_time = time :2017-11-03 11:20:30
 * bet_stop_time = time :2017-11-03 11:20:30
 * // 開獎間距5分鐘
// 封盤往前推1分鐘，開盤往前推5分鐘
// 開獎間距10分鐘
// 封盤往前推1分鐘，開盤往前推11分鐘
// 開獎間距預設10分鐘，以防有未知彩種不知道如何處理
 */
function processNext($data, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5)
{

    $now_time = time();
    // 期數太多位元的要砍掉左邊開頭的年份20
    $expect = null;
    if (in_array($data->code, $cutLength)) {
        $expect = substr($data->expect, 2);
    } else {
        $expect = $data->expect;
    }
    //  echo '- process [next] lottery:'.$data->code.'['.$codeMap[$data->code].'] round:'.$data->expect.'<br />';
    $unixOpentime = strtotime($data->opentime);

    $isNext = strtotime($isNext);

    // pk10, 幸運飛艇, 幸運28, 北京快樂8
    if (in_array($data->code, $gap5)) {
        // 開盤的時間提前3秒
        if ($now_time > ($unixOpentime + 3) || $isNext > ($unixOpentime + 3)) {
            $bet_start_time = $unixOpentime;
            $bet_stop_time = $unixOpentime + 240;
            $expect = $expect + 1;
        } else {
            $bet_start_time = $unixOpentime - 300;
            $bet_stop_time = $unixOpentime - 60;
        }

        if ($data->code == 'bjpk10') {
            $bet_start_time += 40;
            $bet_stop_time += 10;
        }

        // Log::record('[sync_lottery_result] bet_start_time => ' . date('Y-m-d H:i:s',$bet_start_time) . ' |bet_stop_time => ' . date('Y-m-d H:i:s',$bet_stop_time),'debug','test');
        // 重慶時時彩, 快樂十分, 11x5, 快3
    } elseif (in_array($data->code, $gap10)) {
        if ($now_time > ($unixOpentime + 3) || $isNext > ($unixOpentime + 3)) {
            $bet_start_time = $unixOpentime;
            $bet_stop_time = $unixOpentime + 540;
            $expect = $expect + 1;

        } else {

            $ten_minutes_first = strtotime(date('Y-m-d 10:00:00'));
            $ten_minutes_last = strtotime(date('Y-m-d 22:00:00'));

            $five_minutes_first = strtotime(date('Y-m-d 22:00:01'));
            $five_minutes_last = strtotime(date('Y-m-d 02:00:00'));
            $five_minutes_last = strtotime('+1 day', $five_minutes_last);

            if ($data->code == 'cqssc' &&
                $unixOpentime >= $five_minutes_first &&
                $unixOpentime <= $five_minutes_last) {

                $bet_start_time = $unixOpentime - 300;
                $bet_stop_time = $unixOpentime - 60;

            } else {
                $bet_start_time = $unixOpentime - 600;
                $bet_stop_time = $unixOpentime - 60;
            }
        }

        switch ($data->code) {
            case 'cqssc':
            case 'xjssc':
                $bet_start_time -= 30;
                $bet_stop_time -= 30;
                break;
            case 'gdklsf':
            case 'cqklsf':
            case 'gd11x5':
            case 'jx11x5':
                $bet_start_time -= 60;
                $bet_stop_time -= 60;
                break;
            case 'hubk3':
            case 'ahk3':
            case 'jsk3':
            case 'gxk3':
            case 'hebk3':
                $bet_start_time -= 90;
                $bet_stop_time -= 90;
                break;
        }

        // 加拿大
    } elseif (in_array($data->code, $gap3_5)) {
        $bet_start_time = $unixOpentime - 210; // origin : 180
        $bet_stop_time = $unixOpentime - 30; // origin: 0
        //六合彩
    } else {
        $bet_start_time = $unixOpentime - 660;
        $bet_stop_time = $unixOpentime - 60;
    }

    $query = 'act=next';
    $query .= '&lottery=' . $codeMap[$data->code];
    $query .= '&bet_round=' . $expect;
    $query .= '&bet_start_time=' . $bet_start_time;
    $query .= '&bet_stop_time=' . $bet_stop_time;
    $urlQuery = $domain . $port . '/sync/nb_lottery_sync.php?' . $query;
    //  echo $urlQuery;
    // echo "<br>" ;
    $is_create_new_round = true;
    switch ($data->code) {
        // 120期
        case 'cqssc':
            $tail_expect = intval(substr($expect, -3));
            $is_create_new_round = $tail_expect > 120 ? false : true;
            break;
        // 96期
        case 'xjssc':
            $tail_expect = intval(substr($expect, -3));
            $is_create_new_round = $tail_expect > 96 ? false : true;
            break;
        // 84期
        case 'gdklsf':
        case 'gd11x5':
        case 'jx11x5':
            $tail_expect = intval(substr($expect, -2));
            $is_create_new_round = $tail_expect > 84 ? false : true;
            break;
        // 97期
        case 'cqklsf':
            $tail_expect = intval(substr($expect, -2));
            $is_create_new_round = $tail_expect > 97 ? false : true;
            break;
        // 78期
        case 'hubk3':
        case 'gxk3':
            $tail_expect = intval(substr($expect, -2));
            $is_create_new_round = $tail_expect > 78 ? false : true;
            break;
        //80期
        case 'ahk3':
            $tail_expect = intval(substr($expect, -2));
            $is_create_new_round = $tail_expect > 80 ? false : true;
            break;
        // 81期
        case 'hebk3':
            $tail_expect = intval(substr($expect, -2));
            $is_create_new_round = $tail_expect > 81 ? false : true;
            break;
        // 82期
        case 'jsk3':
            $tail_expect = intval(substr($expect, -3));
            $is_create_new_round = $tail_expect > 82 ? false : true;
            break;
    }

    if ($is_create_new_round) {

        Log::record('[' . $data->code . '][function.processNext] expect => ' . $expect .
            ' |open_time(timestamp) => ' . strtotime($data->opentime) .
            ' |open_time(date_time) => ' . $data->opentime .
            ' | urlQuery => ' . $urlQuery .
            ' | bet_start_time => ' . date('Y-m-d H:i:s', $bet_start_time) .
            ' | bet_stop_time => ' . date('Y-m-d H:i:s', $bet_stop_time), 'debug', 'auto_create');

        $result = getUrlContent($urlQuery);
        $resultJson = json_decode($result);
        //if ($resultJson->error) {
        //  Log::record('sync lottery result [next] :' . print_r($resultJson->data, true),'info,' 'check_valid_bet/');
        //}
    } else {
        Log::record('[' . $data->code . '] expect => ' . $expect, 'debug', 'extra_create_round/');
    }

}
/*
send get to nb_lottery_sync.php
 * param:
 * act= settle
 * lottery = id, ex: $codeMap[$json->data[$i]->code]
 * round = string 期數: 810239
 * numbers = string 開獎結果: "8,10,2,4"
 */

function processOpen($data, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5)
{
    $expect = null;
    if (in_array($data->code, $cutLength)) {
        $expect = substr($data->expect, 2);
    } else {
        $expect = $data->expect;
    }
    $query = 'act=settle';
    $query .= '&lottery=' . $codeMap[$data->code];
    $query .= '&round=' . $expect;
    $query .= '&numbers=' . $data->opencode;
    $urlQuery = $domain . $port . '/sync/nb_lottery_sync.php?' . $query;
    //  echo $urlQuery.'<br />';

    $cache_handle = new Cache();
    $redisJson = $cache_handle->hGet(CUSTOMER_NAME . '-lottery', $codeMap[$data->code]);

    $redisJson = json_decode($redisJson, true);

    if (count($redisJson) > 0) {
        if ($redisJson[0]['round'] < $expect) {
            $result = getUrlContent($urlQuery);
            $resultJson = json_decode($result);
        }
    } else {
        $result = getUrlContent($urlQuery);
        $resultJson = json_decode($result);
    }

    // if ($resultJson->error) {
    //     //log error
    //     // Log::record('sync lottery result [settle] :' . print_r($resultJson->data, true),'info','check_valid_bet');
    // }
}

$json = file_get_contents(urldecode($src));
Log::record($json, 'info', 'crawler/');

$json = json_validate($json);

if (!$json) {
    Log::record('api json decode error', 'error', 'api_json/');
    return;
}

// if ($json->next) {
//     $isNext = $json->time;
//     //echo 'process next array<br />';
//     for ($i = 0; $i < count($json->next); $i++) {
//         if (isset($codeMap[$json->next[$i]->code])) {

//             processNext($json->next[$i], $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5);

//             if ($json->next[$i]->code == 'bjkl8') {
//                 // 多處理塞入北京28資料」
//                 $bk28 = $json->next[$i];
//                 $bk28->code = 'bj28';
//                 processNext($bk28, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5);
//             }
//             if ($json->next[$i]->code == 'cakeno') {
//                 // echo 'process jnd28 <br />';
//                 // 多處理塞入北京28資料」
//                 $jnd28 = $json->next[$i];
//                 $jnd28->code = 'jnd28';
//                 processNext($bk28, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5);
//             }
//         } else {
//             //echo '- no match [next] lottery:'.$json->data[$i]->code.' round:'.$json ->data[$i]->expect.'<br />';
//         }
//         echo '---------------------<br>';
//     }
// }
$count = 0;
$countSecond = 0;
if ($json->open) {
    $isNext = $json->time;
    //   echo 'process open array<br />';
    for ($i = 0; $i < count($json->open); $i++) {

        if (isset($codeMap[$json->open[$i]->code])) {

            if ($json->open[$i]->code == 'cakeno') {
                /*save result*/
                $cakeno = $json->open[$i];
                processOpen($cakeno, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5);
                $jnd28 = $cakeno;
                $jnd28->code = 'jnd28';
                $ra = explode(',', $cakeno->opencode);
                // $number1 = ($ra[1]+$ra[4]+$ra[7]+$ra[10]+$ra[13]+$ra[16])%10;
                // $number2 = ($ra[2]+$ra[5]+$ra[8]+$ra[11]+$ra[14]+$ra[17])%10;
                // $number3 = ($ra[3]+$ra[6]+$ra[9]+$ra[12]+$ra[15]+$ra[18])%10;
                $number1 = ($ra[0] + $ra[1] + $ra[2] + $ra[3] + $ra[4] + $ra[5]) % 10;
                $number2 = ($ra[6] + $ra[7] + $ra[8] + $ra[9] + $ra[10] + $ra[11]) % 10;
                $number3 = ($ra[12] + $ra[13] + $ra[14] + $ra[15] + $ra[16] + $ra[17]) % 10;
                $jnd28->opencode = $number1 . ',' . $number2 . ',' . $number3;
                processOpen($jnd28, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5);

                $countSecond++;
                /*create new round*/
                $is_Monday = date("D");

                $flag = false;
                if ('Mon' == $is_Monday) {
                    $flag = true;
                }

                if ($countSecond == 1) {
                    $getGame = $json->open[$i]->expect;
                    $get_open_unix_time = strtotime($json->open[$i]->opentime);
                    $cakeno = $json->open[$i];

                    //新一天的第一盤
                    $cakeno_unix_time = strtotime(date('Y-m-d 00:02:30'));

                    while (true) {
                        if ($cakeno_unix_time > $get_open_unix_time) {
                            $new_Game = $getGame + 1;
                            $next_open_time = date('Y-m-d H:i:s', ($cakeno_unix_time));

                            if (strtotime('Y-m-d 19:03:30') == $next_open_time) {
                                if ($flag) {
                                    $next_open_time = date('Y-m-d H:i:s 20:57:00');
                                } else {
                                    $next_open_time = date('Y-m-d H:i:s 19:57:30');
                                }
                            }

                            $cakeno->opentime = $next_open_time;
                            $cakeno->code = 'cakeno';
                            $cakeno->expect = $new_Game;
                            unset($cakeno->opencode);
                            processNext($cakeno, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5);

                            $jnd28 = $cakeno;
                            $jnd28->code = 'jnd28';
                            processNext($jnd28, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $isNext, $gap3_5);
                            break;
                        }
                        $cakeno_unix_time += 210;
                    }
                }
            } elseif ($json->open[$i]->code == 'bjkl8') {
                $bjki8 = $json->open[$i];
                if (strpos($bjki8->opencode, '+') !== false) {
                    // 拿掉多出來的+0X
                    // 07,09,18,21,25,28,29,34,37,38,39,41,44,56,57,61,64,65,71,74+05
                    $bjki8->opencode = substr($bjki8->opencode, 0, -3);
                }

                processOpen($bjki8, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5);

                $bk28 = $bjki8;
                $bk28->code = 'bj28';
                $ra = explode(',', $bk28->opencode);
                $number1 = ($ra[0] + $ra[1] + $ra[2] + $ra[3] + $ra[4] + $ra[5]) % 10;
                $number2 = ($ra[6] + $ra[7] + $ra[8] + $ra[9] + $ra[10] + $ra[11]) % 10;
                $number3 = ($ra[12] + $ra[13] + $ra[14] + $ra[15] + $ra[16] + $ra[17]) % 10;
                $bk28->opencode = $number1 . ',' . $number2 . ',' . $number3;
                // result
                processOpen($bk28, $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5);

            } else {
                processOpen($json->open[$i], $codeMap, $cutLength, $gap5, $gap10, $roundMaxLength, $domain, $port, $gap3_5);
            }
        } else {
            //echo '- no match [settle] lottery:'.$json->data[$i]->code.' round:'.$json ->data[$i]->expect.'<br />';
        }
        echo '---------------------<br>';
    }
}

/**
 * [auto_create_round] 封盤後立馬開新的一盤
 *
 * @param [type] $count 計數器,為了抓取最新的結果
 * @param [type] $lottery_json api的json
 * @param [type] $first_timestamp 第一盤的時間
 * @param [type] $last_timestamp  最後一盤的時間
 * @param [type] $plus_timestamp  加減多少時間/一盤多少時間
 * @return void
 */
function auto_create_round($count, $lottery_json, $first_timestamp, $last_timestamp, $plus_timestamp)
{
    global $codeMap;
    global $cutLength;
    global $gap5;
    global $gap10;
    global $roundMaxLength;
    global $domain;
    global $port;
    global $isNext;
    global $gap3_5;

    $current_timestamp = strtotime(date('Y-m-d H:i:s'));
    $code = $lottery_json->code;

    if ($count % 3 == 1) {

        $cache_handle = new Cache();
        $lottery_info = json_decode($cache_handle->hGet(CUSTOMER_NAME . '-lottery', $codeMap[$code]), true);

        if ($lottery_info) {
            $open_term_time = $lottery_info['open_term_time']; //下一期開盤時間
            $bet_round = $lottery_info['bet_round'];
            $last_round = $lottery_info['round'];
            $api_expect = $lottery_json->expect;

            if (in_array($lottery_json->code, $cutLength)) {
                $api_expect = substr($api_expect, 2);
            }

            Log::record('[' . $code . '] bet_round => ' . $bet_round .
                ' | last_round => ' . $last_round .
                ' | api last round => ' . $api_expect .
                ' | open_term_time => ' . $open_term_time .
                ' |date_time open_term_time => ' . date('Y-m-d H:i:s', $open_term_time),
                'debug', 'auto_create');
            Log::record('[' . $code . '] time_diff => ' . ($current_timestamp - $open_term_time), 'debug', 'auto_create');

            if ($current_timestamp >= $open_term_time &&
                $current_timestamp >= $first_timestamp &&
                $current_timestamp < $last_timestamp &&
                $api_expect == $last_round) {

                Log::record('[' . $code . '][before create] open_term time[timestamp] => ' . $open_term_time .
                    ' |open term time[datetime] => ' . date('Y-m-d H:i:s', $open_term_time),
                    'debug', 'auto_create');

                $open_time = $open_term_time + $plus_timestamp; // origin 540s +  90s + 60s
                $expect = $lottery_json->expect;
                $lottery_json->code = $code;
                $lottery_json->expect = $expect + 2;
                $lottery_json->opentime = date('Y-m-d H:i:s', $open_time);

                Log::record('[' . $code . '][processNext][create]  expect_new_round => ' . $lottery_json->expect .
                    ' | open_time(datetime) => ' . $lottery_json->opentime .
                    ' | open_time(timestamp) => ' . $open_time, 'debug', 'auto_create');

                processNext($lottery_json, $codeMap, $cutLength,
                    $gap5, $gap10, $roundMaxLength,
                    $domain, $port, $isNext, $gap3_5);
            } else {
                Log::record('[' . $code . '] not open . |first_time (datetime) => ' . date('Y-m-d H:i:s', $first_timestamp) .
                    ' |last_time (datetime) => ' . date('Y-m-d H:i:s', $last_timestamp) .
                    ' |current_time (datetime) => ' . date('Y-m-d H:i:s', $current_timestamp) .
                    ' |open_term_time (datetime) => ' . date('Y-m-d H:i:s', $open_term_time), 'debug', 'auto_create');
            }
        }
        echo '---------------------<br>';
    }
}
