#!/bin/sh

CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
cd $CURRENT_PATH

start() {
    pm2 start $CURRENT_PATH/nb_sport_client.js --name sport_client
    pm2 start $CURRENT_PATH/nb_lottery_client.js --name lottery_client
}

stop() {
    pm2 delete sport_client
    pm2 delete lottery_client
}

restart() {
    stop
    start
}

if [ "$1" = "start" ]; then
    start
elif [ "$1" = "stop" ]; then
    stop
elif [ "$1" = "restart" ]; then
    restart
else
    printf "Usage: ./nb_client.sh {start|stop|restart}\n"
fi