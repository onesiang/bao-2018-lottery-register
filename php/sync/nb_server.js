var app = require('express')();

var server = require('http').Server(app);
var io = require('socket.io')(server);
var moment = require('moment')

var port=3003;


var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

//var lottery = require("./lottery.js");
//var sport = require("./sport.js");

app.get('/', function (req, res) {
   res.send('Hello World');
   //res.sendfile(__dirname + '/index.html');
})


app.get('/lottery_result', function (req, res) {
	//type,round,numbers,website
	var code = req.query.code; 
	var round = req.query.round; 
	var numbers = req.query.numbers; 
	var website = req.query.website; 
	//console.log(code,round,numbers,website);
	if(code==undefined||round==undefined||numbers==undefined)
	{
		res.send("数据有错，请检查");
		console.log("Lottery lottery_result",code,round,numbers,website);
	}
	else
	{
		var type=lottery_value(code);
		if(type!="00")
		{
			if(website==null){website='';}
			lottery_emit_settle(type,round,numbers,website);
			res.writeHead(200, {'Content-Type': 'text/plain'});  
			
		}
	}
	
	res.end("ok"); 
	
   //res.sendfile(__dirname + '/index.html');
})

app.post('/', function (req, res) {
	
	
  //console.log(req.body);
  //res.json(req.body);
  res.send('Hello World');
})



app.post('/lottery_data',  function(req, res) {
	try
	{
		var content=req.body.content;
		var website=req.body.website;
		//console.log(content);
		
		if(website==null){website='';}
		
		if(content!=null&&content!="")
		{
			content=JSON.parse(content);
			lottery_push_result(website,content.open);
			//lottery_push_next(website,content.next);
		}
	}
	catch(e)
	{
		console.log(e.stack);
	}
    
	res.writeHead(200, {'Content-Type': 'text/plain'});  
	res.end("ok"); 
});






var HashMap = require('hashmap').HashMap;
var map = new HashMap();



function lottery_push_result(website,result) {
	//console.dir(result);
	//console.log("result start.......");
	try
	{		
		for(i=0;i<result.length;i++)
		{
			//{"code":"bjpk10","expect":"595635","opencode":"07,09,03,04,02,10,08,01,06,05","opentime":"2017-01-03 18:42:39"}
			var lottery=result[i];
			var type=lottery_value(lottery.code);
			//console.log(lottery.code,type,lottery.expect,lottery.opencode);
			//var round=result.expect;		
			if(type!="00")
			{
				//lottery_emit_settle(type,round,numbers,server)
				var round=lottery_round(lottery.code,lottery.expect);
				var numbers=lottery.opencode;
				lottery_emit_settle(type,round,numbers,website);
			}			
		}
	}
	catch(e)
	{
		console.log(e.stack);
	}
	//console.log("result end.......");
}


function lottery_push_next(website,result) {
	//console.log("bet start.......");
	try
	{		
		//console.dir(result);
		for(i=0;i<result.length;i++)
		{
			//{"code":"bjkl8","expect":"801068","opentime":"2017-01-03 19:05:00"}
			var bet=result[i];
			var type=lottery_value(bet.code);
			//var bet_round=result.expect;		
			if(type!="00")
			{
				//lottery_emit_next(type,bet_round,bet_stop_time,open_term_time);
				//bet_stop_time=parseInt(nowDateTime.getTime()/1000)+10*60;
				//open_term_time=bet_stop_time+60;
				//new Date("2013-02-15 21:00:00");
				//console.log(newDate.format('yyyy-MM-dd h:m:s'));
				var opentime=new Date(bet.opentime);
				bet_stop_time=parseInt(opentime.getTime()/1000)-2*60;
				open_term_time=bet_stop_time+2*60;
				var bet_round=lottery_round(bet.code,bet.expect);
				//console.log(bet.code,type,bet.expect,bet_round,stamp2date(bet_stop_time),stamp2date(open_term_time));
				lottery_emit_next(type,bet_round,bet_stop_time,open_term_time);
			}			
		}
	}
	catch(e)
	{
		console.log(e.stack);
	}
	//console.log("bet end.......");
}






function lottery_emit_settle(type,round,numbers,server)
{
	var betObj=map.get("next_"+type);
	var bet_round;
	var bet_stop_time=parseInt(new Date().getTime()/1000);
	var open_term_time=parseInt(new Date().getTime()/1000);
	if(betObj!=null)
	{
		bet_round=betObj.bet_round;		
		bet_stop_time=betObj.bet_stop_time;
		open_term_time=betObj.open_term_time;
	}
	
	var settleObj=map.get("settle_"+type);
	if(settleObj==null)
	{
		settleObj={"round":round,"numbers":numbers,"server":server};
		map.set("settle_"+type,settleObj);
		console.log("Lottery settle_"+type+" "+JSON.stringify(settleObj));		
		var d = {act:"settle",type:type,data:{round:round,numbers:numbers,open_term_time:open_term_time,bet_round:bet_round,bet_stop_time:bet_stop_time,server:server}};
		//console.dir(d);
		io.emit('notice', d);
	}
	else if(settleObj.round<round)
	{
		settleObj={"round":round,"numbers":numbers};
		map.set("settle_"+type,settleObj);
		console.log("Lottery settle_"+type+" "+JSON.stringify(settleObj));
		var d = {act:"settle",type:type,data:{round:round,numbers:numbers,open_term_time:open_term_time,bet_round:bet_round,bet_stop_time:bet_stop_time,server:server}};
		//console.dir(d);
		io.emit('notice', d);
	}	
}

function lottery_emit_next(type,bet_round,bet_stop_time,open_term_time)
{
	var settleObj=map.get("settle_"+type);
	var round;
	var numbers;
	var server="";
	if(settleObj!=null)
	{
		round=settleObj.round;
		numbers=settleObj.numbers;
		server=settleObj.server;
	}	
	var betObj=map.get("next_"+type);
	if(betObj==null)
	{
		betObj={"bet_round":bet_round,"bet_stop_time":bet_stop_time,"open_term_time":open_term_time};
		map.set("next_"+type,betObj);
		console.log("Lottery next_"+type+" "+JSON.stringify(betObj));	
		var d = {act:"next",type:type,data:{round:round,numbers:numbers,open_term_time:open_term_time,bet_round:bet_round,bet_stop_time:bet_stop_time,server:server}};
		//console.dir(d);
		io.emit('notice', d);
	}
	else if(betObj.bet_round<bet_round)
	{
		betObj={"bet_round":bet_round,"bet_stop_time":bet_stop_time,"open_term_time":open_term_time};
		map.set("next_"+type,betObj);
		console.log("Lottery next_"+type+" "+JSON.stringify(betObj));
		var d = {act:"next",type:type,data:{round:round,numbers:numbers,open_term_time:open_term_time,bet_round:bet_round,bet_stop_time:bet_stop_time,server:server}};
		//console.dir(d);
		io.emit('notice', d);
	}
}


function lottery_value(code)
{
/*
type
 9  7 香港六合彩
10  5 重慶時時彩(10-22 點10分鐘一期，22到第二天1點55分5分鐘一期，總共120期，5位，0到9，10個數字）
11  廣東快樂十分
12 10 北京赛车（北京PK10） （頻率5分鐘一期，總共179期，10位，01到10，10個數字，每個都會出現）
13  8 重慶快樂十分（20個數字，01到20，8位，頻率10分鐘，總共84期一天）
14 江西時時彩
15  5 廣東11選5
16  5 重慶11選5
17  5 江西11選5
18 21 北京快樂8 2,4,7,14,15,16,20,23,32,33,35,44,47,49,53,54,61,63,69,77+04
19  3 福彩3D
20  3 排列三
*/	
	switch (code) 
	{
		case "cqssc": //重慶時時彩
			return "10";
			break;			
		case "gdklsf": //廣東快樂十分
			return "11";
			break;			
		case "bjpk10": //北京赛车（北京PK10）
			return "12";
			break;
		case "cqklsf": //重慶快樂十分
			return "13";
			break;
		case "jxssc": //江西時時彩,现在停了
			return "14";
			break;			
		case "gd11x5": //廣東11選5
			return "15";
			break;			
		case "cq11x5": //重慶11選5,现在停了
			return "16";
			break;			
		case "jx11x5": //江西11選5
			return "17";
			break;
		case "bjkl8": //北京快樂8
			return "18";
			break;
		case "fc3d": //福彩3D
			return "19";
			break;
		case "pl3": //排列三
			return "20";
			break;			
		default:
			return "00";
			break;
	}	
}

function lottery_round(code,round)
{
	/*
type
 9  7 香港六合彩
10  5 重慶時時彩(10-22 點10分鐘一期，22到第二天1點55分5分鐘一期，總共120期，5位，0到9，10個數字）
11  廣東快樂十分
12 10 北京赛车（北京PK10） （頻率5分鐘一期，總共179期，10位，01到10，10個數字，每個都會出現）
13  8 重慶快樂十分（20個數字，01到20，8位，頻率10分鐘，總共84期一天）
14 江西時時彩
15  5 廣東11選5
16  5 重慶11選5
17  5 江西11選5
18 21 北京快樂8 2,4,7,14,15,16,20,23,32,33,35,44,47,49,53,54,61,63,69,77+04
19  3 福彩3D
20  3 排列三
{"code":"bjkl8","expect":"801127","opentime":"2017-01-04 09:05:00"},
{"code":"bjpk10","expect":"595699","opentime":"2017-01-04 09:07:30"},
{"code":"cqklsf","expect":"20170104002","opentime":"2017-01-04 00:13:40"},
{"code":"cqssc","expect":"20170104002","opentime":"2017-01-04 00:10:40"},
{"code":"gd11x5","expect":"2017010401","opentime":"2017-01-04 09:11:00"},
{"code":"gdklsf","expect":"20170104001","opentime":"2017-01-04 09:12:20"},
{"code":"jx11x5","expect":"2017010401","opentime":"2017-01-04 09:11:20"}
*/	
	switch (code) 
	{
		case "cqssc": //重慶時時彩
			return round.substring(2);
			break;			
		case "gdklsf": //廣東快樂十分
			return round.substring(2);
			break;			
		case "bjpk10": //北京赛车（北京PK10）
			return round;
			break;
		case "cqklsf": //重慶快樂十分
			return round.substring(2);
			break;
		case "jxssc": //江西時時彩,现在停了
			return round.substring(2);
			break;			
		case "gd11x5": //廣東11選5
			return round.substring(2);
			break;			
		case "cq11x5": //重慶11選5,现在停了
			return round.substring(2);
			break;			
		case "jx11x5": //江西11選5
			return round.substring(2);
			break;
		case "bjkl8": //北京快樂8
			return round;
			break;
		case "fc3d": //福彩3D
			return round.substring(2);
			break;
		case "pl3": //排列三
			return round.substring(2);
			break;			
		default:
			return round;
			break;
	}
	return round;
}


function stamp2date(stamp)
{
	/*
	  "M+": this.getMonth() + 1,
              "d+": this.getDate(),
              "h+": this.getHours(),
              "m+": this.getMinutes(),
              "s+": this.getSeconds(),
              "q+": Math.floor((this.getMonth() + 3) / 3),
              "S+": this.getMilliseconds()	
	*/
	var date = new Date(stamp * 1000 );//.转换成毫秒
	var time = date.getFullYear() + "-" + (date.getMonth() < 10 ? '0' + (date.getMonth()+1) : (date.getMonth()+1)) + "-" + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + " "+ (date.getHours() < 10 ? '0' + date.getHours() : date.getHours())+ ":"+ (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())+ ":"+ (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return time;
}





//sport begin



app.post('/sport_data',  function(req, res) {
	//console.log(req.body);
	try
	{
		var content=req.body.content;
		var type=req.body.type;
		var subtype=req.body.subtype;
		var date_time=req.body.date_time;
		var page_num=req.body.page_num;
		var total_page=req.body.total_page;
		var total_record=req.body.total_record;		
		var stat=req.body.stat;	
		var data=req.body.data;	
		if(date_time!='')
		{
			var nowtime=new Date(date_time);
			date_time=parseInt(new Date().getTime()/1000);
		}
		else
		{
			date_time=parseInt(new Date().getTime()/1000);
			
		}
		
		if(content!=null)
		{			
			 switch (type) {
				case "FS":
					sport_push_fs(type,subtype,date_time,page_num,total_page,total_record,stat,content);
					break;
					
				case "RESULT":
					sport_push_result(type,subtype,date_time,content);
					break;	

				case "RESULT_FS":
					sport_push_result_fs(type,subtype,date_time,content);
					break;						

					
				default:
					sport_push_default(type,subtype,date_time,page_num,total_page,total_record,stat,content);
					break;

			 }		
		}
	}
	catch(e)
	{
		console.log(e.stack);
	}
    
	res.writeHead(200, {'Content-Type': 'text/plain'});  
	res.end("ok"); 
});

function sport_push_default(type, subtype, date_time, page_num, total_page, total_record, stat, content) {
	sport_push_stat(stat);
	var matches=[];
	//console.log(data);		
	var r, re; // 声明变量。
	re = /g\((.*?)\);/g; 
	//]=new Array('');
	r=content.match(re);
	if(r!=null)
	{
		for(i=0;i<r.length;i++)
		{
			//console.log(r[i].replace("g(","").replace(");","")+"\n");
			matches.push(eval(r[i].replace("g(","").replace(");","")));
		}
		
		var dataArray=[];
		for(var i=0;i<matches.length;i++)
		{
			dataArray.push(JSON.stringify(matches[i]));
		}
		
		
		if(dataArray.length>0)
		{
			var data={"type":type,"subtype":subtype,"time":date_time,"page":page_num,"total_page":total_page,"total_record":total_record,"data":JSON.stringify(dataArray)};
		}	

		console.log("Sport ",type,subtype,date_time,total_page,page_num,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
		io.emit('data', data); 		
	}

	
}

function sport_push_fs(type, subtype, date_time, page_num, total_page, total_record, stat, content) {
	sport_push_stat(stat);
	var matches=[];
	var r, re; // 声明变量。
	re = /]=new Array\('(.*?)'\);/g; 
	r=content.match(re);
	if(r!=null)
	{		

		for(i=0;i<r.length;i++)
		{
			var item=r[i].replace("]=new Array(","[").replace(");","]");
			var item=eval(item);
			
			teamNum=item[5];
			
			var k=0;
			var teamStr="";
			var teamArray=[];
			for(var j=0;j<teamNum;j++)
			{
				teamStr="['"+item[j*4+6]+"','"+item[j*4+7]+"','"+item[j*4+8]+"','"+item[j*4+9]+"']";				
				teamArray.push(eval(teamStr));		
			}
			

			var result=eval("['"+item[0]+"','"+item[1]+"','"+item[2]+"','"+item[3]+"','"+item[4]+"','"+item[5]+"','']");
			
			result[6]=teamArray;
			
			matches.push(eval(result));
		}
		/*
		var dataArray=[];
		for(var i=0;i<matches.length;i++)
		{
			dataArray.push(JSON.stringify(matches[i]));
		}
		*/
		//console.dir(dataArray.length);
		if(matches.length>0)
		{
			var data={"type":type,"subtype":subtype,"time":date_time,"page":page_num,"total_page":total_page,"total_record":total_record,"data":JSON.stringify(matches)};
			//console.dir(data);
			/*
			fs.writeFile('./resut.text',JSON.stringify(data),function(err){
				if(err) throw err;
				console.log('has finished');
			});
			*/
		}		
	
		console.log("Sport ",type,subtype,date_time,total_page,page_num,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
		io.emit('data', data); 	
		
	}
		
}

function sport_push_result(type, subtype, date_time, content) {
	//console.log(type,subtype,date_time)
	//console.dir(content);

	if(content!=null)
	{	
		data={"type":"RESULT","subtype":subtype,"time":date_time,"data":content};		
		//console.dir(data);
		console.log("Sport ",type,subtype,date_time,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
		io.emit('data', data); 	
		
	}
		
}

function sport_push_result_fs(type, subtype, date_time, content) {
	//console.log(type,subtype,date_time)
	//console.dir(content);

	if(content!=null)
	{	
		data={"type":"RESULT_FS","subtype":subtype,"time":date_time,"data":content};		
		//console.dir(data);
		console.log("Sport ",type,subtype,date_time,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
		io.emit('data', data); 	
		
	}
		
}

function sport_push_stat(stat) {		
	//console.log(stat+"\n");
	if(stat!="")
	{
		var statData={"type":"stat","data":""};
		statData.data=eval(stat);
		//console.dir(statData);
		console.log("Sport ","Stat",moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
		io.emit('data', statData);	
		
	}
}


//sport end



server.listen(port, function () {
  console.log("Server started on", port);
})




io.on('connection', function (socket){
	console.log('connection');
	socket.on('lottery_settle', function (type,round,numbers,server) {
		lottery_emit_settle(type,round,numbers,server);
	});
	socket.on('lottery_next', function (type,bet_round,bet_stop_time,open_term_time) {
		lottery_emit_next(type,bet_round,bet_stop_time,open_term_time);
	});  
	socket.on('sport_data', function (sport_data) {
		console.log(sport_data);
		io.emit('data', sport_data);
	});
});

/*
io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
});
*/