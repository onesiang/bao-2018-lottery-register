<?php
define('KGS', true);
define('NO_XHPROF', true);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/settle.pid')) {

    // lottery
    $lottery_handle = new LotteryProcess();
    $lottery_handle->settleAllBets();
    unset($lottery_handle);
} else {
    // kg_echo('This script file (settle) has aleady been running...');
}
exit();
