exports.redis={
    ip: "127.0.0.1",
    port: 6379,
    db: 1
};
exports.server={
    ip: "7088kj.com",
    sport_port: 8002,
    lottery_port: 8003,
    chat_port: 8004,
    data_port: 8005
};
exports.save_data={
    host: "sync.7088kj.com",
    port: 80,
    maintaince_path: "nb_save_maintaince.php",
    notice_path: "nb_save_notice.php",
    live_path: "nb_save_live.php",
    stat_path: "nb_save_stat.php",
    running_ball_path: "nb_save_running_ball.php",
    today_path: "nb_save_today.php",
    future_path: "nb_save_future.php",
    result_path: "nb_save_result.php",
    champion_path: "nb_save_champion.php",
    champion_result_path: "nb_save_champion_result.php",
    get_sport_host_path: "/nb_get_sport_host.php",
    get_lottery_host_path: "/nb_get_lottery_host.php",
    lottery_path: "/nb_lottery_sync.php",
    auth_sync_path: "/nb_authorization_sync.php"
};
exports.php="/home/wewin/apt/php/bin/php";