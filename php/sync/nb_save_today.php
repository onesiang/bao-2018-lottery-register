<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';
require '../library/include/sport_variable.php';

//if (single_process(get_current_command(), '../log/today.pid')) {
    $key = isset($argv[1]) ? $argv[1] : '';
    if (!$key) {
        exit();
    }
    $cache = new Cache();
    $content = $cache->get($key);

    if ($content) {
        $match_handle = new Match();
        $match_handle->saveToday($content);
        unset($match_handle);
    }

    $cache->del($key);
    unset($cache);
//}
//else {
//    kg_echo('This script file has aleady been running...');
//}

exit();