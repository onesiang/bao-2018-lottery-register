
var cheerio = require("cheerio");
var http = require('http');
var server = require("./curl");
var qs = require('querystring');
var schedule = require("node-schedule");
var io = require('socket.io-client');
var moment = require('moment')
var fs = require('fs')

//var socket = io.connect('http://222.239.87.224:3000', {reconnect: true});
//var socket = io.connect('http://118.142.69.166:3000', {reconnect: true});
var socket = io.connect('http://127.0.0.1:3000', {reconnect: true});



socket.on('connect', function (socket) {
    console.log('sport Connected!');
});

socket.on('notice', function (msg) {
		//console.log('get server test', msg);
		//socket.emit('test', '123');
});

var express = require('express');
var app = express();


app.get('/', function (req, res) {
   res.send('Hello World');
})


var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

app.post('/sport_data',  function(req, res) {
	//console.log(req.body);
	try
	{
		var content=req.body.content;
		var type=req.body.type;
		var subtype=req.body.subtype;
		var date_time=req.body.date_time;
		var page_num=req.body.page_num;
		var total_page=req.body.total_page;
		var total_record=req.body.total_record;		
		var stat=req.body.stat;	
		var data=req.body.data;	
		if(date_time!='')
		{
			var nowtime=new Date(date_time);
			date_time=parseInt(new Date().getTime()/1000);
		}
		else
		{
			date_time=parseInt(new Date().getTime()/1000);
			
		}
		date_time=parseInt(new Date().getTime()/1000);
		if(content!=null)
		{			
			 switch (type) {
				case "FS":
					push_fs(type,subtype,date_time,page_num,total_page,total_record,stat,content);
					break;
					
				case "RESULT":
					push_result(type,subtype,date_time,content);
					break;	

				case "RESULT_FS":
					push_result_fs(type,subtype,date_time,content);
					break;						

					
				default:
					push_default(type,subtype,date_time,page_num,total_page,total_record,stat,content);
					break;

			 }		
		}
	}
	catch(e)
	{
		console.log(e.stack);
	}
    
	res.writeHead(200, {'Content-Type': 'text/plain'});  
	res.end("ok"); 
});

function push_default(type, subtype, date_time, page_num, total_page, total_record, stat, content) {
	push_stat(stat);
	var matches=[];
	//console.log(data);		
	var r, re; // 声明变量。
	re = /g\((.*?)\);/g; 
	//]=new Array('');
	r=content.match(re);
	if(r!=null)
	{
		for(i=0;i<r.length;i++)
		{
			//console.log(r[i].replace("g(","").replace(");","")+"\n");
			matches.push(eval(r[i].replace("g(","").replace(");","")));
		}
		
		var dataArray=[];
		for(var i=0;i<matches.length;i++)
		{
			dataArray.push(JSON.stringify(matches[i]));
		}
		
		
		if(dataArray.length>0)
		{
			var data={"type":type,"subtype":subtype,"time":date_time,"page":page_num,"total_page":total_page,"total_record":total_record,"data":JSON.stringify(dataArray)};
		}	

		console.log(type,subtype,date_time,total_page,page_num,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),"\n");
		socket.emit('sport_data', data); 		
	}

	
}

function push_fs(type, subtype, date_time, page_num, total_page, total_record, stat, content) {
	push_stat(stat);
	var matches=[];
	var r, re; // 声明变量。
	re = /]=new Array\('(.*?)'\);/g; 
	r=content.match(re);
	if(r!=null)
	{		

		for(i=0;i<r.length;i++)
		{
			var item=r[i].replace("]=new Array(","[").replace(");","]");
			var item=eval(item);
			
			teamNum=item[5];
			
			var k=0;
			var teamStr="";
			var teamArray=[];
			for(var j=0;j<teamNum;j++)
			{
				teamStr="['"+item[j*4+6]+"','"+item[j*4+7]+"','"+item[j*4+8]+"','"+item[j*4+9]+"']";				
				teamArray.push(eval(teamStr));		
			}
			

			var result=eval("['"+item[0]+"','"+item[1]+"','"+item[2]+"','"+item[3]+"','"+item[4]+"','"+item[5]+"','']");
			
			result[6]=teamArray;
			
			matches.push(eval(result));
		}
		/*
		var dataArray=[];
		for(var i=0;i<matches.length;i++)
		{
			dataArray.push(JSON.stringify(matches[i]));
		}
		*/
		//console.dir(dataArray.length);
		if(matches.length>0)
		{
			var data={"type":type,"subtype":subtype,"time":date_time,"page":page_num,"total_page":total_page,"total_record":total_record,"data":JSON.stringify(matches)};
			//console.dir(data);
			/*
			fs.writeFile('./resut.text',JSON.stringify(data),function(err){
				if(err) throw err;
				console.log('has finished');
			});
			*/
		}		
	
		console.log(type,subtype,date_time,total_page,page_num,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),"\n");
		socket.emit('sport_data', data); 	
		
	}
		
}

function push_result(type, subtype, date_time, content) {
	//console.log(type,subtype,date_time)
	//console.dir(content);

	if(content!=null)
	{	
		data={"type":"RESULT","subtype":subtype,"time":date_time,"data":content};		
		console.dir(data);
		console.log(type,subtype,date_time,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),"\n");
		socket.emit('sport_data', data); 	
		
	}
		
}

function push_result_fs(type, subtype, date_time, content) {
	//console.log(type,subtype,date_time)
	//console.dir(content);

	if(content!=null)
	{	
		data={"type":"RESULT_FS","subtype":subtype,"time":date_time,"data":content};		
		console.dir(data);
		console.log(type,subtype,date_time,moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),"\n");
		socket.emit('sport_data', data); 	
		
	}
		
}

function push_stat(stat) {		
	//console.log(stat+"\n");
	if(stat!="")
	{
		//stat="{\"FT_RB\":\"1\",\"FT_FT\":\"79\",\"FT_FU\":\"270\",\"FT_P3\":\"316\",\"FT_HOT_RB\":\"0\",\"FT_HOT_FT\":\"6\",\"FT_HOT_FU\":\"5\",\"FT_HOT_P3\":\"6\",\"FT_SP_RB\":\"0\",\"FT_SP_FT\":\"0\",\"FT_SP_FU\":\"0\",\"FT_SP_P3\":\"0\",\"BK_RB\":\"0\",\"BK_FT\":\"59\",\"BK_FU\":\"19\",\"BK_P3\":\"78\",\"BK_HOT_RB\":\"0\",\"BK_HOT_FT\":\"0\",\"BK_HOT_FU\":\"0\",\"BK_HOT_P3\":\"0\",\"BK_SP_RB\":\"0\",\"BK_SP_FT\":\"0\",\"BK_SP_FU\":\"0\",\"BK_SP_P3\":\"0\",\"BS_RB\":\"0\",\"BS_FT\":\"0\",\"BS_FU\":\"0\",\"BS_P3\":\"0\",\"BS_HOT_RB\":\"0\",\"BS_HOT_FT\":\"0\",\"BS_HOT_FU\":\"0\",\"BS_HOT_P3\":\"0\",\"BS_SP_RB\":\"0\",\"BS_SP_FT\":\"0\",\"BS_SP_FU\":\"0\",\"BS_SP_P3\":\"0\",\"TN_RB\":\"0\",\"TN_FT\":\"0\",\"TN_FU\":\"0\",\"TN_P3\":\"0\",\"TN_HOT_RB\":\"0\",\"TN_HOT_FT\":\"0\",\"TN_HOT_FU\":\"0\",\"TN_HOT_P3\":\"0\",\"TN_SP_RB\":\"0\",\"TN_SP_FT\":\"0\",\"TN_SP_FU\":\"0\",\"TN_SP_P3\":\"0\",\"VB_RB\":\"0\",\"VB_FT\":\"4\",\"VB_FU\":\"0\",\"VB_P3\":\"4\",\"VB_HOT_RB\":\"0\",\"VB_HOT_FT\":\"0\",\"VB_HOT_FU\":\"0\",\"VB_HOT_P3\":\"0\",\"VB_SP_RB\":\"0\",\"VB_SP_FT\":\"0\",\"VB_SP_FU\":\"0\",\"VB_SP_P3\":\"0\",\"BM_RB\":\"0\",\"BM_FT\":\"0\",\"BM_FU\":\"0\",\"BM_P3\":\"0\",\"BM_HOT_RB\":\"0\",\"BM_HOT_FT\":\"0\",\"BM_HOT_FU\":\"0\",\"BM_HOT_P3\":\"0\",\"BM_SP_RB\":\"0\",\"BM_SP_FT\":\"0\",\"BM_SP_FU\":\"0\",\"BM_SP_P3\":\"0\",\"TT_RB\":\"0\",\"TT_FT\":\"0\",\"TT_FU\":\"0\",\"TT_P3\":\"0\",\"TT_HOT_RB\":\"0\",\"TT_HOT_FT\":\"0\",\"TT_HOT_FU\":\"0\",\"TT_HOT_P3\":\"0\",\"TT_SP_RB\":\"0\",\"TT_SP_FT\":\"0\",\"TT_SP_FU\":\"0\",\"TT_SP_P3\":\"0\",\"OP_RB\":\"0\",\"OP_FT\":\"72\",\"OP_FU\":\"16\",\"OP_P3\":\"81\",\"OP_HOT_RB\":\"0\",\"OP_HOT_FT\":\"0\",\"OP_HOT_FU\":\"0\",\"OP_HOT_P3\":\"0\",\"OP_SP_RB\":\"0\",\"OP_SP_FT\":\"0\",\"OP_SP_FU\":\"0\",\"OP_SP_P3\":\"0\",\"SK_RB\":\"0\",\"SK_FT\":\"0\",\"SK_FU\":\"0\",\"SK_P3\":\"0\",\"SK_HOT_RB\":\"0\",\"SK_HOT_FT\":\"0\",\"SK_HOT_FU\":\"0\",\"SK_HOT_P3\":\"0\",\"SK_SP_RB\":\"0\",\"SK_SP_FT\":\"0\",\"SK_SP_FU\":\"0\",\"SK_SP_P3\":\"0\",\"FS_HOT_FT\":\"0\",\"FS_HOT_BK\":\"0\",\"FS_HOT_BS\":\"0\",\"FS_HOT_TN\":\"0\",\"FS_HOT_VB\":\"0\",\"FS_HOT_BM\":\"0\",\"FS_HOT_TT\":\"0\",\"FS_HOT_OP\":\"0\",\"FS_HOT_SK\":\"0\",\"FS_SP_FT\":\"0\",\"FS_SP_BK\":\"0\",\"FS_SP_BS\":\"0\",\"FS_SP_TN\":\"0\",\"FS_SP_VB\":\"0\",\"FS_SP_BM\":\"0\",\"FS_SP_TT\":\"0\",\"FS_SP_OP\":\"0\",\"FS_SP_SK\":\"0\",\"FS_FT\":\"79\",\"FS_BK\":\"0\",\"FS_BS\":\"0\",\"FS_TN\":\"0\",\"FS_VB\":\"0\",\"FS_BM\":\"0\",\"FS_TT\":\"0\",\"FS_OP\":\"0\",\"FS_SK\":\"0\"}";

		var statData={"type":"stat","data":""};
		statData.data=eval(stat);
		//console.dir(statData);
		console.log("Stat",moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),"\n");
		socket.emit('sport_data', statData);	
		
	}
}


var server = app.listen(3002, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("http://%s:%s", host, port)

})
 




