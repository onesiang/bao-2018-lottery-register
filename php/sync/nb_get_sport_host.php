<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

$data = array();
$error = '';

$config_handle = new Config();
$config = $config_handle->getByName(array('PROXY_SPORT_HOST', 'PROXY_SPORT_HOST2'));
if($config) {
    $proxy_data = explode(':', $config['PROXY_SPORT_HOST']);
    $data['sport_host'] = trim($proxy_data[0]);
    $data['sport_port'] = trim($proxy_data[1]);
    
    $proxy_data2 = explode(':', $config['PROXY_SPORT_HOST2']);
    $data['sport_host2'] = trim($proxy_data2[0]);
    $data['sport_port2'] = trim($proxy_data2[1]);
}
unset($config_handle);

echo json_encode($data);

exit();
?>