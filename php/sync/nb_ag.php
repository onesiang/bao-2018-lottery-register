<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/ag.pid')) {
    $ag_handle = new Ag();
    $ag_handle->getList();
    $ag_handle->getTransferList();
    //$ag_handle->sync(); 这里是同步转账额度的 没必要
    unset($ag_handle);
}
else {
    // kg_echo('This script file (ag) has aleady been running...');
}
exit();