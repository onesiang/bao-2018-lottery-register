<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/notice.pid')) {
    $key = isset($argv[1]) ? $argv[1] : '';
    if (!$key) {
        exit();
    }
    $cache = new Cache();
    $content = $cache->get($key);

    if ($content) {
        $notice_handle = new Notice();
        $notice_handle->sync($content);
        unset($notice_handle);
    }

    $cache->del($key);
    unset($cache);
}
else {
    kg_echo('This script file has aleady been running...');
}

exit();