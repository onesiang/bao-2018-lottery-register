<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

$member_uid  = kg_get('member_uid');
$utype       = kg_get('utype');
$manager_uid = kg_get('manager_uid');

$error = '';
$data  = array();
if($member_uid) {
    if('member' == $utype) {
        $handle = new Member();
        $result = $handle->getByUid($member_uid, 'account, level, uid, login_ip, login_time');
        if($result) {
            $data['utype']      = 'member';
            $data['login_time'] = $result['login_time'];
            $data['account']    = $result['account'];
            $data['level']      = $result['level'];
            $data['uid']        = $result['uid'];
            $data['city']       = ip_location($result['login_ip']);
            $data['ip']         = $result['login_ip'];
        }
        else {
            $error = 'member uid error!' . $member_uid. ':'.$utype;
        }
        unset($handle);
    }
    else {
        $handle = new Guest();
        $result = $handle->get($member_uid);
        if($result) {
            $data['utype']      = 'guest';
            $data['login_time'] = $result['join_time'];
            $data['account']    = $result['nickname'];
            $data['level']      = 0;
            $data['uid']        = $result['uid'];
            $data['city']       = ip_location($result['ip']);
            $data['ip']         = $result['ip'];
        }
        else {
            $error = 'guest uid error!';
        }
        unset($handle);
    }
}
else if($manager_uid) {
    $handle = new Manager();
    $result   = $handle->getByUid($manager_uid, 'account, nickname, uid, type');
    if($result) {
        $data['account']  = $result['account'];
        $data['nickname'] = $result['nickname'];
        $data['uid']      = $result['uid'];
        $data['type']     = $result['type'];
    }
    else {
        $error = 'manager uid error!';
    }
    unset($handle);
}

$return = array(
    'error' => $error,
    'data'  => $data
);

echo json_encode($return);
exit();
?>