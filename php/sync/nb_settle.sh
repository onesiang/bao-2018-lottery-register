#!/bin/sh

PHP=/usr/local/bin/php
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

cd $CURRENT_PATH
sleep_time=2
cron_time=$[60/$sleep_time]
for(( i=0 ; i<$cron_time ; i++ ))
do
    $PHP ./nb_settle.php
    sleep $sleep_time
done
