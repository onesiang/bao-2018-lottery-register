#!/bin/sh

CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

npm install redis socket.io@0.9.16 socket.io-client@0.9.16

mkdir /home/wewin/wwwroot/wwin/log
chmod 777 -R /home/wewin/wwwroot/wwin/log

mkdir /home/wewin/wwwroot/wwin/home/upload
chmod 777 -R /home/wewin/wwwroot/wwin/home/upload