<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';
/*
確認體育注單是否該滾注單(set comfirm = 1)
*/
if (single_process(get_current_command(), '../log/confirm.pid')) {
    $sb_handle = new SportBet();
    $sb_handle->confirm();
    unset($sb_handle);
}
else {
    // kg_echo('This script file (confirm) has aleady been running...');
}
exit();
