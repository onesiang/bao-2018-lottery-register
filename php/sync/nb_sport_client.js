//var config = require("../library/include/config");
var config = require("./nb_config");
var tool = require("../library/node/tool");
var http = require("http");
var fs = require("fs");
var spawn = require("child_process").spawn;
var io = require("socket.io-client");
var redis = require("redis");
var client = redis.createClient(config.redis.port, config.redis.ip);
var socket;
function socket_start(a, c) {
    var b = {
        reconnect: true,
        "reconnection limit": Infinity,
        "max reconnection attempts": 100000
    };
    socket = io.connect("http://"+ a + ":" + c, b);
    socket.on("connect",
    function() {
        console.log(tool.date("H:i:s") + " connect to " + a + " successful!")
    });
    socket.on("data",
    function(d) {
		//console.log("get data:"+d.data);
        if ("RE" === d.subtype) {
            var e = JSON.parse(d.data);
            save_data("running_ball_path", d)
        } else {
            switch (d.type) {
            case "maintaince":
                save_data("maintaince_path", d);
                break;
            case "notice":
                save_data("notice_path", d);
                break;
            case "live":
                save_data("live_path", d);
                break;
            case "stat":
                save_data("stat_path", d);
                break;
            case "FT":
            case "BK":
            case "TN":
            case "VB":
            case "BS":
            case "OP":
                var e = JSON.parse(d.data);
                save_data("today_path", d);
                break;
            case "FTU":
            case "BKU":
            case "TNU":
            case "VBU":
            case "BSU":
            case "OPU":
                var e = JSON.parse(d.data);
                save_data("future_path", d);
                break;
            case "RESULT":
                var e = JSON.parse(d.data);
                save_data("result_path", d);
                break;
            case "FS":
                var e = JSON.parse(d.data);
                save_data("champion_path", d);
                break;
            case "RESULT_FS":
                var e = JSON.parse(d.data);
                save_data("champion_result_path", d);
                break
            }
        }
    })
}
function save_data(b, c) {
    var d = __dirname;
    var a = config.save_data[b];
    var e = new Date().getTime() + "" + Math.random();
    client.select(config.redis.db,
    function(f) {
        if (f) {
            console.log(f)
        } else {
            client.set(e, JSON.stringify(c),
            function(h, g) {
                if (h) {
                    console.log(h)
                } else {
					//console.dir([d + "/" + a, e, "orz", "&"]);
                    spawn(config.php, [d + "/" + a, e, "orz", "&"])
                }
            })
        }
    })
}
function http_request(g, f, d, j) {
    try {
        var b, a = "";
        for (b in f) {
            a += b + "=" + f[b] + "&"
        }
        var k = {
            host: g.host,
            hostname: g.host,
            port: g.port,
            path: g.path + "?" + a,
            method: g.method ? g.method: "GET"
        };
        k.headers = g.headers ? g.headers: "";
        var h = http.get(k,
        function(e) {
            var i = "";
            e.setEncoding("utf8");
            e.on("data",
            function(l) {
                i += l
            });
            e.on("end",
            function() {
                try {
                    if (d) {
                        d(i)
                    }
                } catch(l) {
                    console.log("http end catch error:" + l);
                    if (j) {
                        j()
                    }
                }
            })
        });
        h.on("error",
        function(i) {
            console.log("http error event: " + i.message);
            if (j) {
                j()
            }
        })
    } catch(c) {
        console.log("http catch: " + c.message);
        if (j) {
            j()
        }
    }
}
function get_host_data() {
    var a = {
        host: config.save_data.host,
        port: config.save_data.port,
        path: config.save_data.get_sport_host_path
    };
    var b = {};
    http_request(a, b,
    function(c) {
        var d = JSON.parse(c);
        console.dir(d);
        if (d.sport_host && d.sport_port) {
            socket_start(d.sport_host, d.sport_port);
            if (d.sport_host2 && d.sport_port2) {
                socket_start(d.sport_host2, d.sport_port2)
            }
        } else {
            console.log("get host error")
        }
    },
    null);
    b = null,
    a = null
}
get_host_data();
