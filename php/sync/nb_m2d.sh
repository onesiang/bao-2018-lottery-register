#!/bin/sh

PHP=/usr/local/bin/php
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

cd $CURRENT_PATH
while true
do
    $PHP ./nb_m2d.php
    sleep 2
done
