<?php
/**
 * 用来执行每小时一次的程序
 * 
 */

define('KGS', TRUE);
define('NO_XHPROF', TRUE);
ini_set('memory_limit', '1500M');

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/per_hour.pid')) {
    //报表发送到DG 3天前的自然月的第一天至最后一天    
    $rand_num = rand(1,10);
    if($rand_num > 3) {
        $st_handle = new MemberStatistics();
        $st_handle->sendReport();
        unset($st_handle);
    }

    $member_handle = new MemberDataArchiving();
    // 每月15号 凌晨4点执行一次 这里是美东时间 凌晨4点实际上是16点
    //echo date('d') . ':' . date('H');
    if('15' == date('d') && '16' == date('H')) {
        $member_handle->cleanup();
    }
    
    // 每月20号 凌晨4点执行一次
    if('20' == date('d') && '16' == date('H')) {
        $member_handle->archiving();
    }
    
    unset($member_handle);
}
exit();
?>