<?php
define('KGS', TRUE);
define('NO_XHPROF', TRUE);

require '../library/include/global.php';

if (single_process(get_current_command(), '../log/ab.pid')) {
    $ab_handle = new AllBet();
    $ab_handle->getList();
    unset($ab_handle);
}
else {
    // kg_echo('This script file (ab) has aleady been running...');
}
exit();