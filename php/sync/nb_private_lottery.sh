#!/bin/sh

PHP=/usr/local/bin/php
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

cd $CURRENT_PATH

# num=`ps aux|grep nb_private_lottery.sh  | grep -v grep  | wc -l`
# if (( $num > 2 )); then
#   ps aux|grep nb_private_lottery.sh  | grep -v grep | awk '{print $2}'  | xargs kill
# fi

if [ $1 ]; then
    #極速賽車
    if [ $1 == 29 ]; then
        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    #超級賽車
    elif [ $1 == 30 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 超級快3
    elif [ $1 == 31 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 江蘇快3
    elif [ $1 == 32 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 廣西快3
    elif [ $1 == 33 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 幸运28
    elif [ $1 == 34 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 美國28
    elif [ $1 == 35 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 極速六合彩
    elif [ $1 == 36 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 600s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 福利時時彩
    elif [ $1 == 37 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 180s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 新疆時時彩
    elif [ $1 == 38 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 120s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    # 河北快3
    elif [ $1 == 39 ]; then

        echo "create new private lottery round $1"
        $PHP ./nb_create_private_lottery.php $1
        sleep 120s

        echo "settle private lottery $1"
        $PHP ./nb_private_lottery.php $1

    fi
else
    echo 'no set parameter'
fi
