//var config = require("../library/include/config");
var config = require("./nb_config");
var tool = require("../library/node/tool");
var socket_io = require("socket.io-client");
var redis = require("redis");
var http = require("http");
var client_redis = redis.createClient(config.redis.port, config.redis.ip);

var client;
var lottery_data = {};
var lottery_data_temp = {};

var set_redis = function(a) {
    client_redis.select(config.redis.db,
    function(b) {
        if (b) {
            console.error(b.message);
        } else {
            //console.log("set_redis lottery:" + a);
            client_redis.hset("lottery", a, JSON.stringify(lottery_data[a]));
			lottery_data_temp[a]=lottery_data[a];
        }
    })
};



var socket_start = function(c, b) {
    var a = {
        reconnect: true,
        "reconnection limit": Infinity,
        "max reconnection attempts": 100000
    };
    client = socket_io.connect("http://" + c + ":" + b, a);
    client.on("connect",
    function() {
        console.log(tool.date("H:i:s") + " lottery connect to " + c + " successful!");
        client.emit("client_get_lotery", {})
    });
    client.on("notice",
    function(d) {
		console.dir(d);
		if(d.type=="18"||d.type=="28")  //北京快乐8，北京28
		{
			try {
				var arr = d.data.numbers.split("+");
				d.data.numbers=arr[0];
			} catch(e) { }
		}
        //lottery_data[d.type] = d.data;
		if(lottery_data[d.type+''] === undefined) {						
			client_redis.select(config.redis.db, 
			function(b){
				if(b) {
					console.error(b.message);
				} else {
						client_redis.hget('lottery',d.type, function(err,res){  
						   if(err){  
							   console.log(err);  
						   } else{  
								console.log(res);
								if(res==null)
								{
									lottery_data[d.type]={};
									lottery_data_temp[d.type]={};
								}
								else
								{
									lottery_data[d.type]=JSON.parse(res);
									lottery_data_temp[d.type]=lottery_data[d.type];
								}
								switch (d.act) {
								case "settle":
									//if(lottery_data[d.type].round<d.data.round)
									//{
										lottery_data[d.type]["round"] = d.data.round;
										lottery_data[d.type]["numbers"] = d.data.numbers;
									//}
									send_result(d.data.round, d.data.numbers, d.type);
									break;
								case "next":
									lottery_data[d.type]["bet_round"]=d.data.bet_round;									
									lottery_data[d.type]["bet_start_time"]=d.data.bet_start_time;
									lottery_data[d.type]["bet_stop_time"]=d.data.bet_stop_time;
									lottery_data[d.type]["open_term_time"]=d.data.open_term_time;
									send_next(d.data.bet_round, d.data.bet_start_time, d.data.bet_stop_time, d.type);
									break
								}
						   } 
						});
					}
			});
		}
		else
		{
			switch (d.act) {
			case "settle":
				//if(lottery_data[d.type].round<d.data.round)
				//{
					lottery_data[d.type]["round"] = d.data.round;
					lottery_data[d.type]["numbers"] = d.data.numbers;
				//}
				send_result(d.data.round, d.data.numbers, d.type);
				break;
			case "next":			
				lottery_data[d.type]["bet_round"]=d.data.bet_round;
				lottery_data[d.type]["bet_start_time"]=d.data.bet_start_time;
				lottery_data[d.type]["bet_stop_time"]=d.data.bet_stop_time;
				lottery_data[d.type]["open_term_time"]=d.data.open_term_time;
				send_next(d.data.bet_round, d.data.bet_start_time, d.data.bet_stop_time, d.type);
				break
			}
		}
		
    });
    client.on("reconnect",
    function(d) {
        console.log("reconnect")
    });
    client.on("error",
    function(d) {
        console.log("connect error!");
        client.socket.reconnect()
    });
    client.on("connect_failed",
    function(d) {
        console.log("connect_failed")
    });
    client.on("close",
    function(d) {
        console.log("close")
    });
    client.on("disconnect",
    function(d) {
        console.log("disconnect");
        client.socket.reconnect()
    });
    client.on("repeat_close",
    function(d) {
        client.socket.options.reconnect = false;
        client.socket.disconnect()
    })
};
http.globalAgent.maxSockets = 100000;
var http_request = function(g, f, d, j) {
    try {
        var b, a = "";
        for (b in f) {
            a += b + "=" + f[b] + "&"
        }
        var k = {
            host: g.host,
            hostname: g.host,
            port: g.port,
            path: g.path + "?" + a,
            method: g.method ? g.method: "GET"
        };
        k.headers = g.headers ? g.headers: "";
        var h = http.get(k,
        function(e) {
            var i = "";
            e.setEncoding("utf8");
            e.on("data",
            function(l) {
                i += l
            });
            e.on("end",
            function() {
                try {
                    if (d) {
                        d(i)
                    }
                } catch(l) {
                    console.log("http end catch error:" + l);
                    if (j) {
                        j()
                    }
                }
            })
        });
        h.on("error",
        function(i) {
            console.log("http error event: " + i.message);
            if (j) {
                j()
            }
        })
    } catch(c) {
        console.log("http catch: " + c.message);
        if (j) {
            j()
        }
    }
};

var send_result = function(b, a, e, d) {
    var c = {
        host: config.save_data.host,
        port: config.save_data.port,
        path: config.save_data.lottery_path
    };
    var f = {
        act: "settle",
        lottery: e,
        round: b,
        numbers: a
    };
	//console.dir(f);
    if (b > 0) {
        http_request(c, f,
        function(g) {
            var h = JSON.parse(g);
            if (h.error) {
				console.dir(f);
                console.log(tool.date("H:i:s") + " lottery:" + e + " send_result error:" + h.error)
            } else {
				
				//lottery_data[d.type]["round"] = d.data.round;
				//lottery_data[d.type]["numbers"] = d.data.numbers;			
				//send_result(d.data.round, d.data.numbers, d.type);
				set_redis(e);
				
				if(lottery_data_temp[e]["round"]<b)
				{				
					
				}
            }
        },
        d)
    }
    a = null,
    d = null,
    f = null,
    c = null
};
var send_next = function(f, s, e, c, b) {
    var a = {
        host: config.save_data.host,
        port: config.save_data.port,
        path: config.save_data.lottery_path
    };
	//console.dir(a);
    var d = {
        act: "next",
        lottery: c,
        bet_round: f,
        bet_start_time: s,
        bet_stop_time: e
    };
	//console.dir(d);
    if (f > 0) {
        http_request(a, d,
        function(g) {
			//console.dir(g);
            var h = JSON.parse(g);
            if (h.error) {
				//console.dir(d);
                console.log(tool.date("H:i:s") + " lottery:" + c + " send_next error:" + h.error)
            } else {
				//console.dir(h);
                set_redis(c)
            }
        },
        b)
    }
    b = null,
    d = null,
    a = null
};
var get_host_data = function() {
    var a = {
        host: config.save_data.host,
        port: config.save_data.port,
        path: config.save_data.get_lottery_host_path
    };
    var b = {};
    http_request(a, b,
    function(c) {
        var d = JSON.parse(c);
        //console.dir(d);
        if (d.lottery_host && d.lottery_port) {
            socket_start(d.lottery_host, d.lottery_port)
        } else {
            console.log("get host error")
        }
    },
    null);
    b = null,
    a = null
};
get_host_data();