<?php

define('KGS', true);

require_once '../library/include/global.php';
require_once '../library/include/variable.php';
require_once '../library/common/Log.class.php';

date_default_timezone_set("Asia/Taipei");

$lottery_open_round = new LotteryOpenRound();
$_db = new KgMysql();

// print_r($lottery_open_round->createSscRound());

$lottery_type = array(
    12, 15, 17, 64, 70, 28, 18, 21, 11, 32, 33, 38, 39,
);

$lottery_handle = new Lottery();

$today_latest_round = array();

//跨日盤
if ($argv[1] === 'special') {
    $lottery_type = array(13, 10);
}

foreach ($lottery_type as $lottery) {
    Log::record('[現在針對彩種: ' . $lottery . '進行自動開盤]', 'debug', 'auto_create');
    // $today_round = $lottery_handle->getTodayLatestRound($lottery);

    // if ($today_round) {
    //     $today_latest_round[$lottery] = $today_round;
    // }

    switch ($lottery) {
        case 12: // pk10
            $round = $lottery_handle->getLastLotteryRound($lottery);
            if ($round) {
                $data[$lottery] = $lottery_open_round->createPk10Round($round); //昨天的期數
            } else {
                Log::record('[北京賽車抓不到上一期的期數]', 'debug', 'auto_create');
            }
            break;
        case 15: // gd11x5
        case 17: //jx11x5
            $data[$lottery] = $lottery_open_round->create11x5Round();
            break;
        case 64: //ahk3
            $data[$lottery] = $lottery_open_round->createAhk3Round();
            break;
        case 70: //hubk3
            $data[$lottery] = $lottery_open_round->createHubk3Round();
            break;
        case 28: //pcdd
        case 18: // bjkl8
            $round = $lottery_handle->getLastLotteryRound($lottery);
            if ($round) {
                $data[$lottery] = $lottery_open_round->createPcddRound($round); //昨天的期數
            } else {
                Log::record('[' . $lottery . '抓不到上一期的期數]', 'debug', 'auto_create');
            }
            break;
        case 21: //mlaft
            $data[$lottery] = $lottery_open_round->createMlaftRound();
            break;
        case 13: //cqklsf
            $data[$lottery] = $lottery_open_round->createCqklsfRound();
            break;
        case 11: // gdklsf
            $data[$lottery] = $lottery_open_round->createGdklsfRound();
            break;
        case 10: // ssc
            $data[$lottery] = $lottery_open_round->createSscRound();
            break;

        case 32: //jsk3
            $data[$lottery] = $lottery_open_round->createJsk3Round();
            break;

        case 33: //gxk3
            $data[$lottery] = $lottery_open_round->createGxk3Round();
            break;

        case 38: //xjssc
            $data[$lottery] = $lottery_open_round->createXjsscRound();
            break;

        case 39: //hbk3
            $data[$lottery] = $lottery_open_round->createHbk3Round();
            break;
    }
}

unset($lottery_handle);

$sql = '';
echo 'start';
foreach ($lottery_type as $type) {
    $_db->beginTransaction();
    foreach ($data[$type] as $key => $value) {
        // if ($value['bet_round'] > $today_latest_round[$type]) {
        //     $sql .= "REPLACE INTO `ltt_round` SET `category` = {$type}, `number` = {$key}, `start_time` = '{$value["start_time"]}', `stop_time` = '{$value["stop_time"]}', `result` = '', `settle_time` = '{$value["start_time"]}', `status` = 1;";
        // }
        $sql .= "REPLACE INTO `ltt_round` SET `category` = {$type}, `number` = {$key}, `start_time` = '{$value["start_time"]}', `stop_time` = '{$value["stop_time"]}', `result` = '', `settle_time` = '{$value["start_time"]}', `status` = 1;";
    }

    $result = $_db->multiQuery_new($sql);

    if ($result) {
        $_db->commit();
        $sql = '';
        Log::record('[' . $type . '新增期數成功]', 'debug', 'auto_create');
    } else {
        $_db->rollBack();
    }

}
print_r($data);

Log::record('自動新增盤口結束', 'debug', 'auto_create');
