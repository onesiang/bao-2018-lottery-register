#!/bin/sh

CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)
cd $CURRENT_PATH

FLAG="orz"
start() {
#    nohup $CURRENT_PATH/nb_lebo.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_ag.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_agq.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_sss.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_ds.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_ab.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_bbin.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_mg.sh $FLAG > ../log/run.log 2>&1 &
#    nohup $CURRENT_PATH/nb_transferqos.sh $FLAG > ../log/run.log 2>&1 &
    nohup $CURRENT_PATH/nb_confirm.sh $FLAG > ../log/run.log 2>&1 &
    nohup $CURRENT_PATH/nb_settle.sh $FLAG > ../log/run.log 2>&1 &
    nohup $CURRENT_PATH/nb_m2d.sh $FLAG > ../log/run.log 2>&1 &
    echo "start up"
}

stop() {
    ps aux|grep "$FLAG"|grep -v grep|awk '{print $2}'|xargs kill
    sleep 3
 #   ps aux|grep nb_lebo.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_ag.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_agq.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_ds.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_ab.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_bbin.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_mg.php|grep -v grep|awk '{print $2}'|xargs kill
 #   ps aux|grep nb_transferqos.php|grep -v grep|awk '{print $2}'|xargs kill
    ps aux|grep nb_confirm.php|grep -v grep|awk '{print $2}'|xargs kill
    ps aux|grep nb_settle.php|grep -v grep|awk '{print $2}'|xargs kill
    ps aux|grep nb_m2d.php|grep -v grep|awk '{print $2}'|xargs kill
    echo "already stop"
}

restart() {
    stop
    sleep 1
    start
}

if [ "$1" = "start" ]; then
    start
elif [ "$1" = "stop" ]; then
    stop
elif [ "$1" = "restart" ]; then
    restart
else
    printf "Usage: ./nb_run.sh {start|stop|restart}\n"
fi
