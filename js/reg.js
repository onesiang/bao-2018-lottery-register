var regexj = {
			account: /^[0-9a-z]{8,20}$/i,
			password: /^[0-9a-z]{8,20}$/i,
			auth: /^[0-9a-z]{4}$/i,
			//  real_name: /^[\u4e00-\u9fa5]+$/gi,
			mobile: /^1[34578][0-9]{9}$/,
			withdrawal_code: /^[0-9]{4}$/,
			email: /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/,
			number: /[0-9]+/,
			character: /[a-z]+/i,
			wx: /^[0-9a-zA-Z_\-]{6,20}$/,
			qq: /^[1-9][0-9]{4,11}$/,
		};

$("#auth_code").click(function() {
    $("#auth_code").attr("src", "auth_code.php?" + Math.random());
    $("#auth_code").css("display", "block" );
});


$("#auth").click(function() {
    $("#auth_code").attr("src", "auth_code.php?" + Math.random());
    $("#auth_code").css("display", "block" );
});


$('#account').blur(function(){
    var account = $('#account').val();
    var length = account.length;
    if(length > 20 || length < 8 || !regexj.number.test(account) || !regexj.character.test(account)){
        $('#register_account_tip1').css('display','block');
        $('#register_account_tip1').html('*账号为8-20位数字和英文字符组合！<br>');
    } else{
        $('#register_account_tip1').html('');
        $('#register_account_tip1').css('display','none');
    }

})

$('#password').blur(function(){
    var password = $('#password').val();
    var password_length = password.length;
    if (password_length > 20 || password_length < 8 || !regexj.number.test(password) || !regexj.character.test(password)) {
        $('#register_password_tip').css('display','block');
        $('#register_password_tip').html('*密码为8-20位数字和英文字符组合！<br>');
    } else {
        $('#register_password_tip').html('');
        $('#register_password_tip').css('display','none');
    }
})
$('#password_confirm').blur(function(){
    var password_confirm = $('#password_confirm').val();
    var password = $('#password').val();
    if(password !== password_confirm){
        $('#register_password_confirm_tip').css('display','block');
        $('#register_password_confirm_tip').html('*两次密码输入不同！<br>');
    } else{
        $('#register_password_confirm_tip').html('');
        $('#register_password_confirm_tip').css('display','none');
    }
})



