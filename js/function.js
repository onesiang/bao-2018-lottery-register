var layer_loading_index;

function msg_tips(a, c) {
    c = c ? c : 1;
    var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 80;
    layer.open({
        shade: true,
        shadeClose: true,
        skin: 'msg',
        content: ["<div style='font-size:.30rem'>" + a + "</div>"],
        time: c,
        style: 'background: rgba(0,0,0,0.6); color: #fff; position: relative; top: 0; width: 60% !important;',
        top: b
    })
}

function msg_alert(a, c) {
    c = c ? c: 6;
    var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 80;
    layer.open({
        shadeClose: true,
        btn: [g_msg["zhi dao le"]],
		skin:'layererrorer',
        content: ["<div style='padding:.4rem 0; font-size:.30rem'>"+a+"</div>"],
        time: c,
        top: b
    })
}

function msg_alertRedesign(a, c) {
    c = c ? c: 6;
    var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 80;
    layer.open({
        shadeClose: true,
        btn: [g_msg["zhi dao le"]],
        skin:'layererrorer',
        content: ["<div style='padding:.4rem 40px; font-size:.30rem'>"+a+"</div>"],
        time: c,
        top: b
    })
}

	
function msg_fun(a, c, b) {
    b = b ? b: 0;
    layer.open({
        shadeClose: false,
        fixed: false,
        btn: [g_msg["zhi dao le"]],
		skin:'layererrorer',
        content: ["<div style='padding:.4rem 0; font-size:.30rem'>"+a+"</div>"],
        time: b,
        yes: c
    });
    if (b > 0) {
        setTimeout(function() {
            if (c) {
                c()
            }
        },
        b * 1000)
    }
}

function msg_confirm(a, d, c) {
    var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 80;
    layer.open({
        shadeClose: false,
        //fixed: false,
        btn: [g_msg["que ding"], g_msg["qu xiao"]],
        content: ["<div style='padding:.4rem 0; font-size:.30rem'>"+a+"</div>"],
        top: b,
        yes: d,
        no: c
    });
}




function msg_warning(a,d) {
	var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 80;
	if (d) {
		layer.open({
			shadeClose: false,
			skin:'layererrorer',
			btn: [g_msg["que ding"]],
			content: ["<div style='padding:30px 30px; font-size:15px'>"+a+"</div>"],
			top: b,
			yes: d
		});
	}
	else
	{
		layer.open({
			shadeClose: false,
			skin:'layererrorer',
			btn: [g_msg["zhi dao le"]],
			content: ["<div style='padding:30px 30px; font-size:15px'>"+a+"</div>"],
			time: 6,
			top: b
		})
	}
}

function msg_tip(d, b) {
    var c = {
        content: d,
        style: "background-color:#09C1FF; color:#fff; border:none;"
    };
    if (b) {
        if ("undefined" != typeof(b.top)) {
            c.top = b.top
        }
        if ("undefined" != typeof(b.fixed)) {
            c.fixed = b.fixed
        }
        if ("undefined" != typeof(b.shadeClose)) {
            c.shadeClose = b.shadeClose
        }
        if ("undefined" != typeof(b.style)) {
            c.style = b.style
        }
        if ("undefined" != typeof(b.btn)) {
            c.btn = b.btn
        }
        if ("undefined" != typeof(b.time)) {
            c.time = b.time
        }
    }
    var a = layer.open(c);
    b = null,
    c = null,
    d = null;
    return a
}
function html_alert(d, b) {
    var c = {
        shadeClose: false,
        type: 1,
        content: d
    };
    if (b) {
        if ("undefined" != typeof(b.top)) {
            c.top = b.top
        }
        if ("undefined" != typeof(b.fixed)) {
            c.fixed = b.fixed
        }
        if ("undefined" != typeof(b.shadeClose)) {
            c.shadeClose = b.shadeClose
        }
        if ("undefined" != typeof(b.style)) {
            c.style = b.style
        }
        if ("undefined" != typeof(b.btn)) {
            c.btn = b.btn
        }
    }
    var a = layer.open(c);
    b = null,
    c = null,
    d = null;
    return a
}
function close_layer_index(a) {
    layer.close(a)
}
function show_layer_loading(a) {
    a = a ? a: "";
    var b = window.parent ? $(window.parent.document).scrollTop() : 0;
    b += 100;
    layer_loading_index = layer.open({
        type: 2,
        shadeClose: false,
        fixed: false,
        top: b,
        content: a
    })
}
function close_layer_loading(a) {
    a = a ? a: 300;
    if (layer_loading_index >= 0) {
        setTimeout(function() {
            layer.close(layer_loading_index)
        },
        a)
    }
}
function console_log(a) {
    if (console && console.log) {
        //console.log(a)
    }
}
function go_location(a) {
    parent.show_layer_loading();
    location.href = a
}
function browser() {
    var a = navigator.userAgent,
    e = navigator.appVersion;
    var d = {
        language: (navigator.browserLanguage || navigator.language).toLowerCase(),
        trident: a.indexOf("Trident") > -1,
        presto: a.indexOf("Presto") > -1,
        webKit: a.indexOf("AppleWebKit") > -1,
        gecko: a.indexOf("Gecko") > -1 && a.indexOf("KHTML") == -1,
        mobile: !!a.match(/AppleWebKit.*Mobile.*/),
        ios: !!a.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
        android: a.indexOf("Android") > -1 || a.indexOf("Linux") > -1,
        iPhone: a.indexOf("iPhone") > -1 || a.indexOf("Mac") > -1,
        iPad: a.indexOf("iPad") > -1,
        webApp: a.indexOf("Safari") == -1
    };
    var c = ["ios", "android", "iPhone", "iPad"];
    for (var b = 0; b < c.length; b++) {
        if (d[c[b]]) {
            d.mobile = c[b]
        }
    }
    return d
}
function http_request(a) {
    if (a.loading) {
        show_layer_loading()
    }
    if ("undefined" == typeof(a.close_loading)) {
        a.close_loading = true
    }
    $.ajax({
        type: a.type,
        url: a.url,
        async: a.async,
        dataType: a.data_type ? a.data_type: "json",
        data: a.data,
        timeout: 500,
        error: function(b, c, d) {
            console_log("http_request error: " + c + " " + d);
            if (a.fail) {
                a.fail()
            }
        },
        beforeSend: function() {},
        success: function(b) {
            try {
                if (b) {
                    if (a.success) {
                        try {
                            a.success(b)
                        } catch(c) {
                            console_log("http_request res error!" + c)
                        }
                    }
                } else {
                    console_log("http_request res data empty!")
                }
            } catch(c) {
                console_log("http_request back error:" + c)
            }
        },
        complete: function(b, c) {
            if (a.complete) {
                a.complete()
            }
            if (a.close_loading && close_layer_loading) {
                close_layer_loading()
            }
        }
    })
}
function second_format(d) {
    var a = Math.floor(d / 3600);
    a = a < 10 ? "0" + a: a;
    d = d % 3600;
    var c = Math.floor(d / 60);
    c = c < 10 ? "0" + c: c;
    var b = d % 60;
    b = b < 10 ? "0" + b: b;
    return a + ":" + c + ":" + b
}
function second_format_minute(c) {
    var b = Math.floor(c / 60);
    b = b < 10 ? "0" + b: b;
    var a = c % 60;
    a = a < 10 ? "0" + a: a;
    return b + ":" + a
}
function mian_menu_event() {
    $("#main_menu").click(function() {
        var a = $("#main_menu_content");
        if (a.is(":visible")) {
            a.fadeOut()
        } else {
            a.fadeIn()
        }
    })
}
function logout_event() {
    $("#logout").click(function() {
        logout()
    })
}
function logout2() {
    parent.document.getElementById("home").src = "/logout.php?uid=" + g_uid
}
function amount_changed(b, a) {
    if ("undefined" != typeof(g_amount)) {
        g_amount = a
    }
    $("#amount").html(a + "");
}
function auto_height(c, b, a) {
    var d = $(document.body).outerHeight(true);
    if ("number" == typeof b && d < b) {
        d = b
    }
    d = d > 400 ? d + 10 : 400;
    if (a) {
        if (d > $(parent.document).find("#" + c).height()) {
            $(parent.document).find("#" + c).height(d)
        }
    } else {
        if (d != $(parent.document).find("#" + c).height()) {
            $(parent.document).find("#" + c).height(d)
        }
    }
}

function query_string(c) {
    var b = [];
    for (var a in c) {
        b.push(a + "=" + c[a])
    }
    b = "?" + b.join("&");
    return b;
}

//设置cookies
function setCookie(name,value)
{
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}

//读取cookies
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
 
    if(arr=document.cookie.match(reg))
 
        return unescape(arr[2]);
    else
        return null;
}

//删除cookies
function delCookie(name)
{
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null)
        document.cookie= name + "="+cval+";expires="+exp.toGMTString();
} 


function clearNoNum(obj){  
  obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符   
  obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的   
  obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");  
  obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数   
  if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额  
   obj.value= parseFloat(obj.value);  
  }  
}
//var main;
function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null) return unescape(r[2]); return "";
} 
