######
# See: https://hub.docker.com/_/php/
######
FROM php:7.2.7-fpm
######
# You can install php extensions using docker-php-ext-install
######
RUN apt-get update \
    && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libjpeg-dev \
        libpq-dev \
        libcurl4-gnutls-dev \
        libicu-dev \
        libxml2-dev \
        libpq-dev \
        zlib1g-dev \
        git \
        g++ \
        unzip \
        curl \
        cron \
        libmemcached-dev \
        procps \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && pecl install -o -f redis && docker-php-ext-enable redis \
    && curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz" \
    && mkdir -p /usr/src/php/ext/memcached \
    && tar -C /usr/src/php/ext/memcached -zxvf /tmp/memcached.tar.gz --strip 1 \
    && docker-php-ext-configure memcached \
    && docker-php-ext-install -j$(nproc) gd iconv pdo_mysql intl curl opcache xml mbstring exif memcached pcntl posix\
    && rm /tmp/memcached.tar.gz \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer.phar \

